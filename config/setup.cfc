/*
 
   CF Name:    setup.cfm
 
   Description:
      This module contains the setup information for the 
      system.
 
      Best viewed with Tabs = 3
 
   Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson
 
   Global Session Variables Used:
 
   Mayfair at Parkland - Technical Synergy, Inc.  
   Copyright (c) 1999-2018. All Rights Reserved.   

*/

Component 
{

	function init()
	{
		// Twilio Settings
		str.twilioSettings = {
			FromNumber 				: "+19542899332",
			AccountSid 				: "AC634dbe4304e0b0fc4bfadf859dc30f29",
			AuthToken 				: "3f13eeb0c01b41447e8f3b17f34cc4a4",
			ApiVersion 				: "2010-04-01",
			ApiEndpoint 			: "api.twilio.com",
			ApiResponseFormat 	: "json"
		};

      // return str
      return str;
	}

}
