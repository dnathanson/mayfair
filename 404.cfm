<!doctype html>
<!---                                                      
    CF Name:    404.cfm

    Description:
      This module will display information about a page missing.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	titlePageName = "Page Missing";

</cfscript>


<!--- Include the header --->
<cfoutput>
<html>
<head>
<title>#titlePageName# | Mayfair at Parkland</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="Chinese Shar-Pei Charitable Trust">
<meta name="keyword" content="Homeowners Associations, Parkland, Florida, Mayfair at Parkland, Community Activities">
<meta name="robots" content="index, follow"/>
<meta name="copyright" content="Copyright © 2012-2020 Technical Synergy, Inc. All Rights Reserved."/>
<meta name="author" content="Drew Nathanson - Technical Synergy, Inc.">
<link rel="stylesheet" href="/includes/css/main.min.css" />

<link rel="Shortcut Icon" href="/includes/ico/favicon.ico" />

</head>
</cfoutput>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 75%">
			<header>
				<span class="icon major fa-warning accent5"></span>
				<br />
				<h2>Page Not Found</h2>
				<p>
					You have tried to access a page that doesn't exist on our site. Please check the 
					URL and make the necessary changes.
			   </p>
			</header>
		
		</section>

		<!-- Footer -->
		<footer id="footer" style="position:fixed;bottom:0;width:100%;">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!--- Scripts --->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
	<script src="/includes/js/jquery.dropotron.min.js"></script>
	<script src="/includes/js/jquery.scrollgress.min.js"></script>
	<script src="/includes/js/skel.min.js"></script>
	<script src="/includes/js/util.min.js"></script>
	<script src="/includes/js/main.min.js"></script>

</body>
</cfoutput>
</html>