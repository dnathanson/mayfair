<!doctype html>
<!---                                                      
    CF Name:    mayfairScheduleSP.cfm

    Description:
		This module will review the scudule events and determine if an email 
		reminder is needed to be sent.       

      Best viewed with Tabs = 3

    Program Information:
      Called from:  none
      Calls:        none
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved.   

--->

<cfscript>
	
	// Get the current date plus 1
	// plus1Date = dateAdd("d", 1, now());
	plus1Date = now();

	// Retrieve any event records that need to be checked
	eventRecs  = queryExecute("select * from view_eventCommittees where date(startDateTime) = '#dateFormat(plus1Date, "yyyy-mm-dd")#' and sendNotice = 1 order by startDateTime");

	// Check the results
	if ( eventRecs.recordCount )
	{

		// Create the object
		smtpObj  	= createObject("component", "#application.cfcPath#smtp");
		smtpStruct  = smtpObj.getSMTPinfo();

		for ( rec in eventRecs )
		{
		
			// Check to see if this event is a committee event
			if ( !rec.committeeId )
			{

				// Retrieve all residents who want notifications
				resRecs = queryExecute("select emailAddress from residents where notifications = 1");
				emailAddr = valueList(resRecs.emailAddress, ",");

				// Get the location and check the length
				location = ( rec.locationId != 0 ) ? rec.locationName : rec.otherLocation;
				if ( len(location) > 20 )
				{
					location = "<br />" & location;
				}

				// Create the email message
				saveContent variable="bodyMsg"
				{
					writeOutput("
Dear Neighbor,<br />
	This is a reminder that the following Mayfair at Parkland event is occurring today. <br /><br />
<b>Below are the specifics:</b>
<br />
Title: <span style='color:blue;'>#rec.title#</span><br />
Start Date/Time: <span style='color:blue;'>#dateFormat(rec.startDateTime, "mm/dd/yyyy")# @ #timeFormat(rec.startDateTime, "hh:mm tt")#</span><br />
End Date/Time: <span style='color:blue;'>#dateFormat(rec.endDateTime, "mm/dd/yyyy")# @ #timeFormat(rec.endDateTime, "hh:mm tt")#</span><br />
Location: <span style='color:blue;'>#location#</span><br />
Short Description:<br />
<span style='color:blue;'>#rec.shortDescription#</span><br />
<br />
Your Board of Directors<br />
");
				}

				// Send out all the emails
				for ( em in emailAddr )
				{

					// Create a mail object
					mail = new mail(type="html", charset="utf-8", body="#bodyMsg#");
					mail.setFrom("noreply@mayfairatparkland.com (Mayfair at Parkland Website)");
					mail.setTo("#em#");
					mail.setSubject("Upcoming Event Reminder for Mayfair at Parkland");
					mail.setServer(smtpStruct.host);
					mail.setPort(smtpStruct.port);
					mail.setUsername(smtpStruct.user);
					mail.setPassword(smtpStruct.pass);
					mail.setUseTLS(true); 

					// Send the email
					mail.send();

				}

			} else {

				// Retrieve all residents who want notifications
				cmRecs = queryExecute("select * from view_committeemembers where committeeId = :id",
					                    {
					                    	 id : { cfsqltype:"CF_SQL_INTEGER", value=rec.committeeId }
					                    });

				emailAddr = valueList(cmRecs.emailAddress, ",");
				
				// Get the location and check the length
				location = ( rec.locationId != 0 ) ? rec.locationName : rec.otherLocation;
				if ( len(location) > 20 )
				{
					location = "<br />" & location;
				}

				// Create the email message
				saveContent variable="bodyMsg"
				{
					writeOutput("
Dear Neighbor,<br />
	This is a reminder that the following event for <b>#cmRecs.committeeName# committee</b> for Mayfair at Parkland is occurring today. 
<br /><br />
<b>Below are the specifics:</b>
<br />
Title: <span style='color:blue;'>#rec.title#</span><br />
Start Date/Time: <span style='color:blue;'>#dateFormat(rec.startDateTime, "mm/dd/yyyy")# @ #timeFormat(rec.startDateTime, "hh:mm tt")#</span><br />
End Date/Time: <span style='color:blue;'>#dateFormat(rec.endDateTime, "mm/dd/yyyy")# @ #timeFormat(rec.endDateTime, "hh:mm tt")#</span><br />
Location: <span style='color:blue;'>#location#</span><br />
Short Description:<br />
<span style='color:blue;'>#rec.shortDescription#</span><br />
<br />
Your Committee Chairperson<br />
");
				}

				// Send out all the emails
				for ( em in emailAddr )
				{

					// Create a mail object
					mail = new mail(type="html", charset="utf-8", body="#bodyMsg#");
					mail.setFrom("noreply@mayfairatparkland.com");
					mail.setTo("#em#");
					mail.setSubject("Upcoming Committee Event Reminder for Mayfair at Parkland");
					mail.setServer(smtpStruct.host);
					mail.setPort(smtpStruct.port);
					mail.setUsername(smtpStruct.user);
					mail.setPassword(smtpStruct.pass);
					mail.setUseTLS(true); 

					// Send the email
					mail.send();

				}


			}

		}

	}

	// Check to see if there is a reset array in the application scope
	if ( structKeyExists(application, "resetPassword") )
	{

		// Start a loop to process the records
		arrCnt = 1;
		for ( rec in application.resetPassword )
		{

			if ( dateCompare(now(), dateAdd("h", 1, rec.dateTime) ) > 0 )
			{
				// Remove the record
				arrayDeleteAt(application.resetPassword,arrCnt); 

				// Check for empty array
				if ( arrayLen(application.resetPassword) == 0 )
				{
					// Remove the array
					structDelete(application, "resetPassword");

					// Break the loop
					break;
				}

			}

			// Add 1 to count
			arrCnt++;

		}

	}

</cfscript>
