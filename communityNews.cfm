<!doctype html>
<!---                                                      
    CF Name:    communityNews.cfm

    Description:
      This module will home owners to read about the community news going on.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	titlePageName = "Community News";

	// Build the date range
	if ( Month(Now()) == 1 )
	{
		startDate = Year(DateAdd("yyyy",-1,Now())) & "-01-01";
	} else {
		startDate = Year(Now()) & "-01-01";
	}
	endDate   = Year(Now()) & "-12-31";

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

	// Retreive the community news
	artRecs = queryExecute("Select * from communityNews where articleDate between '#startDate#' and '#endDate#' order by articleDate desc");

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-newspaper-o accent2"></span>
				<br />
				<h2>Community News</h2>
				<p>
					Want to find out about Mayfair at Parkland, this is the place. This page contains 
					all the news and up-coming events.
					<br />
			   </p>
			</header>
			<cfloop query="artRecs">
				<section class="box">
					<h3>#artRecs.articleHead# - #dateFormat(articleDate, "mmmm dd, yyyy")#</h3>
					<p>
						#artRecs.article#
					</p>
				</section>
			</cfloop>
			
		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<script>
		var isMobile = "#session.isMobile#";
		var isPhone  = "#session.isPhone#";
	</script>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>