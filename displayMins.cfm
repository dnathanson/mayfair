<!doctype html>
<!--- 
 
	CF Name:    displayMins.cfm

	Description:
		This page will determine if the user is logged in and
		will extract the information from the into the URL and
		then retrieve the correct file from the database. 

	Best viewed with Tabs = 3

	Program Information:
		Called from:  
		Calls:         
		Parameters:   
		Author:       Drew Nathanson

	Global Session Variables Used:

	Mayfair at Parkland - Technical Synergy, Inc.
	Copyright (c) 1999-2025. All Rights Reserved.   
 
--->

<cfscript>
	
	// Create the objects
	miscObj = createObject("component", "#application.cfcPath#misc");

	// Check to see if the person is logged in
	if ( !structKeyExists(session, "userInfo") )
	{
		abort;
	}

	// Get the url information
	miscObj.getURLVariables();

	// Check the results
	if ( structKeyExists(url, "min") )
	{

		// Extract the record
		rec 		= queryExecute("Select minutes from minutes where minId = :id",
			                     {
			                     	id : { cfsqltype:"CF_SQL_INTEGER", value:url.min }
			                     });
		fileName = "mminutes.pdf";

	} else {

		// Abort the process
		abort;
	}
	
</cfscript>

<cfheader name="content-disposition" value="inline;filename=#fileName#" />
<cfcontent reset="yes" type="application/pdf" variable="#rec.minutes#">

