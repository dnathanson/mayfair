<!doctype html>
<!---                                                      
    CF Name:    userMaint.cfm

    Description:
      This module will allow the management and BOD to manage user access.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Check permissions
	if ( !structKeyExists(session, "userInfo") || 
		  ( !structKeyExists(session.userInfo, "permission") || 
		    ( session.userInfo.permission.blog  == 1 || 
		    	session.userInfo.permission.committee == 1 ) 
		   ) 
		)
	{
		// Send to home page
		location ( url="/index.cfm", addToken=false );
	}

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	selValue      = "";
	titlePageName = "User Maintenance";

	// Create the object
	resObj 	= createObject("component", "#application.cfcPath#residents");

	// Check to see if action exists
	if ( structKeyExists(form, "action") )
	{

		// Chek the value of action
		switch ( form.action )
		{

			case 'selection':

				// Get the street information
				resRecs 	= resObj.getResidents(where="street = '#form.street#'");
				selValue = form.street;

				// Break the switch
				break;

		}

	}

	// Retreive the street information
	addrRecs = queryExecute("Select * from addresses Order By address");

	// Get the statRecs
	statRecs   = queryExecute("Select count(*) rc from residents where residentId > 0");

	statStOnly = queryExecute("Select Distinct street from residents where residentId > 0");

	statCntRecs = queryExecute("Select sum(Notifications) notes, sum(newsletter) news, sum(sms) sms 
		                         from residents
		                         where residentId > 0");

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false,
		includeTiny     : true,
		jsInclude       : ["userMaint.#request.jsType#.js?#cacheValue#"]
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-lock accent5"></span>
				<br />
				<h2>User Account Access Maintenance</h2>
				<p>
					The purpose of this page is to allow changes to user accounts when people move and/or need assistance with their user account.
					<br />
			   </p>
			   <cfif len(compMessage)>
				   <div class="msgFormat">#compMessage#</div>
				</cfif>
			</header>
			<section class="box ">
				<form method="Post" action="userMaint.cfm" name="form" id="form">
				<input type="hidden" name="action" value="selection">
				<div class="row uniform 50%">
					<h3>Search by Address</h3>
					<div class="12u">
						<select name="street" id="street">
						<option value=""> -- Please Select --</option>
						<cfloop query="addrRecs">
							<option value="#addrRecs.address#" <cfif selValue eq addrRecs.address> selected</cfif>>#addrRecs.address#</option>	
						</cfloop>
						</select>
					</div>
				</div>
				</form>
				<a href="##" id="showStats" class="fit">Show Stats</a>
				<div class="row uniform 50%" id="statArea" style="display:none;">
					<h4 style="text-align:center;">Resident Statistics</h4>
					<br />
					<div class="table-wrapper">
						<table>
						<thead>
							<tr>
								<th>Resident Accounts</th>
								<th>No. of Actual Houses</th>
								<th>Notifications</th>
								<th>News Letter</th>
								<th>SMS Message</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="text-align:center;">#statRecs.rc#</td>
								<td style="text-align:center;">#statStOnly.recordCount#</td>
								<td style="text-align:center;">#statCntRecs.notes#</td>
								<td style="text-align:center;">#statCntRecs.news#</td>
								<td style="text-align:center;">#statCntRecs.sms#</td>
							</tr>
						</tbody>
						</table>
					</div>
				</div>
			</section>

			<cfif structKeyExists(form, "action")>
				<section class="box"> 

					<div class="table-wrapper">
						<table id="resultTab">
						<thead>
							<tr>
								<th>Name</th>
								<th>Email</th>
								<th> &nbsp; </th>
							</tr>
						</thead>
						<tbody>
							<cfif !resRecs.recordCount>
								<tr>
									<td>No User Accounts exists</td>
									<td> &nbsp;</td>
									<td> &nbsp;</td>
								</tr>
							<cfelse>
								<cfloop query="resRecs">
									<tr>
										<td>#resRecs.name#</td>
										<td>#resRecs.emailAddress#</td>
										<td>
											<div class="8u 12u(mobilep)">
												<a href="##" id="#resRecs.residentId#" data-id="#resRecs.residentId#" class="fit">Password Reset</a>
												&nbsp;&nbsp;
												<a href="##" id="D-#resRecs.residentId#" data-id="D-#resRecs.residentId#" class="fit">Delete</a>
											</div>
										</td>
									</tr>
								</cfloop>	
							</cfif>
						</tbody>
						</table>
						
						<div class="8u 12u(mobilep)">
							<ul class="actions">
								<li><input type="button" id="rmAllForAddress" value="Remove All Emails for Address" ></li>
								<li><input type="button" id="returnBtn"       value="Reset"></li>
							</ul>
						</div>

					</div>

				</section>

			</cfif>
		
		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<script>
		var isMobile = "#session.isMobile#";
		var isPhone  = "#session.isPhone#";
	</script>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>