<!doctype html> 
<!---                                                      
    CF Name:    index.cfm

    Description:
      Home page for the Mayfair at Parkland Web Site.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved.   
--->

<cfscript>

	// Define fields
	titlePageName 		= "Home";
	marginTopOffset 	= "0px;";

	// Check to see if there are any questions that are active 
	questRecs = queryExecute("Select * from questionnaire where active = 1");

	// Set the structure with information to the request scope
	str    = {
		jsInclude     : [ ],
		lightbox      : true
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

	// We need to make some adjustments for the brower
	if ( !session.isMobile )
	{
		// Set the margin top offset
		marginTopOffset = ( findNoCase("chrome", cgi.http_user_agent) ) ? "0px;" : "100px;";
	}

</cfscript>

<!--- Include the header --->
<cfinclude template="common/header.cfm" />

<style type="text/css">
.picRound
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
}
.surveyA {
		-moz-transition: color 0.2s ease-in-out, border-bottom-color 0.2s ease-in-out;
		-webkit-transition: color 0.2s ease-in-out, border-bottom-color 0.2s ease-in-out;
		-ms-transition: color 0.2s ease-in-out, border-bottom-color 0.2s ease-in-out;
		transition: color 0.2s ease-in-out, border-bottom-color 0.2s ease-in-out;
		border-bottom: dotted 1px;
		color: #fff;
		text-decoration: none;
	}

</style>
<cfoutput>
<body class="landing">
<div id="page-wrapper">

	<!-- Header -->
	<header id="header" class="alt">
		<cfoutput>
		<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a> <cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
		</cfoutput>
		<cfinclude template="common/menu.cfm">
	</header>

	<!-- Banner -->
	<section id="banner">
		<div style="height:300px;"> &nbsp; </div>
	</section>

	<!-- Main -->
	<section id="main" class="container">

		<section class="box special" style="margin-top:#marginTopOffset#">
			<header class="major">
				<h2>Welcome to Mayfair at Parkland</h2>
				<p>We are a lovely small community on the east side of Parkland, <br />with 245 homes and lots of different activities for everyone. </p>
				<br />
			</header>
			<span class="image featured"><img src="includes/img/MainEnt.gif" width="500" height="400" alt="" class="picRound" /></span>
		</section>

		<!--- <section class="box special features">
			<!--- <div class="features-row">
				<h3>Board of Director's Meeting Agenda:</h3>
				<p style="font-size:18px;">
				The Board of Director's meeting on Dec 15th at 7:00pm has the following agenda. <br />Use the 
				link below to view the agenda:<br /><br />
				<a href="pdf/BOD-Agenda121516.pdf" target="new">View BOD Agenda</a>
				</p>
			</div> --->
			<div class="features-row">
				<br />
				<header>
					<span class="icon major fa-tree accent3"></span>
					<h3>Christmas Lights - Front Entrance:</h3>
					<p>Click on the image for a better view of the picture.</p>
				</header>
				<br />
			</div>
			<div class="features-row" style="border:0px;">
				<section style="border:0px;">
					<a href="xmasPictures.cfm?picture=1&" target="new"><img src="includes/img/xmas/xmasFront1Small.jpg" border="0" style="border-radius:8px;" title="Click on the image to see a larger version" /></a>
				</section>
				<section style="border:0px;">
					<a href="xmasPictures.cfm?picture=2&" target="new"><img src="includes/img/xmas/xmasFront2Small.jpg" border="0" style="border-radius:8px;" title="Click on the image to see a larger version" /></a>
				</section>
			</div>
			<div class="features-row">
				<section style="border:0px;">
					<a href="xmasPictures.cfm?picture=3&" target="new"><img src="includes/img/xmas/xmasFront3Small.jpg" border="0" style="border-radius:8px;" title="Click on the image to see a larger version" /></a>
				</section>
				<section style="border:0px;">
					<a href="xmasPictures.cfm?picture=4&" target="new"><img src="includes/img/xmas/xmasFront4Small.jpg" border="0" style="border-radius:8px;" title="Click on the image to see a larger version" /></a>
				</section>
			</div>
			<!--- <p style="font-size:18px;">
					<b style="color:red;font-size:26px;">HURRICANE WARNING - ARE YOU PREPARED ???</b>
					<br />
					The BOD has provided the following documents in order to be prepared for the Hurricane Matthrew<br />
					<a href="pdf/HurricaneEmergencySupplies_checklist.pdf" target="new">Hurricane Emergency Supplies/Checklist</a> <br />
					<a href="pdf/HurricanePrepGuide-Print.pdf" target="new">Hurricane Preparedness Guide</a>
				</p> --->
				<!--- <cfif questRecs.recordCount gt 0 and structKeyExists(session, "userInfo")>
					<p style="font-size:18px;">
						<b>TAKE OUR LOX ROAD SURVEY</b>, WE NEED EVERYONE TO DO THIS. ITS IMPORTANT !!!!<br />
						<br />
						<a href="/survey.cfm" class="surveyA"><img src="/includes/img/survey.png" style="border-radius:8px;border:1px solid red;"></a>
					</p>
				</cfif> --->
				<!--- <p style="font-size:18px;">
					<b>WE HAVE PARTY PICTURES</b>, check out the fun that was had by everyone who attended:<br />
					<br /><br />
					<img src="/includes/img/party2016/partyPic2016.png" style="border-radius:8px;">
					<br /><br />
					<a href="theParties.cfm/poolParty2016/Yes">View Party Pictures</a>
				</p> --->
			<!--- <div class="features-row">
				<br />
				<h3>WE HAVE HALLOWEEN PICTURES</h3>check out the fun that was had by everyone:
				<p style="font-size:18px;">
					<img src="/includes/img/halloween2016/halloweenPoster.png" style="border-radius:8px;" class="img-responsive">
					<br /><br />
				</p>
				<a href="theParties.cfm/halloween2016/Yes">View Party Pictures</a>
			</div> --->
		</section> --->
		<section class="box special features">
			<div class="features-row">
				<section>
					<span class="icon major fa-newspaper-o accent2"></span>
					<h3>Community News</h3>
					<p style="text-align:left;">
						Amy Cook as been appointed to the board of directors after the resignation of Noel Behrmann in November, 
						and Alan Shapiro in February. Board member Amy Cook will assume the responsibilities of Secretary vacated 
						by Alan. The following people make up the board of directors:
						<br />
						<div>
						Steve Capandonis <br />
						Amy Cook<br />
						Charles Wenzel<br />
						</div>
					</p>
					<p>
						Visit our News Page for other stories: ... <a href="communityNews.cfm"> More</a>
					</p>
					<p>
						Here is the Mission &amp; Vision Statement from the Board of Directors:
						<a href="/pdf/MayfairMissionVisionStatement.pdf" target="new">Mission Statement</a>
					</p>
				</section>
				<section>
					<span class="icon major fa-users accent3"></span>
					<h3>Committees</h3>
					<p>
						Want to help make Mayfair better, volunteer your time and be on a committee? The board of directors needs your help. 
						<br />
					</p>
					<p>
						Visit our Committee's Page to learn how: ... <a href="committees.cfm"> More</a>
					</p>
				</section>
			</div>
			<div class="features-row">
				<section>
					<span class="icon major fa-pie-chart accent4"></span>
					<h3>The Wedge</h3>
					<p>
						This is a very important topic to all homeowners, please see our blog on this subject. We ask that everyone
						try to participate in the discussion. We are going to need EVERYONE's help on this.
					</p>
					<p>
						Visit our Wedge Blog Page to learn more: ... <a href="theWedge.cfm"> More</a>
					</p>
				</section>
				<section>
					<span class="icon major fa-lightbulb-o accent5"></span>
					<h3>Ideas</h3>
					<p>
						Help the board of director's out by providing input into how you see the community. Use our suggestion box
						feature to help the Board of Director's stay on top of things.
					</p>
					<p>
						Visit our Ideas Page to learn more: ... <a href="suggestions.cfm"> More</a>
					</p>
				</section>
			</div>
		</section>

	</section>

	<!-- CTA -->
	<section id="cta">

		<h2>Sign up for Community News Emails</h2>
		<p>If you wish to receive correspondence from the Board of Director's, sign up here.</p>

		<form method="post" action="signup.cfm">
			<div class="row uniform 50%">
				<div class="8u 12u(mobilep)">
					<input type="email" name="email" id="email" placeholder="Email Address" />
				</div>
				<div class="4u 12u(mobilep)">
					<input type="submit" value="Sign Up" class="fit" />
				</div>
			</div>
		</form>

	</section>

	<!-- Footer -->
	<footer id="footer">
		<cfinclude template="/common/icons.cfm">
		<ul class="copyright">
			<li><cfoutput>#Year(Now())#</cfoutput> &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li><li>Design: <a href="http://html5up.net" target="new">HTML5 UP</a></li>
		</ul>
	</footer>

</div>

<!-- Scripts -->
<cfinclude template="common/scripts.cfm" />
</body>
</cfoutput>

</html>