<!doctype html>
<!---                                                      
    CF Name:    onStreetParkingApp.cfm

    Description:
      This module will allow the user to apply for an on-street parking
      waiver.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->
<!--- <cfif structKeyExists(session, "parking")>
	<cfdump var="#session#" />
	<cfset structDelete(session, "parking") />
	<cfabort />
</cfif> --->
<cfscript>

	if ( !structKeyExists(session, "userInfo") )
	{
		// Send to home page
		location ( url="/index.cfm", addToken=false );
	}

	// Set the fields
	addRefresh    = 0;
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	msgColor      = "Green";
	msgStyle      = "color:Green";
	titlePageName = "On Street Parkland";

	if ( structKeyExists(form, "email") && !structKeyExists(form, "eventBtn") )
	{
		emailAddress = form.email;
	}

	// Check for the eventBtn
	if ( structKeyExists(form, "eventBtn") )
	{

		// Create the objects
		parkReqObj = createObject("component", "#application.cfcPath#parkingRequest");
		parkCarObj = createObject("component", "#application.cfcPath#parkingReqCars");
		smtpObj    = createObject("component", "#application.cfcPath#smtp");

		emailBod  = queryExecute("Select emailAddress from residents where residentId = -1 or residentId in ( Select residentId from bod )");
		emailList = valueList(emailBod.emailAddress, ",");

		// Build the structure
		strList = {
			residentId : session.userInfo.residentId,
			reason     : session.parking.reason,
			phone      : session.parking.phone,
			startDate  : session.parking.startDate,
			endDate    : session.parking.endDate,
			createDate : now(),
			approved   : 0,
			completed  : 0
		};

		// Insert the record
		reqId  = parkReqObj.insertParkingRequest(strList);

		// Process all the cars that are in the request
		for ( x = 1; x <= arrayLen(session.parking.cars); x++ )
		{
			// Create the structure
			strList   = {
				parkingReqId : reqId,
				make         : session.parking.cars[x].make,
				model        : session.parking.cars[x].model,
				tagNo        : session.parking.cars[x].tagNo,
				state        : session.parking.cars[x].state,
				year         : session.parking.cars[x].year,
				color        : session.parking.cars[x].color
			};

			parkCarObj.insertParkingReqCars(strList);

		}

		// Create the email body
		saveContent variable="body"
		{
			writeOutput("
Request #reqId#<br /><br />
There has been a request for <b>On-Street</b> parking from a homeowner.<br /><br />
The following are the details:<br />
#session.userInfo.name#<br />
<a href='mailto:#session.userInfo.emailAddress#'>#session.userInfo.emailAddress#</a><br />
#session.userInfo.street#<br />
#session.parking.phone#<br />
Start Date: #session.parking.startDate# - End Date: #session.parking.endDate#<br />
Reason for the request:<br />
#session.parking.reason#<br /><br />
The following are the auto's seeking permission:<br />
<table border='0' cellpadding='4' cellspacing='4' width='70%'>
<tr>
	<th><b>Make/Model/Year</b></th>
	<th><b>State/Tag No</b></th>
	<th><b>Color</b></th>
</tr>");
			for ( x = 1; x <= arrayLen(session.parking.cars); x++ )
			{
				writeOutput("<tr><td>#session.parking.cars[x].make#/#session.parking.cars[x].model#/#session.parking.cars[x].year#</td>
					              <td>#session.parking.cars[x].state#/#session.parking.cars[x].tagNo#</td>
					              <td>#session.parking.cars[x].color#</td></tr>");
				// writeOutput("#session.parking.cars[x].make#/#session.parking.cars[x].model#/#session.parking.cars[x].year# &nbsp; #session.parking.cars[x].state#/#session.parking.cars[x].tagNo# &nbsp; Color: #session.parking.cars[x].color#<br />");
			}
			writeOutput("</table><br /><br />
				          Please use this link to approve/disapprove of the request:<br />
				          <a href='#application.httpPath#onStreetRequests.cfm'>#application.httpPath#onStreetRequests.cfm</a>");
		}


		// Create a mail object
		mail = new mail(type="html", charset="utf-8", body="#body#");
		mail.setFrom("noreply@mayfairatparkland.com (Mayfair at Parkland Website)");
		mail.setTo("#emailList#");
		mail.setSubject("On-Street Parking Request - #session.userInfo.name#/#session.userInfo.street#");
		//mail.setServer(smtpStruct.host);
		//mail.setPort(smtpStruct.port);
		//mail.setUsername(smtpStruct.user);
		//mail.setPassword(smtpStruct.pass);
		//mail.setUseTLS(true);

		// Send the email
		mail.send();
		
		// Set the message
		compMessage = "Your request has been submitted. You will be notified by email about the status.";
		
	}

	// Set the structure with information to the request scope
	str    = {
		jsInclude     : [ "onStreetParkingWaiver.full.js"], //"signup.#request.jsType#.js?#cacheValue#"
		jsChose       : false,
		bootstrap     : true,
		datepicker    : true
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 4px;
   -moz-border-radius: 4px;
        border-radius: 4px;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!--- Header --->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 75%">
			<header>
				<span class="icon major fa-car accent4"></span>
				<br />
				<h2>On-Street Parking Waiver</h2>
				<p>
					If you would like to apply for an on-street parking waiver, please complete the following 
					application which will be sent to the Management Company/Board of Directors for approval. 
					You will receive an email as to the status of this request.
					<br /><br />
					<span style="color:red;font-weight:bold;">
						Please be aware that this request is being submitted to the Board of Directors and the property 
						manager for approval. Please allow 24 to 36 hours for a response to your request. 
					</span>
			   </p>
			   <div id="message" class="msgFormat" style="#msgStyle#;font-weight:bold;text-align:center;">#compMessage#</div>
			</header>

			<div class="box">
				<form method="post" action="onStreetParkingWaiver.cfm" name="form" class="simple-validation">
				<input type="hidden" name="residentId" id="residentId" value="#session.userInfo.residentId#" />
				<div class="row uniform 50%">
					<div class="6u 12u(mobilep)">
						<input type="text" name="name" id="name" class="required" title="Your Name is Required" placeholder="Full Name" value="#session.userInfo.name#" />
					</div>
					<div class="6u 12u(mobilep)">
						<input type="email" name="emailAddress" id="emailAddress" value="#session.userInfo.emailAddress#" title="Email Address is Required" class="required email" placeholder="Email Address" />
					</div>
				</div>
				<div class="row uniform 50%">
					<div class="12u">
						<input type="text" name="street" id="street" value="#session.userInfo.street#" class="required" title="Street Address is Required" placeholder="House Number"/>
					</div>
				</div>
				<div class="row uniform 50%">
					<div class="12u">
						<input type="text" name="phone" id="phone" value="" placeholder="Phone Number to Contact You" />
					</div>
				</div>
				<div class="row uniform 50%">
					<div class="6u 12u(mobilep)">
						<input type="text" name="startDate" id="startDate" class="date required" title="Start Date is Required" placeholder="Start Date" value="" />
					</div>
					<div class="6u 12u(mobilep)">
						<input type="text" name="endDate" id="endDate" class="date required" title="End Date is Required" placeholder="End Date" value="" />
					</div>
				</div>
				<div class="row uniform 50%">
					<div class="12u">
						<textarea name="reason" id="reason" rows="3" maxlength="2000" placeholder="Please explain the reason for permission"></textarea>
					</div>
				</div>
				<div class="row uniform">
					<div class="12u">
						<ul class="actions align-center">
							<li><input type="button" name="addVehicleBtn" id="addVehicleBtn" value="Add Vehicle" /></li>
							<li><input type="submit" name="eventBtn" id="eventBtn" value="Submit Request" /></li>`
						</ul>
					</div>
				</div>
				</form>
				
			</div>

			<div id="vehicleList" style="display:none;">
				<input type="hidden" id="vehicleTabId" value="" />
				<div class="box">
					<h3>Vehicle List</h3>
					<div id="vehicleListDisplay" class="12u"> </div>
				</div>
			</div>

			<div id="vehicleInfo" style="display:none;">
				<div class="box">
					<p id="carErrorArea" style="display:none;color:red;font-weight:bold;">
						&nbsp;
					</p>
					<p><b>All fields below are required</b></p>
					<div class="row uniform 50%">
						<div class="12u">
							<input type="text" name="make" id="make" value="" placeholder="Vehicle Make" />
						</div>
					</div>
					<div class="row uniform 50%">
						<div class="12u">
							<input type="text" name="model" id="model" value="" placeholder="Vehicle Model" />
						</div>
					</div>
					<div class="row uniform 50%">
						<div class="12u">
							<input type="text" name="tagNo" id="tagNo" value="" placeholder="Vehicle Tag" />
						</div>
					</div>
					
					<div class="row uniform 50%">
						<div class="12u">
							<input type="text" name="state" id="state" value="" placeholder="State Registered" />
						</div>
					</div>
					
					<div class="row uniform 50%">
						<div class="12u">
							<input type="text" name="year" id="year" value="" placeholder="Vehicle Model Year"/>
						</div>
					</div>
					
					<div class="row uniform 50%">
						<div class="12u">
							<input type="text" name="color" id="color" value="" placeholder="Vehicle Color"/>
						</div>
					</div>
					<div class="row uniform">
						<div class="12u">
							<ul class="actions align-center">
								<li><input type="button" name="saveVehicleBtn" id="saveVehicleBtn" value="Save Vehicle" /></li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</section>

		<!--- DEFINE DELETE MODAL AREA --->
		<div id="deleteDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="deleteDialog" style="margin-top:50px;">
			<div class="modal-dialog">
				<div class="form-horizontal">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" area-hidden="true">X</button>
							<h3>Vehicle Removal</h3>
						</div>
						<div class="modal-body" style="height:100px;">
							<p> Are you sure you wish to delete this vehicle?</p>
						</div>
						<div class="modal-footer" style="height:100px;">
							<br />
							<button id="yesBtn" class="btn btn-primary">Yes</button>
							<button class="btn btn-danger" data-dismiss="modal">No</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--- END DELETE MODAL AREA --->

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<cfinclude template="common/scripts.cfm">
	
</body>
</cfoutput>
</html>