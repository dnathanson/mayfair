/*                                                      
    
    CF Name:     wedgeblog
                                                  
    Description:
	   

      The following is the table layout for the CFC being created:

        Column Name                   Data Type          Data Len   Nullable
        ----------------------------  -----------------  ---------  --------
        wedgeBlogId                   INT                 10          NO      
        residentId                    INT                 10          NO      
        blogDate                      DATE                7           YES     
        subject                       VARCHAR             100         YES     
        blog                          LONGTEXT            4294967295  YES   
        active                        INT                 10          YES  


      The following is a list of functions that are included in
      this module:

        getWedgeblog
        insertWedgeblog
        removeWedgeblog
        updateWedgeblog


    Program Information:                                
      Called from:  None
      Calls:        None
      Parameters:   None
      Author:       Drew Nathanson
      
    Global Session Variables Used:
    
    Mayfair at Parkland HOA - Techncial Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved.
                                
*/

Component
{
	// Set global all fields
	allFields = 'WedgeBlogId, residentId, blogDate, subject, blog, active';

	/**
    * @output  true
    * @returntype Query
    * @hint This function will return the entire record set from table WedgeBLOG.
    */
	public function getWedgeblog ( required numeric wedgeBlogId = 0, string selectLine = '', string where = '', string orderBy = '' )
	{

      /*

         Function Name: getWedgeblog

         Description:
           This function will return the entire record set from table WedgeBLOG.  
           
         Function Information:
            Input Arguments:  WedgeBlogId   
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Check value to see if selectLine is set
		if ( !len(selectLine) ) { selectLine = allFields; }

		// Set field
		sqlLine = "Select #selectLine# from wedgeBlog where 0=0 ";

		// Check for WedgeBlogId
		sqlLine &= ( wedgeBlogId > 0 ) ? " AND wedgeBlogId = #wedgeBlogId#" : "";

		// Check for WHERE
		sqlLine &= ( len(where) > 0 ) ? " AND #PreserveSingleQuotes(where)#" : "";

		// Check for orderBy
		sqlLine &= ( len(orderBy) > 0 ) ? " ORDER BY #PreserveSingleQuotes(orderBy)#" : "";

		// Execute the query
		recs = queryExecute("#sqlLine#",{ datasource:application.databaseSelect } );

		// Return
		return recs;

	}

	/**
    * @output  true
    * @returntype Query
    * @hint This function will return the entire record set from table WedgeBLOG.
    */
	public function getWedgeblogView ( required numeric wedgeBlogId = 0, string selectLine = '', string where = '', string orderBy = '' )
	{

      /*

         Function Name: getWedgeblog

         Description:
           This function will return the entire record set from table WedgeBLOG.  
           
         Function Information:
            Input Arguments:  WedgeBlogId   
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Check value to see if selectLine is set
		if ( !len(selectLine) ) { selectLine = '*'; }

		// Set field
		sqlLine = "Select #selectLine# from view_wedgeBlog where 0=0 ";

		// Check for WedgeBlogId
		sqlLine &= ( wedgeBlogId > 0 ) ? " AND wedgeBlogId = #wedgeBlogId#" : "";

		// Check for WHERE
		sqlLine &= ( len(where) > 0 ) ? " AND #PreserveSingleQuotes(where)#" : "";

		// Check for orderBy
		sqlLine &= ( len(orderBy) > 0 ) ? " ORDER BY #PreserveSingleQuotes(orderBy)#" : "";

		// Execute the query
		recs = queryExecute("#sqlLine#",{ datasource:application.databaseSelect } );

		// Return
		return recs;

	}

	/**
    * @output  true
    * @returntype Numeric
    * @hint This function will insert the data into WedgeBLOG.
    */
	public function insertWedgeblog ( required Struct StrList )
	{

      /*

         Function Name: insertWedgeblog

         Description:
           This function will insert the data into WedgeBLOG.  
           
         Function Information:
            Input Arguments:  StrList  
            Output Arguments: newId (New Id Number)  

       */

		// Define local variables
		
		var newId        = 0;


		// Set up the try/catch environment
		try
		{

			// Define the stored procedure object
			procObj  = new storedproc();

			// Set the default information
			procObj.setDatasource("#Application.Database#");
			procObj.setProcedure("INSERT_WedgeBLOG_PROC");

			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.residentId#");
			procObj.addParam(cfsqltype="CF_SQL_DATE",    type="In", value="#StrList.blogDate#", NULL="Yes");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.subject#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.blog#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="Out", Variable="newId");

			// Execute the procedure
			resultObj = procObj.execute();
			newId = resultObj.getprocOutVariables().newId;

		} catch ( database excpt ) { 

			// Throw the error to the page
			rethrow;

		}

		// Return
		return newId;

	}


	/**
    * @output  true
    * @returntype Void
    * @hint This function will update the data into WedgeBLOG.
    */
	public function updateWedgeblog ( required Struct StrList )
	{

      /*

         Function Name: updateWedgeblog

         Description:
           This function will update the data into WedgeBLOG.  
           
         Function Information:
            Input Arguments:  StrList  
            Output Arguments: None  

       */

						

		// Set up the try/catch environment
		try
		{

			// Define the stored procedure object
			procObj  = new storedproc();

			// Set the default information
			procObj.setDatasource("#Application.Database#");
			procObj.setProcedure("UPDATE_WedgeBLOG_PROC");

			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.wedgeBlogId#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.residentId#");
			procObj.addParam(cfsqltype="CF_SQL_DATE",    type="In", value="#StrList.blogDate#", NULL="Yes");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.subject#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.blog#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.active#");

			// Execute the procedure
			procObj.execute();

		} catch ( database excpt ) { 

			// Throw the error to the page
			rethrow;

		}


		// Return
		return ;

	}

	/**
    * @output  true
    * @returntype Boolean
    * @hint This function will remove a record from the table WedgeBLOG.
    */
	public function removeWedgeblog ( String wedgeblogid = 0, String where = "" )
	{

      /*

         Function Name: removeWedgeblog

         Description:
           This function will remove a record from the table WedgeBLOG.  
           
         Function Information:
            Input Arguments:    
            Output Arguments: True or False  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Set the sqlLine field
		sqlLine  = "Delete from wedgeBlog where 0=0 ";
		sqlLine &= ( structKeyExists(arguments, 'wedgeblogid') && wedgeblogid > 0 ) ? "and wedgeBlogId = #wedgeblogid# " : "";
		// Check for where
		sqlLine &= ( len(where) > 0 ) ? "AND #PreserveSingleQuotes(where)#" : "";

		// Execute the query
		queryExecute("#sqlLine#", { datasource:Application.DatabaseDelete} );


		// Return
		return ;

	}

	/**************************************************************************
	 * The next section is used as an object for the Getter object functions  *
	 **************************************************************************/

	/**
    * @output  true
    * @returntype query
    * @hint 
    */
	public function init ( numeric indexNo = -1, string Where = "" )
	{

      /*

         Function Name: init

         Description:
             
           
         Function Information:
            Input Arguments:  indexNo (primary key)  
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";

		// Retrieve the record
		sqlLine = "Select wedgeBlogId, residentId, blogDate, subject, blog, active from wedgeBlog where 0 = 0";

 		// Check the key
		sqlLine &= ( indexNo > -1 ) ? " AND wedgeBlogId = #indexNo#" : "";

 		// Check for where
		sqlLine &= ( len(where) > 0 ) ? "AND #PreserveSingleQuotes(where)# " : "";

		//Execute the query
		recordSet = queryExecute("#sqlLine#", { datasource:Application.DatabaseSelect } );


		// Return
		return recordset;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint Return the number of record in the query
    */
	public function GetRecordCount ( Void )
	{

      /*

         Function Name: GetRecordCount

         Description:
           Return the number of record in the query  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: recordcount  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.recordcount;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field WedgeBlogId
    */
	public function getWedgeblogid ( Void )
	{

      /*

         Function Name: getWedgeblogid

         Description:
           This function will return the field WedgeBlogId  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.wedgeBlogId;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field residentId
    */
	public function getResidentid ( Void )
	{

      /*

         Function Name: getResidentid

         Description:
           This function will return the field residentId  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.residentId;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field blogDate
    */
	public function getBlogdate ( Void )
	{

      /*

         Function Name: getBlogdate

         Description:
           This function will return the field blogDate  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return DateFormat(recordSet.blogDate, "mm/dd/yyyy");

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field subject
    */
	public function getSubject ( Void )
	{

      /*

         Function Name: getSubject

         Description:
           This function will return the field subject  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.subject;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field blog
    */
	public function getBlog ( Void )
	{

      /*

         Function Name: getBlog

         Description:
           This function will return the field blog  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.blog;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field active
    */
	public function getActive ( Void )
	{

      /*

         Function Name: getActive

         Description:
           This function will return the field active  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.active;

	}


}
