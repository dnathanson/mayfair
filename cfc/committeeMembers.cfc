/*                                                      
    
    CF Name:     Committeemembers
                                                  
    Description:
	   

      The following is the table layout for the CFC being created:

        Column Name                   Data Type          Data Len   Nullable
        ----------------------------  -----------------  ---------  --------
        commitMemberId                INT                 10          NO      
        residentId                    INT                 10          NO      
        signUpDate                    DATETIME            7           NO
        chairman                      INT                 10          YES     


      The following is a list of functions that are included in
      this module:

        getCommitteemembers
        insertCommitteemembers
        removeCommitteemembers
        updateCommitteemembers


    Program Information:                                
      Called from:  None
      Calls:        None
      Parameters:   None
      Author:       Drew Nathanson
      
    Global Session Variables Used:
    
    Mayfair at Parkland HOA - Techncial Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved.
                                
*/

Component
{
	// Set global all fields
	allFields = 'commitMemberId, residentId, signUpDate, chairman';

	/**
    * @output  true
    * @returntype Query
    * @hint This function will return the entire record set from table COMMITTEEMEMBERS.
    */
	public function getCommitteemembers ( required numeric commitMemberId = 0, string selectLine = '', string where = '', string orderBy = '' )
	{

      /*

         Function Name: getCommitteemembers

         Description:
           This function will return the entire record set from table COMMITTEEMEMBERS.  
           
         Function Information:
            Input Arguments:  commitMemberId   
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Check value to see if selectLine is set
		if ( !len(selectLine) ) { selectLine = allFields; }

		// Set field
		sqlLine = "Select #selectLine# from committeeMembers where 0=0 ";

		// Check for commitMemberId
		sqlLine &= ( commitMemberId > 0 ) ? " AND commitMemberId = #commitMemberId#" : "";

		// Check for WHERE
		sqlLine &= ( len(where) > 0 ) ? " AND #PreserveSingleQuotes(where)#" : "";

		// Check for orderBy
		sqlLine &= ( len(orderBy) > 0 ) ? " ORDER BY #PreserveSingleQuotes(orderBy)#" : "";

		// Execute the query
		recs = queryExecute("#sqlLine#",{ datasource:application.databaseSelect } );

		// Return
		return recs;

	}

	/**
    * @output  true
    * @returntype Query
    * @hint This function will return the entire record set from table COMMITTEEMEMBERS.
    */
	public function getCommitteeMembersView ( required numeric commitMemberId = 0, string selectLine = '', string where = '', string orderBy = '' )
	{

      /*

         Function Name: getCommitteemembers

         Description:
           This function will return the entire record set from table COMMITTEEMEMBERS.  
           
         Function Information:
            Input Arguments:  commitMemberId   
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Check value to see if selectLine is set
		if ( !len(selectLine) ) { selectLine = '*'; }

		// Set field
		sqlLine = "Select #selectLine# from view_committeeMembers where 0=0 ";

		// Check for commitMemberId
		sqlLine &= ( commitMemberId > 0 ) ? " AND commitMemberId = #commitMemberId#" : "";

		// Check for WHERE
		sqlLine &= ( len(where) > 0 ) ? " AND #PreserveSingleQuotes(where)#" : "";

		// Check for orderBy
		sqlLine &= ( len(orderBy) > 0 ) ? " ORDER BY #PreserveSingleQuotes(orderBy)#" : "";

		// Execute the query
		recs = queryExecute("#sqlLine#",{ datasource:application.databaseSelect } );

		// Return
		return recs;

	}

	/**
    * @output  true
    * @returntype Numeric
    * @hint This function will insert the data into COMMITTEEMEMBERS.
    */
	public function insertCommitteemembers ( required Struct StrList )
	{

      /*

         Function Name: insertCommitteemembers

         Description:
           This function will insert the data into COMMITTEEMEMBERS.  
           
         Function Information:
            Input Arguments:  StrList  
            Output Arguments: newId (New Id Number)  

       */

		// Define local variables
		
		var newId        = 0;


		// Set up the try/catch environment
		try
		{

			// Define the stored procedure object
			procObj  = new storedproc();

			// Set the default information
			procObj.setDatasource("#Application.Database#");
			procObj.setProcedure("insert_committeemembers_proc");

			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.residentId#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.chairman#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="Out", Variable="newId");

			// Execute the procedure
			resultObj = procObj.execute();
			newId = resultObj.getprocOutVariables().newId;

		} catch ( database excpt ) { 

			// Throw the error to the page
			rethrow;

		}

		// Return
		return newId;

	}


	/**
    * @output  true
    * @returntype Void
    * @hint This function will update the data into COMMITTEEMEMBERS.
    */
	public function updateCommitteemembers ( required Struct StrList )
	{

      /*

         Function Name: updateCommitteemembers

         Description:
           This function will update the data into COMMITTEEMEMBERS.  
           
         Function Information:
            Input Arguments:  StrList  
            Output Arguments: None  

       */

						

		// Set up the try/catch environment
		try
		{

			// Define the stored procedure object
			procObj  = new storedproc();

			// Set the default information
			procObj.setDatasource("#Application.Database#");
			procObj.setProcedure("UPDATE_COMMITTEEMEMBERS_PROC");

			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.commitMemberId#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.residentId#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.chairman#");

			// Execute the procedure
			procObj.execute();

		} catch ( database excpt ) { 

			// Throw the error to the page
			rethrow;

		}


		// Return
		return ;

	}

	/**
    * @output  true
    * @returntype Boolean
    * @hint This function will remove a record from the table COMMITTEEMEMBERS.
    */
	public function removeCommitteemembers ( String commitmemberid = 0, String where = "" )
	{

      /*

         Function Name: removeCommitteemembers

         Description:
           This function will remove a record from the table COMMITTEEMEMBERS.  
           
         Function Information:
            Input Arguments:    
            Output Arguments: True or False  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Set the sqlLine field
		sqlLine  = "Delete from committeeMembers where 0=0 ";
		sqlLine &= ( structKeyExists(arguments, 'commitmemberid') && commitmemberid > 0 ) ? "and commitMemberId = #commitmemberid# " : "";
		// Check for where
		sqlLine &= ( len(where) > 0 ) ? "AND #PreserveSingleQuotes(where)#" : "";

		// Execute the query
		queryExecute("#sqlLine#", { datasource:Application.DatabaseDelete} );


		// Return
		return ;

	}

	/**************************************************************************
	 * The next section is used as an object for the Getter object functions  *
	 **************************************************************************/

	/**
    * @output  true
    * @returntype query
    * @hint 
    */
	public function init ( numeric indexNo = -1, string Where = "" )
	{

      /*

         Function Name: init

         Description:
             
           
         Function Information:
            Input Arguments:  indexNo (primary key)  
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";

		// Retrieve the record
		sqlLine = "Select commitMemberId, residentId, signUpDate, chairman from committeeMembers where 0 = 0";

 		// Check the key
		sqlLine &= ( indexNo > -1 ) ? " AND commitMemberId = #indexNo#" : "";

 		// Check for where
		sqlLine &= ( len(where) > 0 ) ? "AND #PreserveSingleQuotes(where)# " : "";

		//Execute the query
		recordSet = queryExecute("#sqlLine#", { datasource:Application.DatabaseSelect } );


		// Return
		return recordset;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint Return the number of record in the query
    */
	public function GetRecordCount ( Void )
	{

      /*

         Function Name: GetRecordCount

         Description:
           Return the number of record in the query  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: recordcount  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.recordcount;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field commitMemberId
    */
	public function getCommitmemberid ( Void )
	{

      /*

         Function Name: getCommitmemberid

         Description:
           This function will return the field commitMemberId  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.commitMemberId;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field residentId
    */
	public function getResidentid ( Void )
	{

      /*

         Function Name: getResidentid

         Description:
           This function will return the field residentId  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.residentId;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field chairman
    */
	public function getChairman ( Void )
	{

      /*

         Function Name: getChairman

         Description:
           This function will return the field chairman  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.chairman;

	}


}
