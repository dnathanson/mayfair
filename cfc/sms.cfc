/********************************************************
 *
 *   CF Name:    sms.cfc
 *
 *   Description:
 *     This module contains all the necessary process
 *     information to send SMS messages from the 
 *     twilio system. 
 *
 *     Best viewed with Tabs = 3
 *
 *    Program Information:
 *      Called from:  
 *      Calls:         
 *      Parameters:   
 *      Author:       Drew Nathanson
 *
 *   Global Session Variables Used:
 *
 *   Mayfair at Parkland - Technical Synergy, Inc.   
 *   Copyright (c) 1999-2018. All Rights Reserved.   
 *
 ********************************************************/

 component
 {

 	/**
 	 * @output true
 	 * @returnvariable boolean
 	 * @hint this function will send the sms messages to the users.
 	 */
 	public function sendSMS ( required string phoneList, string msg = "" )
 	{
 	
 	   /*
 	    * Function Name: sendSMS
 	    *
 	    * Description:
 	    *   This function will create a message and then send it to the 
 	    *   homeoweners who have signed up for it.
 	    *
 	    * Function Information:
 	    *   Input Arguments:  phoneList,msg
 	    *   Output Argument:  true/false
 	    *
 	    */
 	
 		// Define variables
 		var error      = 0;
 		var message 	= "";
 		var result     = "";

 		// Create the object
 		var twilio 		= createObject("component", "cfc.twilio.TwilioLib").init(APPLICATION.twilioSettings.AccountSid, 
 		 	                                                                 		APPLICATION.twilioSettings.AuthToken, 
 			                                                                 		APPLICATION.twilioSettings.ApiVersion, 
 			                                                                 		APPLICATION.twilioSettings.ApiEndPoint, 
 			                                                                 		APPLICATION.twilioSettings.ApiResponseFormat);
 		
 		// Check to see if sms already happened today.
 		if ( !structKeyExists(application, "lastSMS") || dateCompare(now(), application.lastSMS) != 0 )
 		{

	 		// Create the message
	 		message = ( len(msg) ) ? msg : "There is new information on the Mayfair at Parkland Web Site. Visit it at: #application.httpPath#";

	 		// Start a loop to process each person in the list
	 		for ( phoneNo in phoneList )
	 		{

	 			// Remove any leading/trailing blanks
	 			phoneNo = trim(phoneNo);

	 			// Edit the phone to remove any possible errors
	 			if ( REFindNoCase('^(\+\d{11})$',phoneNo) ) 
	 			{

	 				// Make request to Twilio and get response object back
					result = twilio.newRequest("Accounts/{AccountSid}/SMS/Messages", "POST", { From = APPLICATION.twilioSettings.FromNumber, 
						                                                                        To   = phoneNo, 
						                                                                        Body = message});

					// Check the results
					var myResult = results.getResponse.output();
					if ( myResult.status == "failed" )
					{
						// Write out dump and break the loop
						writeDump("#myResults#");
						error   = 1;
						break;
					}

	 			}

	 		}

	  		// Check error
 			if ( !error )
 			{

 				// Set the application data
 				application.lastSMS = createDate(year(now()),month(now()),day(now()));

 			}

 		}

 		// Return true
 		return true;

 	}
 	
 }