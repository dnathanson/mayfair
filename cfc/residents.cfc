/*                                                      
    
    CF Name:     Residents
                                                  
    Description:
	   

      The following is the table layout for the CFC being created:

        Column Name                   Data Type          Data Len   Nullable
        ----------------------------  -----------------  ---------  --------
        residentId                    INT                 10          NO      
        name                          VARCHAR             200         YES     
        street                        VARCHAR             200         YES     
        emailAddress                  VARCHAR             150         YES     
        cellPhone                     VARCHAR             20          YES     
        password                      VARCHAR             50          YES     
        notifications                 INT                 10          YES     
        newsletter                    INT                 10          YES     
        sms                           INT                 10          YES     


      The following is a list of functions that are included in
      this module:

        getResidents
        insertResidents
        removeResidents
        updateResidents


    Program Information:                                
      Called from:  None
      Calls:        None
      Parameters:   None
      Author:       Drew Nathanson
      
    Global Session Variables Used:
    
    Mayfair at Parkland HOA - Techncial Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved.
                                
*/

Component
{
	// Set global all fields
	allFields = 'residentId, name, street, emailAddress, cellPhone, password, notifications, newsletter, sms';

	/**
    * @output  true
    * @returntype Query
    * @hint This function will return the entire record set from table RESIDENTS.
    */
	public function getResidents ( numeric residentId = 0, string selectLine = '', string where = '', string orderBy = '' )
	{

      /*

         Function Name: getResidents

         Description:
           This function will return the entire record set from table RESIDENTS.  
           
         Function Information:
            Input Arguments:  residentId   
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Check value to see if selectLine is set
		if ( !len(selectLine) ) { selectLine = allFields; }

		// Set field
		sqlLine = "Select #selectLine# from residents where 0=0 ";

		// Check for residentId
		sqlLine &= ( residentId > 0 ) ? " AND residentId = #residentId#" : "";

		// Check for WHERE
		sqlLine &= ( len(where) > 0 ) ? " AND #PreserveSingleQuotes(where)#" : "";

		// Check for orderBy
		sqlLine &= ( len(orderBy) > 0 ) ? " ORDER BY #PreserveSingleQuotes(orderBy)#" : "";

		// Execute the query
		recs = queryExecute("#sqlLine#",{ datasource:application.databaseSelect } );

		// Return
		return recs;

	}

	/**
    * @output  true
    * @returntype Numeric
    * @hint This function will insert the data into RESIDENTS.
    */
	public function insertResidents ( required Struct StrList )
	{

      /*

         Function Name: insertResidents

         Description:
           This function will insert the data into RESIDENTS.  
           
         Function Information:
            Input Arguments:  StrList  
            Output Arguments: newId (New Id Number)  

       */

		// Define local variables
		
		var newId        = 0;


		// Set up the try/catch environment
		try
		{

			// Define the stored procedure object
			procObj  = new storedproc();

			// Set the default information
			procObj.setDatasource("#Application.Database#");
			procObj.setProcedure("INSERT_RESIDENTS_PROC");

			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.name#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.street#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.emailAddress#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.cellPhone#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.password#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.notifications#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.newsletter#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.sms#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="Out", Variable="newId");

			// Execute the procedure
			resultObj = procObj.execute();
			newId = resultObj.getprocOutVariables().newId;

		} catch ( database excpt ) { 

			// Throw the error to the page
			rethrow;

		}

		// Return
		return newId;

	}


	/**
    * @output  true
    * @returntype Void
    * @hint This function will update the data into RESIDENTS.
    */
	public function updateResidents ( required Struct StrList )
	{

      /*

         Function Name: updateResidents

         Description:
           This function will update the data into RESIDENTS.  
           
         Function Information:
            Input Arguments:  StrList  
            Output Arguments: None  

       */

						

		// Set up the try/catch environment
		try
		{

			// Define the stored procedure object
			procObj  = new storedproc();

			// Set the default information
			procObj.setDatasource("#Application.Database#");
			procObj.setProcedure("UPDATE_RESIDENTS_PROC");

			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.residentId#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.name#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.street#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.emailAddress#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.cellPhone#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.password#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.notifications#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.newsletter#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.sms#");

			// Execute the procedure
			procObj.execute();

		} catch ( database excpt ) { 

			// Throw the error to the page
			rethrow;

		}


		// Return
		return ;

	}

	/**
    * @output  true
    * @returntype Boolean
    * @hint This function will remove a record from the table RESIDENTS.
    */
	public function removeResidents ( String residentid = 0, String where = "" )
	{

      /*

         Function Name: removeResidents

         Description:
           This function will remove a record from the table RESIDENTS.  
           
         Function Information:
            Input Arguments:    
            Output Arguments: True or False  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Set the sqlLine field
		sqlLine  = "Delete from residents where 0=0 ";
		sqlLine &= ( structKeyExists(arguments, 'residentid') && residentid > 0 ) ? "and residentId = #residentid# " : "";
		// Check for where
		sqlLine &= ( len(where) > 0 ) ? "AND #PreserveSingleQuotes(where)#" : "";

		// Execute the query
		queryExecute("#sqlLine#", { datasource:Application.DatabaseDelete} );


		// Return
		return ;

	}

	/**************************************************************************
	 * The next section is used as an object for the Getter object functions  *
	 **************************************************************************/

	/**
    * @output  true
    * @returntype query
    * @hint 
    */
	public function init ( numeric indexNo = -1, string Where = "" )
	{

      /*

         Function Name: init

         Description:
             
           
         Function Information:
            Input Arguments:  indexNo (primary key)  
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";

		// Retrieve the record
		sqlLine = "Select residentId, name, street, emailAddress, cellPhone, password, notifications, newsletter, sms from residents where 0 = 0";

 		// Check the key
		sqlLine &= ( indexNo > -1 ) ? " AND residentId = #indexNo#" : "";

 		// Check for where
		sqlLine &= ( len(where) > 0 ) ? "AND #PreserveSingleQuotes(where)# " : "";

		//Execute the query
		recordSet = queryExecute("#sqlLine#", { datasource:Application.DatabaseSelect } );


		// Return
		return recordset;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint Return the number of record in the query
    */
	public function GetRecordCount ( Void )
	{

      /*

         Function Name: GetRecordCount

         Description:
           Return the number of record in the query  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: recordcount  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.recordcount;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field residentId
    */
	public function getResidentid ( Void )
	{

      /*

         Function Name: getResidentid

         Description:
           This function will return the field residentId  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.residentId;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field name
    */
	public function getName ( Void )
	{

      /*

         Function Name: getName

         Description:
           This function will return the field name  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.name;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field street
    */
	public function getStreet ( Void )
	{

      /*

         Function Name: getStreet

         Description:
           This function will return the field street  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.street;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field emailAddress
    */
	public function getEmailaddress ( Void )
	{

      /*

         Function Name: getEmailaddress

         Description:
           This function will return the field emailAddress  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.emailAddress;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field cellPhone
    */
	public function getCellphone ( Void )
	{

      /*

         Function Name: getCellphone

         Description:
           This function will return the field cellPhone  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.cellPhone;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field password
    */
	public function getPassword ( Void )
	{

      /*

         Function Name: getPassword

         Description:
           This function will return the field password  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.password;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field notifications
    */
	public function getNotifications ( Void )
	{

      /*

         Function Name: getNotifications

         Description:
           This function will return the field notifications  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.notifications;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field newsletter
    */
	public function getNewsletter ( Void )
	{

      /*

         Function Name: getNewsletter

         Description:
           This function will return the field newsletter  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.newsletter;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field sms
    */
	public function getSms ( Void )
	{

      /*

         Function Name: getSms

         Description:
           This function will return the field sms  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.sms;

	}


}
