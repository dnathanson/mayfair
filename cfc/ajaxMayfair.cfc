/*************************************************************
 *
 *   CF Name:    ajaxMayfair.cfc
 *
 *   Description:
 *      This module contains the code to support all
 *      ajax functions within the system.
 *
 *      Best viewed with Tabs = 3
 *
 *    Program Information:
 *      Called from:  
 *      Calls:         
 *      Parameters:   
 *      Author:       Drew Nathanson
 *
 *    Global Session Variables Used:
 *
 *    Mayfair at Parkland - Technical Synergy, Inc.
 *    Copyright (c) 1999-2025. All Rights Reserved.   
 *
 ************************************************************/

component
{
	
	/**
	 * @output true
	 * @returnvariable numeric
	 * @returnformat json
	 * @hint this module will handle the checking to see if an email exists
	 */
	remote function ifEmailExists ( required string email )
	{
	
	   /*
	    * Function Name: ifEmailExists
	    *
	    * Description:
	    *   Check to see if the email exists.
	    *
	    * Function Information:
	    *   Input Arguments:  email
	    *   Output Argument:  true or false
	    *
	    */
	
		// Create the object
		var resObj = createObject("component","#application.cfcPath#residents");

		// get the record
		var rec 	= resObj.getResidents(where="emailAddress = '#email#'");

		local.rc = ( rec.recordCount ) ? "yes" : "no";

		// Return the results
		return serializeJSON(local.rc);
		
	}
	
	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will deterine if the address entered is correct
	 */
	remote function ifAddressCorrect ( required string address )
	{
	
	   /*
	    * Function Name: ifAddressCorrect
	    *
	    * Description:
	    *   This function will check to see if the address passed is correct.
	    *
	    * Function Information:
	    *   Input Arguments: address
	    *   Output Argument: retArray
	    *
	    */
	
		// Define variables
		var retArray   = [ ];

		// Create the query to test
		var addRec = queryExecute("Select * from addresses where address like '#address#%'");

		// Check the results
		if ( addRec.recordCount )
		{

			// Process the loop to load the array
			for ( var line in addRec )
			{

				// Append the address
				arrayAppend(retArray, line.address);

			}

		} else {
			// Pull the number out of the address and get all records for that number
			var nos  = address.split(" ");

			// Query on the results
			var addRecs = queryExecute("Select * from addresses where address like '#nos[1]#%'");
		
			// Check the results
			if ( !addRecs.recordCount )
			{

				retArray[1] = "Invalid Number";

			} else {

				// Process the loop to load the array
				for ( var line in addRecs )
				{

					// Append the address
					arrayAppend(retArray, line.address);
				}

			}

		}

		// Return the array
		return serializeJSON(retArray);

	}
	
	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will return the newsletter article for editing.
	 */
	remote function getArticleForUpdate ( required numeric id  )
	{
	
	   /*
	    * Function Name: getArticleForUpdate
	    *
	    * Description:
	    *   This function will retrieve and return the newsleter article 
	    *   for editing purposes.
	    *
	    * Function Information:
	    *   Input Arguments:  id
	    *   Output Argument:  retStruct
	    *
	    */
	
		// Define local variables
		var retStruct = { };

		// Retrieve the record
		rec = queryExecute("Select * from communityNews where commId = :id",
			                {
			                	id : { cfsqltype:"CF_SQL_INTEGER",value:id }
			                });

		// Set up the returm structure
		retStruct  = {
			articleDate : dateFormat(rec.articleDate,"mm/dd/yyyy"),
			articleHead : rec.articleHead,
			article     : rec.article
		};

		// Return the record
		return serializeJSON(retStruct);

	}
	
	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will return the blog entry for editing.
	 */
	remote function getBlogEntryForUpdate ( required numeric id  )
	{
	
	   /*
	    * Function Name: getBlogEntryForUpdate
	    *
	    * Description:
	    *   This function will retrieve and return the blog 
	    *   for editing purposes.
	    *
	    * Function Information:
	    *   Input Arguments:  id
	    *   Output Argument:  retStruct
	    *
	    */
	
		// Define local variables
		var retStruct = { };

		// Retrieve the record
		rec = queryExecute("Select * from wedgeBlog where wedgeBlogId = :id",
			                {
			                	id : { cfsqltype:"CF_SQL_INTEGER",value:id }
			                });

		// Set up the returm structure
		retStruct  = {
			blogDate : dateFormat(rec.blogDate,"mm/dd/yyyy"),
			subject  : rec.subject,
			blog     : rec.blog,
			active   : rec.active
		};

		// Return the record
		return serializeJSON(retStruct);

	}

	/**
	 * @output true
	 * @returnvariable boolean
	 * @hint this function will return the blog entry for remove.
	 */
	remote function removeBlogEntry ( required numeric id  )
	{
	
	   /*
	    * Function Name: removeBlogEntry
	    *
	    * Description:
	    *   This function will remove the blog entry.
	    *
	    * Function Information:
	    *   Input Arguments:  id
	    *   Output Argument:  retStruct
	    *
	    */
	
		// Retrieve the record
		rec = queryExecute("Delete from wedgeBlog where wedgeBlogId = :id",
			                {
			                	id : { cfsqltype:"CF_SQL_INTEGER",value:id }
			                });

		return true;
	}
	
	/**
	 * @output true
	 * @returnvariable boolean
	 * @hint this function will remove the resident.
	 */
	remote function removeResidentId ( required numeric id  )
	{
	
	   /*
	    * Function Name: removeResidentId
	    *
	    * Description:
	    *   This function will remove the resident account.
	    *
	    * Function Information:
	    *   Input Arguments:  id
	    *   Output Argument:  retStruct
	    *
	    */
	
		// Retrieve the record
		rec = queryExecute("Delete from residents where residentId = :id",
			                {
			                	id : { cfsqltype:"CF_SQL_INTEGER",value:id }
			                });

		return true;
	}

	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will remove all emails for the address
	 */
	remote function removeEmailsForAddress ( required string address )
	{
	
	   /*
	    * Function Name: removeEmailsForAddress
	    *
	    * Description:
	    *   This function will remove all emails associated with the 
	    *   address.
	    *
	    * Function Information:
	    *   Input Arguments:  address
	    *   Output Argument:  retString
	    *
	    */
	
		// Define variables
		var retString = "";

		// Create the object
		var resObj 	= createObject("component", "#application.cfcPath#residents");

		// Get all the addresses
		var resRecs = resObj.getResidents(where="street = '#address#'");

		// Create a list
		var resList = valueList(resRecs.residentId, ",");

		// Remove the accounts
		queryExecute("Delete from residents where residentId in ( :list )",
			           {
		           	 		list : { cfsqltype:"CF_SQL_VARCHAR",value:resList } 
			           });

		// Return the statement
		return serializeJSON("Email records have been removed");

	}
	
	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this funciton will send out email to reset the password
	 */
	remote function passwordResetUserAdmin ( required numeric id )
	{
	
	   /*
	    * Function Name: passwordResetUserAdmin
	    *
	    * Description:
	    *   This function will send an email to reset an email password 
	    *   for the web site.
	    *
	    * Function Information:
	    *   Input Arguments:  id
	    *   Output Argument:  retString
	    *
	    */
	
		// Define local variables
		var mailBody   = "";

		// Create the smtp object
		var smtpObj  	= createObject("component", "#application.cfcPath#smtp");

		// // Search for the email address
		var resRec = queryExecute("Select * from residents where residentId = :id",
			                        {
			                        	id : { cfsqltype:"CF_SQL_INTEGER",value:id }
			                        });

		// Build a link line
		var uuid 		= createUUID();
		var linkLine = '#application.httpPath#resetPassword.cfm/id/#uuid#';

		// Create the body of the message
		saveContent variable="mailBody"
		{
			writeOutput("
Dear #resRec.Name#,<br /><br />
  A request that your account password for Mayfair At Parkland Web Site be reset. 
  <br /><br />Please Use the link below to reset that password.
  <br /><br />
  <a href='#linkLine#'>#linkLine#</a>
  <br /><br />
  This link will be valid for only 1 hour.
  <br /><br />
  Mayfair at Parkland HOA<br />");

		}

		// Get the stmp informatation
		smtpStruct  = smtpObj.getSMTPinfo();

		// Create the mail object
		var mail = new mail(type="html", charset="utf-8", body="#mailBody#");

		// Set the mail object fields
		mail.setFrom("noreply@mayfairatparkland.com (Mayfair at Parkland Website)");
		mail.setTo("#resRec.emailAddress#");
		mail.setSubject("Reset your Password");
		mail.setServer(smtpStruct.host);
		mail.setPort(smtpStruct.port);
		mail.setUsername(smtpStruct.user);
		mail.setPassword(smtpStruct.pass);
		mail.setUseTLS(true);
		mail.send();

		// Build a struct
		var str = {
			residentId : resRec.residentId,
			uuid       : uuid,
			email      : resRec.emailAddress,
			dateTime   : Now()
		};

		// Set informatation in the 
		if ( !structKeyExists(application, "resetPassword") )
		{
			// Create the array
			application.resetPassword = [ ];
		} 

		// Append to the array
		arrayAppend(application.resetPassword, str);

		// Return the string
		return serializeJSON("The email for password reset has been successful mailed");

	}
	
	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will handle the sign up. 
	 */
	remote function signUpCommittee ( required numeric residentId, requird string committeeName )
	{
	
	   /*
	    * Function Name: signUpCommittee
	    *
	    * Description:
	    *   This function will get the resident id and committee name 
	    *   and then add that persome to the list. 
	    *
	    * Function Information:
	    *   Input Arguments:  residentId, committeeName
	    *   Output Argument:  retString
	    *
	    */
	
		// Define local variables
		var retString    	= "";

		// Create the objects
		var commObj 	= createObject("component", "#application.cfcPath#committees");
		var commMemObj = createObject("component", "#application.cfcPath#committeeMembers");
		var smtpObj  	= createObject("component", "#application.cfcPath#smtp");
		var resObj     = createObject("component", "#application.cfcPath#residents");

		// Get the committee number
		commRec 			= commObj.getCommittees(where="committeeName = '#committeeName#'");

		// Check to see if the resident is already a member
		commMemRec 		= commMemObj.getCommitteeMembers(Where="residentId=#residentId# and committeeId = #commRec.committeeId#");

		// Check the results
		if ( commMemRec.recordCount )
		{

			// Set the field
			retString = "You have already signed up for the Committee - #committeeName#";

		} else {

			// Check to see if there is a committeeMember who is in charge
			var commChairRec 	= commMemObj.getCommitteeMembersView(Where="committeeId = #commRec.committeeId# and chairman = 1");
			
			// Get the resident record
			var resRec        = resObj.getResidents(where="residentId = #residentId#");

			/* var strList = {
				committeeId : commRec.committeeId,
				residentId  : residentId,
				chairman    : 0
			}; */

			// Set record
			// commMemObj.insertCommitteemembers(strList);

			// Insert the person to the committee
			queryExecute("Insert into committeeMembers ( committeeId, residentId, signupDate, chairman ) 
				             values ( :commId, :resId, now(), :chair )",
				           {
				           	  commId : { cfsqltype:"CF_SQL_INTEGER", value:commRec.committeeId },
				           	  resId  : { cfsqltype:"CF_SQL_INTEGER", value:residentId },
				           	  chair  : { cfsqltype:"CF_SQL_INTEGER", value:0 }
				           });

			// Set the message
			retString = "You have been signed up for the Committee. You will be contacted shortly";

			// Check to see if there is a chairman
			if ( commChairRec.recordCount )
			{
				// Get the stmp informatation
				smtpStruct  = smtpObj.getSMTPinfo();

				// Create email box
				savecontent variable="mailBody"
				{
					writeOutput(
"Dear #commRec.committeeName# Chairperson,<br /><br />
This is to inform you that #resRec.Name# has joined your committee today (#dateFormat(now(), "mm/dd/yyyy")#). 
<br /><br />
The specific information is below:<br />
Address: #resRec.street#<br />
Email: <a href='mailto:#resRec.emailAddress#'>#resRec.emailAddress#</a><br />
<br /><br />
Mayfair at Parkland Web Site<br />");
				}

				// Create the mail object and send
				mail = new mail(type="html", charset="utf-8", body="#mailBody#");
				mail.setFrom("noreply@mayfairatparkland.com (Mayfair at Parkland Website)");
				mail.setTo("#commChairRec.emailAddress#");
				mail.setSubject("New Committee Member");
				mail.setServer(smtpStruct.host);
				mail.setPort(smtpStruct.port);
				mail.setUsername(smtpStruct.user);
				mail.setPassword(smtpStruct.pass);
				mail.setUseTLS(true);
				mail.send();
			}
		}

		// Return the text
		return serializeJSON(retString);

	}
	
	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will return a json object with permissions (if any)
	 */
	remote function getPermissionById ( required numeric residentId )
	{
	
	   /*
	    * Function Name: getPermissionById
	    *
	    * Description:
	    *   This function will check to see if there is any records for permission
	    *   for this user.
	    *
	    * Function Information:
	    *   Input Arguments:  residentId
	    *   Output Argument:  retStruct (json)
	    *
	    */
	
		// Define variables
		var retStruct     = { };

		// Create the object
		permObj = createObject("component", "#application.cfcPath#permissions");

		// Get the record
		permRec = permObj.getPermissions(where="residentId='#residentId#'");

		// Check the results
		if ( !permRec.recordCount )
		{
			retStruct   = {
				admin      : 0,
				management : 0,
				blog       : 0
			};

		} else {

			// Set the structure
			retStruct    = {
				admin       : permRec.admin,
				management  : permRec.management,
				blog        : permRec.blog
			};

		}

		// Return the structure
		return serializeJSON(retStruct);

	}
	
	/**
	 * @output true
	 * @returnvariable void
	 * @returnformat json
	 * @hint this function will handle the update/insert of the permissions
	 */
	remote function processPermissions ( required string formJson )
	{
	
	   /*
	    * Function Name: processPermissions
	    *
	    * Description:
	    *   THis funciton will handle the insert/update of the permissions.
	    *
	    * Function Information:
	    *   Input Arguments: formJson
	    *   Output Argument: None
	    *
	    */
	
		// Define variables
		var formData    = deserializeJSON(formJson);

		// Create the object
		var permObj  = createObject("component", "#application.cfcPath#permissions");

		// Check to see if the resident exists
		permRec 		= permObj.getPermissions(where="residentId = #formData.residentId#");

		// Check the results
		if ( permRec.recordCount )
		{
			// Update the record
			queryExecute("Update permissions
				             set admin = :admin,
				                 management = :mgt,
				                 blog       = :blog,
				                 committee  = :comm
				           where residentId = :id",
				          {
				          	admin : { cfsqltype:"CF_SQL_INTEGER", value:formData.admin },
				          	mgt   : { cfsqltype:"CF_SQL_INTEGER", value:formData.management },
				          	blog  : { cfsqltype:"CF_SQL_INTEGER", value:formData.blog },
				          	comm  : { cfsqltype:"CF_SQL_INTEGER", value:formData.committee },
				          	id    : { cfsqltype:"CF_SQL_INTEGER", value:formData.residentId }
				          });

			// Set the message
			session.message = "Permissions have been updated";
		
		} else {

			// Update the record
			queryExecute("Insert into  permissions ( residentId, admin, management, blog, committee )
				             values ( :id, :admin, :mgt, :blog, :commit )",
				          {
				          	id      : { cfsqltype:"CF_SQL_INTEGER", value:formData.residentId },
				          	admin   : { cfsqltype:"CF_SQL_INTEGER", value:formData.admin },
				          	mgt     : { cfsqltype:"CF_SQL_INTEGER", value:formData.management },
				          	blog    : { cfsqltype:"CF_SQL_INTEGER", value:formData.blog },
				          	commit  : { cfsqltype:"CF_SQL_INTEGER", value:formData.committee }
				          });

			// Set the message
			session.message = "Permissions has been added to the resident";

		} 
	
		// Return 

	}
	
	/**
	 * @output true
	 * @returnvariable void
	 * @returnformat json
	 * @hint this function will remove the permissions from the resident
	 */
	remote function removePermissionById ( required numeric residentId )
	{
	
	   /*
	    * Function Name: removePermissionById
	    *
	    * Description:
	    *   This funciton will remove the permissions from the resident.
	    *
	    * Function Information:
	    *   Input Arguments:  residentId
	    *   Output Argument:  None
	    *
	    */
	
		// Remove the record
		queryExecute("delete from permissions where residentId = :id",
			          {
			          	 id : { cfsqltype:"CF_SQL_INTEGER", value:residentId }
			          });

		// Set the field
		session.message = "Permission has been removed";

	}
	
	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this funciton will change status of the minutes
	 */
	remote function setFinalOnMinutes ( required numeric minuteId )
	{
	
	   /*
	    * Function Name: setFinalOnMinutes
	    *
	    * Description:
	    *   This function will change the status of the minutes.
	    *
	    * Function Information:
	    *   Input Arguments:  minutesId
	    *   Output Argument:  retString
	    *
	    */
	
		local.retString = "";

		// Update the record
		queryExecute("Update minutes set approved = 1 where minId = :id", { id: { cfsqltype:"CF_SQL_INTEGER",value:minuteId} } );

		// Set the text
		local.retString = "Minutes Status has been Updated";

		// Return retString
		return serializeJSON(local.retString);

	}
	
	/**
	 * @output true
	 * @returnvariable boolean
	 * @returnformat 'json'
	 * @hint this funciton will delete the minutes.
	 */
	remote function deleteMinutesById ( required numeric minuteId )
	{
	
	   /*
	    * Function Name: deleteMinutesById
	    *
	    * Description:
	    *   This funciton will delete the minutes by the id passed. 
	    *
	    * Function Information:
	    *   Input Arguments: minuteId
	    *   Output Argument: None
	    *
	    */
	
		// Remove the entry
		queryExecute("Delete from minutes where minId = :id", { id: { cfsqltype:"CF_SQL_INTEGER",value:minuteId} } );

		// Set the session message
		session.message = "The minutes you have selected has been removed";

		return serializeJSON(true);

	}
	
	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will set a member as chairman
	 */
	remote function setCommitteeChairman ( required numeric committeeMemberId )
	{
	
	   /*
	    * Function Name: setCommitteeChairman
	    *
	    * Description:
	    *   This function will set a committeeMember chairman.
	    *
	    * Function Information:
	    *   Input Arguments:  committeeMemberId
	    *   Output Argument:  retString
	    *
	    */
	
		var retString     = "";

		// Update the member
		queryExecute("Update committeeMembers set chairman = 1 where commitMemberId = :id",
			          {
			          	 id: { cfsqltype:"CF_SQL_INTEGER",value:committeeMemberId }
			          });

		// Return Text
		return serializeJSON("Committee Member has been made chairperson or co-chairperson");

	}
	
	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will remove the comittee member sdrom the list
	 */
	remote function deleteCommitteeMember ( required numeric committeeMemberId, required numeric commitId )
	{
	
	   /*
	    * Function Name: deleteCommitteeMember
	    *
	    * Description:
	    *   This function will remove the comittee memb er from the list.
	    *
	    * Function Information:
	    *   Input Arguments:  committeeId, memberId
	    *   Output Argument:
	    *
	    */
	
		// Remove the record
		queryExecute("delete from committeeMembers where commitMemberId = :id",
			           {
			           	  id : { cfsqltype:"CF_SQL_INTEGER",value:committeeMemberId }
			           });

		// Retrieve the list of people in the commitee
		cRecs = queryExecute("select * from view_committeeMembers where committeeId = :commId",
			                  {
			                  	commId : { cfsqltype:"CF_SQL_INTEGER", value:commitId }
			                  });

		// Save the information
		saveContent variable="retString"
		{
			writeOutput("<table>
<thead>
<tr>
<th>Name</th>
<th>Email</th>
<th>Chair Person </th>
<th> &nbsp; </th>
</tr>
</thead>
<tbody>");
			if ( !cRecs.recordCount )
			{
				writeOutput("<tr>
<td>No Committee Members Exist</td>
<td> &nbsp;</td>
<td> &nbsp;</td>
<td> &nbsp;</td>
</tr>");

			} else {

				// Process the records
				for ( var line in cRecs )
				{
					writeOutput("<tr>
<td>#line.name#</td>
<td>#line.emailAddress#</td>
<td id='C-#line.commitMemberId#'>");
					if ( !line.chairman ) { writeOutput("No"); } else { writeOutput("Yes"); }
					writeOutput("</td>
<td>
<div class='8u 12u(mobilep)'>
<a href='##' id='#line.commitMemberId#' data-id='#line.commitMemberId#' class='fit'>Make Chair Person</a>
&nbsp;
<a href='##' id='D-#line.commitMemberId#' data-id='D-#line.commitMemberId#' data-commid='#commitId#' class='fit'>Delete</a>
</div>
</td>
</tr>"); 
				}

				writeOutput("</tbody></table>");

			}

		}

		// Return the message
		session.message = "Committee Member has been removed";

		// Return retString
		return serializeJSON("#retString#");

	}
	
	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will return an individual event
	 */
	remote function editEvent ( required numeric eventId )
	{
	
	   /*
	    * Function Name: editEvent
	    *
	    * Description:
	    *   This function will retreive the information for an event.
	    *
	    * Function Information:
	    *   Input Arguments: eventId
	    *   Output Argument: retString
	    *
	    */
	
		// Define local variables 
		var strList    = { };

		// Create the object
		var eventObj = createObject("component", "#application.cfcPath#events");

		// Retrieve the record
		eventRec 	 = eventObj.getEvents(eventId="#eventId#");

		// Load the structure
		strList 			= {
			eventId 		  : eventRec.eventId,
			residentId 	  : eventRec.residentId,
			title         : eventRec.title,
			startDate     : dateFormat(eventRec.startDateTime, "mm/dd/yyyy"),
			startTime     : timeFormat(eventRec.startDateTime, "HH:mm"),
			endDate       : dateFormat(eventRec.endDateTime, "mm/dd/yyyy"),
			endTime       : timeFormat(eventRec.endDateTime, "HH:mm"),
			shortDesc     : eventRec.shortDescription,
			sendNotice    : eventRec.sendNotice,
			committeeId   : eventRec.committeeId,
			locationId    : eventRec.locationId,
			otherLocation : eventRec.otherLocation
		};

		// Return
		return serializeJSON(strList);

	}
	
	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will remove the event from the database table.
	 */
	remote function removeEvent ( required numeric id, string sDate = "", string eDate = "", numeric committeeId = 0 )
	{
	
	   /*
	    * Function Name: removeEvent
	    *
	    * Description:
	    *   This function will remove the event from the events table and re-d
	    *
	    * Function Information:
	    *   Input Arguments: id, sDate, eDate
	    *   Output Argument: retString
	    *
	    */
	
		// Define local variables
		var retString = "";

		// Remove the event from the calendar
		queryExecute("delete from events where eventId = :id",
						 {
						 	 id : { cfsqltype:"CF_SQL_INTEGER", value:id }
						 	});

		// Check the values
		if ( len(sDate) )
		{

			// Get a list of the other events
			eRecs = queryExecute("Select * from events where startDateTime between :s and :e",
										{
											s : { cfsqltype:"CF_SQL_DATE", value:sDate },
											e : { cfsqltype:"CF_SQL_DATE", value:eDate }
										});
		} else {

			eRecs = queryExecute("Select * from events where committeeId = :commId and startDateTime between :s and :e ",
				                  {
				                  	commId : { cfsqltype:"CF_SQL_INTEGER", value:committeeId },
				                  	s      : { cfsqltype:"CF_SQL_DATE", value:"#year(now())#-#month(now())#-01" },
				                  	e      : { cfsqltype:"CF_SQL_DATE", value:"#year(now())#-12-31" }
				                  });

		}

		// Save info
		saveContent variable="retString"
		{
			writeOutput("<table>
<thead>
<tr>
<th>Start Date/Time</th>
<th>End Date/Time</th>
<th>Event Title</th>
<th> &nbsp;</th>
</tr>
</thead>
<tbody>");
			if ( !eRecs.recordCount )
			{
				writeOutput("<tr>
<td> &nbsp;</td>
<td> &nbsp;</td>
<td> No Event Listed</td>
</tr>");
			} else {

				for ( var line in eRecs )
				{
					writeOutput("<tr>
<td>#dateFormat(line.startDateTime,"mm/dd/yyyy")#@#timeFormat(line.startDateTime)#</td>
<td>#dateFormat(line.endDateTime,"mm/dd/yyyy")#@#timeFormat(line.endDateTime)#</td>
<td>#line.title#</td>
<td><a href=""##"" id=""E-#line.eventId#"" data-id=""#line.eventId#"">Edit</a>&nbsp;&nbsp;<a href=""##"" id=""D-#line.eventId#"" data-id=""D-#line.eventId#"">Delete</a></td>
</tr>");
				}

			}

			writeOutput("</tbody></table>");	
		}

		// Return the value
		return serializeJSON("#retString#");

	}
	
	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will return a list of the events
	 */
	remote function getCommitteeEvents ( required numeric committeeId )
	{
	
	   /*
	    * Function Name: getCommitteeEvents
	    *
	    * Description:
	    *   This function will get the events for a particular committee.
	    *
	    * Function Information:
	    *   Input Arguments: committeeId
	    *   Output Argument: retString
	    *
	    */
	
		// Define variables
		var retString  = "";
		var startDate  = month(now()) & "-01-" & year(now());

		// Retrieve the records
		eventRecs = queryExecute("select * from events where committeeId = :id and date(startDateTime) >= :stDate order by startDateTime",
			                       {
			                       		id     : { cfsqltype:"CF_SQL_INTEGER", value:committeeId },
			                       		stDate : { cfsqltype:"CF_SQL_DATE",    value:startDate }
			                       });

		saveContent variable="retString"
		{
			writeOutput("
<table id='eventTab'>
<thead>
	<tr>
		<th>Start Date/Time</th>
		<th>End Date/Time</th>
		<th>Event Title</th>
		<th> &nbsp;</th>
	</tr>
</thead>
<tbody>");
			if ( !eventRecs.recordCount )
			{
				writeOutput("
				<tr>
					<td> &nbsp;</td>
					<td> &nbsp;</td>
					<td> No Event Listed</td>
				</tr>");
			} else {
				for ( rec in eventRecs )
				{
					writeOutput("
					<tr>
						<td>#dateFormat(rec.startDateTime,'mm/dd/yyyy')#@#timeFormat(rec.startDateTime)#</td>
						<td>#dateFormat(rec.endDateTime,'mm/dd/yyyy')#@#timeFormat(rec.endDateTime)#</td>
						<td>#rec.title#</td>
						<td><a href='##' id='E-#rec.eventId#' data-id='E-#rec.eventId#'>Edit</a>&nbsp;&nbsp;<a href='##' id='D-#rec.eventId#' data-id='D-#rec.eventId#'>Delete</a></td>
					</tr>");
				}
			
				writeOutput("</tbody></table>");

			}
		}

		// Return the information
		return serializeJSON(retString);

	}
	
	/**
	 * @output true
	 * @returnvariable void
	 * @returnformat json
	 * @hint this function will remove an event
	 */
	remote function deleteEvent ( required numeric eventId )
	{
	
	   /*
	    * Function Name: deleteEvent
	    *
	    * Description:
	    *   This function will get an event id and remove it from the 
	    *   database.
	    *
	    * Function Information:
	    *   Input Arguments: eventId
	    *   Output Argument: none
	    *
	    */
	
		// Remove the event
		queryExecute("delete from events where eventId = :id",
			          {
			          	 id : { cfsqltype:"CF_SQL_INTEGER", value: eventId }
			          });

		// Return

	}
	
	/**
	 * @output true
	 * @returnvariable void
	 * @hint this function will update the event on the calendar
	 */
	
	remote function updateCommEvent ( required string formJson )
	{
	
	   /*
	    * Function Name: updateCommEvent
	    *
	    * Description:
	    *   This function will display the committee events 
	    *
	    * Function Information:
	    *   Input Arguments:
	    *   Output Argument:
	    *
	    */

		// Define variables
		var formData 	= deserializeJSON(formJson);

		// Create the object
		eventObj 		= createObject("component", "#application.cfcPath#events");

		// Retrieve the record
		eventRec 		= eventObj.getEvents(where="eventId = #formData.eventId#");

		// Create the date time for the database
		var stDateTime = createDateTime( year(formData.startDate), month(formData.startDate), day(formData.startDate), 
			                              hour(formData.startTime), minute(formData.startTime), "00");

		var edDateTime = createDateTime( year(formData.endDate), month(formData.endDate), day(formData.endDate), 
			                              hour(formData.endTime), minute(formData.endTime), "00");

		// Add fields to the structure
		formData.startDateTime = stDateTime;
		formData.endDateTime   = edDateTime;
		formData.residentId    = eventRec.residentId;

		// Update the information
		eventObj.updateEvents( formData );

		// Return 

	}
	
	/**
	 * @output true
	 * @returnvariable void
	 * @returnformat json
	 * @hint this function will insert the new event to the databas
	 */
	remote function addCommEvent ( required string formJson )
	{
	
	   /*
	    * Function Name: addCommEvent
	    *
	    * Description:
	    *   This function will recevie data and insert it into the 
	    *   events table.
	    *
	    * Function Information:
	    *   Input Arguments:  formJson
	    *   Output Argument:  none
	    *
	    */
	
		// Define variables
		var formData 	= deserializeJSON(formJson);

		// Create the object
		eventObj 		= createObject("component", "#application.cfcPath#events");

		// Create the date time for the database
		var stDateTime = createDateTime( year(formData.startDate), month(formData.startDate), day(formData.startDate), 
			                              hour(formData.startTime), minute(formData.startTime), "00" );

		var edDateTime = createDateTime( year(formData.endDate), month(formData.endDate), day(formData.endDate), 
			                              hour(formData.endTime), minute(formData.endTime), "00" );

		// Add fields to the structure
		formData.startDateTime    = stDateTime;
		formData.endDateTime      = edDateTime;
		formData.residentId       = session.userInfo.residentId;
		formData.shortDescription = rereplace(formData.shortDescription, chr(13)&chr(10), "", "ALL");

		// Update the information
		eventObj.insertEvents( formData );
		/*
		queryExecute("insert into events ( title, startDateTime, endDateTime, residentId, shortDescription, sendNotice, committeeId, locationId, otherLocation )
			            values ( :t, :stDT, :endDTime, :resId, :shortDesc, :notice, :commId, :locId, :oLoc )",
			         {
			         	t                : { cfsqltype:"CF_SQL_VARCHAR",   value:formData.title },
			         	stDT             : { cfsqltype:"CF_SQL_TIMESTAMP", value:stDateTime },
			         	endDTime         : { cfsqltype:"CF_SQL_TIMESTAMP", value:edDateTime },
			         	resId            : { cfsqltype:"CF_SQL_INTEGER",   value:formData.residentId },
			         	shortDesc        : { cfsqltype:"CF_SQL_VARCHAR",   value:formData.shortDescription },
			         	notice           : { cfsqltype:"CF_SQL_INTEGER",   value:formData.sendNotice },
			         	commId           : { cfsqltype:"CF_SQL_INTEGER",   value:formData.committeeId },
			         	locId            : { cfsqltype:"CF_SQL_INTEGER",   value:formData:locationId },
			         	oLoc             : { cfsqltype:"CF_SQL_VARCHAR",   value:formData:otherLocation }
			         });
		*/
	}
	
	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will return the text from the log file.
	 */
	remote function getCommitteeLog ( required numeric commLogId )
	{
	
	   /*
	    * Function Name: getCommitteeLog
	    *
	    * Description:
	    *   This function will return the log entry by id number.
	    *
	    * Function Information:
	    *   Input Arguments: commLogId
	    *   Output Argument: retString
	    *
	    */
	
		// Retrieve the record
		var logRec = queryExecute("Select * from committeeLog where committeeLogId = :id",
			                       {
			                       	 id : { cfsqltype:"CF_SQL_INTEGER", value:commLogId }
			                       });

		// Return
		return ( logRec.recordCount ) ? serializeJSON("#logRec.logText#") : serializeJSON("None");

	}

	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will return the text from the log file.
	 */
	remote function getCommitteeLogMgr ( required numeric commLogId )
	{
	
	   /*
	    * Function Name: getCommitteeLog
	    *
	    * Description:
	    *   This function will return the log entry by id number.
	    *
	    * Function Information:
	    *   Input Arguments: commLogId
	    *   Output Argument: retString
	    *
	    */
	
		// Define the structure
		var retStruct  = { };

		// Retrieve the record
		var logRec = queryExecute("Select a.*,b.name from committeeLog a, residents b where a.committeeLogId = :id and b.residentId = a.residentId",
			                       {
			                       	 id : { cfsqltype:"CF_SQL_INTEGER", value:commLogId }
			                       });
		retStruct = {
			logDate : dateFormat(logRec.logDate,"mm/dd/yyyy"),
			logText : logRec.logText,
			person  : logRec.name
		};

		// Return
		return  serializeJSON("#retStruct#");

	}

	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will check to see if the events room is taken at the date/time
	 */
	remote function checkEventAvailable ( required numeric locId, required string sDate, required string sTime )
	{
	
	   /*
	    * Function Name: checkEventAvailable
	    *
	    * Description:
	    *   This function will check the events to see if the clubhouse is taken on that 
	    *   date and time.
	    *
	    * Function Information:
	    *   Input Arguments:  locId, sDate, sTime
	    *   Output Argument:  retString
	    *
	    */
	
		var stDt 		= createDateTime(year(sDate), month(sDate), day(sDate), hour(sTime), minute(sTime), "00");
		var beginTime 	= dateAdd("h", -2, stDt);
		var endTime    = dateAdd("h", 4, stDt);
		var retString  = "";

		// Do query to see if there is any event
		var recs = queryExecute("Select * from events where ( startDateTime between 
			                      '#dateFormat(beginTime, 'yyyy-mm-dd')# #timeFormat(beginTime, 'HH:mm:ss')#' and 
			                      '#dateFormat(endTime, 'yyyy-mm-dd')# #timeFormat(endTime, 'HH:mm:ss')#') ");

		// Return the value
		return ( recs.recordCount ) ? serializeJSON("busy") : serializeJSON("open");

	}
	
	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will create a structure in the session scope for the parking request.
	 */
	remote function setParkingRequest ( required string formJson )
	{
	
	   /*
	    * Function Name: setParkingRequest
	    *
	    * Description:
	    *   This function will handle the creation of a structure to 
	    *   handle the parking request.
	    *
	    * Function Information:
	    *   Input Arguments: residentId, phone, startDate, endDate
	    *   Output Argument: retString
	    *
	    */
	
		// Define variables
		var formData  = deserializeJSON("#formJson#");
		var retString = "FAIL";

		// Check to see if the structure exists
		if ( !structKeyExists(session, "parking") )
		{

			var resRec = queryExecute("Select * from residents where residentId = :id", 
				                       {
				                       	 id : { cfsqltype:"CF_SQL_INTEGER", value:formData.residentId }
				                       });

			session.parking = {
				residentId : formData.residentId,
				name       : resRec.name,
				street     : resRec.street,
				phone      : formData.phone,
				startDate  : formData.startDate,
				endDate    : formData.endDate,
				reason     : formData.reason,
				cars       : [ ]
			};

			// Set ethe field to OK
			retString = "OK";

		}
	
		// Return retString
		return serializeJSON("#retString#");

	}
	
	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will add car to the array and create a return table
	 */
	remote function setCar ( required string formJson )
	{

	   /*
	    * Function Name: setCar
	    *
	    * Description:
	    *   This function will add the car information to the array (as a structure)
	    *   and then create a table to be returned and displayed.
	    *
	    * Function Information:
	    *   Input Arguments:  formJson
	    *   Output Argument:  retString
	    *
	    */
	
		// Define variables
		var cnt       = 1;
		var retString = "";

		// Convert the stirng to a structure
		var formData = deserializeJSON("#formJson#");

		// Append this to the structure
		arrayAppend(session.parking.cars, formData);

		// Create the resulting table
		saveContent variable="retString"
		{
			writeOutput("
<table border='0' id='resultTab'>
<thead>
	<tr>
		<th>Make/Model</th>
		<th>Tag/State</th>
		<th>Year</th>
		<th>Color</th>
		<th> &nbsp;</th>
	</tr>
</thead>
<tbody>");
			for ( var line in session.parking.cars )
			{
				writeOutput("
<tr>
	<td> #line.make# - #line.model#</td>
	<td> #line.tagNo# - #line.state#</td>
	<td> #line.year#</td>
	<td> #line.color#</td>
	<td> <a href='##' id='c-#cnt#' data-id='#cnt#'><span class='fa fa-trash' style='color:red;'></span></a></td>
</tr>");
			}
			writeOutput("</tbody></table>");
			cnt++;
		}

		// Return retString
		return serializeJSON("#retString#");

	}	

	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will remove the car from the array.
	 */
	remote function deleteCar ( required numeric carId )
	{
	
	   /*
	    * Function Name: deleteCar
	    *
	    * Description:
	    *   This function will remove the car by index id to the array. It will then 
	    *   retrieve and load the remaining cars,
	    *
	    * Function Information:
	    *   Input Arguments: carId
	    *   Output Argument:
	    *
	    */
	
		// define variables
		var retString  = "";
		
		// Remove the item
		arrayDeleteAt(session.parking.cars, carId);
		
		// Create the resulting table
		saveContent variable="retString"
		{
			writeOutput("
<table border='0' id='resultTab'>
<thead>
	<tr>
		<th>Make/Model</th>
		<th>Tag/State</th>
		<th>Year</th>
		<th>Color</th>
		<th> &nbsp;</th>
	</tr>
</thead>
<tbody>");
			for ( var x = 1; x <= arrayLen(session.parking.cars); x++ )
			{
				writeOutput("
<tr>
	<td> #session.parking.cars[x].make# - #session.parking.cars[x].model#</td>
	<td> #session.parking.cars[x].tagNo# - #session.parking.cars[x].state#</td>
	<td> #session.parking.cars[x].year#</td>
	<td> #session.parking.cars[x].color#</td>
	<td> <a href='##' id='c-#x#' data-id='#x#'><span class='fa fa-trash' style='color:red;'></span></a></td>
</tr>");
			}
			writeOutput("</tbody></table>");
		}

		// Return retString
		return serializeJSON("#retString#");
	
	}

	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function will set the parking request record
	 */
	remote function setParkingDisapproved( required numeric id, required numeric denyParkId )
	{
	
	   /*
	    * Function Name: setParkingDisapproved	    
	    * Description:
	    *   This function will update the database with the disapproval and 
	    *   then email the owner of the results. 
	    *
	    * Function Information:
	    *   Input Arguments: id
	    *   Output Argument: retString
	    *
	    */
	
		// Define variables
		var body     = "";

		// Create the SMTP object
		smtpObj = createObject("component", "#application.cfcPath#smtp");

		// Update the database
		queryExecute("update parkingRequest 
			             set parkDenyId = :denyId, 
			                 approvedDate = :dt, 
			                 completed = 1 
			           where parkingReqId = :id",
			           {
			           		denyId : { cfsqltype:"CF_SQL_INTEGER", value:denyParkId },
			           		dt     : { cfsqltype:"CF_SQL_TIMESTAMP", value:now() },
			           		id     : { cfsqltype:"CF_SQL_INTEGER", value:id }
			           });
	
		// Retrieve the record
		orRec  = queryExecute("select * from view_parkingRequest where parkingReqId = :id",
			                    {
			                    	  id : { cfsqltype:"CF_SQL_INTEGER", value:id }
			                    });	
		// Create the email
		saveContent variable="body"
		{
			writeOutput("
Dear Homeowner,<br />
In reviewing your request for on-street parking request, the BOD has decided to <b>NOT</b> approve your request.
<br /><br />
The reason for your denial was:<br />
<i>#orRec.denyReason#</i><br /><br />
You might wish to contact the management company if you feel an error has been made.
<br /><br />
Thank you for your support,<br />
The Board of Directors");
		}

		// Create a mail object
		mail = new mail(type="html", charset="utf-8", body="#body#");
		mail.setFrom("noreply@mayfairatparkland.com (Mayfair at Parkland Website)");
		mail.setTo("#orRec.emailAddress#");
		mail.setSubject("Your On-Street Parking Request has been denied");
		mail.setServer(smtpStruct.host);
		mail.setPort(smtpStruct.port);
		mail.setUsername(smtpStruct.user);
		mail.setPassword(smtpStruct.pass);
		mail.setUseTLS(true);

		// Send the email
		mail.send();

		// Return okay
		return serializeJSON("Okay");

	}
	
	/**
	 * @output true
	 * @returnvariable string
	 * @returnformat json
	 * @hint this function set the approval for the on-street parking.
	 */
	remote function setParkingApproved ( required numeric id )
	{
	
	   /*
	    * Function Name: setParkingApproved
	    *
	    * Description:
	    *   This function will approve the on-street parking and then 
	    *   send a letter to the homeowner telling them that their 
	    *   request has been approved. We will also be notifying 
	    *   Garing of the request and approval.
	    *
	    * Function Information:
	    *   Input Arguments: id
	    *   Output Argument: retString
	    *
	    */
	
		// Define variables
		var body     = "";

		// Create the SMTP object
		smtpObj = createObject("component", "#application.cfcPath#smtp");

		// Update the database
		queryExecute("update parkingRequest 
			             set approved     = 1, 
			                 approvedDate = :dt, 
			                 completed    = 1 
			           where parkingReqId = :id",
			           {
			           		dt     : { cfsqltype:"CF_SQL_TIMESTAMP", value:now() },
			           		id     : { cfsqltype:"CF_SQL_INTEGER",   value:id }
			           });

		// Retrieve the record
		orRec  = queryExecute("select * from view_parkingRequest where parkingReqId = :id",
			                    {
			                    	  id : { cfsqltype:"CF_SQL_INTEGER", value:id }
			                    });	

		// Retrieve the vehicle information
		vcRecs = queryExecute("select * from parkingReqCars where parkingReqId = :id order by parkingCarId",
			                    {
			                    		id : { cfsqltype:"CF_SQL_INTEGER", value:id }
			                    });

		// Create the email
		saveContent variable="body"
		{
			writeOutput("
Dear Homeowner,<br />
In reviewing your request for on-street parking request, the BOD has decided to <b>Approve</b> your request.
<br /><br />
Please make sure that your guests obey all traffic regulations while in the community such as speed and 
there is to be no parking on any roadway where there is a double yellow line. Broward Sheriff will ticket and
they may also be towed. 
<br /><br />
Thank you for your support,<br />
The Board of Directors");
		}

		// Retrieve the SMTP informtion
		smtpStruct = smtpObj.getSMTPInfo();

		// Create a mail object
		mail = new mail(type="html", charset="utf-8", body="#body#");
		mail.setFrom("noreply@mayfairatparkland.com (Mayfair at Parkland Website)");
		mail.setTo("#orRec.emailAddress#");
		mail.setSubject("Your On-Street Parking Request has been Approved");
		mail.setServer(smtpStruct.host);
		mail.setPort(smtpStruct.port);
		mail.setUsername(smtpStruct.user);
		mail.setPassword(smtpStruct.pass);
		mail.setUseTLS(true);

		// Send the email
		mail.send();

		// ADD IN EMAIL TO GARING HERE

		// Build the email for Garing
		saveContent variable="body"
		{
			writeOutput("
The following notice is to let you know that the Mayfair at Parkland - Board of Directors has approved 
the following on-street request:<br />
#orRec.name#<br />
#orRec.street#<br /><br />
Date: <b>#dateFormat(orRec.startDate, "mm/dd/yyyy")# to #dateFormat(orRec.endDate, "mm/dd/yyyy")#</b>
<br /><br />
Vehicle Information:
<table border='0' cellpadding='4' cellspacing='4' width='70%'>
<tr>
	<th><b>Make/Model/Year</b></th>
	<th><b>State/Tag No</b></th>
	<th><b>Color</b></th>
</tr>");
			for ( x = 1; x <= arrayLen(session.parking.cars); x++ )
			{
				writeOutput("<tr><td>#session.parking.cars[x].make#/#session.parking.cars[x].model#/#session.parking.cars[x].year#</td>
					              <td>#session.parking.cars[x].state#/#session.parking.cars[x].tagNo#</td>
					              <td>#session.parking.cars[x].color#</td></tr>");
			}
			writeOutput("
</table>
<br /><br />
Thank you,<br />
Mayfair at Parkland - Board of Directors<br />");

		}

		mail = new mail(type="html", charset="utf-8", body="#body#");
		mail.setFrom("noreply@mayfairatparkland.com (Mayfair at Parkland Website)");
		mail.setTo("gpeservices@gmail.com");
		mail.setSubject("Mayfair at Parkland - Parking On-Street/Pool Parking Approved");
		mail.setServer(smtpStruct.host);
		mail.setPort(smtpStruct.port);
		mail.setUsername(smtpStruct.user);
		mail.setPassword(smtpStruct.pass);
		mail.setUseTLS(true);

		// Send the email
		mail.send();

		// Return okay
		return serializeJSON("Okay");

	}
	
}
