/*                                                      
    
    CF Name:     events
                                                  
    Description:
	   

      The following is the table layout for the CFC being created:

        Column Name                   Data Type          Data Len   Nullable
        ----------------------------  -----------------  ---------  --------
        eventId                       INT                 10          NO      
        title                         VARCHAR             100         YES     
        startDateTime                 DATETIME            7           NO      
        endDateTime                   DATETIME            7           NO       
        residentId                    INT                 10          YES     
        shortDescription              VARCHAR             2000        YES     
        sendNotice                    INT                 10          NO
        committeeId                   INT                 10          NO
        locationId                    INT                 10          NO
        otherLocation                 VARCHAR             2000        YES


      The following is a list of functions that are included in
      this module:

        getevents
        insertevents
        removeevents
        updateevents


    Program Information:                                
      Called from:  None
      Calls:        None
      Parameters:   None
      Author:       Drew Nathanson
      
    Global Session Variables Used:
    
    Mayfair at Parkland HOA - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved.
                                
*/

Component
{
	// Set global all fields
	allFields = 'eventId, title, startDateTime, endDateTime, residentId, shortDescription, sendNotice, committeeId, locationId, otherLocation';

	/**
    * @output  true
    * @returntype Query
    * @hint This function will return the entire record set from table events.
    */
	public function getEvents ( required numeric eventId = 0, string selectLine = '', string where = '', string orderBy = '', limit = '' )
	{

      /*

         Function Name: getevents

         Description:
           This function will return the entire record set from table events.  
           
         Function Information:
            Input Arguments:  eventId   
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Check value to see if selectLine is set
		if ( !len(selectLine) ) { selectLine = allFields; }

		// Set field
		sqlLine = "Select #selectLine# from events where 0=0 ";

		// Check for eventId
		sqlLine &= ( eventId > 0 ) ? " AND eventId = #eventId#" : "";

		// Check for WHERE
		sqlLine &= ( len(where) > 0 ) ? " AND #PreserveSingleQuotes(where)#" : "";

		// Check for orderBy
		sqlLine &= ( len(orderBy) > 0 ) ? " ORDER BY #PreserveSingleQuotes(orderBy)#" : "";

		// Check for limit
		sqlLine &= ( len(limit) > 0 ) ? " Limit #limit#" : "";

		// Execute the query
		recs = queryExecute("#sqlLine#",{ datasource:application.databaseSelect } );

		// Return
		return recs;

	}

	/**
    * @output  true
    * @returntype Numeric
    * @hint This function will insert the data into events.
    */
	public function insertEvents ( required Struct StrList )
	{

      /*

         Function Name: insertevents

         Description:
           This function will insert the data into events.  
           
         Function Information:
            Input Arguments:  StrList  
            Output Arguments: newId (New Id Number)  

       */

		// Define local variables
		
		var newId        = 0;


		// Set up the try/catch environment
		try
		{

			// Define the stored procedure object
			procObj  = new storedproc();

			// Set the default information
			procObj.setDatasource("#Application.Database#");
			procObj.setProcedure("INSERT_events_PROC");

			procObj.addParam(cfsqltype="CF_SQL_VARCHAR",   type="In", value="#StrList.title#");
			procObj.addParam(cfsqltype="CF_SQL_TIMESTAMP", type="In", value="#StrList.startDateTime#");
			procObj.addParam(cfsqltype="CF_SQL_TIMESTAMP", type="In", value="#StrList.endDateTime#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER",   type="In", value="#StrList.residentId#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR",   type="In", value="#StrList.shortDescription#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER",   type="In", value="#StrList.sendNotice#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER",   type="In", value="#StrList.committeeId#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER",   type="In", value="#StrList.locationId#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR",   type="In", value="#StrList.otherLocation#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER",   type="Out", Variable="newId");

			// Execute the procedure
			resultObj = procObj.execute();
			newId = resultObj.getprocOutVariables().newId;

		} catch ( database excpt ) { 

			// Throw the error to the page
			rethrow;

		}

		// Return
		return newId;

	}


	/**
    * @output  true
    * @returntype Void
    * @hint This function will update the data into events.
    */
	public function updateEvents ( required Struct StrList )
	{

      /*

         Function Name: updateevents

         Description:
           This function will update the data into events.  
           
         Function Information:
            Input Arguments:  StrList  
            Output Arguments: None  

       */

		// Set up the try/catch environment
		try
		{

			// Define the stored procedure object
			procObj  = new storedproc();

			// Set the default information
			procObj.setDatasource("#Application.Database#");
			procObj.setProcedure("UPDATE_events_PROC");

			procObj.addParam(cfsqltype="CF_SQL_INTEGER",    type="In", value="#StrList.eventId#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR",    type="In", value="#StrList.title#");
			procObj.addParam(cfsqltype="CF_SQL_TIMESTAMP",  type="In", value="#StrList.startDateTime#");
			procObj.addParam(cfsqltype="CF_SQL_TIMESTAMP",  type="In", value="#StrList.endDateTime#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER",    type="In", value="#StrList.residentId#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR",    type="In", value="#StrList.shortDescription#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER",    type="In", value="#StrList.sendNotice#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER",    type="In", value="#StrList.committeeId#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER",   type="In", value="#StrList.locationId#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR",   type="In", value="#StrList.otherLocation#");

			// Execute the procedure
			procObj.execute();

		} catch ( database excpt ) { 

			// Throw the error to the page
			rethrow;

		}


		// Return
		return ;

	}

	/**
    * @output  true
    * @returntype Boolean
    * @hint This function will remove a record from the table events.
    */
	public function removeEvents ( String eventid = 0, String where = "" )
	{

      /*

         Function Name: removeevents

         Description:
           This function will remove a record from the table events.  
           
         Function Information:
            Input Arguments:    
            Output Arguments: True or False  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Set the sqlLine field
		sqlLine  = "Delete from events where 0=0 ";
		sqlLine &= ( structKeyExists(arguments, 'eventid') && eventid > 0 ) ? "and eventId = #eventid# " : "";
		// Check for where
		sqlLine &= ( len(where) > 0 ) ? "AND #PreserveSingleQuotes(where)#" : "";

		// Execute the query
		queryExecute("#sqlLine#", { datasource:Application.DatabaseDelete} );


		// Return
		return ;

	}

	/**************************************************************************
	 * The next section is used as an object for the Getter object functions  *
	 **************************************************************************/

	/**
    * @output  true
    * @returntype query
    * @hint 
    */
	public function init ( numeric indexNo = -1, string Where = "" )
	{

      /*

         Function Name: init

         Description:
             
           
         Function Information:
            Input Arguments:  indexNo (primary key)  
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";

		// Retrieve the record
		sqlLine = "Select eventId, title, startDateTime, endDateTime, residentId, shortDescription, sendNotice, committeeId, locationId, otherLocation from events where 0 = 0";

 		// Check the key
		sqlLine &= ( indexNo > -1 ) ? " AND eventId = #indexNo#" : "";

 		// Check for where
		sqlLine &= ( len(where) > 0 ) ? "AND #PreserveSingleQuotes(where)# " : "";

		//Execute the query
		recordSet = queryExecute("#sqlLine#", { datasource:Application.DatabaseSelect } );


		// Return
		return recordset;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint Return the number of record in the query
    */
	public function GetRecordCount ( Void )
	{

      /*

         Function Name: GetRecordCount

         Description:
           Return the number of record in the query  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: recordcount  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.recordcount;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field eventId
    */
	public function getEventId ( Void )
	{

      /*

         Function Name: geteventId

         Description:
           This function will return the field eventId  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.eventId;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field title
    */
	public function getTitle ( Void )
	{

      /*

         Function Name: gettitle

         Description:
           This function will return the field title  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.title;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field startDateTime
    */
	public function getStartDateTime ( Void )
	{

      /*

         Function Name: getstartDateTime

         Description:
           This function will return the field startDateTime
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return dateFormat(recordSet.startDateTime, "mm/dd/yyyy") & "@" & timeFormat(recordSet.startDateTime, "HH:mm:ss");

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field endDate
    */
	public function getEndDateTime ( Void )
	{

      /*

         Function Name: getendDate

         Description:
           This function will return the field endDateTime  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return dateFormat(recordSet.endDateTime, "mm/dd/yyyy") & "@" & timeFormat(recordSet.endDateTime, "HH:mm:ss");

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field residentId
    */
	public function getResidentId ( Void )
	{

      /*

         Function Name: getresidentId

         Description:
           This function will return the field residentId  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.residentId;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field shortDescription
    */
	public function getShortDescription ( Void )
	{

      /*

         Function Name: getshortDescription

         Description:
           This function will return the field shortDescription  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.shortDescription;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field sendNotice
    */
	public function getSendNotice ( Void )
	{

      /*

         Function Name: getSendNotice

         Description:
           This function will return the field sendNotice  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.sendNotice;

	}

}
