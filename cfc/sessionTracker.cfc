/*************************************************************
 *
 *   CF Name:    sessionTracker.cfc
 *
 *   Description:
 *      This module will handle the session tracking for the 
 *      web site.
 *
 *      Best viewed with Tabs = 3
 *
 *    Program Information:
 *      Called from:  
 *      Calls:         
 *      Parameters:   
 *      Author:       Drew Nathanson
 *
 *    Global Session Variables Used:
 *
 *    Technical Synergy, Inc.
 *    Copyright (c) 1999-2020. All Rights Reserved.   
 *
 *************************************************************/

 component
 {

 	// Create the session object
 	property sessTrack;

 	// Get the application name
 	property appName;

 	/**
 	 * @output true
 	 * @returnvariable any
 	 * @hint initializes the session tracking
 	 */
 	public function init ( void )
 	{
 	
 	   /*
 	    * Function Name: init
 	    *
 	    * Description:
 	    *   This function will initialize the sessTrack object. 
 	    *
 	    * Function Information:
 	    *   Input Arguments:
 	    *   Output Argument:
 	    *
 	    */
 	
 		// Create the object
 		sessTrack = createObject("java", "coldfusion.runtime.SessionTracker");
 	
 		// Set appName
 		appName = application.appName;

 		// Return
 		return this;

 	}
 	
 	/**
 	 * @output true
 	 * @returnvariable numeric
 	 * @hint this function will return the number of active sessions.
 	 */
 	public function getSessionCount ( void )
 	{
 	
 	   /*
 	    * Function Name: getSessionCount
 	    *
 	    * Description:
 	    *   This function will return the number of active session for the 
 	    *   application (appName).
 	    *
 	    * Function Information:
 	    *   Input Arguments: none
 	    *   Output Argument: numeric
 	    *
 	    */
 	
 		// return the value
 		return sessTrack.getActiveSessionCount(appName);

 	}
 	
 	/**
 	 * @output true
 	 * @returnvariable array
 	 * @hint this function will return an array of structure keys.
 	 */
 	public function getSessionCollection ( void )
 	{
 	
 	   /*
 	    * Function Name: getSessionCollection
 	    *
 	    * Description:
 	    *   This function will return an array of structure keys.
 	    *
 	    * Function Information:
 	    *   Input Arguments: none
 	    *   Output Argument: struct
 	    *
 	    */
 	
 		return structKeyArray(sessTrack.getSessionCollection(appName));

 	}
 	
 	/**
 	 * @output true
 	 * @returnvariable struct
 	 * @hint this function will return the detail session information
 	 */
 	public function getSessionDetails ( required string sessionId )
 	{
 	
 	   /*
 	    * Function Name: getSessionDetails
 	    *
 	    * Description:
 	    *   This function will return the detail information about the session.
 	    *
 	    * Function Information:
 	    *   Input Arguments:  sessionId
 	    *   Output Argument:  struct
 	    *
 	    */
 	
 		// Define local variables
 		var info         = "";
 		var ipAddr       = "";
 		local.sessInfo   = { };

 		// Remove the appName_ from the id
 		local.id         = replace(sessionId, appName&"_", "");
 		
 		// Retrieve the session information
 		info 	= sessTrack.getSession(appName, local.id);
 		
 		// Check for ipAddress
 		ipAddr = ( !structKeyExists(info, "ipAddress") ) ? cgi.remote_addr : info.ipAddress;

 		// Check to see if userInfo exists
 		if ( !structKeyExists(info, "userInfo") )
 		{

	 		// Build the return structure
	 		local.sessInfo  	= {
	 			email     : "None",
	 			name 		 : "None",
	 			street    : "None",
	 			ipAddress : ipAddr
	 		};

	 	} else {

	 		// Build the return structure
	 		local.sessInfo 	= {
	 			email     : info.userInfo.emailAddress,
	 			name 		 : info.userInfo.name,
	 			street    : info.userInfo.street,
	 			ipAddress : ipAddr
	 		};

	 	}

	 	// Return structure
	 	return local.sessInfo;

 	}
 	
 	/**
 	 * @output true
 	 * @returnvariable array
 	 * @hint this function will return an array of structures
 	 */
 	public function getAllSessionInfo ( void )
 	{
 	
 	   /*
 	    * Function Name: getAllSessionInfo
 	    *
 	    * Description:
 	    *   This function will gather all the session and then create the 
 	    *   array of structures.
 	    *
 	    * Function Information:
 	    *   Input Arguments: none
 	    *   Output Argument: array
 	    *
 	    */
 	
 		// Define variables
 		var arr   		= [ ];
 		var str        = { };

 		// Create an object
 		obj = createObject("component", "#application.cfcPath#sessionTracker");
 		obj.init();

 		// Get the array
 		local.array = obj.getSessionCollection();

 		// Process the array
 		for ( var item in local.array )
 		{

 			// Get the detail
 			str = obj.getSessionDetails( item );

 			// Append to the array
 			arrayAppend(arr, str);

 		}

 		// Return arr
 		return arr;

 	}
 	
 }