/*                                                      
    
    CF Name:     parkingRequest
                                                  
    Description:
	   

      The following is the table layout for the CFC being created:

        Column Name                   Data Type          Data Len   Nullable
        ----------------------------  -----------------  ---------  --------
        parkingReqId                  INT                 10          NO      
        residentId                    INT                 10          NO      
        reason                        VARCHAR             2000        YES     
        parkDenyId                    INT                 10          NO
        phone                         VARCHAR             30          YES     
        startDate                     DATE                7           NO      
        endDate                       DATE                7           NO      
        createDate                    DATETIME            7           NO      
        approved                      INT                 10          YES     
        approvedDate                  DATETIME            7           YES     
        completed                     INT                 10          YES     


      The following is a list of functions that are included in
      this module:

        getParkingRequest
        insertParkingRequest
        removeParkingRequest
        updateParkingRequest


    Program Information:                                
      Called from:  None
      Calls:        None
      Parameters:   None
      Author:       Drew Nathanson
      
    Global Session Variables Used:
    
    Pets-Org
    Copyright (c) 1999-2025. All Rights Reserved.
                                
*/

Component
{
	// Set global all fields
	allFields = 'parkingReqId, residentId, reason, parkDenyId, phone, startDate, endDate, createDate, approved, approvedDate, completed';

	/**
    * @output  true
    * @returntype Query
    * @hint This function will return the entire record set from table parkingRequest.
    */
	public function getParkingRequest ( required numeric parkingReqId = 0, string selectLine = '', string where = '', string orderBy = '', limit = '' )
	{

      /*

         Function Name: getparkingRequest

         Description:
           This function will return the entire record set from table parkingRequest.  
           
         Function Information:
            Input Arguments:  parkingReqId   
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Check value to see if selectLine is set
		if ( !len(selectLine) ) { selectLine = allFields; }

		// Set field
		sqlLine = "Select #selectLine# from parkingRequest where 0=0 ";

		// Check for parkingReqId
		sqlLine &= ( parkingReqId > 0 ) ? " AND parkingReqId = #parkingReqId#" : "";

		// Check for WHERE
		sqlLine &= ( len(where) > 0 ) ? " AND #PreserveSingleQuotes(where)#" : "";

		// Check for orderBy
		sqlLine &= ( len(orderBy) > 0 ) ? " ORDER BY #PreserveSingleQuotes(orderBy)#" : "";

		// Check for limit
		sqlLine &= ( len(limit) > 0 ) ? " Limit #limit#" : "";

		// Execute the query
		recs = queryExecute("#sqlLine#",{ datasource:application.databaseSelect } );

		// Return
		return recs;

	}

	/**
    * @output  true
    * @returntype Numeric
    * @hint This function will insert the data into parkingRequest.
    */
	public function insertParkingRequest ( required Struct StrList )
	{

      /*

         Function Name: insertparkingRequest

         Description:
           This function will insert the data into parkingRequest.  
           
         Function Information:
            Input Arguments:  StrList  
            Output Arguments: newId (New Id Number)  

       */

		// Define local variables
		
		var newId        = 0;


		// Set up the try/catch environment
		try
		{

			// Define the stored procedure object
			procObj  = new storedproc();

			// Set the default information
			procObj.setDatasource("#Application.Database#");
			procObj.setProcedure("INSERT_parkingRequest_PROC");

			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.residentId#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.reason#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.phone#");
			procObj.addParam(cfsqltype="CF_SQL_DATE",    type="In", value="#StrList.startDate#");
			procObj.addParam(cfsqltype="CF_SQL_DATE",    type="In", value="#StrList.endDate#");
			procObj.addParam(cfsqltype="CF_SQL_DATE",    type="In", value="#StrList.createDate#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.approved#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.completed#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="Out", Variable="newId");

			// Execute the procedure
			resultObj = procObj.execute();
			newId     = resultObj.getprocOutVariables().newId;

		} catch ( database excpt ) { 

			// Throw the error to the page
			rethrow;

		}

		// Return
		return newId;

	}


	/**
    * @output  true
    * @returntype Void
    * @hint This function will update the data into parkingRequest.
    */
	public function updateParkingRequest ( required Struct StrList )
	{

      /*

         Function Name: updateparkingRequest

         Description:
           This function will update the data into parkingRequest.  
           
         Function Information:
            Input Arguments:  StrList  
            Output Arguments: None  

       */

						

		// Set up the try/catch environment
		try
		{

			// Define the stored procedure object
			procObj  = new storedproc();

			// Set the default information
			procObj.setDatasource("#Application.Database#");
			procObj.setProcedure("UPDATE_parkingRequest_PROC");

			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.parkingReqId#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.residentId#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.reason#");
      procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.parkDenyId#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.phone#");
			procObj.addParam(cfsqltype="CF_SQL_DATE",    type="In", value="#StrList.startDate#");
			procObj.addParam(cfsqltype="CF_SQL_DATE",    type="In", value="#StrList.endDate#");
			procObj.addParam(cfsqltype="CF_SQL_DATE",    type="In", value="#StrList.createDate#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.approved#");
			procObj.addParam(cfsqltype="CF_SQL_DATE",    type="In", value="#StrList.approvedDate#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.completed#");

			// Execute the procedure
			procObj.execute();

		} catch ( database excpt ) { 

			// Throw the error to the page
			rethrow;

		}


		// Return
		return ;

	}

	/**
    * @output  true
    * @returntype Boolean
    * @hint This function will remove a record from the table parkingRequest.
    */
	public function removeParkingRequest ( String parkingreqid = 0, String where = "" )
	{

      /*

         Function Name: removeparkingRequest

         Description:
           This function will remove a record from the table parkingRequest.  
           
         Function Information:
            Input Arguments:    
            Output Arguments: True or False  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Set the sqlLine field
		sqlLine  = "Delete from parkingRequest where 0=0 ";
		sqlLine &= ( structKeyExists(arguments, 'parkingreqid') && parkingreqid > 0 ) ? "and parkingReqId = #parkingreqid# " : "";
		// Check for where
		sqlLine &= ( len(where) > 0 ) ? "AND #PreserveSingleQuotes(where)#" : "";

		// Execute the query
		queryExecute("#sqlLine#", { datasource:Application.DatabaseDelete} );


		// Return
		return ;

	}

	/**************************************************************************
	 * The next section is used as an object for the Getter object functions  *
	 **************************************************************************/

	/**
    * @output  true
    * @returntype query
    * @hint 
    */
	public function init ( numeric indexNo = -1, string Where = "" )
	{

      /*

         Function Name: init

         Description:
             
           
         Function Information:
            Input Arguments:  indexNo (primary key)  
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";

		// Retrieve the record
		sqlLine = "Select parkingReqId, residentId, reason, parkDenyId, phone, startDate, endDate, createDate, approved, approvedDate, completed from parkingRequest where 0 = 0";

 		// Check the key
		sqlLine &= ( indexNo > -1 ) ? " AND parkingReqId = #indexNo#" : "";

 		// Check for where
		sqlLine &= ( len(where) > 0 ) ? "AND #PreserveSingleQuotes(where)# " : "";

		//Execute the query
		recordSet = queryExecute("#sqlLine#", { datasource:Application.DatabaseSelect } );


		// Return
		return recordset;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint Return the number of record in the query
    */
	public function GetRecordCount ( Void )
	{

      /*

         Function Name: GetRecordCount

         Description:
           Return the number of record in the query  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: recordcount  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.recordcount;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field parkingReqId
    */
	public function getparkingReqId ( Void )
	{

      /*

         Function Name: getparkingReqId

         Description:
           This function will return the field parkingReqId  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.parkingReqId;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field residentId
    */
	public function getresidentId ( Void )
	{

      /*

         Function Name: getresidentId

         Description:
           This function will return the field residentId  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.residentId;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field reason
    */
	public function getreason ( Void )
	{

      /*

         Function Name: getreason

         Description:
           This function will return the field reason  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.reason;

	}

  /**
    * @output  true
    * @returntype string
    * @hint This function will return the field reason
    */
  public function getparkdenyid ( Void )
  {

      /*

         Function Name: getparkdenyid

         Description:
           This function will return the field parkDenyId  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

    // Define local variables
    var sqlLine      = "";

    // Return
    return recordSet.getparkdenyid;

  }

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field phone
    */
	public function getphone ( Void )
	{

      /*

         Function Name: getphone

         Description:
           This function will return the field phone  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.phone;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field startDate
    */
	public function getstartDate ( Void )
	{

      /*

         Function Name: getstartDate

         Description:
           This function will return the field startDate  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return DateFormat(recordSet.startDate, "mm/dd/yyyy");

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field endDate
    */
	public function getendDate ( Void )
	{

      /*

         Function Name: getendDate

         Description:
           This function will return the field endDate  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return DateFormat(recordSet.endDate, "mm/dd/yyyy");

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field createDate
    */
	public function getcreateDate ( Void )
	{

      /*

         Function Name: getcreateDate

         Description:
           This function will return the field createDate  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return DateFormat(recordSet.createDate, "mm/dd/yyyy");

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field approved
    */
	public function getapproved ( Void )
	{

      /*

         Function Name: getapproved

         Description:
           This function will return the field approved  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.approved;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field approvedDate
    */
	public function getapprovedDate ( Void )
	{

      /*

         Function Name: getapprovedDate

         Description:
           This function will return the field approvedDate  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return DateFormat(recordSet.approvedDate, "mm/dd/yyyy");

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field completed
    */
	public function getcompleted ( Void )
	{

      /*

         Function Name: getcompleted

         Description:
           This function will return the field completed  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.completed;

	}


}
