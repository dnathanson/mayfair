/*******************************************************
 *
 *   CF Name:    smtp.cfc
 *
 *   Description:
 *      This function will provide the stmp information
 *      for distributing emails.
 *
 *      Best viewed with Tabs = 3
 *
 *    Program Information:
 *      Called from:  
 *      Calls:         
 *      Parameters:   
 *      Author:       Drew Nathanson
 *
 *    Global Session Variables Used:
 *
 *    Mayfair at Parkland - Technical Synergy, Inc.
 *    Copyright (c) 1999-2018. All Rights Reserved.   
 *
 ********************************************************/

 Component
 {

 	/**
 	 * @output true
 	 * @returnvariable 
 	 * @hint 
 	 */
 	public function getSMTPinfo ( void )
 	{
 	
 	   /*
 	    * Function Name: getSMTPinfo
 	    *
 	    * Description:
 	    *    
 	    *
 	    * Function Information:
 	    *   Input Arguments:
 	    *   Output Argument:
 	    *
 	    */
 	
 		var smtp   = {
 			host  : "smtp.sparkpostmail.com",
 			port  : "587",
 			auth  : "AUTH LOGIN",
 			enc   : "STARTTLS",
 			user  : "SMTP_Injection",
 			pass  : "fa2d103d4425921b309c1ff7b8ab3cb40400573e"
 		};

 		// Return smtp structure
 		return smtp;

 	}
 	
 	/**
 	 * @output true
 	 * @returnvariable void
 	 * @hint this function will send an email.
 	 */
 	public function sendEMail ( required string emailAddr, required string body, required string subject )
 	{
 	
 	   /*
 	    * Function Name: sendMail
 	    *
 	    * Description:
 	    *   This funciton will send an email.
 	    *
 	    * Function Information:
 	    *   Input Arguments: emailAddr, body, subject
 	    *   Output Argument: none
 	    *
 	    */
 	
 		// Create the object
 		smtpObj = createObject("component", "#application.cfcPath#smtp");

 		// Retrieve the smtp specifics
 		smtpStruct = smtpObj.getSMTPInfo();

 		// Create a mail object
		mail = new mail();
		mail.setFrom("noreply@mayfairatparkland.com");
		mail.setTo("#emailAddr#");
		mail.setSubject("#subject#");
		mail.setServer(smtpStruct.host);
		mail.setPort(smtpStruct.port);
		mail.setUsername(smtpStruct.user);
		mail.setPassword(smtpStruct.pass);
		mail.setUseTLS(true);

		// Add the body of the message
		mail.addPart(type="html", charset="utf-8", body="#body#");

		// Send the email
		mail.send();

 	}
 	
 }