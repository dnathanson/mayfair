/*                                                      
    
    CF Name:     Permissions
                                                  
    Description:
	   

      The following is the table layout for the CFC being created:

        Column Name                   Data Type          Data Len   Nullable
        ----------------------------  -----------------  ---------  --------
        permissionId                  INT                 10          NO      
        residentId                    INT                 10          NO      
        admin                         INT                 10          NO     
        management                    INT                 10          NO     
        blog                          INT                 10          NO    
        committee                     INT                 10          NO 


      The following is a list of functions that are included in
      this module:

        getPermissions
        insertPermissions
        removePermissions
        updatePermissions


    Program Information:                                
      Called from:  None
      Calls:        None
      Parameters:   None
      Author:       Drew Nathanson
      
    Global Session Variables Used:
    
    Mayfair at Parkland HOA - Techncial Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved.
                                
*/

Component
{
	// Set global all fields
	allFields = 'permissionId, residentId, admin, management, blog, committee';

	/**
    * @output  true
    * @returntype Query
    * @hint This function will return the entire record set from table PERMISSIONS.
    */
	public function getPermissions ( required numeric permissionId = 0, string selectLine = '', string where = '', string orderBy = '' )
	{

      /*

         Function Name: getPermissions

         Description:
           This function will return the entire record set from table PERMISSIONS.  
           
         Function Information:
            Input Arguments:  permissionId   
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Check value to see if selectLine is set
		if ( !len(selectLine) ) { selectLine = allFields; }

		// Set field
		sqlLine = "Select #selectLine# from permissions where 0=0 ";

		// Check for permissionId
		sqlLine &= ( permissionId > 0 ) ? " AND permissionId = #permissionId#" : "";

		// Check for WHERE
		sqlLine &= ( len(where) > 0 ) ? " AND #PreserveSingleQuotes(where)#" : "";

		// Check for orderBy
		sqlLine &= ( len(orderBy) > 0 ) ? " ORDER BY #PreserveSingleQuotes(orderBy)#" : "";

		// Execute the query
		recs = queryExecute("#sqlLine#",{ datasource:application.databaseSelect } );

		// Return
		return recs;

	}

	/**
    * @output  true
    * @returntype Numeric
    * @hint This function will insert the data into PERMISSIONS.
    */
	public function insertPermissions ( required Struct StrList )
	{

      /*

         Function Name: insertPermissions

         Description:
           This function will insert the data into PERMISSIONS.  
           
         Function Information:
            Input Arguments:  StrList  
            Output Arguments: newId (New Id Number)  

       */

		// Define local variables
		
		var newId        = 0;


		// Set up the try/catch environment
		try
		{

			// Define the stored procedure object
			procObj  = new storedproc();

			// Set the default information
			procObj.setDatasource("#Application.Database#");
			procObj.setProcedure("INSERT_PERMISSIONS_PROC");

			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.residentId#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.admin#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.management#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.blog#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.committee#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="Out", Variable="newId");

			// Execute the procedure
			resultObj = procObj.execute();
			newId = resultObj.getprocOutVariables().newId;

		} catch ( database excpt ) { 

			// Throw the error to the page
			rethrow;

		}

		// Return
		return newId;

	}


	/**
    * @output  true
    * @returntype Void
    * @hint This function will update the data into PERMISSIONS.
    */
	public function updatePermissions ( required Struct StrList )
	{

      /*

         Function Name: updatePermissions

         Description:
           This function will update the data into PERMISSIONS.  
           
         Function Information:
            Input Arguments:  StrList  
            Output Arguments: None  

       */

						

		// Set up the try/catch environment
		try
		{

			// Define the stored procedure object
			procObj  = new storedproc();

			// Set the default information
			procObj.setDatasource("#Application.Database#");
			procObj.setProcedure("UPDATE_PERMISSIONS_PROC");

			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.permissionId#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.residentId#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.admin#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.management#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.blog#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.committee#");

			// Execute the procedure
			procObj.execute();

		} catch ( database excpt ) { 

			// Throw the error to the page
			rethrow;

		}


		// Return
		return ;

	}

	/**
    * @output  true
    * @returntype Boolean
    * @hint This function will remove a record from the table PERMISSIONS.
    */
	public function removePermissions ( String permissionid = 0, String where = "" )
	{

      /*

         Function Name: removePermissions

         Description:
           This function will remove a record from the table PERMISSIONS.  
           
         Function Information:
            Input Arguments:    
            Output Arguments: True or False  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Set the sqlLine field
		sqlLine  = "Delete from permissions where 0=0 ";
		sqlLine &= ( structKeyExists(arguments, 'permissionid') && permissionid > 0 ) ? "and permissionId = #permissionid# " : "";
		// Check for where
		sqlLine &= ( len(where) > 0 ) ? "AND #PreserveSingleQuotes(where)#" : "";

		// Execute the query
		queryExecute("#sqlLine#", { datasource:Application.DatabaseDelete} );


		// Return
		return ;

	}

	/**************************************************************************
	 * The next section is used as an object for the Getter object functions  *
	 **************************************************************************/

	/**
    * @output  true
    * @returntype query
    * @hint 
    */
	public function init ( numeric indexNo = -1, string Where = "" )
	{

      /*

         Function Name: init

         Description:
             
           
         Function Information:
            Input Arguments:  indexNo (primary key)  
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";

		// Retrieve the record
		sqlLine = "Select permissionId, residentId, admin, management, blog, committee from permissions where 0 = 0";

 		// Check the key
		sqlLine &= ( indexNo > -1 ) ? " AND permissionId = #indexNo#" : "";

 		// Check for where
		sqlLine &= ( len(where) > 0 ) ? "AND #PreserveSingleQuotes(where)# " : "";

		//Execute the query
		recordSet = queryExecute("#sqlLine#", { datasource:Application.DatabaseSelect } );


		// Return
		return recordset;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint Return the number of record in the query
    */
	public function GetRecordCount ( Void )
	{

      /*

         Function Name: GetRecordCount

         Description:
           Return the number of record in the query  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: recordcount  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.recordcount;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field permissionId
    */
	public function getPermissionid ( Void )
	{

      /*

         Function Name: getPermissionid

         Description:
           This function will return the field permissionId  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.permissionId;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field residentId
    */
	public function getResidentid ( Void )
	{

      /*

         Function Name: getResidentid

         Description:
           This function will return the field residentId  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.residentId;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field admin
    */
	public function getAdmin ( Void )
	{

      /*

         Function Name: getAdmin

         Description:
           This function will return the field admin  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.admin;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field management
    */
	public function getManagement ( Void )
	{

      /*

         Function Name: getManagement

         Description:
           This function will return the field management  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.management;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field blog
    */
	public function getBlog ( Void )
	{

      /*

         Function Name: getBlog

         Description:
           This function will return the field blog  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.blog;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field blog
    */
	public function getCommittee ( Void )
	{

      /*

         Function Name: getcommittee

         Description:
           This function will return the field committee  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.committee;

	}


}
