/*                                                      
    
    CF Name:     Committees
                                                  
    Description:
	   

      The following is the table layout for the CFC being created:

        Column Name                   Data Type          Data Len   Nullable
        ----------------------------  -----------------  ---------  --------
        committeeId                   INT                 10          NO      
        committeeName                 VARCHAR             150         YES     
        shortDesc                     VARCHAR             2000        YES     
        longDesc                      LONGTEXT            --          YES
        active                        INT                 10          YES     


      The following is a list of functions that are included in
      this module:

        getCommittees
        insertCommittees
        removeCommittees
        updateCommittees


    Program Information:                                
      Called from:  None
      Calls:        None
      Parameters:   None
      Author:       Drew Nathanson
      
    Global Session Variables Used:
    
    Mayfair at Parkland HOA - Techncial Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved.
                                
*/

Component
{
	// Set global all fields
	allFields = 'committeeId, committeeName, shortDesc, longDesc, active';

	/**
    * @output  true
    * @returntype Query
    * @hint This function will return the entire record set from table COMMITTEES.
    */
	public function getCommittees ( required numeric committeeId = 0, string selectLine = '', string where = '', string orderBy = '' )
	{

      /*

         Function Name: getCommittees

         Description:
           This function will return the entire record set from table COMMITTEES.  
           
         Function Information:
            Input Arguments:  committeeId   
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Check value to see if selectLine is set
		if ( !len(selectLine) ) { selectLine = allFields; }

		// Set field
		sqlLine = "Select #selectLine# from committees where 0=0 ";

		// Check for committeeId
		sqlLine &= ( committeeId > 0 ) ? " AND committeeId = #committeeId#" : "";

		// Check for WHERE
		sqlLine &= ( len(where) > 0 ) ? " AND #PreserveSingleQuotes(where)#" : "";

		// Check for orderBy
		sqlLine &= ( len(orderBy) > 0 ) ? " ORDER BY #PreserveSingleQuotes(orderBy)#" : "";

		// Execute the query
		recs = queryExecute("#sqlLine#",{ datasource:application.databaseSelect } );

		// Return
		return recs;

	}

	/**
    * @output  true
    * @returntype Numeric
    * @hint This function will insert the data into COMMITTEES.
    */
	public function insertCommittees ( required Struct StrList )
	{

      /*

         Function Name: insertCommittees

         Description:
           This function will insert the data into COMMITTEES.  
           
         Function Information:
            Input Arguments:  StrList  
            Output Arguments: newId (New Id Number)  

       */

		// Define local variables
		
		var newId        = 0;


		// Set up the try/catch environment
		try
		{

			// Define the stored procedure object
			procObj  = new storedproc();

			// Set the default information
			procObj.setDatasource("#Application.Database#");
			procObj.setProcedure("INSERT_COMMITTEES_PROC");

			procObj.addParam(cfsqltype="CF_SQL_VARCHAR",     type="In", value="#StrList.committeeName#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR",     type="In", value="#StrList.shortDesc#");
			procObj.addParam(cfsqltype="CF_SQL_LONGVARCHAR", type="In", value="#StrList.longDesc#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER",     type="In", value="#StrList.active#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER",     type="Out", Variable="newId");

			// Execute the procedure
			resultObj = procObj.execute();
			newId = resultObj.getprocOutVariables().newId;

		} catch ( database excpt ) { 

			// Throw the error to the page
			rethrow;

		}

		// Return
		return newId;

	}


	/**
    * @output  true
    * @returntype Void
    * @hint This function will update the data into COMMITTEES.
    */
	public function updateCommittees ( required Struct StrList )
	{

      /*

         Function Name: updateCommittees

         Description:
           This function will update the data into COMMITTEES.  
           
         Function Information:
            Input Arguments:  StrList  
            Output Arguments: None  

       */

						

		// Set up the try/catch environment
		try
		{

			// Define the stored procedure object
			procObj  = new storedproc();

			// Set the default information
			procObj.setDatasource("#Application.Database#");
			procObj.setProcedure("UPDATE_COMMITTEES_PROC");

			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.committeeId#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.committeeName#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.shortDesc#");
			procObj.addParam(cfsqltype="CF_SQL_LONGVARCHAR", type="In", value="#StrList.longDesc#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.active#");

			// Execute the procedure
			procObj.execute();

		} catch ( database excpt ) { 

			// Throw the error to the page
			rethrow;

		}


		// Return
		return ;

	}

	/**
    * @output  true
    * @returntype Boolean
    * @hint This function will remove a record from the table COMMITTEES.
    */
	public function removeCommittees ( String committeeid = 0, String where = "" )
	{

      /*

         Function Name: removeCommittees

         Description:
           This function will remove a record from the table COMMITTEES.  
           
         Function Information:
            Input Arguments:    
            Output Arguments: True or False  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Set the sqlLine field
		sqlLine  = "Delete from committees where 0=0 ";
		sqlLine &= ( structKeyExists(arguments, 'committeeid') && committeeid > 0 ) ? "and committeeId = #committeeid# " : "";
		// Check for where
		sqlLine &= ( len(where) > 0 ) ? "AND #PreserveSingleQuotes(where)#" : "";

		// Execute the query
		queryExecute("#sqlLine#", { datasource:Application.DatabaseDelete} );


		// Return
		return ;

	}

	/**************************************************************************
	 * The next section is used as an object for the Getter object functions  *
	 **************************************************************************/

	/**
    * @output  true
    * @returntype query
    * @hint 
    */
	public function init ( numeric indexNo = -1, string Where = "" )
	{

      /*

         Function Name: init

         Description:
             
           
         Function Information:
            Input Arguments:  indexNo (primary key)  
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";

		// Retrieve the record
		sqlLine = "Select committeeId, committeeName, shortDesc, longDesc, active from committees where 0 = 0";

 		// Check the key
		sqlLine &= ( indexNo > -1 ) ? " AND committeeId = #indexNo#" : "";

 		// Check for where
		sqlLine &= ( len(where) > 0 ) ? "AND #PreserveSingleQuotes(where)# " : "";

		//Execute the query
		recordSet = queryExecute("#sqlLine#", { datasource:Application.DatabaseSelect } );


		// Return
		return recordset;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint Return the number of record in the query
    */
	public function GetRecordCount ( Void )
	{

      /*

         Function Name: GetRecordCount

         Description:
           Return the number of record in the query  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: recordcount  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.recordcount;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field committeeId
    */
	public function getCommitteeid ( Void )
	{

      /*

         Function Name: getCommitteeid

         Description:
           This function will return the field committeeId  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.committeeId;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field committeeName
    */
	public function getCommitteeName ( Void )
	{

      /*

         Function Name: getCommitteename

         Description:
           This function will return the field committeeName  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.committeeName;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field shortDesc
    */
	public function getShortDesc ( Void )
	{

      /*

         Function Name: getshortDesc

         Description:
           This function will return the field shortDesc  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.shortDesc;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field longDesc
    */
	public function getLongDesc ( Void )
	{

      /*

         Function Name: getLongDesc

         Description:
           This function will return the field longDesc  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.longDesc;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field active
    */
	public function getActive ( Void )
	{

      /*

         Function Name: getActive

         Description:
           This function will return the field active  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.active;

	}


}
