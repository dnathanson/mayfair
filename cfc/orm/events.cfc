/**
 * @table      events
 * @entityname events
 * @output     false
 * @persistent true
 * @cacheUse   transactional
 */
Component
{
   property name="eventId"          ormType="integer" fieldType="ID"  generator="native" ;
   property name="title"            ormType="string"  length="100";
   property name="startDate"        ormType="date"    length="7";
   property name="startTime"        ormType="string"  length="10";
   property name="endDate"          ormType="date"    length="7";
   property name="endTime"          ormType="string"  length="10";
   property name="residentId"       ormType="integer";
   property name="shortDescription" ormType="string"  length="2000";

}

