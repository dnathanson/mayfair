/**
 * @table      parkingReqCars
 * @entityname Parkingreqcars
 * @output     false
 * @persistent true
 * @cacheUse   transactional
 */
Component
{
   property name="parkingCarId" ormType="integer" fieldType="ID"  generator="native" ;
   property name="parkingReqId" ormType="integer";
   property name="make" ormType="string" length="100";
   property name="model" ormType="string" length="100";
   property name="tagNo" ormType="string" length="10";
   property name="state" ormType="string" length="100";
   property name="year" ormType="string" length="10";
   property name="color" ormType="string" length="100";

}

