/**
 * @table      bod
 * @entityname bod
 * @output     false
 * @persistent true
 * @cacheUse   transactional
 */
Component
{
   property name="bodId"      ormType="integer" fieldType="ID"  generator="native" ;
   property name="residentId" ormType="integer";
   property name="name"       ormType="string" length="100";

}

