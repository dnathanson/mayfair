/**
 * @table      committees
 * @entityname committees
 * @output     false
 * @persistent true
 * @cacheUse   transactional
 */
Component
{
   property name="committeeId"   ormType="integer" fieldType="ID"  generator="native" ;
   property name="committeeName" ormType="string"  length="150";
   property name="shortDesc"     ormType="string"  length="2000";
   property name="longDesc"      ormType="text";
   property name="active"        ormType="integer";

}

