/**
 * @table      wedgeBlog
 * @entityname wedgeblog
 * @output     false
 * @persistent true
 * @cacheUse   transactional
 */
Component
{
   property name="wedgeBlogId" ormType="integer" fieldType="ID"  generator="native" ;
   property name="residentId"  ormType="integer";
   property name="blogDate"    ormType="date"    length="7";
   property name="subject"     ormType="string"  length="100";
   property name="blog"        ormType="integer";
   property name="active"      ormType="integer";

}

