/**
 * @table      committeeMembers
 * @entityname committeeMembers
 * @output     false
 * @persistent true
 * @cacheUse   transactional
 */
Component
{
   property name="commitMemberId" ormType="integer" fieldType="ID"  generator="native" ;
   property name="committeeId"    ormType="integer";
   property name="residentId"     ormType="integer";
   property name="signupDate"     ormType="date"    length="7";
   property name="chairman"       ormType="integer";

}

