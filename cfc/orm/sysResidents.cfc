/**
 * @table      sysResidents
 * @entityname sysResidents
 * @output     false
 * @persistent true
 * @cacheUse   transactional
 */
component
{
   property name="residentId"    ormType="integer" fieldType="ID";
   property name="keyId"         ormType="string" length="50";
}

