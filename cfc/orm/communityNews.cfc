/**
 * @table      communityNews
 * @entityname Communitynews
 * @output     false
 * @persistent true
 * @cacheUse   transactional
 */
Component
{
   property name="commId"      ormType="integer" fieldType="ID"  generator="native" ;
   property name="articleDate" ormType="date"    length="7";
   property name="articleHead" ormType="string"  length="200";
   property name="article"     ormType="string"  length="6000";

}

