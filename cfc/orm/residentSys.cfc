/**
 * @table      residents
 * @entityname residentSys
 * @output     false
 * @persistent true
 * @cacheUse   transactional
 */
component
{
   property name="residentId"    ormType="integer" fieldType="ID"  generator="native" ;
   property name="name"          ormType="string" length="200";
   property name="street"        ormType="string" length="200";
   property name="emailAddress"  ormType="string" length="150";
   property name="cellPhone"     ormType="string" length="20";
   property name="password"      ormType="string" length="50";
   property name="notifications" ormType="integer";
   property name="newsletter"    ormType="integer";
   property name="sms"           ormType="integer";
   property name="createDate"    ormType="timestamp";
   property name="keyId"         ormType="string" table="sysResidents" joincolumn="residentId";
}

