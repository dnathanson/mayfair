/**
 * @table      addresses
 * @entityname addresses
 * @output     false
 * @persistent true
 * @cacheUse   transactional
 */
Component
{
   property name="addressId" ormType="integer" fieldType="ID"  generator="native" ;
   property name="address"   ormType="string"  length="60";
   property name="lot"       ormType="integer";
   property name="block"     ormType="integer";

}

