/**
 * @table      minutes
 * @entityname minutes
 * @output     false
 * @persistent true
 * @cacheUse   transactional
 */
Component
{
   property name="minId"      ormType="integer" fieldType="ID"  generator="native" ;
   property name="minuteDate" ormType="date"    length="7";
   property name="approved"   ormType="integer";
   property name="minutes"    ormType="blob";

}

