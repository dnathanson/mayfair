/**
 * @table      permissions
 * @entityname permissions
 * @output     false
 * @persistent true
 * @cacheUse   transactional
 */
Component
{
   property name="permissionId" ormType="integer" fieldType="ID"  generator="native" ;
   property name="residentId"   ormType="integer";
   property name="admin"        ormType="integer";
   property name="management"   ormType="integer";
   property name="blog"         ormType="integer";
   property name="committee"    ormType="integer";

}

