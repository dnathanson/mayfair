/**
 * @table      parkingRequest
 * @entityname Parkingrequest
 * @output     false
 * @persistent true
 * @cacheUse   transactional
 */
Component
{
   property name="parkingReqId" ormType="integer" fieldType="ID"  generator="native" ;
   property name="residentId"   ormType="integer";
   property name="reason"       ormType="string" length="2000";
   property name="parkDenyId"   ormType="integer";
   property name="phone"        ormType="string" length="30";
   property name="startDate"    ormType="date"   length="7";
   property name="endDate"      ormType="date"   length="7";
   property name="createDate"   ormType="date"   length="7";
   property name="approved"     ormType="integer";
   property name="approvedDate" ormType="date"   length="7";
   property name="completed"    ormType="integer";

}

