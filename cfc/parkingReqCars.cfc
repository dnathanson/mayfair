/*                                                      
    
    CF Name:     parkingReqCars
                                                  
    Description:
	   

      The following is the table layout for the CFC being created:

        Column Name                   Data Type          Data Len   Nullable
        ----------------------------  -----------------  ---------  --------
        parkingCarId                  INT                 10          NO      
        parkingReqId                  INT                 10          NO      
        make                          VARCHAR             100         YES     
        model                         VARCHAR             100         YES     
        tagNo                         VARCHAR             10          YES     
        state                         VARCHAR             100         YES     
        year                          VARCHAR             10          YES     
        color                         VARCHAR             100         YES     


      The following is a list of functions that are included in
      this module:

        getParkingReqCars
        insertParkingReqCars
        removeParkingReqCars
        updateParkingReqCars


    Program Information:                                
      Called from:  None
      Calls:        None
      Parameters:   None
      Author:       Drew Nathanson
      
    Global Session Variables Used:
    
    Pets-Org
    Copyright (c) 1999-2025. All Rights Reserved.
                                
*/

Component
{
	// Set global all fields
	allFields = 'parkingCarId, parkingReqId, make, model, tagNo, state, year, color';

	/**
    * @output  true
    * @returntype Query
    * @hint This function will return the entire record set from table parkingReqCars.
    */
	public function getParkingReqCars ( required numeric parkingCarId = 0, string selectLine = '', string where = '', string orderBy = '', limit = '' )
	{

      /*

         Function Name: getparkingReqCars

         Description:
           This function will return the entire record set from table parkingReqCars.  
           
         Function Information:
            Input Arguments:  parkingCarId   
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Check value to see if selectLine is set
		if ( !len(selectLine) ) { selectLine = allFields; }

		// Set field
		sqlLine = "Select #selectLine# from parkingReqCars where 0=0 ";

		// Check for parkingCarId
		sqlLine &= ( parkingCarId > 0 ) ? " AND parkingCarId = #parkingCarId#" : "";

		// Check for WHERE
		sqlLine &= ( len(where) > 0 ) ? " AND #PreserveSingleQuotes(where)#" : "";

		// Check for orderBy
		sqlLine &= ( len(orderBy) > 0 ) ? " ORDER BY #PreserveSingleQuotes(orderBy)#" : "";

		// Check for limit
		sqlLine &= ( len(limit) > 0 ) ? " Limit #limit#" : "";

		// Execute the query
		recs = queryExecute("#sqlLine#",{ datasource:application.databaseSelect } );

		// Return
		return recs;

	}

	/**
    * @output  true
    * @returntype Numeric
    * @hint This function will insert the data into parkingReqCars.
    */
	public function insertParkingReqCars ( required Struct StrList )
	{

      /*

         Function Name: insertparkingReqCars

         Description:
           This function will insert the data into parkingReqCars.  
           
         Function Information:
            Input Arguments:  StrList  
            Output Arguments: newId (New Id Number)  

       */

		// Define local variables
		
		var newId        = 0;


		// Set up the try/catch environment
		try
		{

			// Define the stored procedure object
			procObj  = new storedproc();

			// Set the default information
			procObj.setDatasource("#Application.Database#");
			procObj.setProcedure("INSERT_parkingReqCars_PROC");

			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.parkingReqId#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.make#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.model#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.tagNo#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.state#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.year#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.color#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="Out", Variable="newId");

			// Execute the procedure
			resultObj = procObj.execute();
			newId = resultObj.getprocOutVariables().newId;

		} catch ( database excpt ) { 

			// Throw the error to the page
			rethrow;

		}

		// Return
		return newId;

	}


	/**
    * @output  true
    * @returntype Void
    * @hint This function will update the data into parkingReqCars.
    */
	public function updateParkingReqCars ( required Struct StrList )
	{

      /*

         Function Name: updateparkingReqCars

         Description:
           This function will update the data into parkingReqCars.  
           
         Function Information:
            Input Arguments:  StrList  
            Output Arguments: None  

       */

						

		// Set up the try/catch environment
		try
		{

			// Define the stored procedure object
			procObj  = new storedproc();

			// Set the default information
			procObj.setDatasource("#Application.Database#");
			procObj.setProcedure("UPDATE_parkingReqCars_PROC");

			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.parkingCarId#");
			procObj.addParam(cfsqltype="CF_SQL_INTEGER", type="In", value="#StrList.parkingReqId#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.make#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.model#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.tagNo#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.state#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.year#");
			procObj.addParam(cfsqltype="CF_SQL_VARCHAR", type="In", value="#StrList.color#");

			// Execute the procedure
			procObj.execute();

		} catch ( database excpt ) { 

			// Throw the error to the page
			rethrow;

		}


		// Return
		return ;

	}

	/**
    * @output  true
    * @returntype Boolean
    * @hint This function will remove a record from the table parkingReqCars.
    */
	public function removeParkingReqCars ( String parkingcarid = 0, String where = "" )
	{

      /*

         Function Name: removeparkingReqCars

         Description:
           This function will remove a record from the table parkingReqCars.  
           
         Function Information:
            Input Arguments:    
            Output Arguments: True or False  

       */

		// Define local variables
		var sqlLine      = "";
		

		// Set the sqlLine field
		sqlLine  = "Delete from parkingReqCars where 0=0 ";
		sqlLine &= ( structKeyExists(arguments, 'parkingcarid') && parkingcarid > 0 ) ? "and parkingCarId = #parkingcarid# " : "";
		// Check for where
		sqlLine &= ( len(where) > 0 ) ? "AND #PreserveSingleQuotes(where)#" : "";

		// Execute the query
		queryExecute("#sqlLine#", { datasource:Application.DatabaseDelete} );


		// Return
		return ;

	}

	/**************************************************************************
	 * The next section is used as an object for the Getter object functions  *
	 **************************************************************************/

	/**
    * @output  true
    * @returntype query
    * @hint 
    */
	public function init ( numeric indexNo = -1, string Where = "" )
	{

      /*

         Function Name: init

         Description:
             
           
         Function Information:
            Input Arguments:  indexNo (primary key)  
            Output Arguments: recs (Result Query)  

       */

		// Define local variables
		var sqlLine      = "";

		// Retrieve the record
		sqlLine = "Select parkingCarId, parkingReqId, make, model, tagNo, state, year, color from parkingReqCars where 0 = 0";

 		// Check the key
		sqlLine &= ( indexNo > -1 ) ? " AND parkingCarId = #indexNo#" : "";

 		// Check for where
		sqlLine &= ( len(where) > 0 ) ? "AND #PreserveSingleQuotes(where)# " : "";

		//Execute the query
		recordSet = queryExecute("#sqlLine#", { datasource:Application.DatabaseSelect } );


		// Return
		return recordset;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint Return the number of record in the query
    */
	public function GetRecordCount ( Void )
	{

      /*

         Function Name: GetRecordCount

         Description:
           Return the number of record in the query  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: recordcount  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.recordcount;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field parkingCarId
    */
	public function getparkingCarId ( Void )
	{

      /*

         Function Name: getparkingCarId

         Description:
           This function will return the field parkingCarId  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.parkingCarId;

	}

	/**
    * @output  true
    * @returntype numeric
    * @hint This function will return the field parkingReqId
    */
	public function getparkingReqId ( Void )
	{

      /*

         Function Name: getparkingReqId

         Description:
           This function will return the field parkingReqId  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.parkingReqId;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field make
    */
	public function getmake ( Void )
	{

      /*

         Function Name: getmake

         Description:
           This function will return the field make  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.make;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field model
    */
	public function getmodel ( Void )
	{

      /*

         Function Name: getmodel

         Description:
           This function will return the field model  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.model;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field tagNo
    */
	public function gettagNo ( Void )
	{

      /*

         Function Name: gettagNo

         Description:
           This function will return the field tagNo  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.tagNo;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field state
    */
	public function getstate ( Void )
	{

      /*

         Function Name: getstate

         Description:
           This function will return the field state  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.state;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field year
    */
	public function getyear ( Void )
	{

      /*

         Function Name: getyear

         Description:
           This function will return the field year  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.year;

	}

	/**
    * @output  true
    * @returntype string
    * @hint This function will return the field color
    */
	public function getcolor ( Void )
	{

      /*

         Function Name: getcolor

         Description:
           This function will return the field color  
           
         Function Information:
            Input Arguments:  None  
            Output Arguments: Object Field  

       */

		// Define local variables
		var sqlLine      = "";

		// Return
		return recordSet.color;

	}


}
