/*
	The MIT License (MIT)

	Copyright (c) 2011 Jason Fill (@jasonfill)
	
	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
	associated documentation files (the "Software"), to deal in the Software without restriction, including
	without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
	copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the 
	following conditions:
	
	The above copyright notice and this permission notice shall be included in all copies or substantial 
	portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
	NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
	OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/**
* @name         TwilioLib
* @hint         Used to integrate with Twilio
*/
component accessors=true output=false {

	property name="AccountSid" 			default="" hint="The AccountSid provided by Twilio.";
	property name="AuthToken" 			default="" hint="The AuthToken provided by Twilio.";
	property name="ApiVersion" 			default="" hint="The version of the Twilio API to be used.";
	property name="ApiEndPoint" 		default="" hint="The Twilio API endpoint.";
	property name="ApiResponseFormat" 	default="" hint="The default return format that should be used. This can be overridden in for REST request as well.";
	property name="RESTClient" 			default="" hint="";

	/**
	* @output       false
	* @hint         Call on load
	*/
	public TwilioLib function init (
		required string AccountSid,
		required string AuthToken,
		required string ApiVersion,
		required string ApiEndPoint,
		required string ApiResponseFormat
	) {
		SetAccountSid(arguments.AccountSid);
		SetAuthToken(arguments.AuthToken);
		SetApiVersion(arguments.ApiVersion);
		SetApiEndPoint(arguments.ApiEndPoint);
		SetApiResponseFormat(arguments.ApiResponseFormat);

		SetRESTClient(createObject("component", "classes.RESTClient").init(arguments.AccountSid, arguments.AuthToken, arguments.ApiVersion, arguments.ApiEndPoint));

		return this;
	}

	/**
	* @output       false
	* @hint         Creates a new TwiML response object.
	*/
	public any function newResponse () {
		return createObject("component", "classes.TwiML").init(GetAccountSid(), GetAuthToken(), GetApiVersion(), GetApiEndPoint());
	}
	
	/**
	* @output       false
	* @hint         Creates a new REST request object.
	* @Resource     The resource that is to be consumed.
	* @Method       The HTTP method to be used.
	* @Vars         Any variables that are to be sent with the request.
	*/
	public any function newRequest (
		required string Resource,
		required string Method,
		required struct Vars
	) {
		return GetRESTClient().sendRequest(argumentCollection:arguments);
	}
	
	/**
	* @output       false
	* @hint         Creates a new TwiML utility object.
	*/
	public any function getUtils () {
		return createObject("component", "classes.Utils").init(GetAccountSid(), GetAuthToken());
	}
	
	/**
	* @output       false
	* @hint         Creates a new Twilio capability object.
	*/
	public any function getCapability () {
		return createObject("component", "classes.Capability").init(GetAccountSid(), GetAuthToken());
	}
	
}