/*                                                   
    
    CF Name:     Misc.cfc
                                                  
    Description:
	   This module contains miscellaneous functions used in the ICeS system.
                                         
    Program Information:                                
      Called from:  Anywhere
      Calls:        None
      Parameters:   By Function
      Author:       Drew Nathanson
      
    Global Session Variables Used:
      None
    
    Copyright (c) 1999-2012. All Rights Reserved.   
    Technical Synergy, Inc.                         

*/

component
{

	/**
	* @output true
	* @returnvariable struct
	* @hint loads a query of columns into a structure
	*/
	public struct function loadQueryToStruct ( required query qName, 
																		 string fromName = "",
		                                          		 string toName   = "",
		                                          		 numeric rowNo   = 0 ) 
	{

		// Define variables
		var fldVal     = ""; 
		var newName 	= "";
		var strList		= StructNew();
		var x          = 0;

		// Start a loop to process each coloumn
		for ( local.fld in qName)
		{

			if ( !rowNo )
			{
				
				// Set the field
				strList = local.fld;

				if ( len(fromName) && len(toName) )
				{
				
					// Set the new Name
					structInsert(strList, toName, strlist.fromName);
					// Remove the old fidld
					structDelete(strList, "#fromName#");

				}

			} else {

				// Check the current row
				if ( qName.currentRow != rowNo )
				{
					// Continue in loop
					continue;
				}

				// Set the field
				strList = local.fld;

				if ( len(fromName) && len(toName) )
				{
				
					// Set the new Name
					structInsert(strList, toName, strlist.fromName);
					// Remove the old fidld
					structDelete(strList, "#fromName#");

				}

			}

		} 

		// Return the structure
		return ( strList );

	}

	/**
	* @output true
	* @hint loads a query (multiple records) into an array of structures
	*/
	public array function loadQueryToArray ( required query qName, 
		                                		  string fldName,
		                                      string fldName2 = "",
		                                      string fldName3 = "",
		                                      numeric all     = 0)
	{

		// Define variables
		var retArray     = ArrayNew(1);
		var strList      = { };

		// Start the loop
		for ( local.row in qName )
		{

			// Set the field
			//strList[fldName] = Evaluate(local.row[#fldName#]) );

			// Check the second field
			/* if ( len(fldName2) )
			{
				// Set the field
				strList[fldName2] = Evaluate('qName' & local.row.fldName2);
			} */

			// Check for third field
			/* if ( len(fldName3) )
			{
				// Set the field
				strList[fldName3] = Evalaute('qName' & local.row.fldName3);
			} */

			// Append the structure
			arrayAppend(retArray, ( all == 0 ) ? local.row : strList);

		}

		// Return the array
		return retArray;

	}

	/**
	* @output true
	* @returntype numeric
	* @hint compares two structures to see if they match
	*/
	public function compareStructs( required struct struct1, required struct struct2 )
	{

		// Return true or false
		return ( struct1.hashCode() == struct2.hashCode() ) ? true : false;

	}

	/**
	* @output true
	* @returntype string
	* @hint sets string to a specific length.
	*/
	public function setString ( required string text, required numeric length )
	{

		// Define local variables
		var retString = "";

		// Determine if the field is smaller 
		if ( len(text) > length )
		{
			// Set the field
			retString = Mid(text, 1, length);

		} else {

			// Set the field
			retString = text & repeatString(" ",length - len(text));
		}

		// Return retString
		return retString;

	}

	/**
	* @output true
	* @returntype string
	* @hint check for a comma in the line of text
	*/
	public function checkForComma ( required string line )	
	{

		// Define local variables
		var newLine    = "";
		var pos        = len(line);
		var singleChar = "";

		// Get the last character 
		singleChar = mid(line, pos, 1);

		// Set ethe new line
		newLine = ( singleChar == ",") ? mid(line, 1, pos-1) : line;

		// Return line
		return newLine;
		
	}

	/**
	* @output true
	* @hint parse out the url from key/value to a url variable.
	*/
	public function getURLVariables ( void )
	{

		/*
		 *  @name: getURLVariables
		 *  @description: This function will parse out the information in the 
		 *                cgi.path_info variable into the normal URL variables with the Key=Value pairing.
		 *  @input: None
		 *  @output: none
		 */

		// Define variables
		var urlVars 	= reReplaceNoCase(trim(cgi.path_info), '.+\.cfm/? *', '');
	   var i 			= 1;
 		var lastKey 	= "";
 		var value 		= "";

		// Check to see if there are URL variables
	  	if( len(urlVars) )
		{

			// Start a loop to process all the variables 
    		for( i = 1 ; i <= listLen(urlVars, "/"); i++ )
			{

				// Get the value
     			value = listGetAt(urlVars, i, "/");

				// Determine if this variable is part of a pair
				if ( ( i mod 2 ) == 0 )
				{
					
					// Set the value to the URL scope
					url[lastKey] = value;

				} else {
					
					// Set the last key (no value)
					lastKey = value;
					
				}

    		}

			// Determine if the last key has a value
    		if( ( (i-1) mod 2 ) == 1 )
			{
				
				// Set the last key to null in the url scope
				url[lastKey] = "";
			}

		}

	}

	/**
	 * @output true
	 * @returnvariable string
	 * @hint this function will create a password.
	 */
	public function makePassword ( void )
	{
	
	   /*
	    * Function Name: makePassword
	    *
	    * Description:
	    *   This function will create a password on the fly that is 
		 *	  random. 
	    *
	    * Function Information:
	    *   Input Arguments:  none
	    *   Output Argument:  new_password
	    *
	    */
	
		// Define variables
		var placeCharacter 		= "";
		var currentPlace   		= 0 ;
		var group          		= 0 ;
		var subGroup       		= 0 ;
		var numberofCharacters = 8;
		var characterFilter 	= 'O,o,0,i,l,1,I,5,S';
		var characterReplace 	= repeatString(",", listlen(characterFilter)-1);
	
		// Check the argument count - this is will never be used however
		if( arrayLen(arguments) >= 1 ) 
			numberofCharacters = val(arguments[1]);

		// Check for arguments 
		if( arrayLen(arguments) >= 2 ) 
		{
			// Seg the value
			characterFilter 	= listsort(rereplace(arguments[2], "([[:alnum:]])", "\1,", "all"),"textnocase");
			characterReplace 	= repeatString(",", listlen(characterFilter)-1);
		}

		// Start a loop to process
		while ( len(placeCharacter) < numberofCharacters ) 
		{

			// Set group with range
			group = randRange(1,4, 'SHA1PRNG');
			switch(group) 
			{

				// Check value = 1 
				case "1":

					// Set subGroup to rand()
					subGroup = rand();
				   
				   // Check subGroup
				   switch(subGroup) 
				   {

				   	// Check for value = 0
						case "0":

							// Set the characters
							placeCharacter = placeCharacter & chr(randRange(33,46, 'SHA1PRNG'));

							// Break the switch
							break;

						case "1":

							// Set the characters
							placeCharacter = placeCharacter & chr(randRange(58,64, 'SHA1PRNG'));
						
							// Break the switch
							break;
					
					}

				case "2":

					// Set the characters
					placeCharacter = placeCharacter & chr(randRange(97,122, 'SHA1PRNG'));
					
					// Break the switrch
					break;
				
				case "3":

					// Set the characters
					placeCharacter = placeCharacter & chr(randRange(65,90, 'SHA1PRNG'));

					// Break the switch
					break;

				case "4":
					
					// Set the characters
					placeCharacter = placeCharacter & chr(randRange(48,57, 'SHA1PRNG'));

					// Break the switch
					break;

			}

			// Check the length
			if ( listLen(characterFilter) ) 
			{

				// Set the characters
				placeCharacter = replacelist(placeCharacter, characterFilter, characterReplace);

			}
		
		}

		// return placeCharacter
		return placeCharacter;

	}
		
	/**
	 * @output true
	 * @returnvariable boolean
	 * @hint this function will create the excel sheet
	 */
	public function createExcelEmails ( void )
	{
	
	   /*
	    * Function Name: createExcelEmails
	    *
	    * Description:
	    *   This function will create the excel sheet needed to 
	    *   dump all the residetns email address along with the 
	    *   home address.
	    *
	    * Function Information:
	    *   Input Arguments: 
	    *   Output Argument:
	    *
	    */
	
		// Define local variables
		var cnt        = 1;
		var fileName   = application.excelPath & "mayfairEmails.xls";
		var text       = { };
		var row        = 1;

		// Define hte formatting structures
		var boldStruct    = { };
		var normalStruct  = { };
		var tempStruct    = { };

		// Create the objects
		rwObj  			= createObject("component", "#application.cfcPath#readWriteExcel");
		resObj 			= createObject("component", "#application.cfcPath#residents");

		// Get all the addresses
		addRecs 			= queryExecute("Select * from addresses Order by lot,block");

		// Get all the residents
		resRecs 			= resObj.getResidents (where="residentId > 0", orderBy="street");

		// Creata the sheet object and the first several rows
		var sheetObj 	= SpreadsheetNew("Mayfair at Parkland", false);

		// Set formatting
		boldStruct     = rwObj.loadAttribs(21);
		normalStruct   = rwObj.loadAttribs(20);
		tempStruct     = rwObj.loadAttribs(4);
		tempStruct.alignment = "center";
		tempStruct.fontsize  = "18";

		// Merge the cells todays
		SpreadsheetMergeCells(sheetObj, row, row, 1, 5);

		// Set first line
		text.a         = "Mayfair at Parkland HOA";

		// Write to the sheet
		sheetObj       = rwObj.genericFormatCells(sheetObj, tempStruct, row++, text, 1);

		// Set the headers
		row++;
		text           = {
			a    : "Lot",
			b    : "Block",
			c    : "Address",
			d    : "Name",
			e    : "Email"
		};

		// Write to the sheet
		sheetObj       = rwObj.genericFormatCells(sheetObj, boldStruct, row++, text, 5);

		// Process the addresses
		for ( addLine in addRecs )
		{

			// Set cnt to 1
			cnt = 1;

			// Get the residents
			asRecs 	= queryExecute("Select * from resRecs where street = :add",
				                     {
				                     	add : { cfsqltype:"CF_SQL_VARCHAR", value:addLine.address }
				                     },
				                     { 
				                     	dbtype:"query"
				                     });

			if ( !asRecs.recordCount )
			{

				// Set text
				text =    {
					a : addLine.lot,
					b : addLine.block,
					c : addLine.address
				};

				// Write to the sheet
				sheetObj       = rwObj.genericFormatCells(sheetObj, normalStruct, row++, text, 3);
				row++;

			} else {

				// Set text
				text =    {
					a : addLine.lot,
					b : addLine.block,
					c : addline.address
				};

				// Process the resident records
				for ( res in asRecs )
				{

					if ( cnt == 1 )
					{
						text.d  = res.name;
						text.e  = res.emailAddress;
						cnt++;
					} else {
						text.c = "";
						text.d  = res.name;
						text.e  = res.emailAddress;
						cnt++;
					}

					// Write to the sheet
					sheetObj       = rwObj.genericFormatCells(sheetObj, normalStruct, row++, text, 5);

				}

				// Add 1
				row++;

			}

		}

		// Write the sheet
		spreadsheetWrite(sheetObj, "#fileName#", true);

		// Return true
		return true;

	}
			
}
