/*********************************************************
 *
 *   CF Name:    readWriteExcel.cfc
 *
 *   Description:
 *      This module contains all the functions needed to 
 *      read and write an excel document.
 *
 *      Best viewed with Tabs = 3
 *
 *    Program Information:
 *      Called from:  
 *      Calls:         
 *      Parameters:   
 *      Author:       Drew Nathanson
 *
 *    Global Session Variables Used:
 *
 *    Technical Synergy, Inc.
 *    Copyright (c) 1999-2018. All Rights Reserved.   
 *
 **********************************************************/

Component
{

	/**
	 * @output true
	 * @returnvariable any
	 * @hint this function will write the information to the excel sheet.
	 */
	public function genericFormatCells ( required any sheetObj,
	                                     required struct fStruct, 
	                                     required numeric row,
	                                     required struct text,
	                                     required numeric totCells,
	                                              string alignCells = "", 
	                                              string skipCells  = "",
	                                              string decFields  = "",
	                                              string altDecFlds = "" )
	{
	
	   /*
	    * Function Name: genericFormatCells
	    *
	    * Description:
	    *   This function will write the information to the excel sheet.
	    *
	    * Function Information:
	    *   Input Arguments:
	    *   Output Argument:
	    *
	    */
	
		// Define local variables 
	   var a					= 0;
	   var altFlag			= 0;
	   var found			= 0;
	   var i					= 1;
	   var items			= "";
	   var iValue			= "";
	   var let				= "";
	   var letterArray  	= [ "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
	                         "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z","aa","bb","cc",
	                         "dd","ee","ff" ];
	   var newList			= "";
	   var rightCells		= [ ];
	   var rightList		= "";
	   var rowCell			= [ ];
		var tempStruct		= { };
		var rightStruct  	= { };

		// Check to see if alignCells has something
   	if ( len(alignCells) )
   	{
   		
   		// Set the flag
   		altFlag = 1;
   		
   		// Copy  the structure
   		StructAppend(tempStruct, fStruct);
   		StructAppend(rightStruct, fStruct);
   		
   		// Parse out the information
   		items = alignCells.split(":");
   		
   		// Set the alignment
   		tempStruct.alignment = items[1];
   		
   		if ( items[1] == "right" && Len(altDecFlds) )
   		{
   			// Set alignment
   			rightStruct.alignment = "right";
   			
   			// Build rightCells
   			rightCells = ListToArray(altDecFlds);
   			 
   		}
	   		
   		// Check to see if the value is "right"
   		if ( items[1] == "right" && decFields == "true" )
   		{
   		
   			// Add an additional attribute
   			tempStruct.dataFormat = "0.00";
   						
   		}
	   		
   		// Check the value
   		if ( decFields == "true" && len(altDecFlds) > 0 )
   		{
   		
   			// Start a loop to rebuild the list
   			for ( i = 1; i <= ListLen(items[2]); i++ )
   			{
   			
   				// Set iValue
   				iValue = ListGetAt(items[2], i);
   				
   				// Check to see if the value is in the alDecFlds list
   				if ( ListFind(altDecFlds, iValue) == 0 )
   				{
   					
   					// Set new list 
   					newList &= "#iValue#,";
   					
   				}	
   				
   			}	
   			
   			// Remove the last , from the list
   			newList = Mid(newList, 1, len(newList) - 1);
   			
   			// Set the rows
   			rowCell = ListToArray(newList);
   			
   		} else {
   		
   			// Build an array wit the cells
   			rowCell = ListToArray(items[2]);
   			
   		}

   	}
   		
   	// Start a loop to set the correct formatting --->
   	for ( i = 1; i <= totCells; i++ )
   	{
   		
   	
   		// Check to see if there are any skipped cells
   		if ( len(skipCells) )
   		{
   				
   			// Check to see if the current cell is in the list
   			if ( !ListFind(skipCells, i) )
   			{
   				
   				// Continue in loop
   				continue;
   				
   			}
   			
   		}
   		
   		// Check to see if rightArray has any value
   		if ( ArrayLen(rightCells) )
   		{
   			
   			// Check to see if value exists in right cells
   			if ( ArrayFind(rightCells, i) )
   			{
   				
   				// Set cell format
   				SpreadsheetFormatCellRange(sheetObj, rightStruct, row, i, row, i);
   				
   				// Continue to next record
   				continue;
   				
   			}
   			
   		}
   		
   		// Check the altFlag
   		if ( !altFlag )
   		{
   		
   			// Format the cell
   			SpreadsheetFormatCellRange(sheetObj, fStruct, row, i, row, i);
   			
   			// Set found to 0
   			found = 0;
   			
   		} else {
   			
   			// Set found to 0
   			found = 0;
   			
   			// Check to see if the row exists
   			if ( ArrayFind(rowCell, i) )
   			{	   
   				
   				// WriteOutput("FOUND IN ROWCELL - I = #I#<br />");
   								
   				// Set found to 1
   				found = 1;
   					
   			} 
   			
   			// Check the found flag
   			if ( found )
   			{
   				
   				// WriteOutput("FOUND = 1, I = #i#<BR />");
   				
   				// Format the cell
   				SpreadsheetFormatCellRange(sheetObj, tempStruct, row, i, row, i);
   			
   			} else {
   			
   				// WriteOutput("FOUND = 0, I = #i#<br />");
   				
   				// Format the cell
	   			SpreadsheetFormatCellRange(sheetObj, fStruct, row, i, row, i);
	   		
   			}
   			
   		}
   		
   	}

   	// Check to see if there is text to display
		if ( !StructIsEmpty(text) )
		{
		
			// Start a loop to process the information
			for ( i = 1; i <= totCells; i++ )
			{
				
				// Set the letter 
				let = letterArray[i];
				
				// Check to see if the text structure has the value
				if ( StructKeyExists(text, "#let#") )
				{
				
					// Set the cell value
					SpreadsheetSetCellValue(sheetObj, text[let], row, i);
			
				}
					
			}
		
		}

		// Return sheetObj
		return sheetObj;

	}

	/**
	 * @output true
	 * @returnvariable struct
	 * @hint this function will load the excel attributes into an struct
	 */
	public function loadAttribs ( required numeric opt  )
	{
	
	   /*
	    * Function Name: loadAttribs
	    *
	    * Description:
	    *   This function will load the excel attributes into an struct. 
	    *
	    * Function Information:
	    *   Input Arguments:  opt
	    *   Output Argument:  retStruct
	    *
	    */
	
		// Define local variables
		var retStruct = { };

		// Determine what to create
		switch ( opt )
		{
			
			case 1:
			
				// Create the line header structure
				retStruct				= {
					fgColor			: "dark_blue",
					color				: "white",
					font				: "Calibri",
					fontsize			: 20,
					alignment		: "center",
					bold				: "True"
				};

				// Break
				break;
				
			case 2:
			
				// Create the light grey structure
				retStruct            	= {
					fgColor				: "grey_25_percent",
					color					: "Black",
					font					: "Calibri",
					fontsize				: "11",
					alignment			: "left",
					bold					: "True",
					bottomBorder  		: "thin",
					bottomBorderColor	: "black",
					topBorder	   	: "thin",
					topBorderColor		: "black",
					leftBorder			: "thin",
					leftBorderColor	: "black",
					rightBorder			: "thin",
					rightBorderColor	: "black"
				};

				// Break
				break;
				
			case 3:
			
				// Create the darker grey structure
				retStruct           	= {
					fgColor			: "White",
					color				: "Black",
					font				: "Calibri",
					fontsize			: "11",
					alignment		: "left",
					bold				: "false"
				};

				// Break
				break;
				
			case 4:
		
				// Create the lightBlue struct
				retStruct       		= {
					fgColor			: "blue_grey",
					color				: "White",
					font				: "Calibri",
					fontsize			: "11",
					alignment		: "left",
					bold				: "true"
				};
	
				// Break
				break;
			
			case 5:
			
				// Create the royal blue struct
				retStruct         	= {
					fgColor			: "royal_blue",
					color				: "White",
					font				: "Calibri",
					fontsize			: "11",
					alignment		: "left",
					bold				: "true"
				};

				// Break
				break;
					
			case 6:
			
				// Create the bold structure
				retStruct           = {
					font				: "Calibri",
					bold				: "True",
					fontsize			: "11",
					alignment		: "left"
				};

				// Break
				break;
				
			case 7:
			
				// Create the black struct
				retStruct            = {
					fgColor			: "Black",
					color				: "White",
					font				: "Calibri",
					fontsize			: "11",
					alignment		: "left",
					bold				: "true"
				};

				// Break
				break;
				
			case 8:
				
				// Create the red struct
				retStruct              = {
					fgColor			: "White",
					color				: "Red",
					font				: "Calibri",
					fontsize			: "11",
					alignment		: "left",
					bold				: "true"
				};
				
				// Break
				break;

			case 9:
			
				// Create the red (bg) struct
				retStruct         	= {
					fgColor			: "Red",
					color				: "White",
					font				: "Calibri",
					fontsize			: "11",
					alignment		: "left",
					bold				: "true"
				};

				// Break
				break;
			
			case 10:
			
				// Create the green structure
				retStruct 								= {
					fgColor					: "Bright_Green",
					color						: "Black",
					font						: "Calibri",
					bold						: "false",
					fontsize					: "11",
					alignment				: "left",
					bottomBorder  			: "thin",
					bottomBorderColor		: "black",
					topBorder	   		: "thin",
					topBorderColor			: "black",
					leftBorder				: "thin",
					leftBorderColor		: "black",
					rightBorder				: "thin",
					rightBorderColor		: "black",
					textWrap					: "true"
				};

				// Break
				break;

			case 20:
			
				// Create the normal structure
				retStruct  								= {
					font						: "Calibri",
					bold						: "false",
					fontsize					: "11",
					alignment				: "left",
					bottomBorder  			: "thin",
					bottomBorderColor		: "grey_25_percent",
					topBorder	   		: "thin",
					topBorderColor			: "grey_25_percent",
					leftBorder				: "thin",
					leftBorderColor		: "grey_25_percent",
					rightBorder				: "thin",
					rightBorderColor		: "grey_25_percent",
					textWrap					: "true"
				};
				
				// Break
				break;
				
			case 21:
			
				// Create the normal structure
				retStruct  								= {
					font						: "Calibri",
					bold						: "true",
					fontsize					: "11",
					alignment				: "left",
					bottomBorder  			: "thin",
					bottomBorderColor		: "grey_25_percent",
					topBorder	   		: "thin",
					topBorderColor			: "grey_25_percent",
					leftBorder				: "thin",
					leftBorderColor		: "grey_25_percent",
					rightBorder				: "thin",
					rightBorderColor		: "grey_25_percent",
					textWrap					: "true"
				};
				
				// Break
				break;

		}

		// Return retStruct
		return retStruct;

	}
			
	/**
	 * @output true
	 * @returnvariable any
	 * @hint this will set the pallette colors
	 */
	public function setColorPalette ( required any sheetObj )
	{
	
	   /*
	    * Function Name: setColorPalette
	    *
	    * Description:
	    *   This function will set the color palette for the different updates to the excel sheet.
	    *
	    * Function Information:
	    *   Input Arguments: sheetObj
	    *   Output Argument: sheetObj
	    *
	    */
	
		// Get the palette
		palette = sheetObj.getworkbook().getcustompalette();
		
		// Set up structure for color change
		r = javacast("int",221).bytevalue();
		g = javacast("int",221).bytevalue();
		b = javacast("int",221).bytevalue();
		
		// Replace the grey_25_percent with my new color
		palette.setcoloratindex(22, r, g, b);
		
		// Set up structure for color change
		r = javacast("int",79).bytevalue();
		g = javacast("int",129).bytevalue();
		b = javacast("int",189).bytevalue();
	
		// Replace the dark_blue with my new color
		palette.setcoloratindex(18, r, g, b);
		
		// Set up structore for color change
		r = javacast("int", 54).bytevalue();
		g = javacast("int", 164).bytevalue();
		b = javacast("int", 175).bytevalue();
		
		// Replace the blue_grey with new color
		palette.setcoloratindex(54, r,g, b);	   
   	
   	// Set up structore for color change
		r = javacast("int", 39).bytevalue();
		g = javacast("int", 64).bytevalue();
		b = javacast("int", 139).bytevalue();
		
		// Replace the royal_blue with new color
		palette.setcoloratindex(30, r,g, b);   
		
		// Set up structore for color change
		r = javacast("int", 192).bytevalue();
		g = javacast("int", 244).bytevalue();
		b = javacast("int", 185).bytevalue();
		
		// Replace the bright_green with new color
		palette.setcoloratindex(11, r,g, b);   
	
		// Return sheetObj
		return sheetObj;

	}
	
}
