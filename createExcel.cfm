<!doctype html>
<!---                                                      
    CF Name:    createExcel.cfm

    Description:
      This module will create an excel sheet with all the 
      residents and their email addresses.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Check permissions
	if ( !structKeyExists(session, "userInfo") || 
		  ( !structKeyExists(session.userInfo, "permission") || 
		    ( session.userInfo.permission.blog  == 1 || 
		    	session.userInfo.permission.committee == 1 
		    ) 
		  ) 
		)
	{
		// Send to home page
		location ( url="/index.cfm", addToken=false );
	}

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	msgColor      = "red";
	titlePageName = "Create Excel";

	// Create the objects
	miscObj = createObject("component", "#application.cfcPath#Misc");

	// Create the excel sheet
	miscObj.createExcelEmails();

	// Set the structure with information to the request scope
	str    = {
		jsInclude     : [ ],
		jsChose       : false,
		bootstrap     : true
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 75%">
			<header>
				<span class="icon major fa-file-excel-o accent3"></span>
				<br />
				<h2></h2>
				<p>
					Use the link below to download your copy of the Mayfair at Parkland Residents
					Email information Excel sheet. 
			   </p>
				<!--- <p id="msg" style="color:#msgColor#;font-weight:bold;text-align:center;">#compMessage#</p> --->
			</header>
			<div class="box">
				<p> 
					Here is your link:<br />
					<a href="#application.httpPath#excel/mayfairEmails.xls"> Mayfair at Parkland Residents Email</a>
				</p>
			</div>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>