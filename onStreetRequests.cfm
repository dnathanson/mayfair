<!doctype html>
<!---                                                      
    CF Name:    onStreetRequests.cfm

    Description:
     	This module will allow the administrator/management
     	the ability to either approve or disapprove of on-street
     	parking.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Check permissions
	if ( !structKeyExists(session, "userInfo") || 
		  ( !structKeyExists(session.userInfo, "permission") || 
		    ( session.userInfo.permission.blog == 1 || session.userInfo.permission.committee == 1 ) ) )
	{
		if ( !structKeyExists(session, "userInfo") )
		{
			session.onStreeRequest = 1;
			location ( url="/login.cfm", addToken=false);
		} else {

			// Send to home page
			location ( url="/index.cfm", addToken=false );
		}

	}

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	titlePageName = "On Street Requests";

	// Get all the open requests
	orRecs = queryExecute("select A.*, B.name, B.street,B.emailAddress from parkingRequest A, residents B where A.completed = 0 and A.residentID = B.residentId order by createDate");

	// Get the reasons
	dRecs  = queryExecute("select * from parkingDenyReason where parkDenyId > 0");

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : true,
		includeTiny     : false,
		jsInclude       : [ "onStreetRequests.full.js" ] // "notifications.#request.jsType#.js?#cacheValue#"
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!--- Define variables --->
		<input type="hidden" id="reqId" value="" />
		<input type="hidden" id="denyReasonId" value="" />
		
		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-car accent4"></span>
				<br />
				<h2>On-Street Requests</h2>
				<p>
					Use this area to either approve or disapprove requests for on-street parking overnight. 
					<br />
					<cfif !application.useEmail>
						<span style="color:red;font-weight:bold;">EMAIL PROCESSING HAS BEEN DISABLED</span><br />
					</cfif>
			   </p>
			   <div class="msgFormat">#compMessage#</div>
			</header>
			<section class="box ">
				<div class="row uniform 50%">
					<div class="12u">
						<div class="table-wrapper">
							<table id="resultTab">
							<thead>
							<tr>
								<th>General Info</th>
								<th>Reason</th>
								<th>Dates</th>
								<th>## of Auto's</th>
								<th>Actions</th>
							</tr>
							</thead>
							<tbody>
							<cfloop query="orRecs">
								<cfquery datasource="#application.databaseSelect#" name="orCnt">
									Select count(*) as rc from parkingReqCars where parkingReqId = #orRecs.parkingReqId#
								</cfquery>
								<tr valign="top">
									<td>
										#orRecs.name#<br />
										#orRecs.street#<br />
										#orRecs.phone#<br />
										#orRecs.emailAddress#<br />
									</td>
									<td>#orRecs.reason#</td>
									<td>#dateFormat(orRecs.startDate,"mm/dd/yyyy")# <br />to<br />#dateFormat(orRecs.endDate,"mm/dd/yyyy")#</td>
									<td>#orCnt.rc#</td>
									<td id="H-#orRecs.parkingReqId#"> 
										<a href="##" id="A-#orRecs.parkingReqId#" data-id="A-#orRecs.parkingReqId#"><span class="fa fa-thumbs-o-up" /></a>
										&nbsp;&nbsp;
										<a href="##" id="D-#orRecs.parkingReqId#" data-id="D-#orRecs.parkingReqId#"><span class="fa fa-thumbs-o-down" /></a>
									 </td>
								</tr>	
							</cfloop>
							</tbody>
							</table>
						</div>
					</div>
				</div>
				
			</section>

		</section>

		<!--- DEFINE THE MODAL AREA --->
		<div id="getReasonArea" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="getReasonArea" style="margin-top:100px;">
			<div class="modal-dialog">
				<form class="form-horizontal">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" area-hidden="true">X</button>
						<h3>Select Reason for Denying Parking</h3>
					</div>
					<div class="modal-body" style="height:200px;">
						<p> Please select your address from the list below.
						<br />
						</p>
						<div class="row-fluid">
							<div class="col-md-9">
								<div class="form-group">
									<select name="selReason" id="selReason" class="form-control">
									<cfloop query="dRecs">
										<option value="#dRecs.parkDenyId#">#dRecs.reason#</option>
									</cfloop>
									</select>
								</div>
							</div>
							<br /><br />
						</div>
					</div>
					<div class="modal-footer" style="height:100px;">
						<br />
						<button id="continueBtn" class="btn btn-primary">Continue</button>
						<button id="closeModalBtn" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</div>
				</form>
			</div>
		</div>
		<!--- END OF MODAL AREA --->

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<script>
		var isMobile = "#session.isMobile#";
		var isPhone  = "#session.isPhone#";
	</script>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>