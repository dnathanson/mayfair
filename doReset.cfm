<!doctype html>
<!---                                                      
    CF Name:    doReset.cfm

    Description:
      This module will create the email message with the information 
      required to re-set a password.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved.   
--->

<cfscript>
	
	structDelete(application, "resetPassword");
	
	// Create the objects
	resObj 	= createObject("component", "#application.cfcPath#residents");
	miscObj 	= createObject("component", "#application.cfcPath#Misc");
	smtpObj  = createObject("component", "#application.cfcPath#smtp");

	// Parse out the query string
	miscObj.getURLVariables();

	// Check to see if url.email exists
	if ( !structKeyExists(url, "email") )
	{
		
		// Go back to the login page
		location("/login.cfm");

	} else {

		// Search for the email address
		resRec 		= resObj.getResidents(where="emailAddress = '#url.email#'");

		// Check the results the resident serach by email
		if ( resRec.recordCount == 0 )
		{

			// Set session variable
			session.passwdResetError = 1;
			
			// Go back to the login page
			location(url="/login.cfm",addToken=false);

		} else {

			smtpStruct  = smtpObj.getSMTPinfo();

			// Build a link line
			uuid 		= createUUID();
			linkLine = '#application.httpPath#resetPassword.cfm/id/#uuid#';

			// Create the body of the message
			saveContent variable="mailBody"
			{
				writeOutput("
Dear #resRec.Name#,<br /><br />
  You have requested that your account password for Mayfair At Parkland Web Site be reset. 
  <br /><br />Please Use the link below to reset that password.
  <br /><br />
  <a href='#linkLine#'>#linkLine#</a>
  <br /><br />
  This link will be valid for only 1 hour.
  <br /><br />
  Mayfair at Parkland HOA<br />");
			}

			// Create the mail object and send
			mail = new mail(type="html", charset="utf-8", body="#mailBody#");
			mail.setFrom("noreply@mayfairatparkland.com (Mayfair at Parkland Website)");
			mail.setTo("#url.email#");
			mail.setSubject("Reset your Password");
			mail.setServer(smtpStruct.host);
			mail.setPort(smtpStruct.port);
			mail.setUsername(smtpStruct.user);
			mail.setPassword(smtpStruct.pass);
			mail.setUseTLS(true);
			mail.send();

			// Build a struct
			str = {
				residentId : resRec.residentId,
				uuid       : uuid,
				email      : resRec.emailAddress,
				dateTime   : now()
			};

			// Set informatation in the 
			if ( !structKeyExists(application, "resetPassword") )
			{
				// Create the array
				application.resetPassword = [ ];
			} 

			// Append to the array
			arrayAppend(application.resetPassword, str);

			session.passwordReset = 1;

			// Go back to the login page
			location(url="/login.cfm",addToken=false);
		} 

	}
</cfscript>