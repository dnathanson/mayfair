<!doctype html>
<!---                                                      
    CF Name:    permissionMaint.cfm

    Description:
      This module will allow the the administrator the ability to assign 
      permissions to individual accounts.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Check permissions
	if ( !structKeyExists(session, "userInfo") || 
		  ( !structKeyExists(session.userInfo, "permission") || 
		    session.userInfo.permission.admin == 0 ) )
	{
		// Send to home page
		location ( url="/index.cfm", addToken=false );
	}

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = ( structKeyExists(session, "message") ) ? session.message : "";
	emailAddress  = "";
	selValue      = "";
	titlePageName = "Permission Maintenance";

	// Create the object
	resObj 	= createObject("component", "#application.cfcPath#residents");

	// Check to see if action exists
	if ( structKeyExists(form, "action") )
	{

		// Chek the value of action
		switch ( form.action )
		{

			case 'selection':

				// Get the street information
				resRecs 	= resObj.getResidents(where="street = '#form.street#'");
				selValue = form.street;

				// Break the switch
				break;

		}

	}

	// Retreive the street information
	addrRecs = queryExecute("Select * from addresses Order By address");

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false,
		includeTiny     : true,
		jsInclude       : ["permissionMaint.#request.jsType#.js?#cacheValue#"]
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-paw accent5"></span>
				<br />
				<h2>User Permission Access Maintenance</h2>
				<p>
					The will allow for user permission access maintenance. 
					<br />
			   </p>
			   <cfif len(compMessage)>
				   <div class="msgFormat">#compMessage#</div>
				</cfif>
			</header>
			<section class="box ">
				<form method="Post" action="permissionMaint.cfm" name="form" id="form">
				<input type="hidden" name="action"            value="selection" />
				<input type="hidden" name="resId"  id="resId" value="" />
				<div class="row uniform 50%">
					<h3>Search by Address</h3>
					<div class="12u">
						<select name="street" id="street">
						<option value=""> -- Please Select --</option>
						<cfloop query="addrRecs">
							<option value="#addrRecs.address#" <cfif selValue eq addrRecs.address> selected</cfif>>#addrRecs.address#</option>	
						</cfloop>
						</select>
					</div>
				</div>
				</form>
				
			</section>

			<cfif structKeyExists(form, "action")>
				<section class="box"> 

					<div class="table-wrapper">
						<table id="resultTab">
						<thead>
							<tr>
								<th>Name</th>
								<th>Email</th>
								<th> &nbsp; </th>
							</tr>
						</thead>
						<tbody>
							<cfif !resRecs.recordCount>
								<tr>
									<td>No User Accounts exists</td>
									<td> &nbsp;</td>
									<td> &nbsp;</td>
								</tr>
							<cfelse>
								<cfloop query="resRecs">
									<tr>
										<td>#resRecs.name#</td>
										<td>#resRecs.emailAddress#</td>
										<td>
											<div class="8u 12u(mobilep)">
												<a href="##" id="#resRecs.residentId#" data-id="#resRecs.residentId#" class="fit">Edit</a>
												&nbsp;&nbsp;
												<a href="##" id="D-#resRecs.residentId#" data-id="D-#resRecs.residentId#" class="fit">Delete</a>
											</div>
										</td>
									</tr>
								</cfloop>	
							</cfif>
						</tbody>
						</table>
						
					</div>

				</section>

				<section class="box" id="permEditSection" style="display:none;">

					<div class="row uniform 50%">
						<header>
							<h4>Permissions for this User:</h4>
						</header>
						<div class="row uniform 50%">
							<div class="3u 12u(narrower)">
								<input type="checkbox" name="admin" id="admin" value="1">
								<label for="admin">Administrator</label>
							</div>
							<div class="3u 12u(narrower)">
								<input type="checkbox" name="management" id="management" value="1">
								<label for="management">Management</label>
							</div>
							<div class="3u 12u(narrower)">
								<input type="checkbox" name="blog" id="blog" value="1">
								<label for="blog">Blog</label>
							</div>
							<div class="3u 12u(narrower)">
								<input type="checkbox" name="committee" id="committee" value="1">
								<label for="committee">Committee</label>
							</div>
						</div>
						<br /><br />
						<div class="4u 12u(mobilep)">
							<input type="button" id="savePermissionBtn" value="Save Permission" class="fit" />
						</div>
					</div>

				</section>

			</cfif>
		
		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<script>
		var isMobile = "#session.isMobile#";
		var isPhone  = "#session.isPhone#";
	</script>
	
	<cfif structKeyExists(session, "message")>
		<cfset structDelete(session, "message") />
	</cfif>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">
	
</body>
</cfoutput>
</html>