<!doctype html>
<!---                                                      
    CF Name:    wedgeBlogMaint.cfm

    Description:
     	This module will contains all the necessary information
     	to enter/udpate blog entries.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Check permissions
	if ( !structKeyExists(session, "userInfo") || 
		  ( !structKeyExists(session.userInfo, "permission") ||
		    session.userInfo.permission.admin == 0 &&
		    session.userInfo.permission.management == 0 &&
		    session.userInfo.permission.committee == 1 ) )
	{
		// Send to home page
		location ( url="/index.cfm", addToken=false );
	}

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	titlePageName = "Wedge Blog Maint";

	// Check to see if action exists
	if ( structKeyExists(form, "action") )
	{

		// Check to see what action is set to
		if ( form.action == "add" )
		{

			// Check for value entries
			blogDate = ( isDate(form.blogDate) ) ? form.blogDate : dateFormat(now(),"mm/dd/yyyy");
			subject  = ( len(form.subject) )     ? form.subject : "Wedge Blog Entry - #dateFormat(now(),"mm/dd/yyyy")#";
			active   = ( structKeyExists(form, "active") ) ? 1 : 0;

			// Check to see if there is any article
			if ( !len(form.blog) )
			{
				// Set the message
				compMessage = "There is no blog entry to POST therefore notthing was saved";
				className   = "msgFormatError";

			} else {

				// Insert the article into the database
				queryExecute("Insert into wedgeBlog ( residentId, blogDate, subject, blog, active ) 
					            values ( :resId, :bDate, :sub, :blog, :act)",
					            {
					            	resId : { cfsqltype:"CF_SQL_INTEGER",     value:session.userInfo.residentId },
					            	bDate : { cfsqltype:"CF_SQL_DATE",        value:dateFormat(blogDate,"yyyy-mm-dd") },
					            	sub   : { cfsqltype:"CF_SQL_VARCHAR",     value:subject },
					            	blog  : { cfsqltype:"CF_SQL_LONGVARCHAR", value:form.blog },
					            	act   : { cfsqltype:"CF_SQL_INTEGER",     value:active }
					            });

				compMessage = "Your blog entry has been saved.";

				// Check for email
				if ( application.useEmail )
				{

					// Create the objects
					resObj 	= createObject("component", "#application.cfcPath#residents");
					smtpObj  = createObject("component", "#application.cfcPath#smtp");

					// Get the email addresses
					resRecs 	= resObj.getResidents(selectLine="emailAddress",where="notifications = 1",orderBy="emailAddress");
					resList  = valueList(resRecs.emailAddress,",");

					// Get the smtp informaetion
					smtpStruct  = smtpObj.getSMTPinfo();
					compMessage = "Your email has been sent.";

					// Set body to the form
					saveContent variable="body"
					{
							writeOutput("
Dear Homeowner,<br />
A new Wedge Blog entry is available for your reivew. This very important issue affects everyone
in the community. We urge you to read the new information.<br /><br />
Thank you for your continued interest in our community.<br /><br />
Your Board of Director's.<br /><br />
<a href='#application.httpPath#theWedgeBlog.cfm'>#application.httpPath#theWedgeBlog.cfm</a><br /><br />");
					}

					// Process the emails
					for ( email in resList )
					{

						// Create a mail object
						mail = new mail(type="html", charset="utf-8", body="#body#");
						mail.setFrom("noreply@mayfairatparkland.com (Mayfair at Parkland Website)");
						mail.setTo("#email#");
						mail.setSubject("#form.subject#");
						mail.setServer(smtpStruct.host);
						mail.setPort(smtpStruct.port);
						mail.setUsername(smtpStruct.user);
						mail.setPassword(smtpStruct.pass);
						mail.setUseTLS(true);
						
						// Send the email
						mail.send();

					}

				}

				// Check for message
				if ( application.useTwilio )
				{
					// Credate the object
					smsObj 		= createObject("component", "#application.cfcPath#sms");
					if ( !isDefined("resObj") )
						resObj 		= createObject("component", "#application.cfcPath#residents");

					// Get all phone numbers
					resRecs 	 	= resObj.getResidents(selectLine="cellPhone", where="sms = 1 and length(cellphone) > 0");
					phoneList 	= valueList(resRecs.cellPhone, ","); 

					// Get the phone numbers
					smsObj.sendSMS(phoneList="#phoneList#",msg="New Wedge Blog available at #application.httpPath#theWedgeBlog.cfm");

				}
				
			} 

		} else if ( form.action == "update" ) {

			// Check for active
			active   = ( structKeyExists(form, "active") ) ? 1 : 0;
			
			// Insert the article into the database
			queryExecute("Update wedgeBlog
			               Set blogDate   = :bDate,
			                   subject    = :sub,
			                   active     = :act,
			                   blog       = :blog			                   
			              where wedgeBlogId = :id", 
				            {
				            	bDate  : { cfsqltype:"CF_SQL_DATE",        value: dateFormat(blogDate,"yyyy-mm-dd") },
				            	sub    : { cfsqltype:"CF_SQL_VARCHAR",     value:subject },
				            	act    : { cfsqltype:"CF_SQL_INTEGER",     value:active },
				            	blog   : { cfsqltype:"CF_SQL_LONGVARCHAR", value:form.blog },
				            	id     : { cfsqltype:"CF_SQL_INTEGER",     value:form.blogId }
				            });

			// Update the message
			compMessage = "Your blog entry has been saved.";
		}

	}

	// Build the date range
	if ( month(now()) == 1 )
	{
		startDate = DateAdd("yyyy",-1,Now()) & "-12-01";
	} else {
		startDate = Year(Now()) & "-01-01";
	}
	endDate   = Year(Now()) & "-12-31";

	// Create the object
	blogRecs = queryExecute("Select * from wedgeBlog where blogDate between '#startDate#' and '#endDate#' Order by blogDate desc");

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false,
		includeTiny     : true,
		jsInclude       : ["wedgeBlogMaint.#request.jsType#.js?#cacheValue#"]
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-newspaper-o accent2"></span>
				<br />
				<h2>Wedge Blog Entry</h2>
				<p>
					Use this area to write about the wedge to be added to the blog about the 
					subject.
					<br />
					<cfif !application.useEmail>
						<span style="color:red;font-weight:bold;">EMAIL PROCESSING HAS BEEN DISABLED</span><br />
					</cfif>
			   </p>
			   <cfif len(compMessage)>
				   <div class="msgFormat">#compMessage#</div>
				</cfif>
			</header>
			<section class="box ">
				<form method="post" action="wedgeBlogMaint.cfm" name="form" id="form">
				<input type="hidden" name="action" id="action" value="add" />
				<input type="hidden" name="blogId" id="blogId" value="" />
				<div class="row uniform 50%">
					<div class="12u">
						<input type="text" name="blogDate" id="blogDate" value="#dateFormat(now(),"mm/dd/yyyy")#" placeholder="Date of Blog - MM/DD/YYYY" />
					</div>
				</div>
				<div class="row uniform 50%">
					<div class="12u">
						<input type="text" name="subject" id="subject" value="" maxlength="100" placeholder="Subject of Blog Entry" />
					</div>
				</div>
				<div class="row uniform 50%">
					<div class="4u 12u(narrower)">
						<input type="checkbox" name="active" id="active" value="1">
						<label for="active">Show Blog Entry to Public?</label>
					</div>
				</div>
				<div class="row uniform 50%">
					<div class="12u">
						<textarea name="blog" id="blog"></textarea>
						<br /><br />
						<div class="4u 12u(mobilep)">
							<input type="submit" id="sendBtn" value="Save Blog" class="fit" />
						</div>
					</div>
				</div>
				</form>

			</section>

			<section class="box">
				<header>
					<h3>Previous Blog Entries</h3>
					<p>The following is a list of blog entries that you can edit.</p>
				</header>		
				<div class="table-wrapper" style="overflow-y:scroll;height:40%;">
					<table id="prevBlogTable">
					<thead>
						<tr>
							<th>Blog Subject</th>
							<th>Blog Date</th>
							<th>Blog Status</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<cfloop query="blogRecs">
							<tr>
								<td>#blogRecs.subject#</td>
								<td>#dateFormat(blogRecs.blogDate,"mm/dd/yyyy")#</td>
								<td><cfif blogRecs.active eq 0> Draft <cfelse> Final </cfif></td>
								<td>
									<div class="8u 12u(mobilep)">
										<a href="##" id="E-#blogRecs.wedgeBlogId#" data-id="#blogRecs.wedgeBlogId#" class="fit">Edit</a>
										&nbsp;&nbsp;
										<a href="##" id="D-#blogRecs.wedgeBlogId#" data-id="D-#blogRecs.wedgeBlogId#" class="fit">Delete</a>
									</div>
								</td>
							</tr>
						</cfloop>
					</tbody>
					</table>

				</div>
			</section>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<script>
		var isMobile = "#session.isMobile#";
		var isPhone  = "#session.isPhone#";
	</script>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>