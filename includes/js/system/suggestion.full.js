/* JS File for the suggestion page */

// Define the fields
var comments = $('#comments');

// Do initialization
$(function()
{

	tinymce.init(
	{
    	selector: "textarea#comments",
	   theme: "modern",
	   width: '100%',
	   height: 400,
	   plugins: [
	         "advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker",
	         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
	         "save table contextmenu directionality emoticons template paste textcolor"
	   ],
	   content_css: "includes/js/plugins/tinymce/skins/lightgray/content.min.css",
	   toolbar: "insertfile undo redo | styleselect | link| bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | print preview media fullpage | forecolor backcolor emoticons", 
	   menubar: "insert",
	   style_formats: [
	        {title: 'Bold text', inline: 'b'},
	        {title: 'Red text', inline: 'span', styles: {color: '##ff0000'}},
	        {title: 'Red header', block: 'h1', styles: {color: '##ff0000'}},
	        {title: 'Example 1', inline: 'span', classes: 'example1'},
	        {title: 'Example 2', inline: 'span', classes: 'example2'},
	        {title: 'Table styles'},
	        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
	    ]
 	}); 

	// Set the focus on the subject line
	comments.focus();
	
});
