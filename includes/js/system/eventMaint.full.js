/* JS File for the news article maintenance */
var hostName            = location.hostname;
var urlPath             = '//'+ hostName +  '/cfc/ajaxMayfair.cfc?wsdl';

// Define variables
var addEditArea         = $('#addEditArea');
var addRepeatBtn        = $('#addRepeatBtn');
var clearEABtn          = $('#clearEABtn');
var clearRepeatBtn      = $('#clearRepeatBtn');
var committeeId         = $('#committeeId');
var dateList            = $('#dateList');
var endDate             = $('#endDate');
var endTime             = $('#endTime');
var eventId             = $('#eventId');
var eventSaveBtn        = $('#eventSaveBtn');
var eventSaveRepeatBtn  = $('#eventSaveRepeatBtn');
var eventTitle          = $('#eventTitle');
var locationId          = $('#locationId');
var missingFields       = $('#missingFields');
var modalBodyInfo       = $('#modalBodyInfo');
var month               = $('#month');
var newEventBtn         = $('#newEventBtn');
var otherLocation       = $('#otherLocation');
var repeatArea          = $('#repeatArea');
var repeatDate          = $('#repeatDate');
var repeatFld           = $('#repeatFld');
var resultTab           = $('#resultTab');
var returnBtn           = $('#returnBtn');
var saveRepeatBtn       = $('#saveRepeatBtn');
var sendNotice          = $('#sendNotice');
var shortDescription    = $('#shortDescription');
var startDate           = $('#startDate');
var startTime           = $('#startTime');
var year                = $('#year');

// Load the defaults
$(function()
{

	// Define the fields that need a calendar
	$('#startDate').datepicker();
	$('#endDate').datepicker();
	$('#repeatDate').datepicker();

});

// Create the listener for the street
year.on('change', function()
{
	// Submit the form
	form.submit();
});

// Create the listener
returnBtn.on('click', function()
{
	// Re-display the page
	location = 'eventMaint.cfm';

	// Return false
	return false;
});

// Create the listener
clearRepeatBtn.on('click', function()
{
	dateList.empty();
	return false;
});

// Create the listener
eventSaveRepeatBtn.on('click', function()
{

	// show the field
	repeatArea.show();

	// Return false
	return false;

});

// Create the listener
addRepeatBtn.on('click', function()
{

	// Retrieve the field
	var rDate = repeatDate.val();

	// Append the value to the list
	var text = dateList.append("<option value='"+rDate+"'>"+rDate+"</option>");

	// Close the text field
	repeatDate.val("");
	
	// Return false
	return false;
});

// Create the listener
clearEABtn.on('click', function()
{
	// Clear the fields
	eventTitle.val("");
	startDate.val("");
	startTime.val("");
	endDate.val("");
	endTime.val("");
	shortDescription.val("");
	sendNotice.prop('checked', false);
	committeeId.val(0);
	locationId.val(0);
	otherLocation.val("");

	// Close the window
	addEditArea.hide();

	// Return false
	return false;

});

// Create the listener for the entries
resultTab.on('click', 'a', function()
{
	// Define the variables
	var me   = $(this);
	var id   = me.data("id");
	var sid  = new String(id);

	// Check to see id D 
	if ( sid.indexOf("D-") == -1 )
	{

		// Request a new password
		$.ajax(
		{
			url: urlPath + '&method=editEvent',
			type: 'post',
			data: { eventId : id },
			dataType: 'json',
			success: function ( data )
						{

							// Load the fields
							eventId.val(data.EVENTID);
							eventTitle.val(data.TITLE);
							startDate.val(data.STARTDATE);
							startTime.val(data.STARTTIME);
							endDate.val(data.ENDDATE);
							endTime.val(data.ENDTIME);
							shortDescription.val(data.SHORTDESC);
							committeeId.val(data.COMMITTEEID).prop('selected', true);
							locationId.val(data.LOCATIONID).prop('selected', true);
							otherLocation.val(data.OTHERLOCATION);

							// Check the send notice
							if ( data.SENDNOTICE )
								sendNotice.prop('checked', true);

							// Show the area
							addEditArea.show();

						}
		});

	} else {

		// Remove the D- from the id
		id 		 = id.replace("D-", "");
		var sDate = $('#sDate').val();
		var eDate = $('#eDate').val();

		// Confirm the removal
		if ( confirm("Are you sure you want to remove the event from the calendar? Press OK for Yes, Cancel for No") )
		{

			// Remove the account
			$.ajax(
			{
				url: urlPath + '&method=removeEvent',
				type: 'post',
				data: { id: id, sDate : sDate, eDate : eDate },
				dataType: 'json',
				success: function ( data )
							{

								// Re-display the page
								resultTab.html(data);

							}
			});

		}
	}

	// Return false
	return false;

});

// Create the listener for the newEntry
newEventBtn.on('click', function()
{
	// Show the area
	addEditArea.show();

	// Set the focus
	eventTitle.focus();

	// Return false
	return false;

});

// Create the listener for the eventSaveBtn
eventSaveBtn.on('click', function()
{
	// Define fields
	var title 				= eventTitle.val();
	var stDate 				= startDate.val();
	var stTime 				= startTime.val();
	var edDate  			= endDate.val();
	var edTime 				= endTime.val();
	var shortDesc        = shortDescription.val();
	var location         = locationId.val();
	var otherLoc         = otherLocation.val();
	var sendNotice 		= $('#sendNotice').prop('checked');
	var blkStTime        = stTime.split(/\:/);
	var blkEdTime        = edTime.split(/\:/);	
	var displayErrors    = "<ul>";
	var errors           = 0;

	// Check the dates
	var chStDate = !/Invalid|NaN/.test(new Date(stDate));
	var chEdDate = !/Invalid|NaN/.test(new Date(edDate));
	var chStTime = false;
	var chEdTime = false;
	var bgDate   = "";
	var eddDate  = "";

	// Create the date object for start/stop date
	if ( chStDate ) { bgDate = new Date(stDate); eddDate = new Date(edDate); }
		
	// Check the time
	if ( ( stTime.length == 0 ) || 
		  ( blkStTime[0] > 24 || blkStTime[1] > 59 ) )
	{
		chStTime = false;
	} else {
		chStTime = true;
	}

	if ( ( edTime.length == 0 ) || 
		  ( blkEdTime[0] > 24 || blkEdTime[1] > 59 ) )
	{
		chEdTime = false;
	} else {
		chEdTime = true;
	}

	// Test for title
	if ( !title.length )
	{
		displayErrors += "<li>Event Title is missing</li>";
		errors++;
	}

	// Check for start date
	if ( !chStDate )
	{
		displayErrors += "<li>Start Date is either missing or invalid</li>";
		errors++;
	}

	// Check the start time
	if ( !chStTime )
	{
		displayErrors += "<li>Start Time is either missing or invalid</li>";
		errors++;
	}

	// Check for end date
	if ( !chEdDate )
	{
		displayErrors += "<li>End Date is either missing or invalid</li>";
		errors++;
	}

	// Check the end time
	if ( !chEdTime )
	{
		displayErrors += "<li>End Time is either missing or invalid</li>";
		errors++;
	}

	// Check for start date after end date
	if ( bgDate.getTime() > eddDate.getTime() )
	{
		displayErrors += "<li>Start Date is after End Date</li>";
		errors++;
	}
	
	// Check for short description
	if ( shortDesc.length == 0 )
	{
		displayErrors += "<li>Short Description is missing</li>";
		errors++;
	}

	if ( location == 0 && otherLoc.length == 0 )
	{
		displayErrors += "<li>You must select a location from either the location or enter in other location</li>";
		errors++;
	}

	// Check to see if there are errors
	if ( errors )
	{

		// Close the messages
		displayErrors += "</ul>";

		// display the modal window
		missingFields.modal();
		modalBodyInfo.html(displayErrors);

	} else {


		if ( !$('#dateList option:selected').length )
		{
			$('#dateList option').prop('selected', true);
			var fld = dateList.val();
			repeatFld.val(fld);
		}

		// Submit the form
		$('#form').submit();

	}

	// Return false
	return false;

});

// Create the listener for the eventSaveBtn
saveRepeatBtn.on('click', function()
{
	// Define fields
	var title 				= eventTitle.val();
	var stDate 				= startDate.val();
	var stTime 				= startTime.val();
	var edDate  			= endDate.val();
	var edTime 				= endTime.val();
	var shortDesc        = shortDescription.val();
	var location         = locationId.val();
	var otherLoc         = otherLocation.val();
	var sendNotice 		= $('#sendNotice').prop('checked');
	var blkStTime        = stTime.split(/\:/);
	var blkEdTime        = edTime.split(/\:/);	
	var displayErrors    = "<ul>";
	var errors           = 0;

	// Check the dates
	var chStDate = !/Invalid|NaN/.test(new Date(stDate));
	var chEdDate = !/Invalid|NaN/.test(new Date(edDate));
	var chStTime = false;
	var chEdTime = false;

	// Check the time
	if ( ( stTime.length == 0 ) || 
		  ( blkStTime[0] > 24 || blkStTime[1] > 59 ) )
	{
		chStTime = false;
	} else {
		chStTime = true;
	}

	if ( ( edTime.length == 0 ) || 
		  ( blkEdTime[0] > 24 || blkEdTime[1] > 59 ) )
	{
		chEdTime = false;
	} else {
		chEdTime = true;
	}

	// Test for title
	if ( !title.length )
	{
		displayErrors += "<li>Event Title is missing</li>";
		errors++;
	}

	// Check for start date
	if ( !chStDate )
	{
		displayErrors += "<li>Start Date is either missing or invalid</li>";
		errors++;
	}

	// Check the start time
	if ( !chStTime )
	{
		displayErrors += "<li>Start Time is either missing or invalid</li>";
		errors++;
	}

	// Check for end date
	if ( !chEdDate )
	{
		displayErrors += "<li>End Date is either missing or invalid</li>";
		errors++;
	}

	// Check the end time
	if ( !chEdTime )
	{
		displayErrors += "<li>End Time is either missing or invalid</li>";
		errors++;
	}

	// Check for short description
	if ( shortDesc.length == 0 )
	{
		displayErrors += "<li>Short Description is missing</li>";
		errors++;
	}

	if ( location == 0 && otherLoc.length == 0 )
	{
		displayErrors += "<li>You must select a location from either the location or enter in other location</li>";
		errors++;
	}

	// Check to see if there are errors
	if ( errors )
	{

		// Close the messages
		displayErrors += "</ul>";

		// display the modal window
		missingFields.modal();
		modalBodyInfo.html(displayErrors);

	} else {

		if ( !$('#dateList option:selected').length )
		{
			$('#dateList option').prop('selected', true);
			var fld = dateList.val();
			repeatFld.val(fld);
		}

		// Submit the form
		$('#form').submit();

	}

	// Return false
	return false;

});

startTime.on('blur', function()
{

	// Define variables
	var me    = $(this);
	var tm    = me.val().toUpperCase();

	// Check to see if the value contains A or P
	if ( tm.indexOf("P") > 0 || tm.indexOf("A") > 0 )
	{
		// Replace the values
		var oldTM = tm;
		tm = tm.replace(/A|P|M/gi,'');
		
		// Parse out the 
		var parts = tm.split(/\:/);
		var hr    = parts[0];
		
		if ( oldTM.indexOf("P") > 0 )
		{
			// Check to see if the time is 12:00
			if ( tm != "12:00" )
			{
				// Add 12 to it
				hr = parseInt(hr) + 12;
			}
		} 

		// Update the field
		startTime.val(hr+":"+parts[1]);
	}

	// Return false
	return false;

});

endTime.on('blur', function()
{

	// Define variables
	var me    = $(this);
	var tm    = me.val().toUpperCase();

	// Check to see if the value contains A or P
	if ( tm.indexOf("P") > 0 || tm.indexOf("A") > 0 )
	{
		// Replace the values
		var oldTM = tm;
		tm = tm.replace(/A|P|M/gi,'');
		
		// Parse out the 
		var parts = tm.split(/\:/);
		var hr    = parts[0];
		
		if ( oldTM.indexOf("P") > 0 )
		{
			// Check to see if the time is 12:00
			if ( tm != "12:00" )
			{
				// Add 12 to it
				hr = parseInt(hr) + 12;
			}
			// Add 12 to it
			hr = parseInt(hr) + 12;
		} 

		// Update the field
		endTime.val(hr+":"+parts[1]);
	}

	// Return false
	return false;

});