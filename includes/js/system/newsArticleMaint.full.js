/* JS File for the news article maintenance */
var hostName            = location.hostname;
var urlPath             = '//'+ hostName +  '/cfc/ajaxMayfair.cfc?wsdl';

// Define variables
var article     = $('#article');
var articleDate = $('#articleDate');
var articleHead = $('#articleHead');
var artId       = $('#artId');
var prevArt   	 = $('#prevArticles');

// Initialize the date picker
$(function()
{
 
	// Set the date picker configuration
	/*    
	$('.datepicker').datepicker(
	{
		format: 'mm/dd/yyyy',
		todayHighlight: 'true',
		autoclose: true
		// startDate: '#DateFormat('01/01/#Year(Now())#', "mm/dd/yyyy")#'
	});
	*/

	tinymce.init(
	{
    	selector: "textarea#article",
	   theme: "modern",
	   width: '100%',
	   height: 400,
	   mode : 'textarea',
	   plugins: [
	         "advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker",
	         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
	         "save table contextmenu directionality emoticons template paste textcolor"
	   ],
	   content_css: "includes/js/plugins/tinymce/skins/lightgray/content.min.css",
	   toolbar: "insertfile undo redo | styleselect | link| bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | print preview media fullpage | forecolor backcolor emoticons", 
	   menubar: "insert",
	   style_formats: [
	        {title: 'Bold text', inline: 'b'},
	        {title: 'Red text', inline: 'span', styles: {color: '##ff0000'}},
	        {title: 'Red header', block: 'h1', styles: {color: '##ff0000'}},
	        {title: 'Example 1', inline: 'span', classes: 'example1'},
	        {title: 'Example 2', inline: 'span', classes: 'example2'},
	        {title: 'Table styles'},
	        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
	    ]
 	}); 

});

// Create the listener
prevArt.on('click', 'a', function()
{
	// Define the variables
	var me   = $(this);
	var id   = me.data("id");
	var sid  = new String(id);

	// Check to see if id has D- in it
	if ( sid.indexOf("D-") == -1 )
	{

		// Get the article
		$.ajax(
		{
			url : urlPath + '&method=getArticleForUpdate',
			type : 'post',
			data : { id : id },
			dataType : 'json',
			success: function ( data )
						{

							// Define the object
							
							// Set the fields
							articleDate.val(data.ARTICLEDATE);
							articleHead.val(data.ARTICLEHEAD);
							tinymce.activeEditor.setContent(data.ARTICLE);
							articleDate.focus();

							// Set the id
							artId.val(id);

						}
		});

	} else {

		id = id.replace("D-","");

		
		// Display a message for verification
		if ( confirm("Are you sure you want to remove this blog entry? Press OK for Yes, Cancel for No") )
		{

			// Delete the article
			$.ajax(
			{
				url: urlPath + '&method=removeNewsArticle',
				type: 'post',
				data: { id : id },
				success: function ( data )
							{

								// Re-display the page
								location = 'newsArticleMaint.cfm';
							}
			});

		}
		
	}

	// Return false
	return false;
});
