/* JS File for the news article maintenance */
var hostName            = location.hostname;
var urlPath             = '//'+ hostName +  '/cfc/ajaxMayfair.cfc?wsdl';

// Define variables
var admin             = $('#admin');
var blog              = $('#blog');
var committee         = $('#committee');
var form      			 = $('#form');
var management        = $('#management');
var permEditSection   = $('#permEditSection');
var resId             = $('#resId');
var resultTab 			 = $('#resultTab');
var savePermissionBtn = $('#savePermissionBtn');
var street    			 = $('#street');

// Create the listener for the street
street.on('change', function()
{

	// Submit the form
	form.submit();

});

// Create the listener for the entries
resultTab.on('click', 'a', function()
{
	// Define the variables
	var me   = $(this);
	var id   = me.data("id");
	var sid  = new String(id);

	// Check to see id D 
	if ( sid.indexOf("D-") == -1 )
	{

		// Retreive the information
		$.ajax(
		{
			url: urlPath + '&method=getPermissionById',
			type: 'post',
			data: { residentId : id },
			dataType: 'json',
			success: function ( data )
						{

							// check the results
							if ( data.ADMIN )
								admin.prop('checked', true);

							if ( data.MANAGEMENT )
								management.prop('checked', true);

							if ( data.BLOG )
								blog.prop('checked', true);

							// Set the resId field
							resId.val(id);

							// Show the edit area
							permEditSection.show();

						}
		});

		

	} else {

		// Remove the D- from the id
		id = id.replace("D-", "");

		// Confirm the removal
		if ( confirm("Are you sure you want to remove the permission? Press OK for Yes, Cancel for No") )
		{

			// Remove the account
			$.ajax(
			{
				url: urlPath + '&method=removePermissionById',
				type: 'post',
				data: { id: id },
				success: function ( data )
							{

								// Re-display the page
								location = 'userMaint.cfm';

							}
			});

		}
	}

	// Return false
	return false;

});

// Create the listener
savePermissionBtn.on('click', function()
{

	// Define variables 
	var id   = resId.val();

	// Get the fields
	var adm      = ( admin.is(":checked") ) ? 1 : 0;
	var mgt      = ( management.is(":checked") ) ? 1 : 0
	var bl       = ( blog.is(":checked") ) ? 1 : 0;
	var comm     = ( committee.is(":checked") ) ? 1 : 0;

	var formData = {
		residentId : id,
		admin      : adm,
		management : mgt,
		blog       : bl,
		committee  : comm
	};

	// Convert to json
	var formJson = JSON.stringify(formData);
	
	// Send to the server for processing
	$.ajax(
	{
		url : urlPath + '&method=processPermissions',
		type: 'post',
		data: { formJson : formJson },
		success: function ( data )
					{

						// Re-display the page
						location = 'permissionMaint.cfm';
					}
	});


	// Return false
	return false;

});
