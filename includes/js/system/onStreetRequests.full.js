/* JS File for the on street parking */
var hostName            = location.hostname;
var urlPath             = '//'+ hostName +  '/cfc/ajaxMayfair.cfc?wsdl';

// Define fields
var continueBtn         = $('#continueBtn');
var denyReasonId        = $('#denyReasonId');
var getReasonArea       = $('#getReasonArea');
var reqId               = $('#reqId');
var resultTab           = $('#resultTab');
var selReason           = $('#selReason');

// Check the action
resultTab.on('click', 'a', function()
{
	// Define the fields
	var me     = $(this);
	var id     = me.data('id');
	var sid    = new String (id);

	// Set the new id
	var item  = sid.split(/\-/);
	var newId = item[1];
			
	// Check the value
	if ( sid.indexOf("D-") > -1 )
	{

		reqId.val(newId);

		// Display the module window
		getReasonArea.modal();

	} else {

		// Update the database
		$.ajax(
		{
			url      : urlPath + '&method=setParkingApproved',
			type     : 'post',
			data     : { id : newId },
			dataType : 'json',
			success  : function ( data )
						  {

						  		// Update the table entry
						  		$('#H-'+newId).html('<span style="color:green;font-weight:bold;">Request has been processed</span>');

						  } 
		});

	}
	// Return false
	return false;

});

continueBtn.on('click', function()
{

	var id           = reqId.val();
	var denyReasonId = selReason.val();
	console.log(denyReasonId);
	console.log(id);

	getReasonArea.modal("hide");

	// Update the database
	$.ajax(
	{
		url      : urlPath + '&method=setParkingDisapproved',
		type     : 'post',
		data     : { id : id, denyParkId : denyReasonId },
		dataType : 'json',
		success  : function ( data )
					  {

					  		// Update the table entry
					  		$('#H-'+id).html('<span style="color:green;font-weight:bold;">Request has been processed</span>');

					  } 
	});
	
	// Return false
	return false;

});
