// Set up for aaajx call
var hostName            = location.hostname;
var urlPath             = '//'+ hostName +  '/cfc/ajaxMayfair.cfc?wsdl';

// Define fields
var Architectural         = $('#Architectural');
var mArchitectural        = $('#M-Architectural');
var Communications        = $('#Communications');
var mCommunications       = $('#M-Communications');
var Documents             = $('#Convenants');
var mDocuments            = $('#M-Convenants');
var Enforcement           = $('#Enforcement');
var mEnforcement          = $('#M-Enforcement');
var Events                = $('#Events');
var mEvents               = $('#M-Events');
var message               = $('#message');
var NeighborhoodWatering  = $('#NeighborhoodWatering');
var mNeighborhoodWatering = $('#M-NeighborhoodWatering');
var Safety                = $('#Safety');
var mSafety               = $('#M-Safety');
var SocialMedia           = $('#SocialEvents');
var mSocialMedia          = $('#M-SocialEvents');
var Wedge                 = $('#Wedge');
var mWedge                = $('#M-Wedge');

// Create the Architectural listener
Architectural.on('click', function()
{
	// Add the information
	$.ajax(
	{
		url: urlPath + '&method=signUpCommittee',
		type: 'post',
		data: {residentId : resId, committeeName : 'Architectural' },
		dataType: 'json',
		success: function ( data )
					{
						// Check the results
						if ( data.indexOf("already") != -1 )
						{

							// Update the message
							message.html(data);

							// Update the area
							message.css('background-color','red');

						} else {
						
							// Update the message
							message.html(data);

							// Update the local message
							mArchitectural.html("You have successfully join this committee");

						}

					}
	});

	// Return false
	return false;

});

Communications.on('click', function()
{
	// Add the information
	$.ajax(
	{
		url: urlPath + '&method=signUpCommittee',
		type: 'post',
		data: {residentId : resId, committeeName : 'Communications' },
		dataType: 'json',
		success: function ( data )
					{
						// Check the results
						if ( data.indexOf("already") != -1 )
						{

							// Update the message
							message.html(data);

							// Update the area
							message.css('background-color','red');

						} else {
						
							// Update the message
							message.html(data);

							// Update the area
							message.css('background-color','green');

							// Update the local message
							mCommunications.html("You have successfully join this committee");

						}

					}
	});

	// Return false
	return false;

});

// Create the Documents listener
Documents.on('click', function()
{
	// Add the information
	$.ajax(
	{
		url: urlPath + '&method=signUpCommittee',
		type: 'post',
		data: {residentId : resId, committeeName : 'Documents' },
		dataType: 'json',
		success: function ( data )
					{
						// Check the results
						if ( data.indexOf("already") != -1 )
						{

							// Update the message
							message.html(data);

							// Update the area
							message.css('background-color','red');

						} else {
						
							// Update the message
							message.html(data);

							// Update the area
							message.css('background-color','green');

							// Update the local message
							mDocuments.html("You have successfully join this committee");

						}

					}
	});

	// Return false
	return false;

});

// Create the Enforcement listener
Enforcement.on('click', function()
{
	// Add the information
	$.ajax(
	{
		url: urlPath + '&method=signUpCommittee',
		type: 'post',
		data: {residentId : resId, committeeName : 'Enforcement' },
		dataType: 'json',
		success: function ( data )
					{
						// Check the results
						if ( data.indexOf("already") != -1 )
						{

							// Update the message
							message.html(data);

							// Update the area
							message.css('background-color','red');

						} else {
						
							// Update the message
							message.html(data);

							// Update the area
							message.css('background-color','green');

							// Update the local message
							mEnforcement.html("You have successfully join this committee");

						}

					}
	});

	// Return false
	return false;

});

// Create the Events listener
Events.on('click', function()
{
	// Add the information
	$.ajax(
	{
		url: urlPath + '&method=signUpCommittee',
		type: 'post',
		data: {residentId : resId, committeeName : 'Events' },
		dataType: 'json',
		success: function ( data )
					{
						// Check the results
						if ( data.indexOf("already") != -1 )
						{

							// Update the message
							message.html(data);

							// Update the area
							message.css('background-color','red');

						} else {
						
							// Update the message
							message.html(data);

							// Update the area
							message.css('background-color','green');

							// Update the local message
							mEvents.html("You have successfully join this committee");

						}

					}
	});

	// Return false
	return false;

});

// Create the NeighborhoodWatering listener
NeighborhoodWatering.on('click', function()
{
	// Add the information
	$.ajax(
	{
		url: urlPath + '&method=signUpCommittee',
		type: 'post',
		data: {residentId : resId, committeeName : 'Neighborhood Watering' },
		dataType: 'json',
		success: function ( data )
					{
						// Check the results
						if ( data.indexOf("already") != -1 )
						{

							// Update the message
							message.html(data);

							// Update the area
							message.css('background-color','red');

						} else {
						
							// Update the message
							message.html(data);

							// Update the area
							message.css('background-color','green');

							// Update the local message
							mNeighborhoodWatering.html("You have successfully join this committee");
						}

					}
	});

	// Return false
	return false;

});

// Create the Safety listener
Safety.on('click', function()
{
	// Add the information
	$.ajax(
	{
		url: urlPath + '&method=signUpCommittee',
		type: 'post',
		data: {residentId : resId, committeeName : 'Safety' },
		dataType: 'json',
		success: function ( data )
					{
						// Check the results
						if ( data.indexOf("already") != -1 )
						{

							// Update the message
							message.html(data);

							// Update the area
							message.css('background-color','red');

						} else {
						
							// Update the message
							message.html(data);

							// Update the area
							message.css('background-color','green');

							// Update the local message
							mSafety.html("You have successfully join this committee");
						}

					}
	});

	// Return false
	return false;

});

// Create the SocialMedia listener
SocialMedia.on('click', function()
{
	// Add the information
	$.ajax(
	{
		url: urlPath + '&method=signUpCommittee',
		type: 'post',
		data: {residentId : resId, committeeName : 'Social Events' },
		dataType: 'json',
		success: function ( data )
					{
						// Check the results
						if ( data.indexOf("already") != -1 )
						{

							// Update the message
							message.html(data);

							// Update the area
							message.css('background-color','red');

						} else {
						
							// Update the message
							message.html(data);

							// Update the area
							message.css('background-color','green');

							// Update the local message
							mSocialMedia.html("You have successfully join this committee");
						}

					}
	});

	// Return false
	return false;

});

// Create the Wedge listener
Wedge.on('click', function()
{
	// Add the information
	$.ajax(
	{
		url: urlPath + '&method=signUpCommittee',
		type: 'post',
		data: {residentId : resId, committeeName : 'Wedge' },
		dataType: 'json',
		success: function ( data )
					{
						// Check the results
						if ( data.indexOf("already") != -1 )
						{

							// Update the message
							message.html(data);

							// Update the area
							message.css('background-color','red');

						} else {
						
							// Update the message
							message.html(data);
							
							// Update the area
							message.css('background-color','green');

							// Update the local message
							mWedge.html("You have successfully join this committee");

						}

					}
	});

	// Return false
	return false;

});
