/* JS File for the news article maintenance */
var hostName            = location.hostname;
var urlPath             = '//'+ hostName +  '/cfc/ajaxMayfair.cfc?wsdl';

// Define variables
var form      			= $('#form');
var street    			= $('#street');
var resultTab 			= $('#resultTab');
var rmAllForAddress  = $('#rmAllForAddress');
var returnBtn        = $('#returnBtn');
var showStats        = $('#showStats');
var statArea         = $('#statArea');

// Create the listener for the street
street.on('change', function()
{

	// Submit the form
	form.submit();

});

// Create the listener
returnBtn.on('click', function()
{

	// Re-display the page
	location = 'userMaint.cfm';

	// Return false
	return false;
});

// Create the listener for the entries
resultTab.on('click', 'a', function()
{
	// Define the variables
	var me   = $(this);
	var id   = me.data("id");
	var sid  = new String(id);

	// Check to see id D 
	if ( sid.indexOf("D-") == -1 )
	{

		// Request a new password
		$.ajax(
		{
			url: urlPath + '&method=passwordResetUserAdmin',
			type: 'post',
			data: { id : id },
			dataType: 'json',
			success: function ( data )
						{
							console.log(data);
						}
		});

	} else {

		// Remove the D- from the id
		id = id.replace("D-", "");

		// Confirm the removal
		if ( confirm("Are you sure you want to remove the email account? Press OK for Yes, Cancel for No") )
		{

			// Remove the account
			$.ajax(
			{
				url: urlPath + '&method=removeResidentId',
				type: 'post',
				data: { id: id },
				success: function ( data )
							{

								// Re-display the page
								location = 'userMaint.cfm';

							}
			});

		}
	}

	// Return false
	return false;

});

// Create the listener
rmAllForAddress.on('click', function()
{
	// Get the address selected
	var addr     = street.val();

	// Remove all emails to this address
	$.ajax(
	{
		url: urlPath + '&method=removeEmailsForAddress',
		type: 'post',
		data: { address : addr },
		dataType: 'json',
		success: function ( data )
					{

					}
	});

	// Return false
	return false;

});

// Create the listener
showStats.on('click', function()
{

	// Show the area
	statArea.show();

	// Return false
	return false;
	
});
