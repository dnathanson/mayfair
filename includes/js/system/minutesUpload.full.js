/* JS File for the minutes process */
var hostName            = location.hostname;
var urlPath             = '//'+ hostName +  '/cfc/ajaxMayfair.cfc?wsdl';

// Define the field
var message       = $('#message');
var minutesTable 	= $("#minutesTable");

// Create the listener
minutesTable.on('click', 'a', function()
{

	// Define the fields
	var id    = $(this).data("id");
	var sid   = new String(id);

	// Check to see if id has a D- in it
	if ( sid.indexOf("D-") == -1 )
	{

		// Update the status
		$.ajax(
		{
			url: urlPath + '&method=setFinalOnMinutes',
			type: 'post',
			data: { minuteid : id },
			dataType: 'json',
			success: function ( data )
						{

							// Set message
							message.html(data);

						}
		});

	} else {

		// Remove the D-
		id = id.replace("D-","");

		// Prompt for delete 
		if ( confirm("Are you sure you want to delete these minutes? Press OK for Yes, Cancel for No") )
		{

			// Delete the item
			$.ajax(
			{
				url : urlPath + '&method=deleteMinutesById',
				type: 'post',
				data: { minuteId : id },
				dataType: 'json',
				success: function ( data )
							{

								// Re-load the page
								location = 'minutesUpload.cfm';

							}
			});

		}

	}

	// Return false
	return false;

});