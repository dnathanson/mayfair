/* JS File for the signup process */
var hostName            = location.hostname;
var urlPath             = '//'+ hostName +  '/cfc/ajaxMayfair.cfc?wsdl';

// Set the fields
var continueBtn      = $("#continueBtn");
var eventBtn      	= $('#eventBtn');
var message       	= $('#message');
var nameFld 			= $('#name');
var notifications		= $('#notifications');
var loginBtn      	= $('#loginBtn');
var message          = $('#message');
var email         	= $('#email');
var street        	= $('#street');
var selAddress    	= $('#selAddress');
var selectAddrArea 	= $('#selectAddrArea');
var cellPhone        = $('#cellPhone');

/* Have the loading */
$(function()
{
	// Set the focus()
	nameFld.focus();

});

// Create the listener
loginBtn.on('click', function()
{
	
	// go to the login page
	location='/login.cfm';

	// Return false
	return false;

});

// Create the listen
email.on('blur', function()
{
	// Define variable
	var me 			= $(this);
	var emailAddr 	= me.val();

	// Set up the filter for the email address
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

	// Test to make sure that it fits
	if ( !filter.test(emailAddr) )
	{
		
		message.html("You have entered an invalid Email Address");
		message.css('color','red');
		eventBtn.prop('disabled', true);

	} else {

		// Check to see the email address
		$.ajax(
		{
			url: urlPath + '&method=ifEmailExists',
			type: 'post',
			data: { email : emailAddr },
			dataType: 'json',
			success: function ( data )
						{
							if ( data )
							{
								// set the message
								message.html("This email address already exists within the system ");
								message.css('color','red');
								eventBtn.prop('disabled', true);
							
							} else {

								// enable the button
								eventBtn.prop('disabled', false);
								message.html("");
								message.css('color','green');

							}

						}
		});

	}

	// Return false
	return false;

});

// Create a listener for the street
street.on('change', function()
{

	// Define variables 
	var me 	= $(this);
	var addr = me.val();

	// make the button available
	eventBtn.prop('disabled', false);
	
	// Call the ajax function to process
	$.ajax(
	{
		url: urlPath + '&method=ifAddressCorrect',
		type: 'post',
		data: { address : addr },
		dataType: 'json',
		success: function ( data )
					{
						// Check the results
						if ( !data.length == 0 )
						{

							// Check to see if data[1] == "Invalid Number"
							if ( data[0] == "Invalid Number" )
							{

								message.html("You have entered in an invalid street address");
								message.css('color','red');
								eventBtn.prop('disabled', true);

							} else {

								// Clear the previous address information
								selAddress.empty();

								// Load the selection box
								for ( var x = 0; x < data.length; x++ )
								{
									// Set address to the selection box and set 1 item as selected.
									selAddress.append('<option value="'+data[x]+'"> '+data[x]+'</option>');
									$('#selAddress option').eq(0).prop('selected',true);
								}

								// Display the modal window						
							   selectAddrArea.modal();
							   
							   $(".modal").on('shown.bs.modal', function()
							   {
							   	selAddress.focus();
							   });

							}

						}

					}
	});

	// Return false
	return false;

});

continueBtn.on('click', function()
{

	// Retrieve the address selected
	var addr = selAddress.val();

	// Set the value
	street.val(addr);

	// Close the modal window
	selectAddrArea.modal("hide");

	cellPhone.focus();
	
	// Return false
	return false;

});

