/* JS file for the resetPassword module */

// Define variables
var confirmPassword 	= $('#confirmPassword');
var eventBtn         = $('#eventBtn');
var noMatch          = $('#noMatch');
var password  			= $('#password');


// Create the listener for the submit
eventBtn.on('click', function()
{
	// Define variables 
	var pass 	= password.val();
	var cPass 	= confirmPassword.val();

	// Check to see if the match
	if ( pass == cPass )
	{

		// Return true to let processing go
		return true;

	} else {

		noMatch.modal();

		// Return false
		return false;

	}

});