// Set up the URL path for the ajax call
var hostName            = location.hostname;
var urlPath             = '//'+ hostName +  '/cfc/ajaxMayfair.cfc?wsdl';

var email   	= $('#email');
var forgotBtn 	= $('#forgotBtn');
var msg        = $('#msg');


// Set the focus
$(function()
{
	email.focus();

	// Return false
	return false;

});

// Create the listener for forgotBtn
forgotBtn.on('click', function()
{
	// Define fields
	var emailAddr = email.val();

	// Check the value
	if ( !emailAddr.length )
	{
		// Update the message
		msg.html("You have NOT entered in your email address.");
	
	} else {

		// Check to see if the email address eixsts
		$.ajax(
		{
			url:  urlPath + '&method=ifEmailExists',
			type: 'post',
			data: { email : emailAddr },
			success: function ( data )
						{
							// Check the value
							if ( data == 0 )
							{
								// Update the form
								msg.html("Your Email Address doesn't exist in our system, please create an account.");

							} else {

								// Re-direct to place
								location = '/doReset.cfm/email/'+emailAddr;

							}

						}
		});

	}

	// Return false
	return false;

});
