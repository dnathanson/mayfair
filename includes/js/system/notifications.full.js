/* JS File for the news article maintenance */

// Define variables
var comments     	= $('#comments');
var includeSMS    = $('#includeSMS');
var lc            = $('#lc');
var overLimit     = $('#overLimit');
var noteDate 		= $('#noteDate');
var showLC        = $('#showLC');
var subject  		= $('#subject');

// Initialize the date picker
$(function()
{
 	// Set up the editor
	tinymce.init(
	{
    	selector: "textarea#comments",
	   theme: "modern",
	   width: '100%',
	   height: 400,
	   mode : 'textarea',
	   plugins: [
	         "advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker",
	         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
	         "save table contextmenu directionality emoticons template paste textcolor"
	   ],
	   content_css: "includes/js/plugins/tinymce/skins/lightgray/content.min.css",
	   toolbar: "insertfile undo redo | styleselect | link| bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | print preview media fullpage | forecolor backcolor emoticons", 
	   menubar: "insert",
	   setup: function( ch )
	   		 {
	   		 	ch.on('keyup', function (e)
	   		 	{
	   		 		var count = countCharacters();
	   		 		lc.html(count);
	   		 		if ( count > 160 ) 
	   		 		{ 
	   		 			overLimit.html("You have exceed 1 SMS message"); 
	   		 		} else{
	   		 			overLimit.html("");
	   		 		}
	   		 	});
	   		 },
	   style_formats: [
	        {title: 'Bold text', inline: 'b'},
	        {title: 'Red text', inline: 'span', styles: {color: '##ff0000'}},
	        {title: 'Red header', block: 'h1', styles: {color: '##ff0000'}},
	        {title: 'Example 1', inline: 'span', classes: 'example1'},
	        {title: 'Example 2', inline: 'span', classes: 'example2'},
	        {title: 'Table styles'},
	        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
	    ]
 	}); 

	// Set the date field
	$('#noteDate').datepicker();
	
	// Set the focus()
	noteDate.focus();

});

includeSMS.on('change', function()
{
	var me   = $(this);

	if ( me.prop('checked') )
	{

		// Show the letter counter
		showLC.show();

	} else {

		// Hide the value
		showLC.hide();

	} 

	// Return false
	return false;

});

function countCharacters()
{
	var body 	= tinymce.get("comments").getBody();
	var content = tinymce.trim(body.innerText || body.textContent );
	return content.length;
}

