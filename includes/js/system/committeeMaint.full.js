/* JS File for the news article maintenance */
var hostName            = location.hostname;
var urlPath             = '//'+ hostName +  '/cfc/ajaxMayfair.cfc?wsdl';

// Define variables
var form      			= $('#form');
var committeeId    	= $('#committeeId');
var message          = $("#message");
var resultTab 			= $('#resultTab');


// Create the listener for the street
committeeId.on('change', function()
{

	// Submit the form
	form.submit();

});

// Create the listener for the entries
resultTab.on('click', 'a', function()
{
	// Define the variables
	var me   = $(this);
	var id   = me.data("id");
	var sid  = new String(id);

	// Check to see id D 
	if ( sid.indexOf("D-") == -1 )
	{

		// Request a new password
		$.ajax(
		{
			url: urlPath + '&method=setCommitteeChairman',
			type: 'post',
			data: { committeeMemberId : id },
			dataType: 'json',
			success: function ( data )
						{
							// Display message
							message.html(data);

							// Update the value
							$('#C-'+id).html("Yes");
						}
		});

	} else {

		// Remove the D- from the id
		id = id.replace("D-", "");

		// Get the committeeId
		var commitId = me.data("commid");
		
		// Confirm the removal
		if ( confirm("Are you sure you want to remove the committee member? Press OK for Yes, Cancel for No") )
		{

			// Remove the account
			$.ajax(
			{
				url: urlPath + '&method=deleteCommitteeMember',
				type: 'post',
				data: { committeeMemberId: id, commitId : commitId },
				dataType: 'json',
				success: function ( data )
							{

								// Clear the area
								resultTab.html("");
								
								// Display the ne wtable
								resultTab.html(data);

							}
			});

		}
	}

	// Return false
	return false;

});
