/* JS File for the committeeInfo page */
var hostName            = location.hostname;
var urlPath             = '//'+ hostName +  '/cfc/ajaxMayfair.cfc?wsdl';

// Define the fields
var closeBtn      		= $('#closeBtn');
var committeeId   		= $('#committeeId');
var eventDisplay  		= $('#eventDisplay');
var form          		= $('#form');
var locationId    		= $('#locationId');
var logDatePerson       = $('#logDatePerson');
var logEditorArea       = $('#logEditorArea');
var logEntry            = $('#logEntry');
var pastLogs            = $('#pastLogs');
var pastLogsArea        = $('#pastLogsArea');
var resultTab     		= $('#resultTab');
var selectAgainBtn      = $('#selectAgainBtn');

// Create the listener
committeeId.on('change', function()
{

	// Submit the form
	form.submit();

	// Return 
	return false;

});

closeBtn.on('click', function()
{
	// Hide the area
	logEditorArea.hide();

	// Close the data
	logDatePerson.html("");
	logEntry.html("");						

	$('html,body').animate(
	{
		scrollTop: ( $('#eventDisplay').offset().top - 200)
	},100);

	// return false
	return false;

});

// Create the listener
selectAgainBtn.on('click', function()
{

	// Reload the page
	location = 'committeeInfoMgr.cfm';

	// Return false
	return false;

});

// HANDLE LOG ENTRY INFORMATION

// Create the listener
pastLogs.on('click', 'a', function()
{

	// Get the id
	var comLogId = $(this).data('id');

	// Retrieve the information
	$.ajax(
	{
		url: urlPath + '&method=getCommitteeLogMgr',
		type: 'post',
		data: { commLogId : comLogId },
		dataType: 'json',
		success: function ( data )
					{
						// Check the value
						
						// Show the area
						logEditorArea.show();

						// Update the editor
						logDatePerson.html(data.LOGDATE + " - " + data.PERSON);
						logEntry.html(data.LOGTEXT);

						$('html,body').animate(
						{
							scrollTop: ( $('#logEditorArea').offset().top - 80)
						},100);
						

					},

	});

	// Return false
	return false;

});