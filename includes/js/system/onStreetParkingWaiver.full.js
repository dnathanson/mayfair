/* JS File for the on street parking */
var hostName            = location.hostname;
var urlPath             = '//'+ hostName +  '/cfc/ajaxMayfair.cfc?wsdl';

// Define variables for values
var inProcess           = 0;

// Define variables
var addVehicleBtn       = $('#addVehicleBtn');
var carErrorArea        = $('#carErrorArea');
var color               = $('#color');
var deleteDialog        = $('#deleteDialog');
var emailAddress        = $('#emailAddress');
var endDate             = $('#endDate');
var make                = $('#make');
var model               = $('#model');
var name                = $('#name');
var phone               = $('#phone');
var reason              = $('#reason');
var residentId          = $('#residentId');
var saveVehicleBtn      = $('#saveVehicleBtn');
var startDate           = $('#startDate');
var state               = $('#state');
var street              = $('#street');
var tagNo               = $("#tagNo");
var vehicleList         = $('#vehicleList');
var vehicleListDisplay  = $("#vehicleListDisplay");
var vehicleInfo         = $('#vehicleInfo');
var vehicleTabId        = $('#vehicleTabId');
var year                = $('#year');
var yesBtn               = $('#yesBtn');

// Set the focus
$(function()
{
	// Set the focus
	phone.focus();

	// Set the dates calendar fields
	$('#startDate').datepicker();
	$('#endDate').datepicker();

	// Return false
	return false;

});

// Create the listener - addVehicleBtn
addVehicleBtn.on('click', function()
{

	// Define local fields
	var resId    = residentId.val();
	var fone   	 = phone.val();
	var stDate   = startDate.val();
	var edDate   = endDate.val();
	var cause    = reason.val();

	// Show the area
	vehicleInfo.show();

	// Set the focus
	make.focus();

	// Check to see if inProcess == 1
	if ( !inProcess )
	{

		var formData = {
			residentId : resId,
			phone      : fone,
			startDate  : stDate,
			endDate    : edDate,
			reason     : cause
		};

		var formJson = JSON.stringify(formData);

		// Create the request to the server
		$.ajax(
		{
			url: urlPath + '&method=setParkingRequest',
			type: 'post',
			data: { formJson : formJson },
			dataType: 'json',
			success: function ( data )
						{

							// Set inProcess
							inProcess = ( data == "OK" ) ? 1 : 0;
							
						},
		});

	}

	// Return false
	return false;

});

// Create the listener - saveVehicleBtn
saveVehicleBtn.on('click', function()
{

	// Define the data fields
	var err    = "The following errors have occurred:<br /><ul>";
	var errCnt = 0;
	var mk 	  = make.val();
	var md     = model.val();
	var tag    = tagNo.val();
	var st     = state.val();
	var yr     = year.val();
	var clr    = color.val();

	// Check to see if we have values
	if ( mk.length == 0 )
	{
		// Set the error info
		err += "<li>Car Make is blank</li>";
		errCnt++;
	}

	// Check to see if we have values
	if ( md.length == 0 )
	{
		// Set the error info
		err += "<li>Car Model is blank</li>";
		errCnt++;
	}

	// Check to see if we have values
	if ( tag.length == 0 )
	{
		// Set the error info
		err += "<li>Tag No is blank</li>";
		errCnt++;
	}

	// Check to see if we have values
	if ( st.length == 0 )
	{
		// Set the error info
		err += "<li>State Registration is blank</li>";
		errCnt++;
	}

	// Check to see if we have values
	if ( yr.length == 0 )
	{
		// Set the error info
		err += "<li>Car Year is blank</li>";
		errCnt++;
	}

	// Check to see if we have values
	if ( clr.length == 0 )
	{
		// Set the error info
		err += "<li>Color is blank</li>";
		errCnt++;
	}

	if ( errCnt > 0 )
	{
		// Close the ul
		err += "</ul>";

		// Set the error
		carErrorArea.html(err);
		carErrorArea.show();

		// Return false
		return false;

	}

	// Create the formData
	var formData = {
		make  : mk,
		model : md,
		tagNo : tag,
		state : st,
		year  : yr,
		color : clr
	};

	// Convert to json 
	var formJson = JSON.stringify(formData);

	// Add to the request
	$.ajax(
	{
		url : urlPath + '&method=setCar',
		type: 'post',
		data: { formJson : formJson },
		dataType: 'json',
		success: function ( data )
					{

						// Update the area
						vehicleListDisplay.html(data);
						vehicleList.show();

						// Hide the vehicleInfo area 
						vehicleInfo.hide();

						// Clear the fields
						make.val("");
						model.val("");
						tagNo.val("");
						state.val("");
						year.val("");
						color.val("");

					},
	});

});

// Create the listener - vehicleListDisplay
vehicleListDisplay.on('click', 'a', function()
{

	// Define variables
	var me    = $(this);
	var vid   = me.data("id");

	// Update the field
	vehicleTabId.val(vid);
	
	// Prompt for the
	deleteDialog.modal();

	// Return false
	return false;

});

// Create the listener - yesNo
yesBtn.on('click', function()
{
	// Get the vehicle id
	var vid = vehicleTabId.val();

	// Update the record
	$.ajax(
	{
		url : urlPath + '&method=deleteCar',
		type: 'post',
		data: { carId : vid },
		dataType: 'json',
		success: function ( data )
					{
						// Update the area
						vehicleListDisplay.html("");
						vehicleListDisplay.html(data);

						// Hide the modal window
						deleteDialog.modal("hide");

					},
	});

	// Return false
	return false;

});
