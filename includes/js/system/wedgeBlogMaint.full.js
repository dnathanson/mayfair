/* JS File for the news article maintenance */

/* JS File for the signup process */
var hostName            = location.hostname;
var urlPath             = '//'+ hostName +  '/cfc/ajaxMayfair.cfc?wsdl';

// Define variables
var action        = $('#action');
var active        = $('#active');
var blog     		= $('#blog');
var blogDate 		= $('#blogDate');
var blogId        = $('#blogId');
var subject  		= $('#subject');
var prevBlogTable = $('#prevBlogTable');

// Initialize the date picker
$(function()
{
 
	// Set the date picker configuration
	/*    
	$('.datepicker').datepicker(
	{
		format: 'mm/dd/yyyy',
		todayHighlight: 'true',
		autoclose: true
		// startDate: '#DateFormat('01/01/#Year(Now())#', "mm/dd/yyyy")#'
	});
	*/

	tinymce.init(
	{
    	selector: "textarea#blog",
	   theme: "modern",
	   width: '100%',
	   height: 400,
	   mode : 'textarea',
	   plugins: [
	         "advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker",
	         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
	         "save table contextmenu directionality emoticons template paste textcolor"
	   ],
	   content_css: "includes/js/plugins/tinymce/skins/lightgray/content.min.css",
	   toolbar: "insertfile undo redo | styleselect | link| bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | print preview media fullpage | forecolor backcolor emoticons", 
	   menubar: "insert",
	   style_formats: [
	        {title: 'Bold text', inline: 'b'},
	        {title: 'Red text', inline: 'span', styles: {color: '##ff0000'}},
	        {title: 'Red header', block: 'h1', styles: {color: '##ff0000'}},
	        {title: 'Example 1', inline: 'span', classes: 'example1'},
	        {title: 'Example 2', inline: 'span', classes: 'example2'},
	        {title: 'Table styles'},
	        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
	    ]
 	}); 

});

// Create the listener
prevBlogTable.on('click', 'a', function()
{
	// Define the variables
	var me   = $(this);
	var id   = me.data("id");
	var sid  = new String(id);

	// Check to see if the id contains a D in it
	if ( sid.indexOf("D-") == -1 )
	{

		// Get the article
		$.ajax(
		{
			url : urlPath + '&method=getBlogEntryForUpdate',
			type : 'post',
			data : { id : id },
			dataType : 'json',
			success: function ( data )
						{

							// Define the object
							
							// Set the fields
							blogDate.val(data.BLOGDATE);
							subject.val(data.SUBJECT);
							tinymce.activeEditor.setContent(data.BLOG);

							// Check to see if active == 1 
							if ( data.ACTIVE == 1 )
								active.prop('checked', true);

							// Set action
							action.val("update");

							// Place the focus()
							blogDate.focus();

							// Set the id
							blogId.val(id);

							var x = blogId.val();
							// console.log(x);

						}
		});

	} else {

		// Remove the D- from the ID
		id = id.replace("D-","");
		
		// Display a message for verification
		if ( confirm("Are you sure you want to remove this blog entry? Press OK for Yes, Cancel for No") )
		{

			// Remove the blog entry
			$.ajax(
			{
				url: urlPath + '&method=removeBlogEntry',
				type: 'post',
				data: { id : id },
				success: function ( data )
							{

								// Do a location to the same page
								location = 'wedgeBlogMaint.cfm';

							}
			});

		}

	}

	// Return false
	return false;
});
