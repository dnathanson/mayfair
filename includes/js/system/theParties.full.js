/* The JS file for theParties web page */
var closeHallowBtn	 = $('#closeHallowBtn');
var closeHallow2016   = $('#closeHallow2016Btn');
var closePoolBtn		 = $('#closePoolBtn');
var closePool2016Btn  = $('#closePool2016Btn');
var halloween     	 = $('#halloween');
var halloween2016     = $('#halloween2016');
var party2000         = $('#party2000');
var party2016         = $('#party2016');
var viewHalloween 	 = $('#viewHalloween');
var viewHalloween2016 = $('#viewHalloween2016');
var viewPool          = $('#viewPool');
var viewPool2016      = $('#viewPool2016');

$(function()
{
	
	// Preload the images
	$.preload( partyList );

	// Preload the halloween pics
	$.preload( hallowList );

	$.preload( partyList2016);

	$.preload( hallow16JSList);

	// Check for picJs
	if ( picJs == 1 )
	{
		// Show the area
		halloween2016.show();

		$('html,body').animate(
		{
			scrollTop: ( $('#halloween2016').offset().top - 100)
		},100);

	}
});

// Create the listener for the pictures area
viewHalloween.on('click', function()
{
	halloween.show();

	$('html,body').animate(
	{
		scrollTop: ( $('#halloween').offset().top - 100)
	},100);

	// return false
	return false;

});

// Create the listner for the process
viewHalloween2016.on('click', function()
{
	halloween2016.show();

	$('html,body').animate(
	{
		scrollTop: ( $('#halloween2016').offset().top - 100)
	},100);

	// return false
	return false;
});

// Create a listener for the pictures area
viewPool.on('click', function()
{
	// Show the area
	party2000.show();

	$('html,body').animate(
	{
		scrollTop: ( $('#party2000').offset().top - 100)
	},100);

	// Return false
	return false;

});

// Create a listener for the pictures area
viewPool2016.on('click', function()
{
	// Show the area
	party2016.show();

	$('html,body').animate(
	{
		scrollTop: ( $('#party2016').offset().top - 100)
	},100);

	// Return false
	return false;

});


// Create the listener to close the area
closeHallowBtn.on('click', function()
{
	// close the window
	halloween.hide();

	// Return false
	return false;
});

// Create the listener to close the area
closeHallow2016.on('click', function()
{

	// Hide the area
	halloween2016.hide();

	// Return false
	return false;

});

// Create the listener to close the area 
closePoolBtn.on('click', function()
{
	// close the window
	party2000.hide();

	// Return false
	return false;
});

closePool2016Btn.on('click', function()
{

	// close the party pictures
	party2016.hide();

	$('html,body').animate(
	{
		scrollTop: ( $('#main').offset().top - 100)
	},100);

	// Return false
	return false;
});