/* JS File for the committeeInfo page */
var hostName            = location.hostname;
var urlPath             = '//'+ hostName +  '/cfc/ajaxMayfair.cfc?wsdl';

// Define the fields
var addNewEventDisplay  = $('#addNewEventDisplay');
var addEventId          = $('#addEventId');
var calEventBtn   		= $('#calEventBtn');
var closeBtn      		= $('#closeBtn');
var closeEventBtn 		= $('#closeEventBtn');
var closeEventEntryBtn  = $('#closeEventEntryBtn');
var closePastLogView    = $('#closePastLogView');
var comments 				= $('#comments');
var committeeId   		= $('#committeeId');
var deleteDialog        = $('#deleteDialog');
var editAddDialog 		= $('#editAddDialog');
var endDate       		= $('#endDate');
var endTime       		= $('#endTime');
var errors              = $('#errors');
var eventDisplay  		= $('#eventDisplay');
var eventId             = $('#eventId');
var eventTab      		= $('#eventTab');
var eventTableArea      = $('#eventTableArea');
var eventTitle    		= $('#eventTitle');
var form          		= $('#form');
var locationId    		= $('#locationId');
var logAction           = $('#logAction');
var logArea             = $('#logArea');
var logCloseBtn         = $('#logCloseBtn');
var logEntryBtn         = $('#logEntryBtn');
var logError            = $('#logError');
var logId               = $('#logId');
var newEventBtn         = $('#newEventBtn');
var otherLocation 		= $('#otherLocation');
var pastLogs            = $('#pastLogs');
var pastLogsArea        = $('#pastLogsArea');
var resultTab     		= $('#resultTab');
var saveEventBtn        = $('#saveEventBtn');
var saveLogBtn          = $('#saveLogBtn');     
var sendEmailBtn 			= $('#sendEmailBtn');
var sendMsgArea			= $('#sendMsgArea');
var sendNotice    		= $('#sendNotice');
var shortDescription    = $('#shortDescription');
var startDate           = $('#startDate');
var startTime           = $('#startTime');
var viewOtherLogs       = $('#viewOtherLogs');
var yesBtn              = $('#yesBtn');

// Do initialization
$(function()
{

	function initMCEall()
	{

		// selector: "textarea#comments",
		tinymce.init(
		{
	    	mode : "specific_textareas",
	    	editor_selector : "mceEditor",
		   theme: "modern",
		   width: '100%',
		   height: 400,
		   plugins: [
		         "advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker",
		         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
		         "save table contextmenu directionality emoticons template paste textcolor"
		   ],
		   content_css: "includes/js/plugins/tinymce/skins/lightgray/content.min.css",
		   toolbar: "insertfile undo redo | styleselect | link| bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | print preview media fullpage | forecolor backcolor emoticons", 
		   menubar: "insert",
		   style_formats: [
		        {title: 'Bold text', inline: 'b'},
		        {title: 'Red text', inline: 'span', styles: {color: '##ff0000'}},
		        {title: 'Red header', block: 'h1', styles: {color: '##ff0000'}},
		        {title: 'Example 1', inline: 'span', classes: 'example1'},
		        {title: 'Example 2', inline: 'span', classes: 'example2'},
		        {title: 'Table styles'},
		        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
		    ]
	 	}); 
	}

	initMCEall();

	// Set the dates calendar fields
	$('#startDate').datepicker();
	$('#endDate').datepicker();

});

// Create the listener
committeeId.on('change', function()
{

	// Submit the form
	form.submit();

	// Return 
	return false;

});

startTime.on('blur', function()
{

	// Define variables
	var me    = $(this);
	var tm    = me.val().toUpperCase();

	// Check to see if the value contains A or P
	if ( tm.indexOf("P") > 0 || tm.indexOf("A") > 0 )
	{
		// Replace the values
		var oldTM = tm;
		tm = tm.replace(/A|P|M/gi,'');
		
		// Parse out the 
		var parts = tm.split(/\:/);
		var hr    = parts[0];
		
		if ( oldTM.indexOf("P") > 0 )
		{
			// Check to see if the time is 12:00
			if ( tm != "12:00" )
			{
				// Add 12 to it
				hr = parseInt(hr) + 12;
			}

			// Add 12 to it
			hr = parseInt(hr) + 12;
		} 

		// Update the field
		startTime.val(hr+":"+parts[1]);
	}

	// Return false
	return false;

});

endTime.on('blur', function()
{

	// Define variables
	var me    = $(this);
	var tm    = me.val().toUpperCase();

	// Check to see if the value contains A or P
	if ( tm.indexOf("P") > 0 || tm.indexOf("A") > 0 )
	{
		// Replace the values
		var oldTM = tm;
		tm = tm.replace(/A|P|M/gi,'');
		
		// Parse out the 
		var parts = tm.split(/\:/);
		var hr    = parts[0];
		
		if ( oldTM.indexOf("P") > 0 )
		{
			// Check to see if the time is 12:00
			if ( tm != "12:00" )
			{
				// Add 12 to it
				hr = parseInt(hr) + 12;
			}
			
			// Add 12 to it
			hr = parseInt(hr) + 12;
		} 

		// Update the field
		endTime.val(hr+":"+parts[1]);
	}

	// Return false
	return false;

});

// Create the listener
sendEmailBtn.on('click', function()
{

	// Display the page
	sendMsgArea.show();

	// Hide the button
	sendEmailBtn.hide();

	// Set the focus()
	tinymce.execCommand('mceFocus',false, 'comments');

	// Return false
	return false;

});

// Create a listener for the delete
resultTab.on('click', 'a', function()
{

	// Define fields
	var id     	 = $(this).data("id");
	var commitId = $(this).data("commitid");
	 
	// Update the record
	$.ajax(
	{
		url : urlPath + '&method=deleteCommitteeMember',
		type: 'post',
		data: { committeeMemberId : id, commitId : commitId },
		success: function ( data )
					{

						// Re-display the page
						location = 'committeeInfo.cfm';

					}
	});

	// Return false
	return false;

});

// Create the listener for the new event modal window
newEventBtn.on('click', function()
{

	// Show the modal window
	// editAddDialog.modal();
	$('#addNewEventDisplay').show();
	eventId.val(0);

	// Set sendNotice to checked
	sendNotice.prop('checked', true);

	// Return false
	return false;

});

// Create the listener for the locationId change (in the modal window)
locationId.on('change', function()
{

	// Get the value
	var locId = $(this).val();

	// Check the value
	if ( locId == 1 )
	{
		// Get the other fields
		var stDate = startDate.val();
		var stTime = startTime.val();

		// Check the events
		$.ajax(
		{
			url: urlPath + '&method=checkEventAvailable',
			type: 'post',
			data: { locId : locId, sDate : stDate, sTime : stTime },
			success: function ( data )
						{
							// Check the results
							if ( data.indexOf('busy') > -1 )
							{
								// Update errors
								var errLine = "IT WOULD APPEAR THAT THE CLUBHOUSE IS BEING USED THEN<br /><br />";
								$('#errors').html(errLine);
 	 						}
 						},
		});

	}

	// Return false
	return false;

});

// Create the listener for the eventTab
eventTableArea.on('click', 'a', function()
{

	// Define variables 
	var me = $(this);
	var id = me.data('id');

	if ( id.indexOf("D-") > -1 )
	{

		// Remove the D- from the id
		id = id.replace("D-", "");
		sel = $('#selValue').val();

		// Confirm the removal
		if ( confirm("Are you sure you want to remove the event from the calendar? Press OK for Yes, Cancel for No") )
		{

			// Remove the account
			$.ajax(
			{
				url: urlPath + '&method=removeEvent',
				type: 'post',
				data: { id: id, committeeId : sel },
				dataType: 'json',
				success: function ( data )
							{

								// Re-display the page
								eventTableArea.html(data);

								//location = 'eventMaint.cfm';

							}
			});

		}


	} else {

		id = id.replace("E-","");

		// Make the ajax call
		$.ajax(
		{
			url: urlPath + '&method=editEvent',
			type: 'post',
			data: { eventId : id },
			dataType: 'json',
			success: function ( data )
						{

							// Load the fields
							eventId.val(data.EVENTID);
							eventTitle.val(data.TITLE);
							startDate.val(data.STARTDATE);
							startTime.val(data.STARTTIME);
							endDate.val(data.ENDDATE);
							endTime.val(data.ENDTIME);
							shortDescription.val(data.SHORTDESC);
							committeeId.val(data.COMMITTEEID).prop('selected', true);
							locationId.val(data.LOCATIONID).prop('selected', true);
							otherLocation.val(data.OTHERLOCATION);

							// Check the send notice
							if ( data.SENDNOTICE )
								sendNotice.prop('checked', true);


						}
		});

		// Show modal window
		// editAddDialog.modal();
		addNewEventDisplay.show();

	}

	// Return false
	return false;

});

yesBtn.on('click', function()
{

	// Close modal window
	deleteDialog.modal('hide');

	// Get the id
	var id 		 = eventId.val();
	var commitId = committeeId.val();
	
	// Remove the record from the events
	$.ajax(
	{
		url: urlPath + '&method=deleteEvent',
		type: 'post',
		data: { eventId : id },
		success: function ( data )
					{

						// Make another ajax call to retrieve the list of events
						$.ajax(
						{
							url: urlPath + '&method=getCommitteeEvents',
							type: 'post',
							data: { committeeId : commitId },
							dataType: 'json',
							success: function ( data )
										{
											
											// Clear the table and append the value
											eventTableArea.html("");
											eventTableArea.append(data);
											
											// Return false
											return false;
										}
						});
					},
	});

	// Return false
	return false;

});

// Create the listener for the eventSaveBtn
saveEventBtn.on('click', function(event)
{

	// Define fields
	var title 				= eventTitle.val();
	var stDate 				= startDate.val();
	var stTime 				= startTime.val();
	var edDate  			= endDate.val();
	var edTime 				= endTime.val();
	var evId             = eventId.val();
	var shortDesc        = shortDescription.val();
	var location         = locationId.val();
	var otherLoc         = otherLocation.val();
	var sendNote   		= $('#sendNotice').prop('checked');
	var commitId         = committeeId.val();
	var blkStTime        = stTime.split(/\:/);
	var blkEdTime        = edTime.split(/\:/);	
	var displayErrors    = "<ul>";
	var errors           = 0;

	// Check the dates
	var chStDate = !/Invalid|NaN/.test(new Date(stDate));
	var chEdDate = !/Invalid|NaN/.test(new Date(edDate));
	var chStTime = false;
	var chEdTime = false;

	// Check the time
	if ( ( stTime.length == 0 ) || 
		  ( blkStTime[0] > 24 || blkStTime[1] > 59 ) )
	{
		chStTime = false;
	} else {
		chStTime = true;
	}

	if ( ( edTime.length == 0 ) || 
		  ( blkEdTime[0] > 24 || blkEdTime[1] > 59 ) )
	{
		chEdTime = false;
	} else {
		chEdTime = true;
	}

	// Test for title
	if ( !title.length )
	{
		displayErrors += "<li>Event Title is missing</li>";
		errors++;
	}

	// Check for start date
	if ( !chStDate )
	{
		displayErrors += "<li>Start Date is either missing or invalid</li>";
		errors++;
	}

	// Check the start time
	if ( !chStTime )
	{
		displayErrors += "<li>Start Time is either missing or invalid</li>";
		errors++;
	}

	// Check for end date
	if ( !chEdDate )
	{
		displayErrors += "<li>End Date is either missing or invalid</li>";
		errors++;
	}

	// Check the end time
	if ( !chEdTime )
	{
		displayErrors += "<li>End Time is either missing or invalid</li>";
		errors++;
	}

	// Check for short description
	if ( shortDesc.length == 0 )
	{
		displayErrors += "<li>Short Description is missing</li>";
		errors++;
	}

	if ( location == 0 && otherLoc.length == 0 )
	{
		displayErrors += "<li>You must select a location from either the location or enter in other location</li>";
		errors++;
	}

	// Check to see if there are errors
	if ( errors )
	{

		// Close the messages
		displayErrors += "</ul>";

		// display the modal window
		$('#errors').html(displayErrors);

	} else {

		// Create a js object
		var formData   = {
			eventId           : evId,
			title             : title,
			startDate         : stDate,
			startTime         : stTime,
			endDate           : edDate,
			endTime           : edTime,
			shortDescription  : shortDesc,
			committeeId       : commitId,
			locationId        : location,
			otherLocation     : otherLoc,
			sendNotice        : sendNote
		};

		// Make a json object
		var formJson = JSON.stringify(formData);
		var meth     = ( evId == 0 ) ? "addCommEvent" : "updateCommEvent";

		// Call the server to 
		$.ajax(
		{
			url: urlPath + "&method="+meth+'&',
			type: "post",
			data: { formJson : formJson },
			success: function ( data )
						{

							// Clear the fields and close the modal window
							eventId.val("");
							eventTitle.val("");
							startDate.val("");
							startTime.val("");
							endDate.val("");
							endTime.val("");
							shortDescription.val("");
							sendNotice.prop('checked', false);
							locationId.val(0);
							otherLocation.val("");

							// Close the window
							addNewEventDisplay.hide();

							$('html,body').animate(
							{
								scrollTop: ( $('#eventDisplay').offset().top - 80)
							},100);

							// Call ajax function
							$.ajax(
							{
								url: urlPath + '&method=getCommitteeEvents',
								type: 'post',
								data: { committeeId : commitId },
								dataType: 'json',
								success: function ( data )
											{
												
												// Prevent the default behavior
												event.preventDefault();

												// Clear the table and append the value
												eventTableArea.html("");
												eventTableArea.append(data);
												
												// Return false
												return false;
											}
							});

						}
		});

	}

	// Return false
	return false;

});

// Create the listener 
closeBtn.on('click', function()
{
	// Clear the value
	comments.val("");

	// Hide the button
	sendEmailBtn.show();

	// Hide the area
	sendMsgArea.hide();

	// Return false
	return false;

});

// Create the listener
closeEventEntryBtn.on('click', function()
{
	// Hide the area
	addNewEventDisplay.hide();

	// Return false
	return false;

});

// Create the listener
calEventBtn.on('click', function()
{
	var commitId = committeeId.val();
	
	// Retrieve the list of events for that committee
	$.ajax(
	{
		url: urlPath + '&method=getCommitteeEvents',
		type: 'post',
		data: { committeeId : commitId },
		dataType: 'json',
		success: function ( data )
					{
						eventTableArea.html(data);
					}
	});

	// Show the area
	eventDisplay.show();
	calEventBtn.hide();

	// Return false
	return false;

});

closeEventBtn.on('click', function()
{

	// Close the area
	eventDisplay.hide();

	// Show the button
	calEventBtn.show();

	// Return false
	return false;


});

// HANDLE LOG ENTRY INFORMATION

// Create the listener for the log area
logEntryBtn.on('click', function()
{

	// Show the area
	logArea.show();

	$('html,body').animate(
	{
		scrollTop: ( $('#logArea').offset().top - 80)
	},100);

	tinymce.get('logEntry').focus();

	// Return false
	return false;

});

// Create the listener
logCloseBtn.on('click', function()
{
	logArea.hide();
	tinymce.get('logEntry').setContent("");

	// Return false
	return false;

});

// Create the listener
saveLogBtn.on('click', function()
{

	// Submit the form
	$('#logForm').submit();

	// Return false
	return false;

});

// Create the listener
viewOtherLogs.on('click', function()
{
	// Show the area
	pastLogsArea.show();

	$('html,body').animate(
	{
		scrollTop: ( $('#pastLogsArea').offset().top)
	},100);

	// Return false
	return false;

});

// Create the listener
closePastLogView.on('click', function()
{

	// Hide the area
	pastLogsArea.hide();

	// Close the data
	tinymce.get('logEntry').setContent("");						

	$('html,body').animate(
	{
		scrollTop: ( $('#page-wrapper').offset().top - 200)
	},100);

	// return false
	return false;
});

// Create the listener
pastLogs.on('click', 'a', function()
{

	// Get the id
	var comLogId = $(this).data('id');

	// Retrieve the information
	$.ajax(
	{
		url: urlPath + '&method=getCommitteeLog',
		type: 'post',
		data: { commLogId : comLogId },
		dataType: 'json',
		success: function ( data )
					{
						// Check the value
						if ( data == "None" )
						{

							// Set message
							logError.show();
							$('#logErrorMsg').html("There is no log for this entry<br />");
							

						} else {

							// Update the editor
							tinymce.get('logEntry').setContent(data);
							logId.val(comLogId);
							logAction.val("update");

							// Set the focus
							tinymce.get('logEntry').focus();

						}

						// Hide the other logs
						pastLogsArea.hide();

						$('html,body').animate(
						{
							scrollTop: ( $('#logArea').offset().top - 80)
						},100);

					},

	});

	// Return false
	return false;

});