<!doctype html>
<!---                                                      
    CF Name:    theBoard.cfm

    Description:
      This module will display the board of directors information.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	titlePageName = "Board of Director's";

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-users accent4"></span>
				<br />
				<h2>Your Board of Directors</h2>
				<p>
					No community can exist without the help of its residents who decide to give of their time and efforts to make the 
					place work. We are no different. Below is information about the current board of directors for Mayfair and what positions 
					they hold, when their term is over and some general information about them.
					<br />
			   </p>
			</header>
			<section class="box special features">
				<div class="features-row">
					<section>
						<span class="icon major fa-user accent5"></span>
						<h3>Steve Capandonis</h3>
						<p style="text-align:left;">
							Title: President/Treasurer<br />
							Term: 2 Year Position, Ends in March 2018<br />
							<br />
							Steve has been a Mayfair resident for over 20 years and 
							recently retired from American Express after 34 years. Steve
							brings new energy to the board and has plans to make our 
							community active again.
						</p>
					</section>
					<section>
						<span class="icon major fa-user accent5"></span>
						<h3>Amy Cook</h3>
						<p style="text-align:left;">
							Title: Secretary<br />
							Term: 2 Year Position, Ends in March 2018<br />
							<br />
							Amy has been a resident of Mayfair since the beginning and 
							bring an understanding of the needs of parents and how
							best to serve them in our community.
						</p>
					</section>
				</div>
				<div class="features-row">
					<section>
						<span class="icon major fa-user accent5"></span>
						<h3>Charles Wenzel</h3>
						<p style="text-align:left;">
							Title: Director<br />
							Term: 2 Year Position, Ends in March 2019<br />
							<br />
							Charles has been a resident of Mayfair for a long time and has 
							helped in many of the events over the years. We know Charles will 
							be helpful in the many events planned for this community.  
						</p>
					</section>
					<section>
						<h3>&nbsp;</h3>
						<p style="text-align:left;">
							&nbsp;
						</p>
					</section>
				</div>
			</section>
		
		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>