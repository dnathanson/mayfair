<!doctype html>
<!---                                                      
    CF Name:    suggestions.cfm

    Description:
      This module will allow homeowners the ability to send emails to the board of directors.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	titlePageName = "Suggestions";

	// Check to see if action exists
	if ( structKeyExists(form, "action") && structKeyExists(session, "userInfo") )
	{

		// Create the object
		smtpObj  	= createObject("component", "#application.cfcPath#smtp");
		smtpStruct  = smtpObj.getSMTPinfo();
		// Create the object
		bodRecs    	= queryExecute("Select A.name, B.emailAddress from bod A, residents B where A.residentId = B.residentId");
		propRec    	= queryExecute("Select emailAddress from residents where residentId = -1");

		bodEmails  	= valueList(bodRecs.emailAddress,", ");
		bodEmails  &= ", " & propRec.emailAddress & ", " & session.userInfo.emailAddress;

		// Create body information
		saveContent variable="body"
		{
			writeOutput("
The following information written by: #session.userInfo.name#<br /><br />
#form.comments#<br /><br />
#session.userInfo.name#<br />
<a href='mailto:#session.userInfo.emailAddress#'>#session.userInfo.emailAddress#</a>");

		}

		// Create a mail object
		mail = new mail(type="html", charset="utf-8", body="#body#");
		mail.setFrom("noreply@mayfairatparkland.com (Mayfair at Parkland Website)");
		mail.setTo("#bodEmails#");
		mail.setBcc("drew@technicalsynergy.com");
		mail.setSubject("Homeowner Comment for Mayfair BOD");
		mail.setServer(smtpStruct.host);
		mail.setPort(smtpStruct.port);
		mail.setUsername(smtpStruct.user);
		mail.setPassword(smtpStruct.pass);
		mail.setUseTLS(true); 

		// Send the email
		mail.send();
		
		// Set display message
		compMessage = "Your email has been sent to the Board of Directors";

	}

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false,
		includeTiny     : true,
		jsInclude       : ["suggestion.#request.jsType#.js?#cacheValue#"]
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-lightbulb-o accent5"></span>
				<br />
				<h2>Your Suggestions</h2>
				<p>
					The Board of Director's (BOD) wants to hear from you. If you have a suggestion, comment or a 
					way to improve the community, they are listening. Fill out the form below and we will forward a copy 
					of your comments to each of the BOD members.
					<br />
			   </p>
			   <cfif len(compMessage)>
				   <div class="msgFormat">#compMessage#</div>
				</cfif>
			</header>
			<section class="box ">
				<cfif !structKeyExists(session, "userInfo")>
					In order to write a comment, please <b>log into the web site</b>. This will allow you the ability 
					to send your comments to the BOD.
				<cfelse>
					<form method="post" action="suggestions.cfm" name="form" id="form">
					<input type="hidden" name="action" value="sendComment" />
					<textarea name="comments" id="comments"></textarea>
					<br /><br />
					<div class="4u 12u(mobilep)">
						<input type="submit" id="sendBtn" value="Send My Comments" class="fit" />
					</div>
					</form>

				</cfif>

			</section>

		
		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<script>
		var isMobile = "#session.isMobile#";
		var isPhone  = "#session.isPhone#";
	</script>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>