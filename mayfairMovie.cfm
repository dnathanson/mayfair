<!doctype html>
<!---                                                      
    CF Name:    mayfairMovie.cfm

    Description:
      This module will allow display the mayfair movie.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	titlePageName = "Mayfair Movie";

	// Set the structure with information to the request scope
	str    = {
		jsInclude       : [ ]
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />

<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-video-camera accent5"></span>
				<br />
				<h2>Our Neighborhood Movie</h2>
				<p>
					The following is a movie that was created for Mayfair by one of the residents 
					who wanted to showcase our lives.
					<br />
			   </p>
			</header>
			<section class="box ">
				<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab##version=7,0,0,0" width="800" height="600" id="Mayfair" align="middle">
					<param name="allowScriptAccess" value="sameDomain" />
					<param name="movie" value="/flash/Mayfair.swf" />
					<param name="quality" value="high" />
					<param name="bgcolor" value="##ffffff" />
					<embed src="/flash/Mayfair.swf" quality="high" bgcolor="##ffffff" width="800" height="600" name="Mayfair" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
				</object>	
				
			</section>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>