<!doctype html>
<!---                                                      
    CF Name:    ourDocuments.cfm

    Description:
      This module will allow you to sign up to the mayfair at parkland
      HOA web site.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	msgColor      = "red";
	titlePageName = "Our Documents";

	// Create the objects
	resObj  = createObject("component", "#application.cfcPath#residents");
	miscObj = createObject("component", "#application.cfcPath#Misc");

	// Get any Parameters
	miscObj.getURLVariables();

	// Check the results
	if ( structKeyExists(url, "m") )
	{
		// Set the message
		compMessage = "An email has been sent allowing you to change your password.";
		msgColor    = "green";
	}

	// Check to see if form.eventBtn
	if ( structKeyExists(form, "eventBtn") )
	{

		// Get the resident information
		resRec = resObj.getResidents(where="emailAddress = '#form.email#' and password = '#form.password#'");

		// Check the results
		if ( resRec.recordCount )
		{

			// Set the information
			Session.userInfo = miscObj.loadQueryToStruct(qName="#resRec#");

			// Set the home page
			location ( url="/index.cfm",addtoken=false);
			
		}

	}

	// Set the structure with information to the request scope
	str    = {
		bootstrap     : true
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 75%">
			<header>
				<span class="icon major fa-file-o accent3"></span>
				<br />
				<h2>Our Documents</h2>
				<p>
					The following are Mayfair at Parkland Community Documents. 
			   </p>
			</header>
			
			<section class="box special features">
			<div class="features-row">
				<section>
					<span class="icon major fa-home accent2"></span>
					<h3>Mayfair HOA Documents</h3>
					<p style="text-align:left;">
						These documents are comprised of The Declaration of Convenants, Restrictions and Easements; Article of
						Incorporation; and By-Laws.
						<br />
						<div>
							<cfif !structKeyExists(session, "userInfo")>
								<span style="float:clear;float:bottom;text-align:center;">
								<ul class="actions">
									<li><a href="/login.cfm" class="button small" style="background-color:##e89980">Log In to View</a></li>
								</ul>
								</span>
							<cfelse>
								<ul>
									<li style="text-align:left;"><a href="/displayDoc.cfm/typeb/1" target="new">Mayfair By-Laws</a></li>
									<li style="text-align:left;"><a href="/displayDoc.cfm/typet/1" target="new">Terramar Master Documents</a></li>
									<li style="text-align:left;"><a href="/displayDoc.cfm/typet/2" target="new">Terramar Master Design Document</a></li>
								</ul>
							</cfif>
						</div>
					</p>
				</section>
				<section>
					<span class="icon major fa-file-o accent4"></span>
					<h3>Forms</h3>
					<p>
						Commonly requested forms from our Property Manager, Club House Rental, Architectural Review Request, Pool Pass, etc.
						<br /><br />
						<b>Note, we have improved the <i>ON-STREET</i> process. In order to request on-street parking, please use the link
						within your menu to expedite the process. However, if you still wish to use the paper version, please click
						on the Street Parking Waiver below.</b>
						<div>
							<cfif !structKeyExists(session, "userInfo")>
								<span style="float:clear;float:bottom;text-align:center;">
								<ul class="actions">
									<li><a href="/login.cfm" class="button small" style="background-color:##90b0ba">Log In to View</a></li>
								</ul>
								</span>
							<cfelse>
								<ul>
									<li style="text-align:left;"><a href="/pdf/ARB-Form.pdf" target="new">Architectural Review Board Form</a></li>
									<li style="text-align:left;"><a href="/pdf/RevisedMayfairColorPalate-March2007.pdf" target="new">Approved Paint Color Chart</a></li>
									<li style="text-align:left;"><a href="/pdf/MAP-CLUBHOUSE-RELEASE.pdf" target="new">Clubhouse Release Form</a></li>
									<li style="text-align:left;"><a href="/pdf/MAP-Gym-&-Pool-Pass.pdf" target="new">Gym &amp; Pool Pass Form</a></li>
									<li style="text-align:left;"><a href="/pdf/MAP-GARING-STREET-PARKING-BLANK.pdf" target="new">Street Parking Waiver</a></li>
									<li style="text-align:left;"><a href="/pdf/MAP-Coupon-Letter.pdf" target="new">Payment Coupon/Instructions</a></li>
									<li style="text-align:left;"><a href="/pdf/MAP-PermanentListForm.pdf" target="new">Permanent List Form</a></li>
								</ul>
								<br /><br /><br />
							</cfif>
						</div>
					</p>
				</section>
			</div>
		</section>


		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>