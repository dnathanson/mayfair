<!doctype html>
<!---                                                      
    CF Name:    theBeginning.cfm

    Description:
      This module will display the history of mayfair at parkland
      HOA web site.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>


	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	cssPicStyle   = ( session.isMobile ) ? "max-width:225%;" : ""; // "max-width:225%;"; // "width:600px;height:414px;";
	cssPicStyle   = ( session.isPhone ) ? "max-width:100%" : cssPicStyle;
	compMessage   = "";
	emailAddress  = "";
	titlePageName = "In the Beginning";

	// Create the objects
	comObj = createObject("component", "#application.cfcPath#Committees");

	comRecs = comObj.getCommittees(OrderBy="CommitteeName");

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<cfoutput>
<body>

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 75%">
			<header>
				<span class="icon major fa-home accent4"></span>
				<br />
				<h2>Our History</h2>
				<!--- <cfoutput>MOBILE = #session.isMobile#<br />PHONE = #session.isPhone#<br /></cfoutput> --->
				<p>
					Building in our community began in 1994 and was to include 245 homes. The first resident moved into
					the property in February 1995. As homes become completed, more neighbors moved in. For those residents 
					that have been here since the beginning (and there are a lot of us), we remember how the community 
					evolved. 
				</p>
				<p>
					We have had some really nice community parties in the past, check out the pictures from those events.
					... <a href="theParties.cfm">View the Pictures</a>
				</p>
				<p>
					Below are some arial pictures taken by a former resident (Charles Ballo) from his plane during the 
					initial phase of the community.
			   </p>
			   
			</header>
			<section class="box special features">
				<div class="features-row">
					<section>
						<img src="includes/img/earlyMayfair/mayfair11.png" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="includes/img/earlyMayfair/mayfair21.png" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row">
					<section>
						<img src="includes/img/earlyMayfair/mayfair31.png" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="includes/img/earlyMayfair/mayfair41.png" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row">
					<section>
						<img src="includes/img/earlyMayfair/mayfair51.png" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
			</section>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>