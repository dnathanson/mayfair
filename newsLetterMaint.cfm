<!doctype html>
<!---                                                      
    CF Name:    newsLetterMaint.cfm

    Description:
     

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Check permissions
	if ( !structKeyExists(session, "userInfo") || 
		  ( !structKeyExists(session.userInfo, "permission") || 
		    ( session.userInfo.permission.blog == 1 || 
		    	session.userInfo.permission.committee == 1 
		    ) 
		  )
		)
	{
		// Send to home page
		location ( url="/index.cfm", addToken=false );
	}

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	titlePageName = "News Letter Maint";

	// Check to see if action exists
	if ( structKeyExists(form, "action") )
	{

		// Check to see what action is set to
		if ( form.action == "add" )
		{

			// Check for value entries
			artDate = ( isDate(form.articleDate) ) ? form.articleDate : dateFormat(now(),"mm/dd/yyyy");
			artHead = ( len(form.articleHead) )    ? form.articleHead : "News Article for Today - #dateFormat(now(),"mm/dd/yyyy")#";

			// Check to see if there is any article
			if ( !len(form.article) )
			{
				// Set the message
				compMessage = "There is no article to POST therefore not entries was performed.";
				className   = "msgFormatError";

			} else {

				// Build the structure
				strList  = {
					articleDate : artDate,
					articleHead : artHead,
					article     : article
				};

				// Insert the article into the database
				queryExecute("Insert into communityNews ( articleDate, articleHead, article ) 
					            values ( :date, :head, :art )",
					            {
					            	date : { cfsqltype:"CF_SQL_DATE",        value: artDate },
					            	head : { cfsqltype:"CF_SQL_VARCHAR",     value:artHead },
					            	art  : { cfsqltype:"CF_SQL_LONGVARCHAR", value:form.article }
					            });

				compMessage = "Your article has been saved.";

				// Check for email
				if ( application.useEmail )
				{

					// Create the objects
					resObj 		= createObject("component", "#application.cfcPath#residents");
					smtpObj  	= createObject("component", "#application.cfcPath#smtp");

					// Get the email addresses
					resRecs 		= resObj.getResidents(selectLine="emailAddress",where="notifications = 1",orderBy="emailAddress");
					resList  	= valueList(resRecs.emailAddress,",");

					// Get the smtp informaetion
					smtpStruct  = smtpObj.getSMTPinfo();
					compMessage = "Your email has been sent.";

					// Set body to the form
					saveContent variable="body"
					{
						writeOutput("
Dear Homeowner,<br />
A new news article is available on the Mayfair at Parkland Web Site. <br /><br />
<a href='#application.httpPath#communityNews.cfm'>#application.httpPath#communityNews.cfm</a><br /><br />
Thank you for your continued interest in our community.<br />
Your Board of Director's.");
					}

					// Process the emails
					for ( email in resList )
					{

						// Create a mail object
						mail = new mail(type="html", charset="utf-8", body="#body#");
						mail.setFrom("noreply@mayfairatparkland.com (Mayfair at Parkland Website)");
						mail.setTo("#email#");
						mail.setSubject("#form.articleHead#");
						mail.setServer(smtpStruct.host);
						mail.setPort(smtpStruct.port);
						mail.setUsername(smtpStruct.user);
						mail.setPassword(smtpStruct.pass);
						mail.setUseTLS(true);

						// Send the email
						mail.send();

					}

				}

				// Check for message
				if ( application.useTwilio )
				{
					// Credate the object
					smsObj 		= createObject("component", "#application.cfcPath#sms");
					if ( !isDefined("resObj") )
						resObj 	= createObject("component", "#application.cfcPath#residents");

					// Get all phone numbers
					resRecs 	 	= resObj.getResidents(selectLine="cellPhone", where="sms = 1 and length(cellphone) > 0");
					phoneList 	= valueList(resRecs.cellPhone, ","); 

					// Get the phone numbers
					smsObj.sendSMS(phoneList="#phoneList#",msg="New community newsletter is available at: #application.httpPath#communityNews.cfm");

				}

			} 

		} else if ( form.action == "udpate" ) {

			// Insert the article into the database
			queryExecute("Update communityNews
			               Set articleDate = :dt,
			                   articleHead = :hd,
			                   article     = :art
			              where commId = :id", 
				            {
				            	dt   : { cfsqltype:"CF_SQL_DATE",        value: dateFormat(form.articleDate, "yyyy-mm-dd") },
				            	hd   : { cfsqltype:"CF_SQL_VARCHAR",     value:form.articleHead },
				            	art  : { cfsqltype:"CF_SQL_LONGVARCHAR", value:form.article },
				            	id   : { cfsqltype:"CF_SQL_INTEGER",     value:form.artId }
				            });

			// Update the message
			compMessage = "Your article has been saved.";
		}

	}

	// Build the date range
	if ( Month(Now()) == 1 )
	{
		startDate = Year(DateAdd("yyyy",-1,Now())) & "-01-01";
	} else {
		startDate = Year(Now()) & "-01-01";
	}
	endDate   = Year(Now()) & "-12-31";

	// Create the object
	artRecs = queryExecute("Select * from communityNews where articleDate between '#startDate#' and '#endDate#' Order by articleDate desc");

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false,
		includeTiny     : true,
		jsInclude       : ["newsArticleMaint.#request.jsType#.js?#cacheValue#"]
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-newspaper-o accent2"></span>
				<br />
				<h2>News Article Maintenance</h2>
				<p>
					Use this to add information to the community news letter area.
					<br />
			   </p>
			   <cfif len(compMessage)>
				   <div class="msgFormat">#compMessage#</div>
				</cfif>
			</header>
			<section class="box ">
				<form method="post" action="newsLetterMaint.cfm" name="form" id="form">
				<input type="hidden" name="action" id="action" value="add" />
				<input type="hidden" name="artId"  id="artId"  value="0" />
				<div class="row uniform 50%">
					<div class="12u">
						<input type="text" name="articleDate" id="articleDate" value="" placeholder="Date of Article - MM/DD/YYYY" />
					</div>
				</div>
				<div class="row uniform 50%">
					<div class="12u">
						<input type="text" name="articleHead" id="articleHead" value="" placeholder="Article Header" />
					</div>
				</div>
				<div class="row uniform 50%">
					<div class="12u">
						<textarea name="article" id="article"></textarea>
						<br /><br />
						<div class="4u 12u(mobilep)">
							<input type="submit" id="sendBtn" value="Save Article" class="fit" />
						</div>
					</div>
				</div>
				</form>

			</section>

			<section class="box">
				<header>
					<h3>Previous Articles</h3>
					<p>The following is a list of articles that you can edit.</p>
				</header>		
				<div class="table-wrapper">
					<table id="prevArticles">
					<thead>
						<tr>
							<th>Article Header</th>
							<th>Article Date</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<cfloop query="artRecs">
							<tr>
								<td>#artRecs.articleHead#</td>
								<td>#dateFormat(artRecs.articleDate,"mm/dd/yyyy")#</td>
								<td>
									<div class="8u 12u(mobilep)">
										<a href="##" id="E-#artRecs.commId#" data-id="#artRecs.commId#" class="fit">Edit</a>
										&nbsp;&nbsp;
										<a href="##" id="D-#artRecs.commId#" data-id="D-#artRecs.commId#" class="fit">Delete</a>
									</div>
								</td>
							</tr>
						</cfloop>
					</tbody>
					</table>

				</div>
			</section>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<script>
		var isMobile = "#session.isMobile#";
		var isPhone  = "#session.isPhone#";
	</script>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>