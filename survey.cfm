<!doctype html>
<!---                                                      
    CF Name:    survey.cfm

    Description:
      This module will allow you the resident to take a survey 
      if they haven't already did it.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	if ( !structKeyExists(session, "userInfo") )
	{
		// Send to home page
		location ( url="/index.cfm", addToken=false );
	}

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	displayArea   = 0;
	emailAddress  = "";
	msgColor      = "Green";
	titlePageName = "Survey";

	// Get the active survey
	qSetRec = queryExecute("Select * from questionnaire where active = 1");

	// Check the results
	if ( !qSetRec.recordCount )
	{
		// Send to home page
		location ( url="/index.cfm", addToken=false );
	}
	
	// Retreive all the questions
	questRecs = queryExecute("Select * from questions where questionSetId = :qid order by questionId",
		                       {
		                       	  qid : { cfsqltype:"CF_SQL_INTEGER", value:qSetRec.questionSetId }
		                       });

	// Let see if the address is in the response
	respRec 	 = queryExecute("Select * from questionResponse 
									  where questionSetId = :setId and 
									  addressId = 
									  			( Select addressId from addresses where address = :street )",
		                      {
		                      	 setId  : { cfsqltype:"CF_SQL_INTEGER", value:qSetRec.questionSetId },
		                      	 street : { cfsqltype:"CF_SQL_VARCHAR", value:session.userInfo.street }
		                      });

	// Check the results
	if ( respRec.recordCount )
	{
		compMessage = "Your address has already voted in this survey.";
		displayArea = 1;

	}

	if ( structKeyExists(form, "eventBtn") )
	{

		// Get the address id
		addRec = queryExecute("Select * from addresses where address = :add", { add : { cfsqltype:"CF_SQL_VARCHAR", value:session.userInfo.street } } );

		// Process all the fields in the form
		keyList = structKeyList(form);
		for ( x = 1; x <= listLen(keyList); x++ )
		{

			if ( find("Q-", listGetAt(keyList, x) ) > 0 )
			{
				// Set value and Remove the Q- from the value
				qId = listGetAt(keyList,x);
				qId = replace(qId, "Q-", "");

				qResp = form["Q-"&qId];
				
				// Insert the information into the database
				queryExecute("
					Insert into questionResponse ( questionId, questionSetId, residentId, addressId, response )
					   values( :qId, :qSetId, :resId, :addId, :resp )",
					   	{
					   		qId    : { cfsqltype:"CF_SQL_INTEGER", value:qid },
					   		qSetId : { cfsqltype:"CF_SQL_INTEGER", value:form.questionSetId },
					   		resId  : { cfsqltype:"CF_SQL_INTEGER", value:session.userInfo.residentId },
					   		addId  : { cfsqltype:"CF_SQL_INTEGER", value:addRec.addressId },
					   		resp   : { cfsqltype:"CF_SQL_VARCHAR", value:qResp }
					   	});
			}

		}

		// Set the message
		compMessage = "Thank you for the taking this survey !!";
		displayArea = 1;
		

	}

	// Set the structure with information to the request scope
	str    = {
		jsInclude     : [ ],
		jsChose       : false,
		bootstrap     : false
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />

<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-question accent4"></span>
				<br />
				<h2>Mayfair Survey</h2>
				<p style="color:blue;">
					We need your help, can you please ask your neighbors if they have signed up on the web site. 
					<br />We need everyone from every household to be signed up.
					<br />
				</p>
			   <div id="message" style="color:#msgColor#;font-weight:bold;text-align:center;">#compMessage#</div>
			</header>

			<cfif displayArea eq 0>
	
				<div class="box">
					<p>
						 <b>Subject: #qSetRec.subject#</b>
				   </p>
					<form method="post" action="survey.cfm" name="form" class="simple-validation">
					<input type="hidden" name="residentId" id="residentId" value="#session.userInfo.residentId#" />
					<input type="hidden" name="questionSetId" value="#qSetRec.questionsetId#" />

						<cfloop query="questRecs">
							<div class="row uniform 50%">
								<div class="12u">
									<cfscript>
										switch ( questRecs.questionType )
										{
											case 'YN':
												writeOutput("#questRecs.question#<br />
													<input type='radio' name='Q-#questRecs.questionId#' id='QY-#questRecs.questionId#' value='Yes' checked>
													<label for='QY-#questRecs.questionId#'>Yes</label> 
													<input type='radio' name='Q-#questRecs.questionId#' id='QN-#questRecs.questionId#' value='No'>
													<label for='QN-#questRecs.questionId#'>No</label>");

												// Break the switch
												break;
											case 'Text':
												writeOutput("#questRecs.question#<br />
												<textarea name='Q-#questRecs.questionId#' id='Q-#questRecs.questionId#' maxlength='1000' rows='3' cols='140'></textarea>");
										}

									</cfscript>
								</div>
							</div>
						</cfloop>
						<br /><br />
						<div class="row uniform">
							<div class="12u">
								<ul class="actions align-center">
									<li><input type="submit" name="eventBtn" id="eventBtn" value="Save Response" /></li>
								</ul>
							</div>
						</div>
					</form>
					
				</div>

			</cfif>
		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">
	
</body>
</cfoutput>
</html>
