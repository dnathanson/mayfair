<!doctype html>
<!--- 
 
	CF Name:    displayDoc.cfm

	Description:
		This page will determine if the user is logged in and
		will extract the information from the into the URL and
		then retrieve the correct file from the database. 

	Best viewed with Tabs = 3

	Program Information:
		Called from:  
		Calls:         
		Parameters:   
		Author:       Drew Nathanson

	Global Session Variables Used:

	Mayfair at Parkland - Technical Synergy, Inc.
	Copyright (c) 1999-2018. All Rights Reserved.   
 
--->

<cfscript>
	
	// Create the objects
	miscObj = createObject("component", "#application.cfcPath#misc");

	// Check to see if the person is logged in
	if ( !structKeyExists(session, "userInfo") )
	{
		abort;
	}

	// Get the url information
	miscObj.getURLVariables();

	// Check the results
	if ( structKeyExists(url, "typeb") )
	{

		// Extract the record
		rec 		= queryExecute("Select doc from mapByLaws");
		fileName = "mayfairByLaws.pdf";

	} else {

		// Extract the record
		rec 		= queryExecute("Select doc from terramar where terrid = :id", { id : { cfsqltype:"CF_SQL_INTEGER", value:url.typet } } );
		fileName = ( url.typet == 1 ) ? "terramarDocs.pdf" : "terramarDesign.pdf";
	}
	
</cfscript>

<cfheader name="content-disposition" value="inline;filename=#fileName#" />
<cfcontent reset="yes" type="application/pdf" variable="#rec.doc#">


