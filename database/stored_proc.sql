DELIMITER //

drop procedure if exists `insert_addresses_proc` //

create procedure insert_addresses_proc
( address varchar(60),
  lot int,
  block int,
  out new_id int )
begin

   /* Insert the new record into the database */
   insert into addresses ( address,lot,block )
     values ( address,lot,block );

   -- Get the id number
   set new_id = Last_Insert_ID();

/* End the Procedure*/
end; //

drop procedure if exists `update_addresses_proc` //

create procedure update_addresses_PROC
( X_addressId int,
  address varchar(60),
  lot int,
  block int )
begin

   /* Update the record into the database */
   update addresses
      Set address  = address,
          lot  = lot,
          block  = block
   where addressId = X_addressId;

end; //

drop procedure if exists `insert_bod_proc` //

create procedure insert_bod_proc
( residentId int,
  name varchar(100),
  out new_id int )
begin

   /* Insert the new record into the database */
   insert into bod ( residentId,name )
     values ( residentId,name );

   -- Get the id number
   set new_id = Last_Insert_ID();

/* End the Procedure*/
end; //

drop procedure if exists `update_bod_proc` //

create procedure update_bod_PROC
( X_bodId int,
  residentId int,
  name varchar(100) )
begin

   /* Update the record into the database */
   update bod
      Set residentId  = residentId,
          name  = name
   where bodId = X_bodId;

end; //

drop procedure if exists `insert_committeeMembers_proc` //

create procedure insert_committeeMembers_proc
( committeeId int,
  residentId int,
  signupDate datetime,
  chairman int,
  out new_id int )
begin

   /* Insert the new record into the database */
   insert into committeeMembers ( committeeId,residentId,signupDate,chairman )
     values ( committeeId,residentId,signupDate,chairman );

   -- Get the id number
   set new_id = Last_Insert_ID();

/* End the Procedure*/
end; //

drop procedure if exists `update_committeeMembers_proc` //

create procedure update_committeeMembers_PROC
( X_commitMemberId int,
  committeeId int,
  residentId int,
  signupDate datetime,
  chairman int )
begin

   /* Update the record into the database */
   update committeeMembers
      Set committeeId  = committeeId,
          residentId  = residentId,
          signupDate  = signupDate,
          chairman  = chairman
   where commitMemberId = X_commitMemberId;

end; //

drop procedure if exists `insert_committees_proc` //

create procedure insert_committees_proc
( committeeName varchar(150),
  shortDesc varchar(2000),
  longDesc longtext,
  active int,
  out new_id int )
begin

   /* Insert the new record into the database */
   insert into committees ( committeeName,shortDesc,longDesc,active )
     values ( committeeName,shortDesc,longDesc,active );

   -- Get the id number
   set new_id = Last_Insert_ID();

/* End the Procedure*/
end; //

drop procedure if exists `update_committees_proc` //

create procedure update_committees_PROC
( X_committeeId int,
  committeeName varchar(150),
  shortDesc varchar(2000),
  longDesc longtext,
  active int )
begin

   /* Update the record into the database */
   update committees
      Set committeeName  = committeeName,
          shortDesc  = shortDesc,
          longDesc  = longDesc,
          active  = active
   where committeeId = X_committeeId;

end; //

drop procedure if exists `insert_communityNews_proc` //

create procedure insert_communityNews_proc
( articleDate date,
  articleHead varchar(200),
  article varchar(6000),
  out new_id int )
begin

   /* Insert the new record into the database */
   insert into communityNews ( articleDate,articleHead,article )
     values ( articleDate,articleHead,article );

   -- Get the id number
   set new_id = Last_Insert_ID();

/* End the Procedure*/
end; //

drop procedure if exists `update_communityNews_proc` //

create procedure update_communityNews_PROC
( X_commId int,
  articleDate date,
  articleHead varchar(200),
  article varchar(6000) )
begin

   /* Update the record into the database */
   update communityNews
      Set articleDate  = articleDate,
          articleHead  = articleHead,
          article  = article
   where commId = X_commId;

end; //

drop procedure if exists `insert_events_proc` //

create procedure insert_events_proc
( title varchar(100),
  startDate date,
  startTime varchar(10),
  endDate date,
  endTime varchar(10),
  residentId int,
  shortDescription varchar(2000),
  out new_id int )
begin

   /* Insert the new record into the database */
   insert into events ( title,startDate,startTime,endDate,endTime,residentId,shortDescription )
     values ( title,startDate,startTime,endDate,endTime,residentId,shortDescription );

   -- Get the id number
   set new_id = Last_Insert_ID();

/* End the Procedure*/
end; //

drop procedure if exists `update_events_proc` //

create procedure update_events_PROC
( X_eventId int,
  title varchar(100),
  startDate date,
  startTime varchar(10),
  endDate date,
  endTime varchar(10),
  residentId int,
  shortDescription varchar(2000) )
begin

   /* Update the record into the database */
   update events
      Set title  = title,
          startDate  = startDate,
          startTime  = startTime,
          endDate  = endDate,
          endTime  = endTime,
          residentId  = residentId,
          shortDescription  = shortDescription
   where eventId = X_eventId;

end; //

drop procedure if exists `insert_mapBylaws_proc` //

create procedure insert_mapBylaws_proc
( doc mediumblob,
  out new_id int )
begin

   /* Insert the new record into the database */
   insert into mapBylaws ( doc )
     values ( doc );

   -- Get the id number
   set new_id = Last_Insert_ID();

/* End the Procedure*/
end; //

drop procedure if exists `update_mapBylaws_proc` //

create procedure update_mapBylaws_PROC
( X_mbId int,
  doc mediumblob )
begin

   /* Update the record into the database */
   update mapBylaws
      Set doc  = doc
   where mbId = X_mbId;

end; //

drop procedure if exists `insert_minutes_proc` //

create procedure insert_minutes_proc
( minuteDate date,
  approved int,
  minutes mediumblob,
  out new_id int )
begin

   /* Insert the new record into the database */
   insert into minutes ( minuteDate,approved,minutes )
     values ( minuteDate,approved,minutes );

   -- Get the id number
   set new_id = Last_Insert_ID();

/* End the Procedure*/
end; //

drop procedure if exists `update_minutes_proc` //

create procedure update_minutes_PROC
( X_minId int,
  minuteDate date,
  approved int,
  minutes mediumblob )
begin

   /* Update the record into the database */
   update minutes
      Set minuteDate  = minuteDate,
          approved  = approved,
          minutes  = minutes
   where minId = X_minId;

end; //

drop procedure if exists `insert_permissions_proc` //

create procedure insert_permissions_proc
( residentId int,
  admin int,
  management int,
  blog int,
  committee int,
  out new_id int )
begin

   /* Insert the new record into the database */
   insert into permissions ( residentId,admin,management,blog,committee )
     values ( residentId,admin,management,blog,committee );

   -- Get the id number
   set new_id = Last_Insert_ID();

/* End the Procedure*/
end; //

drop procedure if exists `update_permissions_proc` //

create procedure update_permissions_PROC
( X_permissionId int,
  residentId int,
  admin int,
  management int,
  blog int,
  committee int )
begin

   /* Update the record into the database */
   update permissions
      Set residentId  = residentId,
          admin  = admin,
          management  = management,
          blog  = blog,
          committee  = committee
   where permissionId = X_permissionId;

end; //

drop procedure if exists `insert_residents_proc` //

create procedure insert_residents_proc
( name varchar(200),
  street varchar(200),
  emailAddress varchar(150),
  cellPhone varchar(20),
  password varchar(50),
  notifications int,
  newsletter int,
  sms int,
  createDate datetime,
  out new_id int )
begin

   /* Insert the new record into the database */
   insert into residents ( name,street,emailAddress,cellPhone,password,notifications,newsletter,sms,createDate )
     values ( name,street,emailAddress,cellPhone,password,notifications,newsletter,sms,createDate );

   -- Get the id number
   set new_id = Last_Insert_ID();

/* End the Procedure*/
end; //

drop procedure if exists `update_residents_proc` //

create procedure update_residents_PROC
( X_residentId int,
  name varchar(200),
  street varchar(200),
  emailAddress varchar(150),
  cellPhone varchar(20),
  password varchar(50),
  notifications int,
  newsletter int,
  sms int,
  createDate datetime )
begin

   /* Update the record into the database */
   update residents
      Set name  = name,
          street  = street,
          emailAddress  = emailAddress,
          cellPhone  = cellPhone,
          password  = password,
          notifications  = notifications,
          newsletter  = newsletter,
          sms  = sms,
          createDate  = createDate
   where residentId = X_residentId;

end; //

drop procedure if exists `insert_terramar_proc` //

create procedure insert_terramar_proc
( doc mediumblob,
  out new_id int )
begin

   /* Insert the new record into the database */
   insert into terramar ( doc )
     values ( doc );

   -- Get the id number
   set new_id = Last_Insert_ID();

/* End the Procedure*/
end; //

drop procedure if exists `update_terramar_proc` //

create procedure update_terramar_PROC
( X_terrId int,
  doc mediumblob )
begin

   /* Update the record into the database */
   update terramar
      Set doc  = doc
   where terrId = X_terrId;

end; //

drop procedure if exists `insert_wedgeBlog_proc` //

create procedure insert_wedgeBlog_proc
( residentId int,
  blogDate datetime,
  subject varchar(100),
  blog longtext,
  active int,
  out new_id int )
begin

   /* Insert the new record into the database */
   insert into wedgeBlog ( residentId,blogDate,subject,blog,active )
     values ( residentId,blogDate,subject,blog,active );

   -- Get the id number
   set new_id = Last_Insert_ID();

/* End the Procedure*/
end; //

drop procedure if exists `update_wedgeBlog_proc` //

create procedure update_wedgeBlog_PROC
( X_wedgeBlogId int,
  residentId int,
  blogDate datetime,
  subject varchar(100),
  blog longtext,
  active int )
begin

   /* Update the record into the database */
   update wedgeBlog
      Set residentId  = residentId,
          blogDate  = blogDate,
          subject  = subject,
          blog  = blog,
          active  = active
   where wedgeBlogId = X_wedgeBlogId;

end; //

