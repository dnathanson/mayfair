use mayfair;

/* Database Creatation script */
drop table if exists residents;
create table residents
(
	residentId     	integer auto_increment primary key,
	name              varchar(200),
	street            varchar(200),
	emailAddress      varchar(150),
	cellPhone         varchar(20),
	password          varchar(50),
	notifications     integer default 0,
	newsletter        integer default 0,
	sms               integer default 0,
	createDate        datetime not null
);

drop table if exists sysResidents;
create table sysResidents
(
	residentId        integer auto_increment primary key,
	keyId             varchar(50)
);

drop table if exists wedgeBlog;
create table wedgeBlog
(
	wedgeBlogId 		integer auto_increment primary key,
	residentId        integer not null,
	blogDate 			datetime default Now(),
	subject           varchar(100),
	blog              longtext,
	active            integer default 1
);

drop table if exists permissions;
create table permissions
(
	permissionId 		integer auto_increment primary key,
	residentId        integer not null,
	admin             integer default 0,
	management 			integer default 0,
	blog 					integer default 0,
	committee         integer default 0
);

drop table if exists committees;
create table committees
(
	committeeId       integer auto_increment primary key,
	committeeName     varchar(150),
	shortDesc         varchar(2000),
	longDesc          longtext,
	active            integer default 1
);

drop table if exists committeeMembers;
create table committeeMembers
(
	commitMemberId  	integer auto_increment primary key,
	committeeId       integer not null,
	residentId        integer not null,
	signupDate        datetime default now(),
	chairman				integer default 0
);

drop table if exists committeeLog;
create table committeeLog
(
	committeeLogId    integer auto_increment primary key,
	committeeId       integer not null,
	residentId        integer not null,
	logDate           datetime,
	logText           longtext not null
);

drop table if exists mapByLaws;
create table mapByLaws
(
	mbId              integer auto_increment primary key,
	doc               mediumblob
);

drop table if exists terramar;
create table terramar
(
	terrId            integer auto_increment primary key,
	doc               mediumblob not null
);

drop table if exists addresses;
create table addresses
(
	addressId         integer auto_increment primary key,
	address           varchar(100) not null,
	lot               integer,
	block             integer
);

drop table if exists bod;
create table bod
(
	bodId             integer auto_increment primary key,
	residentId        integer,
	name              varchar(100)
);

drop table if exists communityNews;
create table communityNews
(
	commId            integer auto_increment primary key,
	articleDate       date not null,
	articleHead       varchar(200),
	article           varchar(6000)
);

drop table if exists minutes;
create table minutes
(
	minId             integer auto_increment primary key,
	minuteDate        date not null,
	approved          integer default 0,
	minutes           mediumblob not null
);

drop table if exists events;
create table events
(
	eventId           integer auto_increment primary key,
	title             varchar(100),
	startDateTime     datetime not null,
	endDateTime       dateTime not null,
	residentId        integer,
	shortDescription  varchar(2000),
   sendNotice        integer default 1,
   committeeId       integer default 0,
   locationId        integer default 0,
   otherLocation     varchar(2000) default null
);

drop table if exists locations
create table locations
(
	locationId        integer auto_increment primary key,
	locationName      varchar(200)
	active            integer default 1
);

drop table if exists parkingRequest;
create table parkingRequest
(
	parkingReqId      integer auto_increment primary key,
	residentId        integer not null,
	reason            varchar(2000),
	parkDenyId        integer default 0,
	phone             varchar(30),
	startDate         date not null,
	endDate           date not null,
	createDate        datetime not null,
	approved          integer default 0,
	approvedDate      datetime null,
	completed         integer default 0
);

drop table if exists parkingReqCars;
create table parkingReqCars
(
	parkingCarId      integer auto_increment primary key,
	parkingReqId      integer not null,
	make              varchar(100),
	model             varchar(100),
	tagNo             varchar(10),
	state             varchar(100),
	year              varchar(10),
	color             varchar(100)
);

drop table if exists parkingDenyReason;
create table parkingDenyReason
(
	parkDenyId        integer auto_increment primary key,
	reason            varchar(2000)
);

drop table if exists questionnaire;
create table questionnaire
(
	questionSetId      integer auto_increment primary key,
	qStartDate         date,
	subject            varchar(1000),
	active             integer default 1
);

drop table if exists questions;
create table questions
(
	questionId         integer auto_increment primary key,
	questionSetId      integer,
	question           varchar(500) not null,
	questionType       varchar(100),
	active             integer default 1
);

drop table if exists questionResponse;
create table questionResponse
(
	qrId               integer auto_increment primary key,
	questionId         integer,
	questionSetId      integer,
	residentId         integer,
	addressId          integer,
	response           varchar(1000)
);

/* Create a view */
drop view if exists view_residents; 
create view view_residents ( residentId, name, street, emailAddress,cellPhone,password,notifications,
	                          newsletter, sms, createDate, admin, management, blog )
as
select a.residentId, a.name, a.street, a.emailAddress,a.cellPhone,a.password,a.notifications,a.sms,
		 a.newsletter, a.createDate, ifnull(b.admin,0) admin, ifnull(b.management,0) management, ifnull(b.blog, 0)
from residents a
   left outer join permissions b on b.residentId = a.residentId;

drop view if exists view_wedgeBlog;
create view view_wedgeBlog ( wedgeBlogId, residentId, blogDate, subject, blog, active, name )
as 
select a.wedgeBlogId, a.residentId, a.blogDate, a.subject, a.blog, a.active, b.name
from wedgeBlog a, residents b
where B.residentId = A.residentId;

/* Create the committee view */
drop view if exists view_committeeMembers;
create view view_committeeMembers ( commitMemberId, committeeId, residentId, signupDate, chairman, committeeName, shortDesc, active, name, 
                                    emailAddress )
as
select a.commitMemberId, a.committeeId, a.residentId, a.signupDate, a.chairman, b.committeeName, b.shortDesc, b.active, c.name,
       c.emailAddress
from committeeMembers a, committees b, residents c
where a.committeeId = b.committeeId and 
      a.residentId  = c.residentId;

/* Create a view */
drop view if exists view_eventCommittees;
create view view_eventCommittee ( eventId, title, startDateTime, endDateTime, residentId, shortDescription, sendNotice, committeeId,
                                  locationId, otherLocation, locationName)
as
select a.eventId, a.title, a.startDateTime, a.endDateTime, a.residentId, a.shortDescription, a.sendNotice, a.committeeId,
       a.locationId, a.otherLocation, ifnull(b.locationName, NULL) locationName
from events a
   left outer join locations b on a.locationId = b.locationId;

drop view if exists view_parkingRequest;
create view view_parkingRequest ( parkingReqId, residentId, reason, parkDenyId, phone, startDate, endDate, createDate, approved, 
                                  approvedDate, completed, denyReason, name, street, emailAddress )
as
  select a.parkingReqId, a.residentId, a.reason, a.parkDenyId, a.phone, a.startDate, a.endDate, a.createDate, a.approved,
         a.approvedDate, a.completed, b.reason denyReason, c.name, c.street, c.emailAddress
  from parkingRequest a, parkingDenyReason b, residents c
  where a.residentId = c.residentId and 
        a.parkDenyId = b.parkDenyId;


/* LOAD DEFAULT INFORMATION */
insert into committees ( committeeName, shortDesc, longDesc )
	values ( 'Architectural', 'This committee will review all aspects addressing our community aesthetics and any rules and regulations governing any and all maintenance and/or improvements. This committee report to the Board of Directors.',
		      'With an eye to integration, this committee will review the aesthetics of our community with regard to, but not restricted to: paint colors for houses and driveways, textural material elements, 
		      such as pavers, roofing replacement materials, landscaping, and other such design elements that will affect the overall unification of our community. This committee will 
		      accomplish this while still leaving room for, and encouraging individual preferences within these established guidelines for our community. This committee is accountable to, 
		      and reports directly to, the Board of Directors.');

insert into committees ( committeeName, shortDesc,longDesc )
	values ( 'Documents', 'This committee will review all Mayfair Basic Governing Documents and will make revision recommendations it determines would be beneficial to the 
		       community at large. This committee will report directly to the Board Of Directors.',
'This committee is responsible for reviewing all of the Basic Governing Documents, i.e. Articles of Incorporation, By-Laws and Declarations of Covenants, Conditions, and 
Restrictions, taking into consideration restrictions set forth within those of our master association, Terramar Master Association, whose umbrella we fall under. 
This committee, is also accountable and reports directly to, the Board of directors. The following communities comprise the Terramar Master Association: 
Carriage Homes, Millrun, Mayfair, Whittier Oaks, Turnbridge, Lakes at Parkland, and Parkwood.<br /><br />
Our Basic Governing Documents include: <br />Articles of Incorporation establish basic information about the Association, it''s name, location, purpose and is 
how an association is created before a development is planned or units within that development are sold. Each purchaser of property in a development 
automatically becomes a member of the Association and by buying in that development acknowledges they will abide by the Associations rules and regulations.<br /><br />
Bylaws are the rules by which an association is run. They establish voting rights and procedures, rules governing meetings, describe the Association''s rights and 
responsibilities, enforcement of rules and regulations, all aspects regarding assessments, and establish procedures for creating annual budgets as well as 
determining assessment amounts.<br /><br />
Declaration of Covenants, Conditions and Restrictions, contain the most comprehensive and most important information about the development and operation of the 
Association and it''s members. This document establishes common areas, and rules and regulations governing them, as well as the rules and regulations and restrictions 
on the use of each owner’s individual property that the association member must abide by. The declarations also provide procedures for amending our documents.<br /><br /> 
All owners should already be in possession of these documents. If you do not have one, please contact our property manager to obtain a copy. There is a nominal charge 
for making a copy of this document, payable directly to the management company, due upon receipt.');

insert into committees ( committeeName, shortDesc, longDesc )
	values ( 'Events', 'This committee will be responsible for planning and coordinating all community events. This committee will report directly to the Board of Directors.',
'This committee’s sole purpose is to help members of our community become acquainted with, and get to know our neighbors, by planning and 
 coordinating multiple, one-time, annual, or special events. This will hopefully help us establish a true sense of community and mutual support network for and among our residents.');

insert into committees ( committeeName, shortDesc )
	values ( 'Wedge', 'This committee will represent our community by gathering information, researching, assisting in finding solutions to, as well as making recommendations to the Board of Directors.');

insert into committees ( committeeName, shortDesc, longDesc )
	values ( 'Enforcement', 'This committee will provide the managment company with supporting information/documentation during off hours about code violations.',
'The Enforcement Committee  will assist our Property Management Company enforce our Bylaws by providing addresses and violations committed after-hours as well as violations 
not readily apparent to them while conducting their normal monthly property inspections. The intention is not to be adversarial but with the intention of upholding our 
regulations/Bylaws as well as addressing safety issues, sanitation issues, and preventing infractions which would negatively affect our collective property values.<br /><br />
For example if a homeowners post light is out for any length of time it would pose a safety issue as we do not have any street lights and depend on our homeowners to 
maintain these lights in good working order. If they are unaware the bulb is out and several homes in a row have that same issue, it creates a large area that is 
blacked out. With insufficient light it puts homeowners and their families at risk while walking through the community after dark as we also have no sidewalks in the 
community and have no option but to walk in the road way along with cars. A list of homes in violation would be provided to the Property Manager who would then inform 
the homeowner to rectify this problem, along with how this would be addressed if it were not corrected within a given timeframe. In the past if the request to correct 
this issue by changing the light bulb was ignored, the management company would then change the bulb and bill the homeowner $25.00 for this service. This was a very 
effective way to eliminate this issue. Of course if there is another reason for the offense and the homeowner has attempted to correct this unsuccessfully, the 
committee could then assist the homeowner by troubleshooting the problem, or suggesting they contact an electrician.'); 

insert into committees ( committeeName, shortDesc, longDesc )
	value ( 'Social Media', 'This committee utilizes computer mediated tools that allow homeowners to share and exchange information and oversees communications to homeowners.',
'This committee can write up blurbs about various board members, what they do, and how it helps the community on the different social media like Facebook, Tweeter, etc. 
They can also highlight any homeowner within the community and their contributions to the community that enhances it in someway. Recognizing the homeowners achievements 
is a nice way to show thanks and to share their efforts with the rest of the community who might other wise not recognize their effort. This  might in turn motivate more 
homeowners to actively participate in the community and engage other homeowners to help. Additionally this would be a way to assist the events committee by soliciting 
opinions of the residents as to the type of events they are interested in, such as community garage sales, block parties, community parties, or obtaining feedback/input 
for the BOD on various issues that affect the community. Photos of these events could be obtained by this committee and passed along to the webmaster for the communities 
website so that they may be posted for all to view.'); 

insert into committees ( committeeName, shortDesc, longDesc )
	values ( 'Neighborhood Watering', 'This committee will research, determine costs and how our community can take advantage of and utilize our lakes as an alternative source of water for our lawns.', 
'This committee will examine all the tasks needed (i.e. permits, regulations, infrastructure, designs, etc.) of how we can make use of the lake in our community as a source 
 for watering our lawns. As the cost continues to increase from the City of Coconut Creek, it becomes imperative that we find another source of water in order for our 
 community to affordably water our lawns. This process will be very long and the committee will be required to present its findings to the Mayfair board of directors 
 with recommendations and plans (if any) on how to accomplish this.<br /><br />
 Any changes that are to be enacted must then be approved by a vote of the Mayfair HOA members as our Documents currently prohibit such usage. Once approved the 
 Document committee would assume responsibility for amending our documents to update/include this change.');

insert into committees ( committeeName, shortDesc, longDesc )
	values ( 'Safety', 'This committee will work with the local police and determine what general safety policies our community should follow.', 
'This committee will examine all the safety policies that our community should have in place. Meeting with local police, fire dept. and other communities to determine how to best 
go about implementing them. Areas that need to be addressed are: speed limits, roadway reflectors, front area lights and other needed areas of interest. <br /><br />
Some changes that are to be enacted might have to be voted on by Mayfair HOA members as our Documents might prohibit such changes. Once approved the 
 Document committee would assume responsibility for amending our documents to update/include this change.');

insert into communityNews( articleDate, articleHead, article ) values ( '2016-04-27', 'Annual Meeting', 'The Board of Directors held an annual meeting in order to vote on replacing the two members whos
positions were ending. We had at least a quorum of members and voted for Steve Capandonis and Noel Behrmann.');

insert into residents ( residentId, name, street, emailAddress, cellPhone, password, notifications, newsletter,sms)
values ( -1, 'Russ Sanguedolce', null, 'russ@impflorida.com',null,'russ',0,0,0);

/* Insert the on-street parking */
insert into parkingDenyReason ( parkDenyId, reason )
	values ( 0, 'None');

insert into parkingDenyReason ( reason )
	values('Already given permisson to park in the pool area.');

insert into parkingDenyReason ( reason )
	values('Already given permisson to park on the street around your home.');

insert into parkingDenyReason ( reason )
	values('There are too many vehicles that we can not approve such a large number');

insert into parkingDenyReason ( reason )
	values('The street around your home has double lines which does not allow parking');

insert into parkingDenyReason ( reason )
	values('Information entered is incomplete, not detailed enough or missing');

use mayfair;

delimiter //

drop procedure if exists `insert_events_proc` //

create procedure insert_events_proc
( title varchar(100),
  startDateTime datetime,
  endDateTime datetime,
  residentId int,
  shortDescription varchar(2000),
  sendNotice int,
  committeeId int,
  locationId  int,
  otherLocation varchar(2000),
  out new_id int )
begin

   /* Insert the new record into the database */
   insert into events ( title,startDateTime,endDateTime, residentId,shortDescription, sendNotice, committeeId, locationId, otherLocation )
     values ( title,startDateTime, endDateTime, residentId,shortDescription,sendNotice, committeeId, locationId, otherLocation );

   -- Get the id number
   set new_id = Last_Insert_ID();

/* End the Procedure*/
end; //

drop procedure if exists `update_events_proc` //

create procedure update_events_PROC
( x_eventId int,
  title varchar(100),
  startDateTime dateTime,
  endDateTime dateTime,
  residentId int,
  shortDescription varchar(2000),
  sendNotice int,
  committeeId int,
  locationId  int,
  otherLocation varchar(2000) )
begin

   /* Update the record into the database */
   update events
      Set title             = title,
          startDateTime     = startDateTime,
          endDateTime       = endDateTime,
          residentId        = residentId,
          shortDescription  = shortDescription,
          sendNotice        = sendNotice,
          committeeId       = committeeId,
          locationId        = locationId,
          otherLocation     = otherLocation
   where eventId = x_eventId;

end; //

