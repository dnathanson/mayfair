<!doctype html>
<!---                                                      
    CF Name:    xmasPictures.cfm

    Description:
      This module will display the different xmas pictures on the mayfair at parkland
      HOA web site. The value in the url will determine which picture to display.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	closeWindow   = 0;
	compMessage   = "";
	emailAddress  = "";
	pictureName   = "xmasFront";
	titlePageName = "Christmas Pictures";

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false,
		jsInclude       : [ ]
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

	// Check the url
	if ( structKeyExists(url, "picture") )
	{
		// Set the name
		pictureName &= url.picture & ".jpg";
	
	} else {
		// Set close window to 1
		closeWindow = 1;
	}

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<!--- <cfinclude template="common/menu.cfm" /> --->
		</header>

		<!-- Main -->
		<section id="main" class="container 125%">
			<header>
				<span class="icon major fa-tree accent3"></span>
				<br />
				<h2>Christmas Lights at Mayfair</h2>
				<p>
					Have a wonderful holiday season.
			   </p>
			</header>
			<section class="box special features 12u 12u(mobilep)" >
				<br />
				<img src="includes/img/xmas/#pictureName#" class="image featured" >
				<br /><br />
				<div class="12u 12u(mobilep)">
					<input type="button" value="Close" onClick="window.close();" />
				</div>
			</section>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	
	<script>

		<cfif structKeyExists(session, "userInfo")>
		// Set the resident id
		var resId    = "#session.userInfo.residentId#";
		<cfelse>
		// Set the resident id
		var resId    = "";
		</cfif>
	</script>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>