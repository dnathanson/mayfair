<!doctype html>
<!---                                                      
    CF Name:    serverStats.cfm

    Description:
      This module will display the different server stats to the screen.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Check permissions
	if ( !structKeyExists(session, "userInfo") || 
		  ( !structKeyExists(session.userInfo, "permission") ||
		  	  ( session.userInfo.permission.admin == 0 && session.userInfo.permission.management == 0 )  )
		)
	{
		// Send to home page
		location ( url="/index.cfm", addToken=false );
	}

	// Set the fields
	cacheValue    	= dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   	= "";
	emailAddress  	= "";
	titlePageName 	= "Server Stats";

	// Create the new object
	sessInfo 		= new cfc.sessionTracker();

	// Get the count
	sessCnt   		= sessInfo.getSessionCount();
	sessArr   		= sessInfo.getAllSessionInfo();

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-spin fa-cog accent3"></span>
				<br />
				<h2>Mayfair Web Server Statistics</h2>
				<p>
					This page will display specific web site information about current users.
					<br />
			   </p>
			</header>
			<section class="box">
				<div class="table-wrapper">
					<table>
					<thead>
						<tr>
							<th>User Name (if any)</th>
							<th>Street Address (if any)</th>
							<th>Email Address (if any)</th>
							<th>IP Address</th>
						</tr>
					</thead>
					<tbody>
						<cfloop index="i" from="1" to="#ArrayLen(sessArr)#">
							<tr>
								<td>#sessArr[i].name#</td>
								<td>#sessArr[i].street#</td>
								<td>#sessArr[i].email#</td>
								<td>#sessArr[i].ipAddress#</td>
							</tr>
						</cfloop>
					</tbody>
					</table>
				</div>
				<br />
				<p>
					Total Count: #sessCnt#
				</p>	
			</section>
			
		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>