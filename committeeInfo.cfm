<!doctype html>
<!---                                                      
    CF Name:    committeeInfo.cfm

    Description:
      This module will allow committee chairman/management the ability to view 
      committee members.
      
      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Check permissions
	if ( !structKeyExists(session, "userInfo") || 
		  ( !structKeyExists(session.userInfo, "permission") ||
		  	 ( session.userInfo.permission.blog  == 1 ) 
		  )
		)
	{
		// Send to home page
		location ( url="/index.cfm", addToken=false );
	}

	// Set the fields
	cacheValue    	= dateFormat(now(),"yyyymmdd")&timeFormat(now(),"HHmmss");
	compMessage   	= ( structKeyExists(session, "message") ) ? session.message : "";
	emailAddress  	= "";
	titlePageName 	= "Committee Info";

	// Create the object
	commMemObj    	= createObject("component", "#application.cfcPath#committeeMembers");
	smtpObj  		= createObject("component", "#application.cfcPath#smtp");
	smtpStruct  	= smtpObj.getSMTPinfo();

	// Find any committee where the resident is the chairman
	commRecs 	  	= commMemObj.getCommitteeMembersView(where="residentId = #session.userInfo.residentId# and chairman = 1");
	commitCnt     	= commRecs.recordCount;
	
	// Check for commRecs > 1
	if ( commRecs.recordCount == 1 )
	{

		// Get the street information
		commMemRecs 	= commMemObj.getCommitteeMembersView(where="committeeId = '#commRecs.committeeId#'");
		selValue 		= commRecs.committeeId;
		if ( structIsEmpty(form) )
		{
			form           = { };
			form.action    = "selection";
		}

		// Retreive the logs for that committee
		logEntRecs = queryExecute("Select * from committeeLog where committeeId = :comId order by logDate desc",
											{
			                       		comId : { cfsqltype:"CF_SQL_INTEGER", value:commRecs.committeeId } 
			                       	} );

	} else {

		// Retreive the street information
		commRecs 	= queryExecute("Select * from committees where active = 1 order by committeeName");
		commitCnt 	= commRecs.recordCount;
		selValue 	= 0;

	}


	// Check to see if action exists
	if ( structKeyExists(form, "action") )
	{

		// Check the value
		switch ( form.action )
		{

			case 'selection':

				// Get the street information
				if ( structKeyExists(form, "committeeId") )
				{
					commMemRecs 	= commMemObj.getCommitteeMembersView(where="committeeId = #form.committeeId#");
					selValue 		= form.committeeId;
					eventRecs      = queryExecute("select * from events where committeeId = :id order by startDateTime",
						                           {
						                           	id : { cfsqltype:"CF_SQL_INTEGER", value:form.committeeId }
						                           });
					locRecs        = queryExecute("select * from locations where active = 1 order by locationName");

					// Retreive the logs for that committee
					logEntRecs = queryExecute("Select * from committeeLog where committeeId = :comId order by logDate desc",
														{
						                       		comId : { cfsqltype:"CF_SQL_INTEGER", value:form.committeeId } 
						                       	} );

				} else {

					eventRecs      = queryExecute("select * from events where committeeId = :id order by startDateTime",
						                           {
						                           	id : { cfsqltype:"CF_SQL_INTEGER", value:commRecs.committeeId }
						                           });
					locRecs        = queryExecute("select * from locations where active = 1 order by locationName");
				}

				// Break the switch
				break;

			case 'sendEmail':

				// Get the email addresses
				cRecs = commMemObj.getCommitteeMembersView(selectLine="emailAddress", where="committeeId = #form.committeeId#");
				cList = valueList(cRecs.emailAddress, ",");

				if ( structKeyExists(form, "includeBOD") )
				{
					// Get the bod 
					bodRecs = queryExecute("Select emailAddress from residents where residentId in ( select residentId from bod )");
					cList  &= "," & valueList(bodRecs.emailAddress,",");
				}
				
				// Create body information
				saveContent variable="body"
				{
					writeOutput("
From your committee chairman: #session.userInfo.name#<br /><br />
#form.comments#<br />");

				}

				// Check to see if application email is set
				if ( application.useEmail )
				{

					// Create a mail object
					mail = new mail(type="html", charset="utf-8", body="#body#");
					mail.setFrom("noreply@mayfairatparkland.com (Mayfair at Parkland Website)");
					mail.setTo("#cList#");
					mail.setSubject("From the committee chairman - #session.userInfo.name#");
					mail.setServer(smtpStruct.host);
					mail.setPort(smtpStruct.port);
					mail.setUsername(smtpStruct.user);
					mail.setPassword(smtpStruct.pass);
					mail.setUseTLS(true);

					// Send the email
					mail.send();
				
				}

				eventRecs      = queryExecute("select * from events where committeeId = :id order by startDateTime",
						                           {
						                           	id : { cfsqltype:"CF_SQL_INTEGER", value:form.committeeId }
						                           });
				locRecs        = queryExecute("select * from locations where active = 1 order by locationName");

				// Retreive the logs for that committee
				logEntRecs = queryExecute("Select * from committeeLog where committeeId = :comId order by logDate desc",
													{
					                       		comId : { cfsqltype:"CF_SQL_INTEGER", value:form.committeeId } 
					                       	} );
				compMessage = "Your email has been sent to the your committee members";
		
				// Break the switch
				break;

			case 'add':

				// Insert the log into the database
				queryExecute("Insert into committeeLog ( committeeId, residentId, logDate, logText ) 
					             values ( :commId, :resId, current_timestamp(), :text )",
					          {
					          	 commId : { cfsqltype:"CF_SQL_INTEGER", value:form.committeeId },
					          	 resId  : { cfsqltype:"CF_SQL_INTEGER", value:session.userInfo.residentId },
					          	 text   : { cfsqltype:"CF_SQL_LONGVARCHAR", value:form.logEntry }
					          });

				// Get the street information
				if ( structKeyExists(form, "committeeId") )
				{
					commMemRecs 	= commMemObj.getCommitteeMembersView(where="committeeId = #form.committeeId#");
					selValue 		= form.committeeId;
					eventRecs      = queryExecute("select * from events where committeeId = :id order by startDateTime",
						                           {
						                           	id : { cfsqltype:"CF_SQL_INTEGER", value:form.committeeId }
						                           });
					locRecs        = queryExecute("select * from locations where active = 1 order by locationName");

				} else {

					eventRecs      = queryExecute("select * from events where committeeId = :id order by startDateTime",
						                           {
						                           	id : { cfsqltype:"CF_SQL_INTEGER", value:commRecs.committeeId }
						                           });
					locRecs        = queryExecute("select * from locations where active = 1 order by locationName");
				}

				// Set the message
				compMessage = "The log entry has been saved.";

				// Break the switch
				break;

			case 'update':

				// Update the log entry
				queryExecute("Update committeeLog
					             set logText = :text
					           where committeeLogId = :id",
					          {
					          	text   : { cfsqltype:"CF_SQL_LONGVARCHAR", value:form.logEntry },
					          	id     : { cfsqltype:"CF_SQL_INTEGER",     value:form.logId }
					          });

				// Get the street information
				if ( structKeyExists(form, "committeeId") )
				{
					commMemRecs 	= commMemObj.getCommitteeMembersView(where="committeeId = #form.committeeId#");
					selValue 		= form.committeeId;
					eventRecs      = queryExecute("select * from events where committeeId = :id order by startDateTime",
						                           {
						                           	id : { cfsqltype:"CF_SQL_INTEGER", value:form.committeeId }
						                           });
					locRecs        = queryExecute("select * from locations where active = 1 order by locationName");

				} else {

					eventRecs      = queryExecute("select * from events where committeeId = :id order by startDateTime",
						                           {
						                           	id : { cfsqltype:"CF_SQL_INTEGER", value:commRecs.committeeId }
						                           });
					locRecs        = queryExecute("select * from locations where active = 1 order by locationName");
				}

				// Set the message
				compMessage = "The log entry has been saved.";

				// Break the switch
				break;

		}
			
	}

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : true,
		datepicker      : true,
		timepicker      : false,
		includeTiny     : true,
		jsInclude       : ["committeeInfo.#request.jsType#.js?#cacheValue#"]  //#request.jsType#
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-users accent5"></span>
				<br />
				<h2>Committee Info</h2>
				<p>
					This will allow the chairman to view all the members of their committee and optionally 
					send them an email.
					<br />
					<cfif commitCnt eq 1>
						Committee Name: <span style="color:blue;">#commRecs.committeeName#</span>
					</cfif>
					<cfif !application.useEmail>
						<span style="color:red;font-weight:bold;">EMAIL PROCESSING HAS BEEN DISABLED</span><br />
					</cfif>
			   </p>
			   <div class="msgFormat">#compMessage#</div>
			</header>
			<cfif commitCnt gt 1>
				<section class="box ">
					<form method="Post" action="committeeInfo.cfm" name="form" id="form">
					<input type="hidden" name="action" value="selection">
					<div class="row uniform 50%">
						<h3>Search by Committee</h3>
						<div class="12u">
							<select name="committeeId" id="committeeId">
							<option value=""> -- Please Select --</option>
							<cfloop query="commRecs">
								<option value="#commRecs.committeeId#" <cfif selValue eq commRecs.committeeId> selected</cfif>>#commRecs.committeeName#</option>	
							</cfloop>
							</select>
						</div>
					</div>
					</form>
				</section>
			</cfif>
			
			<cfif structKeyExists(form, "action")>
				<input type="hidden" id="selValue" value="#selValue#" />
				<section class="box"> 

					<div class="table-wrapper">
						<table id="resultTab">
						<thead>
							<tr>
								<th>Name</th>
								<th>Email</th>
								<th>Chair Person </th>
								<th> &nbsp; </th>
							</tr>
						</thead>
						<tbody>
							<cfif !commMemRecs.recordCount>
								<tr>
									<td>No Committee Members Exist</td>
									<td> &nbsp;</td>
									<td> &nbsp;</td>
									<td> &nbsp;</td>
								</tr>
							<cfelse>
								<cfloop query="commMemRecs">
									<tr>
										<td>#commMemRecs.name#</td>
										<td>#commMemRecs.emailAddress#</td>
										<td id="C-#commMemRecs.commitMemberId#"><cfif commMemRecs.chairman eq 0>No<cfelse>Yes</cfif></td>
										<td>
											<div class="8u 12u(mobilep)">
												<a href="##" id="#commMemRecs.commitMemberId#" data-id="#commMemRecs.commitMemberId#" data-commitid="#commMemRecs.committeeId#" class="fit">Delete</a>
											</div>
										</td>
									</tr>
								</cfloop>	
							</cfif>
						</tbody>
						</table>
						
					</div>

					<div class="12u 12u(mobilep)">
						<ul class="actions">
							<li><input type="button" id="sendEmailBtn" value="Send Email" ></li>
							<li><input type="button" id="calEventBtn" value="View Calendar Events"></li>
							<li><input type="button" id="logEntryBtn" value="Add/Update Log Entry"></li>
						</ul>
					</div>
				</section>

				<section class="box" id="eventDisplay" style="display:none;"> 

					<div class="table-wrapper">
						<div id="eventTableArea">
							<table id="eventTab">
							<thead>
								<tr>
									<th>Start Date/Time</th>
									<th>End Date/Time</th>
									<th>Event Title</th>
									<th> &nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<cfif !eventRecs.recordCount eq 0>
									<tr>
										<td> &nbsp;</td>
										<td> &nbsp;</td>
										<td> No Event Listed</td>
									</tr>
								<cfelse>
									<cfloop query="eventRecs">
										<tr>
											<td>#dateFormat(eventRecs.startDateTime,"mm/dd/yyyy")#@#timeFormat(eventRecs.startDateTime)#</td>
											<td>#dateFormat(eventRecs.endDateTime,"mm/dd/yyyy")#@#timeFormat(eventRecs.endDateTime)#</td>
											<td>#eventRecs.title#</td>
											<td><a href="##" id="E-#eventRecs.eventId#" data-id="E-#eventRecs.eventId#">Edit</a>&nbsp;&nbsp;<a href="##" id="D-#eventRecs.eventId#" data-id="D-#eventRecs.eventId#" data-commitid="#selValue#">Delete</a></td>
										</tr>
									</cfloop>
								</cfif>
							</tbody>
							</table>
						</div>

						<div class="8u 12u(mobilep)">
							<ul class="actions">
								<li><input type="button" id="newEventBtn" value="New Event"></li>
								<li><input type="button" id="closeEventBtn" value="Close"></li>
							</ul> 
						</div>
					
					</div>
				</section> 

				<section class="box" id="addNewEventDisplay" style="display:none;">
					<input type="hidden" id="eventId" value="">
					<h3>Event Add / Update</h3>
					<p><b><span style="color:blue;">Date Format is: MM/DD/YYYY and Time Format is HH:MM (in 24 hour format)</span></b></p>
					<div class="row" id="errors" style="color:red;font-weight:bold;margin-left:0px;"></div>
					<div class="row uniform 50%">
						<div class="12u">
							<input type="text" id="eventTitle" maxlength="200" class="required" title="Event Title is Required" value="" placeholder="Event Title">
						</div>
					</div>
					<div class="row uniform 50%">
						<div class="6u 12u(mobilep)">
							<input type="text" id="startDate" maxlength="10" class="required date" title="Start Date is Required" value="" placeholder="Start Date">
						</div>
						<div class="6u 12u(mobilep)">
							<input type="text" id="startTime" maxlength="6" class="required" value="" title="Start Time Required" placeholder="Start Time - - HH:MM (24 Hour Clock)">
						</div>
					</div>
					<div class="row uniform 50%">
						<div class="6u 12u(mobilep)">
							<input type="text" id="endDate" maxlength="10" class="required date" title="Start Date is Required" value="" placeholder="End Date">
						</div>
						<div class="6u 12u(mobilep)">
							<input type="text" id="endTime" maxlength="6" class="required" value="" title="End Time is Required" placeholder="End Time - - HH:MM (24 Hour Clock)">
						</div>
					</div>
					<div class="row uniform 50%">
						<div class="12u">
							<textarea id="shortDescription" maxlength="2000" placeholder="Short Description"></textarea>
						</div>
					</div>
					<div class="row uniform 50%">
						<div class="6u 12u(mobilep)">
							<select id="locationId" class="required" placeholder="Location">
							<option value="0"> Location </option>
							<option value="0"> None </option>
							<cfloop query="locRecs">
								<option value="#locRecs.locationId#">#locRecs.locationName#</option>
							</cfloop>
							</select>
						</div>
						<div class="6u 12u(mobilep)">
							<textarea id="otherLocation" class="required" maxlength="2000" placeholder="Other Location"></textarea>
						</div>
					</div>
					<div class="row uniform 50%">
						<div class="12u(narrower)">
							<input type="checkbox" name="sendNotice" id="sendNotice" value="1">
							<label for="sendNotice">Send Reminder Notification</label>
						</div>
					</div>
					<div class="row uniform">
						<div class="12u">
							<ul class="actions align-center">
								<li><input type="button" id="saveEventBtn" value="Save Information"></li>
								<li><input type="button" id="closeEventEntryBtn" value="Close"></li>
							</ul>
						</div>
					</div>
					
				</section>

				<section class="box" id="sendMsgArea" style="display:none;">
					<form method="post" action="committeeInfo.cfm" name="form" id="form">
					<input type="hidden" name="action" value="sendEmail" />
					<cfif commitCnt eq 1>
						<input type="hidden" name="committeeId" id="committeeId" value="#commRecs.committeeId#" />
					<cfelse>
						<input type="hidden" name="committeeId" id="committeeId" value="#form.committeeId#" />
					</cfif>
					<input type="checkbox" name="includeBOD" id="includeBOD" value="1">
					<label for="includeBOD">Include BOD in E-Mail</label>
					<textarea name="comments" class="mceEditor" id="comments"></textarea>
					<br /><br />
					<div class="8u 12u(mobilep)">
						<input type="submit" id="sendBtn" value="Send Email to Committee" />
						&nbsp;
						<input type="button" id="closeBtn" value="Close Section" />
					</div>
					</form>

				</section>
			
				<section class="box" id="logArea" style="display:none;">
					<form method="post" action="committeeInfo.cfm" name="logForm" id="logForm">
					<input type="hidden" name="action" id="logAction" value="add" />
					<input type="hidden" name="logId"  id="logId"     value="0" />
					<cfif commitCnt eq 1>
						<input type="hidden" name="committeeId" id="committeeId" value="#commRecs.committeeId#" />
					</cfif>
					<div id="logError" style="display:none;">
						<p id="logErrorMsg" style="color:red;font-weight:bold;"> </p>
					</div>
					<p style="font-weight:bold;">Use this area to log the events of your committee or the meetings.</p>
					<textarea name="logEntry" class="mceEditor" id="logEntry"></textarea>
					<br /><br />
					<div class="12u 12u(mobilep)">
						<input type="submit" id="saveLogBtn" value="Save Log Entry" />
						&nbsp;
						<input type="button" id="logCloseBtn" value="Close Section" />
						&nbsp;
						<input type="button" id="viewOtherLogs" value="View Past Logs" />
					</div>
					</form>
					<div id="pastLogsArea" style="display:none;">
						<p style="font-weight:bold;">The following is a list of the committee log entries</p>
						<div class="table-wrapper" style="height:200px;overflow-y:scroll;overflow-x:hidden;">
							<table id="pastLogs">
							<tr>
								<th> &nbsp;</th>
								<th>Log Date</th>
								<th>Entry</th>
							</tr>
							<cfloop query="logEntRecs">
								<cfscript>
									text = ljustify(logEntRecs.logText, 30);
									if ( find("/p", text) )
									{
										text = rereplace(text, "</p>", " ...</p>", "ONE");
									} else {
										text &= "...";
									}
								</cfscript>
								<tr>
									<td><a href="##" id="log-#logEntRecs.committeeLogId#" data-id="#logEntRecs.committeeLogId#" class="fa fa-pencil-square-o"></a></td>
									<td>#dateFormat(logEntRecs.logDate, "mm/dd/yyyy")# @ #timeFormat(logEntRecs.logDate, "HH:mm")#</td>
									<td>#text#</td>
								</tr>
							</cfloop>
							</table>
						</div>
						<br />
						<div class="8u 12u(mobilep)">
							<input type="button" id="closePastLogView" value="Close List" />
						</div>
					</div>
				</section>

				<!--- DEFINE DELETE MODAL AREA --->
				<div id="deleteDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="deleteDialog" style="margin-top:50px;">
					<div class="modal-dialog">
						<div class="form-horizontal">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" area-hidden="true">X</button>
									<h3>Event Removal</h3>
								</div>
								<div class="modal-body" style="height:100px;">
									<div class="row" id="errors" style="color:red;font-weight:bold;margin-left:25px;">&nbsp;</div>
									<p> Are you sure you wish to delete this event?</p>
								</div>
								<div class="modal-footer" style="height:100px;">
									<br />
									<button id="yesBtn" class="btn btn-primary">Yes</button>
									<button class="btn btn-danger" data-dismiss="modal">No</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--- END DELETE MODAL AREA --->

				<!--- DEFINE THE MODAL AREA
				<div id="editAddDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="editAddDialog" style="margin-top:50px;">
					<div class="modal-dialog">
						<div class="form-horizontal">
						<input type="hidden" id="eventId" value="">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" area-hidden="true">X</button>
								<h3>Event Add / Update</h3>
								<p><b><span style="color:blue;">Date Format is: MM/DD/YYYY and Time Format is HH:MM (in 24 hour format)</span></b></p>
							</div>
							<div class="modal-body" style="height:550px;">
								<div class="row" id="errors" style="color:red;font-weight:bold;margin-left:25px;"></div>
								<div class="row-fluid">
									<div class="col-md-9 col-xs-12">
										<label for="eventTitle" class="col-md-3 col-xs-12 control-label">Event Title:</label>
										<input type="text" id="eventTitle" maxlength="200" class="form-control" value="" placeholder="Event Title">
									</div>
								</div>

								<div class="row-fluid">
									<div class="col-md-6 col-xs-12">
										<label for="startDate" class="col-md-7 col-xs-12 control-label">Start Date:</label>
										<input type="text" id="startDate" maxlength="10" class="col-md-3 col-xs-12 form-control" value="" placeholder="Start Date">
									</div>
									
									<div class="col-md-6 col-xs-12">
										<label for="startTime" class="col-md-7 col-xs-12 control-label">Start Time:</label>
										<input type="text" id="startTime" maxlength="5" class="col-md-3 col-xs-12 form-control" value="" placeholder="Start Time - - HH:MM (24 Hour Clock)">
									</div>

								</div>

								<div class="row-fluid">
									<div class="col-md-6 col-xs-12">
										<label for="endDate" class="col-md-7 col-xs-12 control-label">End Date:</label>
										<input type="text" id="endDate" maxlength="10" class="col-md-3 col-xs-12 form-control" value="" placeholder="End Date">
									</div>
									
									<div class="col-md-6 col-xs-12">
										<label for="endTime" class="col-md-7 col-xs-12 control-label">End Time:</label>
										<input type="text" id="endTime" maxlength="5" class="col-md-3 col-xs-12 form-control" value="" placeholder="End Time -- HH:MM (24 Hour Clock)">
									</div>

								</div>

								<div class="row-fluid">
									<div class="col-md-9 col-xs-12">
										<label for="shortDescription" class="col-md-4 col-xs-12 control-label">Short Description:</label>
										<textarea id="shortDescription" maxlength="2000" class="form-control" placeholder="Event Title"></textarea>
									</div>
								</div>

								<div class="row-fluid">
									<div class="col-md-6 col-xs-12">
										<label for="locationId" class="col-md-4 col-xs-12 control-label">Location:</label>
										<select id="locationId" class="form-control">
										<option value="0"> None </option>
										<cfloop query="locRecs">
											<option value="#locRecs.locationId#">#locRecs.locationName#</option>
										</cfloop>
										</select>
									</div>
									
									<div class="col-md-6 col-xs-12">
										<label for="otherLocation" class="col-md-3 col-xs-12 control-label">Other:</label>
										<textarea id="otherLocation" class="col-md-4 col-xs-12 form-control" maxlength="2000" placeholder="Other Location"></textarea>
									</div>

									<div class="row-fluid">
										<div class="col-md-9 col-xs-12">
											<input type="checkbox" name="sendNotice" id="sendNotice" value="1">
											<label for="sendNotice">Send Reminder Notification</label>
										</div>
									</div>

								</div>

							</div>
							<div class="modal-footer" style="height:100px;">
								<br />
								<button id="saveEventBtn" class="btn btn-primary">Save Information</button>
								<button id="closeModalBtn" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
						</div>
						</div>
					</div>
				</div>--->
				<!--- END OF MODAL AREA --->

			</cfif>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!--- Check for session.message --->
	<cfif structKeyExists(session, "message")>
		<cfset structDelete(session, "message") />
	</cfif>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>