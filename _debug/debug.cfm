<!--- ***************************************************
THIS SECTION IS USED TO DUMP VARIABLES REQUESTED
ENABLE AND DISABLE DEBUGGING INFORMATION
AS WELL AS ADDING IP TO DEBBUGER

IMPORTANT - SECTION MUST BE REMOVED BEFORE PLACING LIVE!

THIS IS A DEVELOPMENT ONLY SECTION  // ONLY ALLOWED FROM SPECIFIED IP'S

WRITTEN BY: FUSE DEVELOPMENTS, INC. (GIANCARLO GOMEZ)
--->

<cfscript>
	param name="request.nodebug" 	default="false";
	param name="url.expand" 		default="true";
	param name="url.top" 			default="9999";
	local.allowDebug = structKeyExists(url, "dump") && len(url.dump);
</cfscript>
<cfif local.allowDebug>
	<style>
		.debug-output {
			background:rgba(255,255,255,.5);
			padding:.5em 1em .5em 3em;
			font:normal 14px/1.4em 'source code pro',monospace;
			position: fixed;
			top:0 ;
			left:0;
			right:0;
			bottom:0;
			z-index:2000;
			overflow-y: scroll;
		}
		.debug-output table{
			font-family:'source code pro',monospace !important;
			font-size:14px !important;
			line-height: 1.4em;
			width:100%;
		}
		.debug-output table th {font-weight:600;}
		.debug-output dl{
			margin:1em 0;
		}
		.debug-output dl dt {
			background: #c7e0ff;
			padding: .3em;
			font-weight:600;
		}
		.debug-output dl dt span{
			color:#fff;
		}
		.debug-output dl dd{
			background-color: #e6efff;
			margin: 1px 0 0;
			padding: .3em;
		}
		.debug-output .debug-var{
			margin-bottom:1em;
		}
		.debug-output .debug-novar {
			background-color: #fcc;
			color:#c00;
			padding:1em;
		}
		.debug-output .debug-novar p{
			font-weight: 600;
			text-transform: uppercase;
		}
	</style>
</cfif>
<!--- DEBUG OUTPUT AND HELPERS --->
<cfif local.allowDebug>
	<style>
		.show-debug {
			padding:.5em 1em .5em .5em;
			border-radius: 0 1.4em 1.4em 0;
			background-color:rgba(255,0,0,.8);
			color:#fff;
			margin:0;
			font:normal 14px/1.4em Menlo, consolas, monospace;
			position:fixed;
			left:0;
			top: 5px;
			z-index:10000;
			text-decoration: none;
			cursor: pointer;
		}
		.show-debug:hover{
			color:#fff;
			text-decoration: none;
			background-color:rgba(255,0,0,1);
		}
	</style>
	<script>
		$(function(){
			var UI = {
				debug 	: $('#debug-output'),
				sd 		: $('.show-debug')
			}
			UI.sd.on('click',function(){
				$('#trace-output').addClass('hide');
				if (UI.debug.hasClass('hide'))
					UI.debug.removeClass('hide');
				else
					UI.debug.addClass('hide');
			});
		})
	</script>
	<a class="show-debug"><i class="fa fa-bug"></i></a>
	<div id="debug-output" class="debug-output hide">
		<cfscript>
			// save vars not found
			local.novar = [];
			local.str 	= "";

			// output variables requested
			for(local.key in url.dump){
				// try to dump request and if fails append to nonvar
				try{
					savecontent variable="local.str"{
						writeOutput(local.str);
						if (isSimpleValue(evaluate(local.key)))
							writeOutput("<dl><dt>" & local.key & " <span>Simple Value</span></dt><dd>" & evaluate(local.key) & "</dd></dl>");
						else
							writeDump(var:evaluate(local.key),label:" REQUESTED OUTPUT FOR VAR - #local.key#",expand:url.expand,top:val(url.top));
					}
				}
				catch(Any e){
					// if the call fails lets add it to our nonvars
					arrayAppend(local.novar,local.key);
				}
			}
			// output if we have something to show
			if (len(local.str))
				writeOutput("<div class=""debug-var"">" & local.str & "</div>");

			// output variables not found
			if(arrayLen(local.novar)){
				local.str = "<div class=""debug-novar""><p>The following variables requested do not exist</p><ul>";
				for(local.key in local.novar){
					local.str &= "<li>" & local.key & "</li>";
				}
				local.str &= "</ul></div>";
				writeOutput(local.str);
			}
		</cfscript>
		<!--- <div style="background-color:yellow;color:black;">
			<br />
			<span style="margin-left:15px;">THIS MODULE WAS WRITTEN BY FUSE DEVELOPMENTS, INC (GIANCARLO GOMEZ) AND USED WITH THEIR PERMISSION</span>
			<br /><br />
		</div> --->
	</div>
</cfif>