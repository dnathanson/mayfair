<!doctype html>
<!---                                                      
    CF Name:    notifications.cfm

    Description:
     	This module will allow the administrator/management
     	the ability to send an email with a message in it.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Check permissions
	if ( !structKeyExists(session, "userInfo") || 
		  ( !structKeyExists(session.userInfo, "permission") || 
		    ( session.userInfo.permission.blog == 1 || session.userInfo.permission.committee == 1 ) ) )
	{
		// Send to home page
		location ( url="/index.cfm", addToken=false );
	}

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	titlePageName = "Notifications";

	// Check to see if action exists
	if ( structKeyExists(form, "action") )
	{

		// Check for value entries
		noteDate = ( isDate(form.noteDate) ) ? form.noteDate : dateFormat(now(),"mm/dd/yyyy");
		subject  = ( len(form.subject) )     ? form.subject : "Wedge Blog Entry - #dateFormat(now(),"mm/dd/yyyy")#";

		// Check to see if there is any article
		if ( !len(form.comments) )
		{
			// Set the message
			compMessage = "There is NO MESSAGE to send .. aborting.";
			className   = "msgFormatError";

		} else {

			// Create the objects
			resObj 	= createObject("component", "#application.cfcPath#residents");
			smtpObj  = createObject("component", "#application.cfcPath#smtp");

			// Get the email addresses
			if ( structKeyExists(form, "sendToAll") )
			{
				// Get all records
				resRecs 	= resObj.getResidents(selectLine="emailAddress",orderBy="emailAddress");
			} else {
				// Get only those with notifications
				resRecs 	= resObj.getResidents(selectLine="emailAddress",where="notifications = 1",orderBy="emailAddress");
			}
			resList  = valueList(resRecs.emailAddress,",");

			phRecs   = resObj.getResidents(selectLine="cellPhone",where="sms = 1 and length(cellPhone) > 0",orderBy="cellPhone");
			phList   = valueList(phRecs.cellPhone, ",");

			// Get the smtp informaetion
			smtpStruct  = smtpObj.getSMTPinfo();
			compMessage = "Your email has been sent.";

			// Process the 
			// Set body to the form
			body = form.comments;

			if ( application.useEmail )
			{

				// Process the emails
				for ( email in resList)
				{

					// Create a mail object
					mail = new mail(type="html", charset="utf-8", body="#body#");
					mail.setFrom("noreply@mayfairatparkland.com (Mayfair at Parkland Website)");
					mail.setTo("#email#");
					mail.setSubject("#form.subject#");
					mail.setServer(smtpStruct.host);
					mail.setPort(smtpStruct.port);
					mail.setUsername(smtpStruct.user);
					mail.setPassword(smtpStruct.pass);
					mail.setUseTLS(true);

					// Send the email
					mail.send();

				}

			}
			
			// Check for message
			if ( application.useTwilio )
			{

				// Credate the object
				smsObj 		= createObject("component", "#application.cfcPath#sms");
				resObj 		= createObject("component", "#application.cfcPath#residents");

				// Get the phone numbers
				smsObj.sendSMS(phoneList="#phList#",msg="#form.comments#");

			}

		}

	}

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false,
		datepicker      : true,
		includeTiny     : true,
		jsInclude       : ["notifications.#request.jsType#.js?#cacheValue#"]
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-envelope accent2"></span>
				<br />
				<h2>Send Notification</h2>
				<p>
					Use this area to write an email to the homeowners. All members who asked to nottified by email will receive a 
					copy.
					<br />
					<cfif !application.useEmail>
						<span style="color:red;font-weight:bold;">EMAIL PROCESSING HAS BEEN DISABLED</span><br />
					</cfif>
			   </p>
			   <div class="msgFormat">#compMessage#</div>
			</header>
			<section class="box ">
				<form method="post" action="notifications.cfm" name="form" id="form">
				<input type="hidden" name="action" id="action" value="send" />
				<div class="row uniform 50%">
					<div class="12u">
						<input type="text" name="noteDate" id="noteDate" class="date" value="#dateFormat(now(),"mm/dd/yyyy")#" placeholder="Date of Notification - MM/DD/YYYY" />
					</div>
				</div>
				<div class="row uniform 50%">
					<div class="12u">
						<input type="text" name="subject" id="subject" value="" maxlength="100" placeholder="Email Subject" />
					</div>
				</div>
				<div class="row uniform 50%">
					<div class="4u 12u(narrower)">
						<input type="checkbox" name="includeSMS" id="includeSMS" value="1">
						<label for="includeSMS">Include SMS</label>
					</div>
					<div class="4u 12u(narrower)">
						<input type="checkbox" name="sendToAll" id="sendToAll" value="1">
						<label for="sendToAll">Send to All</label>
					</div>
				</div>
				<br />
				<div class="12u" id="showLC" style="display:none;">
					<b>Letter Counter: <span style="float:clear;float:right;color:red;font-weight:bold;" id="overLimit"></span><span id="lc"></span></b>
				</div>
				<div class="row uniform 50%">
					<div class="12u">
						<textarea name="comments" id="comments"></textarea>
						<br /><br />
						<div class="4u 12u(mobilep)">
							<input type="submit" id="sendBtn" value="Send Email" class="fit" />
						</div>
					</div>
				</div>
				</form>

			</section>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<script>
		var isMobile = "#session.isMobile#";
		var isPhone  = "#session.isPhone#";
	</script>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>