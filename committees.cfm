<!doctype html>
<!---                                                      
    CF Name:    committees.cfm

    Description:
      This module will display the different committee's at the mayfair at parkland
      HOA web site.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	titlePageName = "Committee's";

	// Create the objects
	comObj 		= createObject("component", "#application.cfcPath#Committees");

	comRecs 		= comObj.getCommittees(OrderBy="CommitteeName");
	jsComList 	= valueList(comRecs.committeeName,",");

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false,
		jsInclude       : ["committees.#request.jsType#.js?#cacheValue#"]
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 75%">
			<header>
				<span class="icon major fa-users accent3"></span>
				<br />
				<h2>Committee's</h2>
				<p>
					This page will display the different committee's within our community. Please consider joining a group
					so that we can improve the development.
					<cfif !structKeyExists(session,"userInfo")>
						<br /><br />
						<b>You will need to Log In to the system before you can 
						sign up to be on the committee</b>
					</cfif>
			   </p>
			   <div id="message" class="msgFormat">#compMessage#</div>
			</header>
			<section class="box special features">
				<cfloop query="comRecs">
					<cfif ( comRecs.currentRow mod 2) eq 0>
						<section>
							<h3>#comRecs.CommitteeName#</h3>
							<p style="height:150px;">#comRecs.shortDesc#</p>
							<cfif CompareNoCase("Wedge", comRecs.CommitteeName)>
								<a href="committeeDetail.cfm/com/#comRecs.committeeId#">View More Information</a>
							<cfelse>
								<a href="theWedge.cfm">View the Wedge Page</a> 
							</cfif>
							<br /><br />
							<cfif structKeyExists(session, "userInfo")>
								#comRecs.committeeName#<br />
								<span style="float:clear;float:bottom;text-align:center;">
								<cfif CompareNoCase("Architectural", comRecs.CommitteeName) eq 0>
									<span style="float:clear;float:bottom;text-align:center;color:red;">
										This committee has currently reached maximum allowable members
									</span>
								<cfelse>
									<ul class="actions">
										<li><a href="##" id="#REReplace(comRecs.committeeName," ","","ALL")#" class="button small"style="background-color:##7fcdb8;a.hover:color:white;">Join</a></li>
									</ul>
									<br /><br />
									<span style="float:clear;float:bottom;text-align:center;color:Green;" id="M-#REReplace(comRecs.committeeName," ","","ALL")#"> </span>
									</span>
								</cfif>
							</cfif>
						</section>
						</div>
					<cfelse>
						<div class="features-row">
						<section>
							<h3>#comRecs.CommitteeName#</h3>
							<p style="height:150px;">#comRecs.shortDesc#</p>
							<cfif CompareNoCase("Wedge", comRecs.CommitteeName)>
								<a href="committeeDetail.cfm/com/#comRecs.committeeId#">View More Information</a>
							<cfelse>
								<a href="theWedge.cfm">View the Wedge Page</a> 
							</cfif>
							<br /><br />
							<cfif structKeyExists(session, "userInfo")>
								<cfif CompareNoCase("Architectural", comRecs.CommitteeName) eq 0>
									<span style="float:clear;float:bottom;text-align:center;color:red;">
										This committee has currently reached maximum allowable members
									</span>
								<cfelse>
									<span style="float:clear;float:bottom;text-align:center;">
									<ul class="actions">
										<li><a href="" id="#REReplace(comRecs.committeeName," ","","ALL")#" class="button small" style="background-color:##7fcdb8">Join</a></li>
									</ul>
									</span>
									<br />
									<span style="float:clear;float:bottom;text-align:center;color:green;" id="M-#REReplace(comRecs.committeeName," ","","ALL")#"> </span>
								</cfif>
							</cfif>
						</section>
					</cfif>
				</cfloop>
				<cfif ( comRecs.currentRow mod 2) neq 0>
					<!--- <div class="features-row"> --->
					<section>
						&nbsp;
					</section>
					</div>
				</cfif>

			</section>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	
	<script>

		<cfif structKeyExists(session, "userInfo")>
		// Set the resident id
		var resId    = "#session.userInfo.residentId#";
		<cfelse>
		// Set the resident id
		var resId    = "";
		</cfif>
	</script>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>