<!doctype html>
<!---                                                      
    CF Name:    committeeInfoMgr.cfm

    Description:
      This module will allow committee chairman/management the ability to view 
      committee members.
      
      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Check permissions
	if ( !structKeyExists(session, "userInfo") || 
		  ( !structKeyExists(session.userInfo, "permission") ||
		  	 ( session.userInfo.permission.blog  == 1 ) 
		  )
		)
	{
		// Send to home page
		location ( url="/index.cfm", addToken=false );
	}

	// Set the fields
	cacheValue    	= dateFormat(now(),"yyyymmdd")&timeFormat(now(),"HHmmss");
	compMessage   	= ( structKeyExists(session, "message") ) ? session.message : "";
	emailAddress  	= "";
	titlePageName 	= "Committee Manager's Info";

	// Create the object
	commMemObj    	= createObject("component", "#application.cfcPath#committeeMembers");
	
	// Retreive the street information
	commRecs 	= queryExecute("Select * from committees where active = 1 order by committeeName");
	commitCnt 	= commRecs.recordCount;
	selValue 	= 0;

	// Check to see if action exists
	if ( structKeyExists(form, "action") )
	{

		// Check the value
		switch ( form.action )
		{

			case 'selection':

				// Build the dates for the event search 
				beginMonth      = dateAdd('m', -2, now());
				endMonth        = dateAdd("m", 1, now());
				beginDate       = month(beginMonth)&"/01/"&year(beginMonth);
				endDate         = month(endMonth)&"/"&daysinMonth(endMonth)&"/"&year(endMonth);

				commMemRecs 	= commMemObj.getCommitteeMembersView(where="committeeId = #form.committeeId#");
				selValue 		= form.committeeId;
				eventRecs      = queryExecute("select * from events 
					                            where committeeId = :id and 
					                            startDateTime between :date1 and :date2
					                            order by startDateTime",
					                           {
					                           	id    : { cfsqltype:"CF_SQL_INTEGER", value:form.committeeId },
					                           	date1 : { cfsqltype:"CF_SQL_DATE", value:beginDate },
					                           	date2 : { cfsqltype:"CF_SQL_DATE", value:endDate }
					                           });
				
				locRecs        = queryExecute("select * from locations where active = 1 order by locationName");

				// Retreive the logs for that committee
				logEntRecs = queryExecute("Select * from committeeLog where committeeId = :comId order by logDate desc",
													{
					                       		comId : { cfsqltype:"CF_SQL_INTEGER", value:form.committeeId } 
					                       	} );

				// Break the switch
				break;

		}
			
	}

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : true,
		datepicker      : true,
		includeTiny     : true,
		jsInclude       : ["committeeInfoMgr.#request.jsType#.js?#cacheValue#"]  //#request.jsType#
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-users accent5"></span>
				<br />
				<h2>Committee Info</h2>
				<p>
					This will allow the board of directors to view all the members of their committee, events and log entries.
					<br />
					<cfif !application.useEmail>
						<span style="color:red;font-weight:bold;">EMAIL PROCESSING HAS BEEN DISABLED</span><br />
					</cfif>
			   </p>
			   <div class="msgFormat">#compMessage#</div>
			</header>
			
			<section class="box ">
				<form method="Post" action="committeeInfoMgr.cfm" name="form" id="form">
				<input type="hidden" name="action" value="selection">
				<div class="row uniform 50%">
					<h3>Search by Committee</h3>
					<div class="12u">
						<select name="committeeId" id="committeeId">
						<option value=""> -- Please Select --</option>
						<cfloop query="commRecs">
							<option value="#commRecs.committeeId#" <cfif selValue eq commRecs.committeeId> selected</cfif>>#commRecs.committeeName#</option>	
						</cfloop>
						</select>
					</div>
				</div>
				</form>
			</section>
			
			<cfif structKeyExists(form, "action")>
				<section class="box"> 

					<div class="table-wrapper">
						<table id="resultTab">
						<thead>
							<tr>
								<th>Name</th>
								<th>Email</th>
								<th>Chair Person </th>
								<th> &nbsp; </th>
							</tr>
						</thead>
						<tbody>
							<cfif !commMemRecs.recordCount>
								<tr>
									<td>No Committee Members Exist</td>
									<td> &nbsp;</td>
									<td> &nbsp;</td>
									<td> &nbsp;</td>
								</tr>
							<cfelse>
								<cfloop query="commMemRecs">
									<tr>
										<td>#commMemRecs.name#</td>
										<td>#commMemRecs.emailAddress#</td>
										<td id="C-#commMemRecs.commitMemberId#"><cfif commMemRecs.chairman eq 0>No<cfelse>Yes</cfif></td>
										<td>
											<div class="8u 12u(mobilep)">
												&nbsp;
											</div>
										</td>
									</tr>
								</cfloop>	
							</cfif>
						</tbody>
						</table>
						
					</div>

				</section>

				<section class="box" id="eventDisplay"> 
					<div class="table-wrapper">
						<div id="eventTableArea">
							<table id="eventTab">
							<thead>
								<tr>
									<th>Start Date/Time</th>
									<th>End Date/Time</th>
									<th>Event Title</th>
									<th> &nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<cfif eventRecs.recordCount eq 0>
									<tr>
										<td> &nbsp;</td>
										<td> &nbsp;</td>
										<td> No Event Listed</td>
									</tr>
								<cfelse>
									<cfloop query="eventRecs">
										<tr>
											<td>#dateFormat(eventRecs.startDateTime,"mm/dd/yyyy")#@#timeFormat(eventRecs.startDateTime)#</td>
											<td>#dateFormat(eventRecs.endDateTime,"mm/dd/yyyy")#@#timeFormat(eventRecs.endDateTime)#</td>
											<td>#eventRecs.title#</td>
											<td>&nbsp;</td>
										</tr>
									</cfloop>
								</cfif>
							</tbody>
							</table>
						</div>

					</div>
					<div class="12u 12u(mobilep)">
						<input type="button" id="selectAgainBtn" value="Select Again" />
					</div>

				</section> 

				<section class="box" id="logArea">
					<div id="pastLogsArea">
						<p style="font-weight:bold;">The following is a list of the committee log entries</p>
						<div class="table-wrapper" style="height:200px;overflow-y:scroll;overflow-x:hidden;">
							<table id="pastLogs">
							<tr>
								<th> &nbsp;</th>
								<th>Log Date</th>
								<th>Entry</th>
							</tr>
							<cfloop query="logEntRecs">
								<cfscript>
									field = ljustify(logEntRecs.logText,"30");
								</cfscript>
								<tr>
									<td><a href="##" id="log-#logEntRecs.committeeLogId#" data-id="#logEntRecs.committeeLogId#" class="fa fa-pencil-square-o"></a></td>
									<td>#dateFormat(logEntRecs.logDate, "mm/dd/yyyy")# @ #timeFormat(logEntRecs.logDate, "HH:mm")#</td>
									<td nowrap>#field#</td>
								</tr>
							</cfloop>
							</table>
						</div>
					</div>
				</section>

				<section class="box" id="logEditorArea" style="display:none;">
					<h4>Log Entry</h4>
					<p id="logDatePerson"></p>
					<div id="logEntry"></div>
					<br /><br />
					<div class="12u 12u(mobilep)">
						<input type="button" id="closeBtn" value="Close" />
					</div>
				</section>

			</cfif>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!--- Check for session.message --->
	<cfif structKeyExists(session, "message")>
		<cfset structDelete(session, "message") />
	</cfif>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>