<!doctype html>
<!---
 *************************************************************
 *
 *   CF Name:    minutesEmail.cfm
 *
 *   Description:
 *      This module will email the residents that new minute
 *      have been upload to the web site. 
 *
 *      Best viewed with Tabs = 3
 *
 *    Program Information:
 *      Called from:  
 *      Calls:         
 *      Parameters:   
 *      Author:       Drew Nathanson
 *
 *    Global Session Variables Used:
 *
 *    Technical Synergy, Inc.
 *    Copyright (c) 1999-2018. All Rights Reserved.   
 *
 *************************************************************
 --->

 <cfscript>

	// Check permissions
	if ( !structKeyExists(session, "userInfo") || 
		  ( !structKeyExists(session.userInfo, "permission") ||
		  		( session.userInfo.permission.blog == 1 || 
		  			session.userInfo.permission.committee == 1 
		  		) 
		  	)
		)
	{
		// Send to home page
		location ( url="/index.cfm", addToken=false );
	}

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = ( structKeyExists(session, "message") ) ? session.message : "";
	emailAddress  = "";
	titlePageName = "Minutes Upload";

	// Check to see if action exosts
	if ( structKeyExists(form, "action") )
	{

		// Check for email
		if ( application.useEmail )
		{

			// Create the objects
			resObj 	= createObject("component", "#application.cfcPath#residents");
			smtpObj  = createObject("component", "#application.cfcPath#smtp");

			// Get the email addresses
			resRecs 	= resObj.getResidents(selectLine="emailAddress",where="notifications = 1",orderBy="emailAddress");
			resList  = valueList(resRecs.emailAddress,",");

			// Get the smtp informaetion
			smtpStruct  = smtpObj.getSMTPinfo();
			compMessage = "Your email has been sent.";

			// Process the 
			// Set body to the form
			saveContent variable="body"
			{
				writeOutput("
Dear Homeowner,<br /><br />
A new set of <b>DRAFT</b> meeting minutes is now available on our community website for your review.<br /><br />
Please note that minutes are not considered official until they are approved at the next Board of Director's meeting. Any minutes 
posted on this web site are for informational purposes only.<br /><br />
Thank you for your continued interest in our community.<br /><br />
Your Board of Director's.<br /><br />
<a href='#application.httpPath#minutes.cfm'>#application.httpPath#minutes.cfm</a><br /><br />");
			}

			// Process the emails
			for ( email in resList )
			{

				// Create a mail object
				mail = new mail(type="html", charset="utf-8", body="#body#");
				mail.setFrom("noreply@mayfairatparkland.com (Mayfair at Parkland Website)");
				mail.setTo("#email#");
				mail.setSubject("New Mayfair at Parkland Minutes have been posted.");
				mail.setServer(smtpStruct.host);
				mail.setPort(smtpStruct.port);
				mail.setUsername(smtpStruct.user);
				mail.setPassword(smtpStruct.pass);
				mail.setUseTLS(true);

				// Send the email
				mail.send();

			}

		}
	}

	// Build the date range
	if ( Month(Now()) == 1 )
	{
		startDate = DateAdd("yyyy",-1,Now()) & "-12-01";
	} else {
		startDate = Year(Now()) & "-01-01";
	}
	endDate   = Year(Now()) & "-12-31";

	// Retreive the community news
	minRecs = queryExecute("Select * from minutes where minuteDate between '#startDate#' and '#endDate#' order by minuteDate desc");

	// Set the structure with information to the request scope
	str    = {
		bootstrap  : false,
		jsInclude  : ["minutesUpload.#request.jsType#.js?#cacheValue#"]
	};


	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>

<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<form method="post" action="minutesEmail.cfm" name="form">
		<input type="hidden" name="action" id="action" value="upload">
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-envelope accent5"></span>
				<br />
				<h2>Minutes Emails</h2>
				<p>
					Use this section to send an email to the residents about new minutes that have been uploaded. 
					<br />
					<cfif !application.useEmail>
						<span style="color:red;font-weight:bold;">EMAIL PROCESSING HAS BEEN DISABLED</span><br />
					</cfif>
			   </p>
			   <div class="msgFormat" id="message">#compMessage#</div>
			</header>
			<section class="box">
				<div class="row uniform">
					<p>
						Press the button below to send the email.
					</p>
					<div class="12u">
						<ul class="actions align-center">
							<li><input type="submit" name="eventBtn" id="eventBtn" value="Send Email" /></li>
						</ul>
					</div>
				</div>
			</section>

			<section class="box"> 

				<div class="table-wrapper">
					<table id="minutesTable">
					<thead>
						<tr>
							<th>Meeting Date</th>
							<th>Minutes Status</th>
							<th> &nbsp; </th>
						</tr>
					</thead>
					<tbody>
						<cfloop query="minRecs">
							<tr>
								<td>#dateFormat(minRecs.minuteDate, "mm/dd/yyyy")#</td>
								<td><cfif minRecs.approved eq 0>Draft<cfelse>Final</cfif></td>
								<td>
									<cfif minRecs.approved eq 0>
										<span id="minuteLinks">
										<a href="##" id="#minRecs.minId#" data-id="#minRecs.minId#">Change Status to Final</a>
										&nbsp;&nbsp;
										<a href="##" id="D-#minRecs.minId#" data-id="D-#minRecs.minId#">Delete</a>
										</span>
									<cfelse>
										&nbsp;
									</cfif>	
								</td>
							</tr>
						</cfloop>
					</tbody>
					</table>
					
				</div>

			</section>
			
		</section>
		</form>
		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!--- Check for session.message --->
	<cfif structKeyExists(session, "message")>
		<cfset structDelete(session, "message") />
	</cfif>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>


