<!doctype html>

<cfscript>
	// Check to see if session.userInfo exists
	if ( structKeyExists(session, "userInfo") )
	{
		// Remove the strucrture
		structDelete(session, "userInfo");
	}

	// Go back to the home page
	location ( url="/index.cfm", addToken=false);

</cfscript>

