/*****************************************************
 *
 *  Module Name: Application.cfc
 *
 *  Module Description:
 *    This is the application file for the 
 * 
 *  Program Information:
 *     Called from:  index.cfm
 *     Calls:        home.cfm
 *     Parameters:   none
 *     Author:       Drew Nathanson
 *
 *  Global Session Variables Used:
 *   Most variables are placed in Application Scope.
 *  
 *  Mayfair at Parkland - Technical Synergy, Inc.
 *  Copyright (c) 1999-2025. All Rights Reserved.
 *
 *****************************************************/
Component
{


	// Set Application Information
	this.name                     = "Mayfair";
	this.sessionmanagement        = true;
	this.sessiontimeout           = createTimeSpan(0,1,0,0);
	this.applicationTimeout			= createTimeSpan(1,0,0,0);
	this.scriptProtect            = true;

	// Set the datasources
	this.datasource               = "MayfairDB";
	this.database	               = "MayfairDB";
	this.DatabaseSelect				= "MayfairDB";
	this.DatabaseInsert				= "MayfairDB";
	this.DatabaseUpdate				= "MayfairDB";
	this.DatabaseDelete				= "MayfairDB";

	// Set the path
	this.root 							= getDirectoryFromPath( getCurrentTemplatePath() );

	// mappings
	this.mappings						= {
		"/app-ram"	   : "ram:///",
		"/orm"		   : ExpandPath("/cfc/orm")
	};

	// java settings
	this.javaSettings    			= {
		LoadPaths      : ["/bin"],
		reloadOnChange : true,
		watchInterval  : 60
	};

	// *** Set up for ORM ***
	// Set ORM Enabled
	this.ormEnabled					= true;
	
	// Set ORM Settings
	this.ormSettings 					= {
		automanageSession		    : false,
		cfclocation				    : ["/cfc/orm"],
		dbcreate					    : "none",
		dialect 					    : "MySQLwithInnoDB",
		eventhandling			    : false,
		flushatrequestend		    : false,
		logSQL					    : false,
		secondaryCacheEnabled	 : false,
		useDBForMapping 		    : false,
		saveMapping              : true
	};

	public boolean function onApplicationStart()
	{

		application.root            = this.root;     
		application.appName         = this.name;
		application.timeStamp       = now();
		application.debug           = true;

		// Global Defaults
		application.title           = "Mayfair at Parkland";
		application.database        = this.database;
		application.databaseDelete  = this.DatabaseDelete;
		application.databaseInsert  = this.DatabaseInsert;
		application.databaseUpdate  = this.DatabaseUpdate;
		application.databaseSelect  = this.DatabaseSelect;

		// application.httpPath      = 'http://#cgi.server_name#/';
		application.rootFolder      = getDirectoryFromPath( getCurrentTemplatePath() );
		application.CFCpath         = 'cfc.';
		application.jsPath          = "/includes/js/system/";
		application.excelPath       = application.rootFolder & "/excel/";
				
		// Add the twilio setup to the application scope
		application.useTwilio       = false;
		application.useEmail        = true;
		structAppend(application,createObject('component','config.setup').init());

		// Set the application httpPath variable
		if ( findNoCase("dev.", cgi.server_name) )
		{
			application.httpPath     = 'http://dev.mayfairatparkland.com/';
			application.isDev        = 1;
		} else {
		   application.httpPath     = 'https://www.mayfairatparkland.com/';
		   isDev                    = 0;
		}

		// Reload the ORM
		ORMReload();
		
		// Return true
		return true;
	}
   
	function onMissingTemplate(targetPage)
	{

		// Include the 404 page
		include "404.cfm";

	}

	/**
	* @output true
	* @hint handles the error processing
	*/
	/*public void function onError( exception, event )
	{

		// Create the email
		cfmail(
			to       : "drew@technicalsynergy.com",
			from     : "noreply@mayfairatparkland.com",
			subject  : "Error on web site",
			type     : "html",
			server   : "smtp.sparkpostmail.com",
			port     : "587",
			username : "SMTP_Injection",
			password : "fa2d103d4425921b309c1ff7b8ab3cb40400573e" )
		{
			writeOutput("<h1>Error on Mayfair at Parkland Web Site</h1><br />");
			writeOutput("<table><tr valign='top'><td>");
			writeDump(var:cgi,label:"CGI");
			writeOutput("</td></tr><tr><td>");
			writeDump(var:url,label:"URL");
			writeOutput("</td></tr><tr><td>");
			writeDump(var:form,label:"FORM");
			writeOutput("</td></tr><tr><td>");
			writeDump(var:arguments,label:"ERROR");
			if ( structKeyExists(session, "userInfo") )
			{
				writeOutput("</td></tr><tr><td>");
				// writeDump(var:session.userInfo,label:"SESSION.USERINFO");
				writeDump(var:session,label:"SESSION");
			}
			writeOutput("</td></tr></table>");
		}

		// Display the error page
		include "/common/errorOccurred.cfm";

	}*/

	function onRequestStart(targetPage)
	{

		// reload application scope issues
		if ( structKeyExists(url,"reload") )
		{
			// Lock the scope
			lock scope="application" type="exclusive" timeout="30"
			{ 
				// Call the function onApplicationStart
				onApplicationStart(); 
			}

		}

		// Set JS type
		request.jsType = ( structKeyExists(url, "ws") ) ? "full" : "min";

		// Check for alternate url code
		if ( request.jsType == "min" )
		{
			// Set the jsType
			request.jsType = ( structKeyExists(url, "aa") ) ? "full" : "min";
		}

		// Create an instances of mobile detect
		local.mbDetect = new cfc.mobileDetect();

		// Set session 
		session.isMobile = local.mbDetect.isMobile();
		session.isPhone  = local.mbDetect.isMobile() && !local.mbDetect.isTablet();

		// Check to see if ipAddress exists
		if ( !structKeyExists(session, "ipAddress") ) 
		{ 
			session.ipAddress = cgi.remote_addr;
		}

		return true;
	}

	public function onRequestEnd()
	{

		// Save the body content
		local.bodyContent = getPageContext().getOut().getString();

		// Clear the page to not have duplicates
		getPageContext().getOut().clearBuffer();

		// Now save the information
		savecontent variable="local.bodyAppend"
		{
			// Include the debug information
			include "_debug/debug.cfm";
		}

		// Check for ws or aa
		if ( !structKeyExists(url, "ws") && !structKeyExists(url, "aa") )
		{
			// Remove the white spaces
			local.bodyAppend = removeWhitespaceFromHtml(local.bodyAppend);
			
		}

		// Write the output
		writeOutput(local.bodyContent.replace("</body>", local.bodyAppend & "</body>"));

	}

	public function onRequest (targetPage)
	{

		// Save the local view
		savecontent variable = "local.view" { include targetPage; }

		// now tidyup the html and output
		if ( structKeyExists(url,"ws") || structKeyExists(url, "aa") )
		{
			// Display with normal view
			writeOutput(local.view);

		} else {
			
			// Display with compress view
			writeOutput(removeWhitespaceFromHtml(local.view));

		}

	}

	/* DEFINE PRIVATE FUNCTIONS */
	private string function removeWhitespaceFromHtml(required string o_html) output="true"
	{

		// Instantiate the java to compress and set the options
		local.html = CreateObject("java","com.googlecode.htmlcompressor.compressor.HtmlCompressor").init();
		local.html.setCompressCss(true);               //compress inline css
		local.html.setCompressJavaScript(true);        //compress inline javascript

		// Return the compressed code
		return local.html.compress(arguments.o_html);

	}

}