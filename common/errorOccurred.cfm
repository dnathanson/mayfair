<!doctype html>
<!---                                                      
    CF Name:    errorOccurred.cfm

    Description:
      This module will display the error occurred page.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland HOA.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	titlePageName = "Error Occurred";

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="../common/header.cfm" />
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="../common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 75%">
			<header>
				<span class="icon major fa-chain-broken accent2"></span>
				<br />
				<h2>An Error Occurred</h2>
			</header>
			<section class="box">
				<p>
					It would appear that an error has occurred in processing your requested page or action. The 
					developer of the web site has been notified of this error and will fix it as soon as possible. The 
					developer may contact you directly to either find out more information about your request or to
					let you know that the issue has been resolved. 
					<br /><br />
					We thank you for your patience.<br /><br />
					The Broad of Director's
			   </p>
			</section>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<ul class="icons">
				<li><a href="##" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
				<li><a href="##" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
				<li><a href="##" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
			</ul>
			<ul class="copyright">
				<li>#Year(Now())# &copy; Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<cfinclude template="../common/scripts.cfm">

</body>
</cfoutput>
</html>