<cfoutput>
<nav id="nav">
	<ul>
		<li><a href="/index.cfm">Home</a></li>
		<li>
			<a href="##" class="icon fa-angle-down">Menu</a>
			<ul>
				<!--- <li><a href="/communityNews.cfm">Community News</a></li> --->
				<li><a href="/committees.cfm">Committees</a></li>
				<li><a href="##" class="icon fa-angle-down">The Wedge</a>
					<ul>
						<li><a href="/theWedge.cfm">The Wedge Information</a></li>
						<li><a href="/theWedgeBlog.cfm">The Wedge Blog</a></li>
					</ul>
				</li>
				<li><a href="##" class="icon fa-angle-down">Board / Documents</a>
					<ul>
						<li><a href="/theBoard.cfm">Your Board of Directors</a></li>
						<li><a href="/ourDocuments.cfm">Our Documents</a></li>
					</ul>
				</li>
				<li><a href="/minutes.cfm">Minutes</a></li>
				<li><a href="##" class="icon fa-angle-down">Our History</a>
					<ul>
						<li><a href="/theBeginning.cfm">The Beginning</a></li>
						<li><a href="/theParties.cfm">The Parties</a></li>
						<cfif structKeyExists(session, "isMobile") && !session.isMobile>
							<li><a href="/mayfairMovie.cfm">Our Movie</a></li>
						</cfif>
					</ul>
				</li>
				<li><a href="/floorPlans.cfm">House Floor Plans</a></li>
				<li><a href="/suggestions.cfm">Suggestions</a></li>
				<li><a href="/theCalendar.cfm">Calendar of Events</a></li>
				<cfif structKeyExists(session, "userInfo")>
					<li><a href="/onStreetParkingWaiver.cfm">On-Street Waiver</a></li>
					<li><a href="/profile.cfm">Your Profile</a></li>
				</cfif>
				</li>
				<cfif structKeyExists(session, "userInfo") and structKeyExists(session.userInfo, "permission")>
					<cfif session.userInfo.permission.admin >			
						<li><a href="##" class="icon fa-angle-down"> Administration</a>
							 <ul>
							 	<li><a href="/userMaint.cfm">User Maintenance</a></li>
							 	<li><a href="/newsLetterMaint.cfm">Community News Letter</a></li>
							 	<li><a href="/notifications.cfm">Notifications</a></li>
							 	<li><a href="##" class="icon fa-angle-down"> Minutes/Permissions</a>
							 		<ul>
								 		<li><a href="/minutesUpload.cfm">Minutes Upload</a></li>
								 		<li><a href="/permissionMaint.cfm">Permissions</a></li>
								 		<li><a href="/onStreetRequests.cfm">On-Street Requests</a></li>
							 		</ul>
							 	</li>
							 	<li><a href="##" class="icon fa-angle-down"> Committee Sub-menu</a>
							 		<ul>
									 	<li><a href="/committeeMaint.cfm">Commit. Mem. Maint</a></li>
									 	<li><a href="/committeeInfo.cfm">Commit. Info.</a></li>
									 	<li><a href="/committeeReport.cfm">Commit. Report</a></li>
									 	<li><a href="/wedgeBlogMaint.cfm">Wedge Blog Entries</a></li>
									 </ul>
								</li>
								<li><a href="##" class="icon fa-angle-down"> Functions </a>
									<ul>
										<li><a href="/createExcel.cfm">Export Email's</a></li>
										<li><a href="/serverStats.cfm">Server Stats</a></li>
									</ul>
								</li>
							 	<li><a href="/eventMaint.cfm">Calendar Event Maint.</a></li>
							 </ul>
						</li>	
					</cfif>
					<cfif session.userInfo.permission.management>
						<li><a href="##" class="icon fa-angle-down"> Management </a>
							 <ul>
							 	<li><a href="/userMaint.cfm">User Maintenance</a></li>
							 	<li><a href="##" class="icon fa-angle-down">Notifications</a>
							 		<ul>
									 	<li><a href="/newsLetterMaint.cfm">Community News Letter</a></li>
									 	<li><a href="/wedgeBlogMaint.cfm">Blog Entries</a></li>
									 	<li><a href="/minutesUpload.cfm">Minutes Upload</a></li>
									 	<li><a href="/notifications.cfm">Notifications</a></li>
									 </ul>
								</li>
							 	<li><a href="##" class="icon fa-angle-down"> Committee Sub-menu</a>
							 		<ul>
							 			<li><a href="/committeeInfoMgr.cfm">Commit. Info</a></li>
							 			<li><a href="/committeeMaint.cfm">Commit. Mem. Maint</a></li>
							 			<li><a href="/committeeReport.cfm">Commit. Report</a></li>
							 		</ul>
							 	</li>
							 	<li><a href="/onStreetRequests.cfm">On-Street Requests</a></li>
							 	<li><a href="/createExcel.cfm">Export Email's</a></li>
							 	<li><a href="/eventMaint.cfm">Calendar Event Maint.</a></li>
							 </ul>
						</li>	
					</cfif>
					<cfif session.userInfo.permission.blog>
						<li><a href="##" class="icon fa-angle-down"> Blog </a>
							 <ul>
							 	<li><a href="/wedgeBlogMaint.cfm">Blog Entries</a></li>
							 </ul>
						</li>	
					</cfif>
					<cfif session.userInfo.permission.committee>
						<li><a href="##" class="icon fa-angle-down"> Committee </a>
							 <ul>
							 	<li><a href="/committeeInfo.cfm">Committee Information</a></li>
							 	<li><a href="/committeeReport.cfm">Committee Report</a></li>
							 </ul>
						</li>	
					</cfif>
				</cfif>
			</ul>
		</li>
		<cfif structKeyExists(session, "userInfo")>
			<li><a href="/logout.cfm" class="button">Log Out</a></li>
		<cfelse>
			<li><a href="/signup.cfm" class="button">Sign Up</a></li>
			<li><a href="/login.cfm" class="button">Login</a></li>
		</cfif>
	</ul>
</nav>


</cfoutput>