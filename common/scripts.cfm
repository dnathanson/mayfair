<cfoutput>
<!--- Scripts --->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<cfif structKeyExists(request,"bootstrap") and request.bootStrap >
	<script>var BootstrapVersion = 3, FontAwesomeVersion = 4;</script>
	<script src="/includes/js/bootstrap.js"></script>
	<!--- <script src="/includes/js/validation.#request.jsType#.js" type="text/javascript"></script> --->
	<script src="/includes/js/validation.min.js" type="text/javascript"></script>
</cfif>
<cfif structKeyExists(request, "datepicker")>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
</cfif>
<!--- Check to see if request.includeTiny exists --->
<cfif structKeyExists(request, "includeTiny")>
	<!--- <script src="includes/js/plugins/tinymce/tinymce.min.js"></script> --->
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
</cfif>
<script src="/includes/js/jquery.dropotron.min.js"></script>
<script src="/includes/js/jquery.scrollgress.min.js"></script>
<script src="/includes/js/skel.min.js"></script>
<script src="/includes/js/util.#request.jsType#.js"></script>

<cfif structKeyExists(request, "calendarJs")>
	<script src="/lib/moment.min.js"></script>
	<script src="/includes/js/fullcalendar.min.js"></script>
</cfif>

<script src="/includes/js/main.#request.jsType#.js"></script>

<cfif structKeyExists(request, "jsInclude")>
	<!--- Start a loop to process the array --->
	<cfloop array="#request.jsInclude#" index="a">
		<script src="#application.jsPath##a#"></script>
	</cfloop>
</cfif>
<cfif structKeyExists(request, "lightbox")>
	<script src="/includes/js/lightbox.min.js"></script>
</cfif>

<cfif structKeyExists(request, "preload")>
	<script src="/includes/js/plugins/jquery.preload.min.js"></script>
</cfif>

</cfoutput>
