
<cfoutput>
<cfif cgi.https eq "off">
	<cfif !application.isDev>
		<cflocation url="https://www.mayfairatparkland.com#cgi.script_name#" addtoken="false" />
	</cfif>
</cfif>
<cfif structKeyExists(request, "refresh")>
	<meta http-equiv="refresh", content="2,url=login.cfm">
</cfif>
<html>
<head>
<title>#titlePageName# | Mayfair at Parkland</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="Mayfair at Parkland, HOA">
<meta name="keyword" content="Homeowners Associations, Parkland, Florida, Mayfair at Parkland, Community Activities">
<meta name="robots" content="index, follow"/>
<meta name="copyright" content="Copyright © 2012-2020 Technical Synergy, Inc. All Rights Reserved."/>
<meta name="author" content="Drew Nathanson - Technical Synergy, Inc.">
<cfif structKeyExists(request,"bootstrap") and request.bootStrap >
	<link href="/includes/css/bootstrap.css" rel="stylesheet">
</cfif>
<cfif structKeyExists(request, "includeTiny")>
	<link rel="stylesheet" href="includes/js/plugins/tinymce/skins/lightgray/content.min.css" type="text/css" />
</cfif>
<cfif structKeyExists(request, "datepicker")>
	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css" type="text/css">
</cfif>
<cfif structKeyExists(request, "lightbox")>
	<link rel="stylesheet" href="includes/css/lightbox.css" type="text/css" />
</cfif>
<cfif structKeyExists(request, "calendarJs")>
	<link rel="stylesheet" href='includes/css/fullcalendar.css' />
	<link rel="stylesheet" href='includes/css/fullcalendar.print.css' rel='stylesheet' media='print' />
</cfif>
<link rel="stylesheet" href="/includes/css/main.#request.jsType#.css" />

<link rel="Shortcut Icon" href="/includes/ico/favicon.ico" />

</head>
</cfoutput>
