<!doctype html>
<!---                                                      
    CF Name:    theParties.cfm

    Description:
      This module will display the parties of mayfair at parkland
      HOA web site.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	cacheValue        = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	cssPicStyle       = ( session.isMobile ) ? "max-width:100%;" : "max-width:150%"; // "width:380px;height:235px;";
	compMessage       = "";
	emailAddress      = "";
	cssParty2016      = "display:none;";
	cssHalloween2016  = "display:none;";
	picJs             = 0;
	titlePageName     = "Our Parties";

	// Create the object
	miscObj 		  = createObject("component", "#application.cfcPath#misc");

	// Check to see if there is a url variable
	miscObj.getURLVariables();

	if ( structKeyExists(url, "halloween2016") )
	{
		cssHalloween2016 = "display:block";
		picJS            = 1;
	}

	/* if ( structKeyExists(url, "poolParty2016") )
	{
		cssParty2016 = "display:block;";
		picJs        = 1;
	} */

	// Get the directory lists
	halloweenList 			= directoryList("/includes/img/halloween2000/", false, 'query' );
	halloween2016List 	= directoryList("/inlcudes/img/halloween2016/", false, 'query');
	partyList     			= directoryList("/includes/img/party2000/",false,'query');
	partyList2016 			= directoryList("/includes/img/party2016/",false,'query');

	hallowJSList  			= "";
	hallow16JSList       = "";
	partyJSList   			= "";
	partyJS2016List 		= "";

	// Build an list for the js file to use to preload
	for ( line in halloweenList )
	{
		hallowJSList &= ( len(hallowJSList) ) ? "," : "";
		hallowJSList &= "'/includes/img/halloween2000/#line.name#'";
	}

	// Build an list for the js file to use to preload
	for ( line in partyList )
	{
		partyJSList &= ( len(partyJSList) ) ? "," : "";
		partyJSList &= "'/includes/img/party2000/#line.name#'";
	}

	// Build an list for the js file to use to preload
	for ( line in partyList2016 )
	{
		partyJS2016List &= ( len(partyJSList) ) ? "," : "";
		partyJS2016List &= "'/includes/img/party2016/#line.name#'";
	}

	// Build an list for the js file to use to preload
	for ( line in halloween2016List )
	{
		hallow16JSList &= ( len(partyJSList) ) ? "," : "";
		hallow16JSList &= "'/includes/img/halloween2016/#line.name#'";
	}

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false,
		preload         : true,
		jsInclude       : [ "theParties.#request.jsType#.js?#cacheValue#" ] 
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 75%">
			<header>
				<span class="icon major fa-music accent4"></span>
				<br />
				<h2>Our Parties</h2>
				<p>
					The Mayfair at Parkland community has had several events that included Halloween Parties, block 
					parties and the like. Below are some of the pictures.
				</p>
							   
			</header>
			<section class="box special features">
				<div class="features-row" >
					<section>
						<span class="icon major fa-camera accent2"></span>
						<h3>Pool Party - May 2016</h3>
						<p style="text-align:left;">
							This is our first party under the new board with Steve &amp; Noel.
							Everyone had a good time .. check out the pictures
							<br /><br />
							<a href="" id="viewPool2016">View Pictures</a>
						</p>
					</section>
					<section>
						<span class="icon major fa-camera accent4"></span>
						<h3>Pool Party - May 2000</h3>
						<p style="text-align:left;">
							We had a great block/pool party to celebrate all the families in 
							our community. A good time was had by all.
							<br /><br />
							<a href="" id="viewPool">View Pictures</a>
						</p>
					</section>
				</div>
				<div class="features-row" >
					<section>
						<span class="icon major fa-camera accent3"></span>
						<h3>Halloween Party - October 2016</h3>
						<p style="text-align:left;">
							We had a Halloween party for the kids in costumes. Lots of 
							fun for everyone.
							<br /><br />
							<a href="" id="viewHalloween2016">View Pictures</a>
						</p>
					</section>
					<section>
						<span class="icon major fa-camera accent2"></span>
						<h3>Halloween Party - October 2000</h3>
						<p style="text-align:left;">
							We had a Halloween party for the kids in the neighborhood with a best costume 
							contest. Many families brought cakes, candy and other goodies.
							<br /><br />
							<a href="" id="viewHalloween">View Pictures</a>
						</p>
					</section>
				</div>
				
			</section>

		</section>

		<section id="halloween2016" class="container 125%" style="#cssHalloween2016#">
			<h3> Halloween Party - October 2016 </h3>
			<section class="box special features">
				<div class="features-row" >
					<section>
						<img src="/includes/img/halloween2016/pic1.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/halloween2016/pic2.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/halloween2016/pic3.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/halloween2016/pic4.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/halloween2016/pic5.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/halloween2016/pic6.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/halloween2016/pic7.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
						
					</section>
					<section>
						<img src="/includes/img/halloween2016/pic8.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/halloween2016/pic9.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/halloween2016/pic10.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/halloween2016/pic11.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
						
					</section>
					<section>
						<img src="/includes/img/halloween2016/pic12.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				
				<div class="3u 12u(mobilep)">
					<ul class="actions"><li><input type="button" id="closeHallow2016Btn" value="Close" ></li></ul>
				</div>
			</section>

		</section>

		<!--- Handle area for Party2000 --->
		<section id="party2016" class="container 125%" style="#cssParty2016#">
			
			<h3> Pool Party - June 2016 </h3>
			<section class="box special features">
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2016/pic23.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
						<p>Accepting our Appreciation Plaque for Larry Silberger (former president) is Mario Porras</p>
					</section>
					<section>
						<img src="/includes/img/party2016/pic22.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
						<p>Bob Mayersohn accepting our Appreciation Plaque from Steve Capendonis</p> 
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2016/pic1.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/party2016/pic2.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2016/pic3.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/party2016/pic4.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2016/pic5.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
						<p>Thanks Erich, you did a GREAT JOB !!!!!</p>
					</section>
					<section>
						<img src="/includes/img/party2016/pic34.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
						<p>It's tough being this good !!!!!</p>
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2016/pic6.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
						
					</section>
					<section>
						<img src="/includes/img/party2016/pic8.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2016/pic10.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
						
					</section>
					<section>
						<img src="/includes/img/party2016/pic11.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2016/pic12.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
						
					</section>
					<section>
						<img src="/includes/img/party2016/pic13.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2016/pic14.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
						
					</section>
					<section>
						<img src="/includes/img/party2016/pic15.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2016/pic16.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
						
					</section>
					<section>
						<img src="/includes/img/party2016/pic17.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2016/pic18.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
						
					</section>
					<section>
						<img src="/includes/img/party2016/pic19.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2016/pic20.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
						
					</section>
					<section>
						<img src="/includes/img/party2016/pic21.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2016/pic24.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
						
					</section>
					<section>
						<img src="/includes/img/party2016/pic25.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2016/pic26.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
						
					</section>
					<section>
						<img src="/includes/img/party2016/pic27.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2016/pic29.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
						
					</section>
					<section>
						<img src="/includes/img/party2016/pic30.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>
				
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2016/pic31.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
						
					</section>
					<section>
						<img src="/includes/img/party2016/pic33.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>

				<div class="features-row" >
					<section>
						<img src="/includes/img/party2016/pic35.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
						
					</section>
					<section>
						<img src="/includes/img/party2016/pic36.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>

				<div class="features-row" >
					<section>
						<img src="/includes/img/party2016/pic37.png" class="img-responsive" style="text-align:left;border-radius:8px;#cssPicStyle#">
						
					</section>
					<section>
						<img src="/includes/img/party2016/pic38.png" class="img-responsive" style="border-radius:8px;#cssPicStyle#">
					</section>
				</div>

				<div class="3u 12u(mobilep)">
					<ul class="actions"><li><input type="button" id="closePool2016Btn" value="Close" ></li></ul>
				</div>
			</section>
		</section>

		<!--- Handle area for Halloween --->
		<section id="halloween" class="container 100%" style="display:none;">
			
			<h3> Halloween - October 2000 </h3>
			<section class="box special features">
				<div class="features-row" >
					<section>
						<img src="/includes/img/halloween2000/H2000-1.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/halloween2000/H2000-2.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/halloween2000/H2000-3.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/halloween2000/H2000-4.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/halloween2000/H2000-5.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/halloween2000/H2000-6.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/halloween2000/H2000-7.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/halloween2000/H2000-8.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/halloween2000/H2000-9.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
					<section>
						&nbsp;
					</section>
				</div>

				<div class="3u 12u(mobilep)">
					<ul class="actions"><li><input type="button" id="closeHallowBtn" value="Close" ></li></ul>
				</div>
				
			</section>

		</section>

		<!--- Handle area for Party2000 --->
		<section id="party2000" class="container 100%" style="display:none;">
			
			<h3> Party - October 2000 </h3>
			<section class="box special features">
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2000/Pic1-1.jpg" class="img-responsive" style="text-align:left;border-radius:15px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/party2000/Pic2.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2000/Pic3.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/party2000/Pic4.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2000/Pic5.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/party2000/Pic6.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2000/Pic7.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/party2000/Pic8.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="/includes/img/party2000/Pic9.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/party2000/Pic10.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>

				<div class="features-row" >
					<section>
						<img src="/includes/img/party2000/Pic11.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/party2000/Pic12.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>

				<div class="features-row" >
					<section>
						<img src="/includes/img/party2000/Pic13.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/party2000/Pic14.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>

				<div class="features-row" >
					<section>
						<img src="/includes/img/party2000/Pic15.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/party2000/Pic16.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>

				<div class="features-row" >
					<section>
						<img src="/includes/img/party2000/Pic17.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/party2000/Pic18.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>

				<div class="features-row" >
					<section>
						<img src="/includes/img/party2000/Pic19.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
					<section>
						<img src="/includes/img/party2000/Pic20.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>

				<div class="3u 12u(mobilep)">
					<ul class="actions"><li><input type="button" id="closePoolBtn" value="Close" ></li></ul>
				</div>
				
			</section>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<script>
		var partyList 		 = "#partyJSList#";
		var hallowList 	 = "#hallowJSList#";
		var partyList2016  = "#partyJS2016List#";
		var hallow16JSList = '#hallow16JSList#';
		var picJs          = "#picJs#";
	</script>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>