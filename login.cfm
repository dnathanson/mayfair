<!doctype html>
<!---                                                      
    CF Name:    login.cfm

    Description:
      This module will allow you to sign up to the mayfair at parkland
      HOA web site.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	email         = "";
	emailAddress  = "";
	msgColor      = "red";
	password      = "";
	titlePageName = "Login";

	// Create the objects
	resObj  = createObject("component", "#application.cfcPath#residents");
	perObj  = createObject("component", "#application.cfcPath#Permissions");
	miscObj = createObject("component", "#application.cfcPath#Misc");

	// Check the results
	if ( structKeyExists(session, "passwordReset") )
	{
		// Set the message
		compMessage = "An email has been sent allowing you to change your password.";
		msgColor    = "green";
		structDelete(session, "passwordReset");
		
	} else {

		if ( structKeyExists(session, "passwdResetError") )
		{
			compMessage = "The email address you provided is incorrect or doesn't exist. No email was sent.";
			msgColor    = "red";
			structDelete(session, "passwdResetError");
		}
		
	}

	// Check to see if form.eventBtn
	if ( structKeyExists(form, "eventBtn") )
	{

		// Get the resident information
		resRec = resObj.getResidents(where="emailAddress = '#form.email#' and password = '#hash(form.password,'md5')#'");

		// Check the results
		if ( !resRec.recordCount )
		{

			compMessage = "You have enter in either an invalid email or password";
			msgColor    = "red";
			
		} else {

			// Set the information
			session.userInfo = miscObj.loadQueryToStruct(qName="#resRec#");
			
			// Check to see if permissions is valid
			permRec = perObj.getPermissions(where="residentId = #resRec.residentId#");

			if ( permRec.recordCount )
			{
				session.userInfo.permission = {
					admin      = permRec.admin,
					management = permRec.management,
					blog       = permRec.blog,
					committee  = permRec.committee
				};

			}

			if ( structKeyExists(session, "onStreeRequest") )
			{
				structDelete(session, "onStreeRequest");
				location ( url="/onStreetRequests.cfm", addtoken=false);
			}

			// Set the home page
			location ( url="/index.cfm",addtoken=false);
			
		}

	}

	// Check to see if session.loginInfo exists
	if ( structKeyExists(session, "loginInfo") )
	{
		email 	= session.loginInfo.email;
		password = session.loginInfo.passwd;
	}

	// Set the structure with information to the request scope
	str    = {
		jsInclude     : [ "login.#request.jsType#.js?#cacheValue#" ],
		jsChose       : false,
		bootstrap     : true
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 75%">
			<header>
				<span class="icon major fa-sign-in accent5"></span>
				<br />
				<h2>Login for Mayfair at Parkland</h2>
				<p>
					You may login into the system by entering in your email address and password. If you don't 
					remember your password or your login, click on the appropriate link.
			   </p>
				<p id="msg" style="color:#msgColor#;font-weight:bold;text-align:center;">#compMessage#</p>
			</header>
			<div class="box">
				<form method="post" action="login.cfm" name="form" class="simple-validation">
					<div class="row uniform 50%">
						<div class="12u">
							<input type="text" name="email" id="email" class="required" title="Email Address is Required" placeholder="Email Address" value="#email#" />
						</div>
					</div>
					<div class="row uniform 50%">
						<div class="12u">
							<input type="password" name="password" id="password" class="required" title="Password is Required" placeholder="Password" value="#password#" />
						</div>
					</div>
					<div class="row uniform">
						<div class="12u">
							<ul class="actions align-center">
								<li><input type="submit" name="eventBtn" id="eventBtn" value="Login" /></li>
							</ul>
						</div>
					</div>
					<br /><br />
					<div class="row uniform 50%">
						<div class="6u 12u(mobilep)">
							<a href="##" id="forgotBtn" class="btn btn-primary">Forgot my Password</a>
						</div>
						<div class="6u 12u(mobilep)">
							&nbsp;
						</div>
					</div>
				</form>
				
			</div>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

	<!--- Check for session.loginInfo --->
	<cfif structKeyExists(session, "loginInfo")>
		<cfset structDelete(session, "loginInfo") />
	</cfif>

</body>
</cfoutput>
</html>