<!doctype html>
<!---                                                      
    CF Name:    theWedgeBlog.cfm

    Description:
      This module will display the wedge blog information.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland HOA.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	titlePageName = "Wedge Blog";

	// Create the object
	wObj = createObject('component', '#application.cfcPath#wedgeBlog');

	// Retrieve all the blog entries 
	wRecs = wObj.getWedgeBlogView(Where="active = 1 and blogDate between '#Year(Now())#-01-01' and '#Year(now())#-12-31'",orderBy="blogDate Desc");

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 75%">
			<header>
				<span class="icon major fa-pie-chart accent4"></span>
				<br />
				<h2>The Wedge Blog</h2>
				<p>
					The following are the blog entries for The Wedge for Mayfair Residents<br />
			   </p>
			</header>
			<section class="box">
				<cfif !wRecs.recordCount>
					<section>
						<p>There are NO Blog Entries.</p>
					</section>
				<cfelse>
					<cfloop query="wRecs">
						<cfscript>
							blogEntry = rereplace(wRecs.blog,"<p>","<p style='color:black;font-weight:bold;'>","ALL");
							blogEntry = rereplace(blogEntry, "<ul>","<ul style='font-weight:bold;'>","ALL");
						</cfscript>
						<div class="features-row">
						<section>
							<h4 style="color:blue;"><i>#wRecs.subject# </h4>
							<div style="color:black;">
								#blogEntry#
							</div>
							<p style="font-weight:bold;">
								<i>#wRecs.name#</i><br />
								#DateFormat(wRecs.blogDate, "mmm d, yyyy")#</i>
							</p>
							<cfif wRecs.currentRow neq wRecs.recordCount>
								<hr style="border-top:solid 2px ##e5e5e5;">
							</cfif>
						</section>
						</div>
					</cfloop>
				</cfif>

			</section>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>