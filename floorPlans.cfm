<!doctype html>
<!---                                                      
    CF Name:    floorPlans.cfm

    Description:
      This module will display the different floor plans of the 
      homes in Mayfair.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	cssPicStyle   = ( session.isMobile ) ? "width:100%;" : "";
	compMessage   = "";
	emailAddress  = "";
	titlePageName = "Floor Plans";

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 75%">
			<header>
				<span class="icon major fa-home accent4"></span>
				<br />
				<h2>Floor Plans For Mayfair</h2>
				<p>
					Our Community has 10 different floor plans that were originally offered. Each model has 2 elevations
					because our by-laws prohibit having the same house on adjoining lots. 
				</p>
							   
			</header>
			<section class="box special features">
				<div class="features-row">
					<section>
						<img src="includes/img/floorPlans/Avalon.jpg" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="includes/img/floorPlans/Brighton.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row">
					<section>
						<img src="includes/img/floorPlans/Cambridge.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="includes/img/floorPlans/Devonshire1.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row">
					<section>
						<img src="includes/img/floorPlans/Devonshire2.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="includes/img/floorPlans/Ellerdale1.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row">
					<section>
						<img src="includes/img/floorPlans/Ellerdale2.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row" >
					<section>
						<img src="includes/img/floorPlans/Ferncroft1.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row">
					<section>
						<img src="includes/img/floorPlans/Ferncroft2.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
				<div class="features-row">
					<section>
						<img src="includes/img/floorPlans/Glendale.jpg" class="img-responsive" style="border-radius:15px;#cssPicStyle#">
					</section>
				</div>
			</section>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>