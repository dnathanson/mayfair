<!doctype html>
<!---                                                      
    CF Name:    signup.cfm

    Description:
      This module will allow you to sign up to the mayfair at parkland
      HOA web site.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	addRefresh    = 0;
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	msgColor      = "Green";
	msgStyle      = "color:Green";
	titlePageName = "Sign Up";

	if ( structKeyExists(form, "email") && !structKeyExists(form, "eventBtn") )
	{
		emailAddress = form.email;
	}

	if ( structKeyExists(form, "eventBtn") )
	{

		// Check to see if security code matches the entered value
		info = {
			google : "https://www.google.com/recaptcha/api/siteverify",
			secret : "6LfwxyATAAAAABdTHLqKUPy141lBdzUeEBeb-_T8",
			ipaddr : "#cgi.remote_addr#",
			resp   : form['g-recaptcha-response']
		};

		// Set the google line
		gUrl = info.google & "?secret=" & info.secret & "&remoteip=" & info.ipaddr & "&response=" & info.resp ;

		// Get google response
		httpUrl = new http(method:"GET",url:gUrl,timeout:10,thrownontimeout:true);
		hpres   = httpUrl.send().getPrefix();

		// Convert the response
		googleResp = deserializeJSON("#hpRes.fileContent#");

		// Check the results
		if ( !googleResp.success )
		{
			// Set compMessage
			compMessage = "You have not successfully completed the Google Verification";
			msgStyle    = "color:white;background-color:red;";

		} else {

			// Check to see if the email always exists
			resChkRec = queryExecute("Select * from residents where emailAddress = :email",
				                      {
				                      	 email : { cfsqltype:"CF_SQL_VARCHAR", value:form.email }
				                      });

			if ( resChkRec.recordCount )
			{
				// Set the message and such
				compMessage = "Your account already exists.";
				msgStyle    = "color:white;background-color:red;";

			} else {

				// Create the object
				resObj 	= createObject("component", "#application.cfcPath#residents");
				smtpObj 	= createObject("component", "#application.cfcPath#smtp");

				// Check to see if the address is right
				addRec = queryExecute("Select * from addresses where address = :addr",
					                   {
					                   	addr : { cfsqltype:"CF_SQL_VARCHAR", value:form.street }
					                   });

				// Check the results
				if ( !addRec.recordCount )
				{

					// Set the message
					compMessage = "The Address provided is incorrect";
					msgStyle    = "color:white;background-color:red;";

				} else {

					// Retrieve the first record from the residents that has the same address
					firstRec  = queryExecute("select * from residents where street = :str order by createDate limit 1",
						                      {
						                      	 str : { cfsqltype:"CF_SQL_VARCHAR", value:form.street }
						                      });

					// Note 
					note  	= ( isDefined("form.notifications") ) ? 1 : 0;
					news  	= ( isDefined("form.newsletter") ) ? 1 : 0;
					sms   	= ( isDefined("form.sms") ) ? 1 : 0;
					phone 	= reReplace(form.cellPhone, "[-( )\. ]","","ALL");
					passHash = hash(form.password, "md5");

					// Insert the values
					resEnt = EntityNew("residents");
					resEnt.setName(form.name);
					resEnt.setStreet(form.street);
					resEnt.setEmailAddress(form.email);
					resEnt.setCellPhone(phone);
					resEnt.setPassword(passHash);
					resEnt.setNotifications(note);
					resEnt.setNewsletter(news);
					resEnt.setSms(sms);
					resEnt.setCreateDate(now());

					// Save the information
					transaction { EntitySave(resEnt); }

					// Insert the second record
					sysRes = EntityNew("sysResidents");
					sysRes.setResidentId(resEnt.getResidentId());
					sysRes.setKeyId(form.password);

					// Save the information
					transaction { EntitySave(sysRes); }
					
					compMessage = "Your account has been created.";
					msgStyle    = "color:white;background-color:green";
					addRefresh  = 1;

					// Save the information to the session
					// session.loginInfo = { };
					session.loginInfo = {
						email  : form.email,
						passwd : form.password
					};

					// Check first record
					if ( firstRec.recordCount )
					{

						// Build an email body
						saveContent variable="body"
						{
							writeOutput("
Hi,<br />
This email is to notify you that a new resident account was created using your home address. <br /><br />
Please review the information below and if you have no objections to it, you don't have to do anything.
However, if you don't know the person or the email address, please notify Russ at Integrity Property Management
(<a href='mailto:russ@ipmflorida.com'>russ@ipmflorida.com</a>) or the board president Steve Capandonis 
(<a href='mailto:stevecap1@comcast.net'>stevecap1@comcast.net</a>) to have the account immediately
removed.<br /><br />
<b>Information:</b><br />
<ul>
	<li>Name: #form.name#</li>
	<li>Address: #form.street#</li>
	<li>Email: #form.email#</li>
</ul>
<br />
Thank you.
");
						}

						// Send the message
						smtpObj.sendEmail(emailAddr="#firstRec.emailAddress#",body="#body#",subject="New Resident Signed Up at Your Address - Please Review");

					}

				}

			}	 

		}

	}

	// Set the structure with information to the request scope
	str    = {
		jsInclude     : [ "signup.#request.jsType#.js?#cacheValue#" ],
		jsChose       : false,
		bootstrap     : true
	};

	// Apppend the information to the request scope
	StructAppend(request,str);
	if ( addRefresh ) { request.refresh = 1; }

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 4px;
   -moz-border-radius: 4px;
        border-radius: 4px;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 75%">
			<header>
				<span class="icon major fa-envelope accent4"></span>
				<br />
				<h2>Email Signup</h2>
				<p>
					If you would like to sign up for the our news letter, Board of Director Memo's, and text message notification, please 
					create an account. If you already have done this, use the login button at the bottom to 
					sign in and review your notification profile.
					<br />
					<div style="color:red;font-size:18px;font-weight:bold;">
					Please be aware that this site uses the Google&trade; reCaptcha&reg; extension for sign up validation. This extension DOES NOT 
					WORK with Microsoft IE 11 (or for most IE versions for that matter) and Microsoft Edge. Once you have created your account,
					then you are free to use whatever browser you wish.
					</div>
			   </p>
			   <div id="message" class="msgFormat" style="#msgStyle#;font-weight:bold;text-align:center;">#compMessage#</div>
			</header>

			<!--- <div class="box">
				<div class="row uniform 50%">
					<div class="3u 12u(mobilep)">
						Already have a Log In? 
					</div>
					<div class="3u 12u(mobilep)">
						<ul class="actions"><li><input type="button" id="loginBtn" value="Login" ></li></ul>
					</div>
					<!--- <div class="3u 12u(mobilep)">
						<a href="##">Forgot your Login?</a> 
					</div>
					<div class="3u 12u(mobilep)">
						<a href="##">Forgot your Password?</a>
					</div> --->
				</div>
				
			</div> --->

			<div class="box">
				<form method="post" action="signup.cfm" name="form" class="simple-validation">
					<div class="row uniform 50%">
						<div class="6u 12u(mobilep)">
							<input type="text" name="name" id="name" value="" class="required" title="Your Name is Required" placeholder="Full Name" />
						</div>
						<div class="6u 12u(mobilep)">
							<input type="email" name="email" id="email" value="#emailAddress#" title="Email Address is Required" class="required email" placeholder="Email" />
						</div>
					</div>
					<div class="row uniform 50%">
						<div class="12u">
							<input type="password" name="password" id="password" value="" class="required" title="Password is Required" placeholder="Password" />
						</div>
					</div>
					<div class="row uniform 50%">
						<div class="12u">
							<input type="text" name="street" id="street" value="" class="required" title="Street Address is Required" placeholder="House Number Only (press TAB to Display List)" />
						</div>
					</div>
					<div class="row uniform 50%">
						<div class="12u">
							<input type="text" name="cellPhone" id="cellPhone" value="" placeholder="Cell Phone Optional (required for SMS Messages)" />
						</div>
					</div>
					<div class="row uniform 50%">
						<div class="4u 12u(narrower)">
							<input type="checkbox" name="notifications" id="notifications" value="1">
							<label for="notifications">Notifications</label>
						</div>
						<div class="4u 12u(narrower)">
							<input type="checkbox" name="newsletter" id="newsletter" value="1">
							<label for="newsletter">News Letters</label>
						</div>
						<div class="4u 12u(narrower)">
							<input type="checkbox" name="sms" id="sms" value="1">
							<label for="sms">SMS</label>
						</div>
					</div>
					<div class="row uniform 50%">
						<div class="12u">
							<br />
							<div class="g-recaptcha" data-sitekey="6LfwxyATAAAAAEKk_ctNY1n31jt2e61E01NNnAZd"></div>
						</div>
					</div>
					<div class="row uniform">
						<div class="12u">
							<ul class="actions align-center">
								<li><input type="submit" name="eventBtn" id="eventBtn" value="Create Account" /></li>
							</ul>
						</div>
					</div>
				</form>
				
			</div>

			<!--- <div class="box">
				<div class="row uniform 50%">
					<div class="3u 12u(mobilep)">
						Already have a Log In? 
					</div>
					<div class="3u 12u(mobilep)">
						<ul class="actions"><li><input type="button" id="loginBtn" value="Login" ></li></ul>
					</div>
					<!--- <div class="3u 12u(mobilep)">
						<a href="##">Forgot your Login?</a> 
					</div>
					<div class="3u 12u(mobilep)">
						<a href="##">Forgot your Password?</a>
					</div> --->
				</div>
				
			</div> --->

		</section>

		<!--- DEFINE THE MODAL AREA --->
		<div id="selectAddrArea" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="selectAddrArea" style="margin-top:100px;">
			<div class="modal-dialog">
				<form class="form-horizontal">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" area-hidden="true">X</button>
						<h3>Select Your Address</h3>
					</div>
					<div class="modal-body" style="height:200px;">
						<div id="fgMessage" style="color:red;display:none;"> </div>
						<p> Please select your address from the list below.
						<br />
						</p>
						<div class="row-fluid">
							<div class="col-md-9">
								<div class="form-group">
									<select name="selAddress" id="selAddress" class="form-control" size="4">
									</select>
								</div>
							</div>
							<br /><br />
						</div>
					</div>
					<div class="modal-footer" style="height:100px;">
						<br />
						<button id="continueBtn" class="btn btn-primary">Continue</button>
						<button id="closeModalBtn" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</div>
				</form>
			</div>
		</div>
		<!--- END OF MODAL AREA --->

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<cfinclude template="common/scripts.cfm">
	
</body>
</cfoutput>
</html>