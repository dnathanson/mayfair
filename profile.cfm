<!doctype html>
<!---                                                      
    CF Name:    profile.cfm

    Description:
      This module will allow you the resident to update their personal
      account information.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	if ( !structKeyExists(session, "userInfo") )
	{
		// Send to home page
		location ( url="/index.cfm", addToken=false );
	}

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	msgColor      = "Green";
	titlePageName = "Profile";

	// Create the object
	resObj = createObject("component", "#application.cfcPath#residents");

	if ( structKeyExists(form, "eventBtn") )
	{

		// Check to see if the address is right
		addRec = queryExecute("Select * from addresses where address = :addr",
			                   {
			                   	addr : { cfsqltype:"CF_SQL_VARCHAR", value:form.street }
			                   });

		// Check the results
		if ( !addRec.recordCount )
		{

			// Set the message
			compMessage = "The Address provided is incorrect";
			msgColor    = "red";

		} else {

			// Retrieve the current resident record
			resRec      = entityLoadByPK("residents", session.userInfo.residentId);
			// resRec 		= resObj.getResidents(residentId="#session.userInfo.residentId#");

			// Note 
			note 	= ( isDefined("form.notifications") ) ? 1 : 0;
			news 	= ( isDefined("form.newsletter") ) ? 1 : 0;
			sms  	= ( isDefined("form.sms") ) ? 1 : 0;
			phone = reReplace(form.cellPhone, "[-( )\. ]","","ALL");

			// Set the fields
			resRec.setName(form.name);
			resRec.setStreet(form.street);
			resRec.setEmailAddress(form.email);
			resRec.setCellPhone(phone);
			resRec.setPassword(hash(form.password, "md5"));
			resRec.setNotifications(note);
			resRec.setNewsletter(news);
			resRec.setSms(sms);

			// Save the entity
			transaction { EntitySave(resRec); }

			// Insert the values
			/*
			queryExecute("Update residents
                         set name          = :name,
                             street        = :st,
									  emailAddress  = :email,
									  cellPhone     = :cell,
									  password      = :pass,
									  notifications = :note,
									  newsletter    = :news,
									  sms           = :sms
								where residentId = :resId",
							{
				           		name  : { cfsqltype:"CF_SQL_VARCHAR", value:form.name,maxlength:200 },
				           		st    : { cfsqltype:"CF_SQL_VARCHAR", value:form.street,maxlength:200 },
				           		email : { cfsqltype:"CF_SQL_VARCHAR", value:form.email,maxlength:150 },
				           		cell  : { cfsqltype:"CF_SQL_VARCHAR", value:phone},
				           		pass  : { cfsqltype:"CF_SQL_VARCHAR", value:hash(form.password,"md5") },
				           		note  : { cfsqltype:"CF_SQL_INTEGER", value:note },
				           		news  : { cfsqltype:"CF_SQL_INTEGER", value:news },
				           		sms   : { cfsqltype:"CF_SQL_INTEGER", value:sms },
				           		resId : { cfsqltype:"CF_SQL_INTEGER", value:residentId }
				           });
			*/

			compMessage = "Your account has been updated.";

			// Check to see if the password was changed
			if ( resRec.getPassword() != form.password )
			{

				// Update the sysResidents table
				queryExecute("Update sysResidents set keyId = :pass where residentId = :id",
					          {
					          	 pass : { cfsqltype:"CF_SQL_VARCHAR", value:form.password },
					          	 id   : { cfsqltype:"CF_SQL_INTEGER", value:session.userInfo.residentId }
					          });
			}

		}

	}

	// Get the resident record
	resRec      = entityLoadByPK("residentSys", session.userInfo.residentId);
	/*
	resRec 		= resObj.getResidents(residentId="#session.userInfo.residentId#");
	sysResRec 	= queryExecute("Select * from sysResidents where residentId = :id",
		                        {
		                        	id : { cfsqltype:"CF_SQL_INTEGER", value:session.userInfo.residentId }
		                        });
	*/

	// Set the structure with information to the request scope
	str    = {
		jsInclude     : [ "profile.#request.jsType#.js?#cacheValue#" ],
		jsChose       : false,
		bootstrap     : true
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />

<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 75%">
			<header>
				<span class="icon major fa-user-secret accent4"></span>
				<br />
				<h2>User Profile</h2>
				<p>
					This section will allow you to update your personal account information. 
			   </p>
			   <div id="message" style="color:#msgColor#;font-weight:bold;text-align:center;">#compMessage#</div>
			</header>

			<div class="box">
				<form method="post" action="profile.cfm" name="form" class="simple-validation">
				<input type="hidden" name="residentId" id="residentId" value="#resRec.getResidentId()#">

					<div class="row uniform 50%">
						<div class="6u 12u(mobilep)">
							<input type="text" name="name" id="name" value="#resRec.getName()#" class="required" title="Your Name is Required" placeholder="Name" />
						</div>
						<div class="6u 12u(mobilep)">
							<input type="email" name="email" id="email" value="#resRec.getEmailAddress()#" title="Email Address is Required" class="required email" placeholder="Email" />
						</div>
					</div>
					<div class="row uniform 50%">
						<div class="12u">
							<input type="password" name="password" id="password" value="#resRec.getKeyId()#" class="required" title="Password is Required" placeholder="Password" />
						</div>
					</div>
					<div class="row uniform 50%">
						<div class="12u">
							<input type="text" name="street" id="street" value="#resRec.getStreet()#" class="required" title="Street Address is Required" placeholder="Street Address" />
						</div>
					</div>
					<div class="row uniform 50%">
						<div class="12u">
							<input type="text" name="cellPhone" id="cellPhone" value="#resRec.getCellPhone()#" placeholder="Cell Phone Optional (required for SMS Messages) - No formatting" />
						</div>
					</div>
					<div class="row uniform 50%">
						<div class="4u 12u(narrower)">
							<input type="checkbox" name="notifications" id="notifications" value="1" <cfif resRec.getNotifications() eq 1> checked</cfif>>
							<label for="notifications">Notifications</label>
						</div>
						<div class="4u 12u(narrower)">
							<input type="checkbox" name="newsletter" id="newsletter" value="1" <cfif resRec.getNewsletter() eq 1> checked</cfif>  >
							<label for="newsletter">News Letters</label>
						</div>
						<div class="4u 12u(narrower)">
							<input type="checkbox" name="sms" id="sms" value="1" <cfif resRec.getSms() eq 1> checked</cfif> >
							<label for="sms">SMS</label>
						</div>
					</div>
					<br /><br />
					<div class="row uniform">
						<div class="12u">
							<ul class="actions align-center">
								<li><input type="submit" name="eventBtn" id="eventBtn" value="Update Account" /></li>
							</ul>
						</div>
					</div>
				</form>
				
			</div>

		</section>

		<!--- DEFINE THE MODAL AREA --->
		<div id="selectAddrArea" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="selectAddrArea" style="margin-top:100px;">
			<div class="modal-dialog">
				<form class="form-horizontal">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" area-hidden="true">X</button>
						<h3>Select Your Address</h3>
					</div>
					<div class="modal-body" style="height:200px;">
						<div id="fgMessage" style="color:red;display:none;"> </div>
						<p> Please select your address from the list below.
						<br />
						</p>
						<div class="row-fluid">
							<div class="col-md-9">
								<div class="form-group">
									<select name="selAddress" id="selAddress" class="form-control" size="4">
									</select>
								</div>
							</div>
							<br /><br />
						</div>
					</div>
					<div class="modal-footer" style="height:100px;">
						<br />
						<button id="continueBtn" class="btn btn-primary">Continue</button>
						<button id="closeModalBtn" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</div>
				</form>
			</div>
		</div>
		<!--- END OF MODAL AREA --->

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">
	
</body>
</cfoutput>
</html>