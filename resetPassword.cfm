<!doctype html>
<!---                                                      
    CF Name:    resetPassword.cfm

    Description:
      This module will allow the resident to reset their password for their account.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	foundId       = 0;
	msgColor      = "red";
	titlePageName = "Reset Password";

	// Create the objects
	resObj  = createObject("component", "#application.cfcPath#residents");
	perObj  = createObject("component", "#application.cfcPath#Permissions");
	miscObj = createObject("component", "#application.cfcPath#Misc");

	// Get any Parameters
	miscObj.getURLVariables();

	// Check the results
	if ( !structKeyExists(url, "id") && !structKeyExists(form, "eventBtn") )
	{
		// Re-direct them to the index.cfm page
		location(url="/index.cfm", addtoken=false);
	}

	// Check to see if the id is in the application structure
	if ( !structKeyExists(application, "resetPassword") )
	{

		// Set the foundId to 0
		foundId = 0;

	} else {

		// Check to see if url.id exists 
		if ( structKeyExists(url, "id") )
		{
			uuid = url.id;
		} else if ( structKeyExists(form, "uuid") ){
			uuid = form.uuid;
		}

		for ( line in application.resetPassword )
		{
			// Check to see if the number is in there
			if ( line.uuid == uuid )
			{

				// Set found id 
				foundId = 1;
				email   = line.email;
				resId   = line.residentId;

				// Break the loop
				break;
			}

		}

	}

	// Check to see if form.eventBtn
	if ( structKeyExists(form, "eventBtn") )
	{

		// Check to see is resId exists
		if ( !isDefined("resId") )
		{

			found = 0;

		} else {

			// Retrieve the entity
			resEnt = entityLoadByPK("residents", resId);

			if ( !isDefined("resEnt") )
			{

				// set found to 0
				found = 0;

			} else {

				// Get the sysRes record
				sysRes = entityLoadByPK("sysResidents", resId);

				// Set the password
				resEnt.setPassword(hash(form.password, "md5") );
				sysRes.setKeyId(form.password);

				// Save the password
				transaction { EntitySave(resEnt); EntitySave(sysRes); }
				
				// Retrieve the record and automatically log them in
				resRec = resObj.getResidents(residentId="#resId#");
				
				// Check the results
				if ( resRec.recordCount )
				{

					for ( x = 1; x <= arrayLen(application.resetPassword); x++ )
					{
						// Check to see if the number is in there
						if ( application.resetPassword[x].uuid == uuid )
						{

							// Remove the element from the array
							arrayDeleteAt(application.resetPassword,x);

							// Check to see if the array is empty
							if ( arrayIsEmpty(application.resetPassword) )
							{
								lock type="exclusive" scope="application" timeout="10"
								{
									// remove the array
									structDelete(application, "resetPassword");
								}
							}

							// Break the loop
							break;
						}

					}

					// Set the information
					Session.userInfo = miscObj.loadQueryToStruct(qName="#resRec#");

					// Check to see if permissions is valid
					permRec = perObj.getPermissions(where="residentId = #resRec.residentId#");

					if ( permRec.recordCount )
					{
						session.userInfo.permission = {
							admin = permRec.admin,
							management = permRec.management,
							blog       = permRec.blog,
							committee  = permRec.committee
						};

					}

					// Set the home page
					location ( url="/index.cfm",addtoken=false);
					
				}

			}

		}

	}

	// Set the structure with information to the request scope
	str    = {
		jsInclude     : [ "resetPassword.#request.jsType#.js?#cacheValue#" ],
		bootstrap     : true
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 75%">
			<header>
				<span class="icon major fa-key accent5"></span>
				<br />
				<h2>Password Reset</h2>
				<p>
					You will be able to change your password from this page.
			   </p>
				<p id="msg" style="color:#msgColor#;font-weight:bold;text-align:center;">#compMessage#</p>
			</header>
			<div class="box">
				<cfif foundId eq 0>
					<div class="row uniform">
						<div class="12u">
							<p> It would appear that the unique id assigned to this password reset was not found. In order to 
							try again, please request another password reset found on this page: <a href="/login.cfm" class="fit">Request again</a>.
						</div>
					</div>
				<cfelse>
					<form method="post" action="resetPassword.cfm" name="form" class="simple-validation">
					<input type="hidden" name="uuid" value="#url.id#" />
						<div class="row uniform 50%">
							<div class="12u">
								<input type="text" name="email" id="email" value="#email#" class="required" title="Email Address is Required" placeholder="Email Address" />
							</div>
						</div>
						<div class="row uniform 50%">
							<div class="12u">
								<input type="password" name="password" id="password" value="" class="required" title="Password is Required" placeholder="Password" />
							</div>
						</div>
						<div class="row uniform 50%">
							<div class="12u">
								<input type="password" name="confirmPassword" id="confirmPassword" value="" class="required" title="Confirm Password is Required" placeholder="Confirm Password" />
							</div>
						</div>
						<div class="row uniform">
							<div class="12u">
								<ul class="actions align-center">
									<li><input type="submit" name="eventBtn" id="eventBtn" value="Reset Password" /></li>
								</ul>
							</div>
						</div>
						
					</form>
				</cfif>
	
			</div>

		</section>

		<!--- DEFINE THE MODAL AREA --->
		<div id="noMatch" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="noMatch" style="margin-top:100px;">
			<div class="modal-dialog">
				<form class="form-horizontal">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" area-hidden="true">X</button>
						<h3>Passwords Do Not Match</h3>
					</div>
					<div class="modal-body">
						<p> The passwords you entered do not match</p>
					</div>
					<div class="modal-footer" style="height:100px;">
						<br />
						<button id="closeModalBtn" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</div>
				</form>
			</div>
		</div>
		<!--- END OF MODAL AREA --->

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>