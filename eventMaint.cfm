<!doctype html>
<!---                                                      
    CF Name:    eventMaint.cfm

    Description:
      This module will allow the management and BOD manage events on the calendar.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Check permissions
	if ( !structKeyExists(session, "userInfo") || 
		  ( !structKeyExists(session.userInfo, "permission") || 
		    ( session.userInfo.permission.blog  == 1 || 
		    	session.userInfo.permission.committee == 1 ) 
		   ) 
		)
	{
		// Send to home page
		location ( url="/index.cfm", addToken=false );
	}

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	selValue      = "";
	titlePageName = "Event Calendar Maintenance";
	formFld       = {
		month : 0,
		year  : 1980
	};

	// Create the object
	eventObj 	= createObject("component", "#application.cfcPath#events");

	// Check to see if action exists
	if ( structKeyExists(form, "action") )
	{

		// Get the right value
		formAction = listLast(form.action,",");

		// Chek the value of action
		switch ( formAction )
		{

			case 'selection':

				sDate = form.year & "-" & form.month & "-01";
				eDate = form.year & "-" & form.month & "-" & daysInMonth(form.month);

				// Get the street information
				eventRecs 	= eventObj.getEvents(where="startDateTime between '#sDate#' and '#eDate#'",orderBy="startDateTime");

				formFld = {
					month : form.month,
					year  : form.year
				};

				// Break the switch
				break;

			case 'addEdit':

				if ( !structKeyExists(form, "sendNotice") )
					form.sendNotice = 0;

				// Set the form structure
				strList = {
					eventId          : form.eventId,
					title            : form.eventTitle,
					startDateTime    : createDateTime(Year(form.startDate), month(form.startDate), day(form.startDate), hour(form.startTime), minute(form.startTime), "00"),
					endDateTime      : createDateTime(Year(form.endDate), month(form.endDate), day(form.endDate), hour(form.endTime), minute(form.endTime), "00"),
					shortDescription : form.shortDescription,
					residentId       : session.userInfo.residentId,
					sendNotice       : form.sendNotice,
					committeeId      : form.committeeId,
					locationId       : form.locationId,
					otherLocation    : form.otherLocation
				};

				if ( strList.eventId != 0 )
				{
					// Update the event
					eventObj.updateEvents(strList);

				} else {
					eventObj.insertEvents(strList);
				}

				// Check to see repeat date
				if ( len(form.repeatFld) )
				{
					// Process the repeat date
					for ( newDate in form.repeatFld )
					{
						// Update the structure
						strList.startDateTime = createDateTime(Year(newDate), month(newDate), day(newDate), hour(form.startTime), minute(form.startTime), "00");
						strList.endDateTime   = createDateTime(Year(newDate), month(newDate), day(newDate), hour(form.endTime), minute(form.endTime), "00");

						// Insert the events
						eventObj.insertEvents(strList);
					}

				}

				sDate = form.year & "-" & form.month & "-01";
				eDate = form.year & "-" & form.month & "-" & daysInMonth(form.month);

				// Get the street information
				eventRecs 	= eventObj.getEvents(where="startDateTime between '#sDate#' and '#eDate#'",orderBy="startDateTime");

				formFld = {
					month : form.month,
					year  : form.year
				};

				// Break the switch
				break;

		}

	}

	// Get the list of committee's
	commRecs = queryExecute("Select * from committees where active = 1 order by committeeName");

	// Get the locations
	locRecs  = queryExecute("select * from locations where active = 1 order by locationName");

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : true,
		datepicker      : true,
		includeTiny     : true,
		jsInclude       : ["eventMaint.full.js?#cacheValue#"]  /*#full */ /*#request.jsType# */
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-calendar accent5"></span>
				<br />
				<h2>Calendar Events Maintenance</h2>
				<p>
					The purpose of this page is to allow changes to the events posted to the calendar. 
					<br />
			   </p>
			   <cfif len(compMessage)>
				   <div class="msgFormat">#compMessage#</div>
				</cfif>
			</header>

			<section class="box ">
				<form method="Post" action="eventMaint.cfm" name="form" id="form" class="simple-validation">
				<input type="hidden" name="action" value="selection">
				<cfif isDefined("sDate")>
					<input type="hidden" id="sDate" value="#sDate#" />
					<input type="hidden" id="eDate" value="#eDate#" />
				</cfif>
				<div class="row uniform 50%">
					<div class="6u 12u(mobilep)">
						<select name="month" id="month" class="required" title="Month is Required" placeholder="Month">
						<option value=""> -- Please Select --</option>
						<option value="01" <cfif formFld.month eq "01"> selected </cfif> >January</option>
						<option value="02" <cfif formFld.month eq "02"> selected </cfif>>February</option>
						<option value="03" <cfif formFld.month eq "03"> selected </cfif>>March</option>
						<option value="04" <cfif formFld.month eq "04"> selected </cfif>>April</option>
						<option value="05" <cfif formFld.month eq "05"> selected </cfif>>May</option>
						<option value="06" <cfif formFld.month eq "06"> selected </cfif>>June</option>
						<option value="07" <cfif formFld.month eq "07"> selected </cfif>>July</option>
						<option value="08" <cfif formFld.month eq "08"> selected </cfif>>August</option>
						<option value="09" <cfif formFld.month eq "09"> selected </cfif>>September</option>
						<option value="10" <cfif formFld.month eq "10"> selected </cfif>>October</option>
						<option value="11" <cfif formFld.month eq "11"> selected </cfif>>November</option>
						<option value="12" <cfif formFld.month eq "12"> selected </cfif>>December</option>
						</select>
					</div>
					<div class="6u 12u(mobilep)">
						<select name="year" id="year" class="required" title="Year is Required" placeholder="Year">
						<option value=""> -- Please Select --</option>
						<cfloop index="y" from="2016" to="2025" step=1>
							<option value="#y#" <cfif formFld.year eq y> selected </cfif>>#y#</option>
						</cfloop>
						</select>
					</div>
				</div>
			</section>

			<cfif structKeyExists(form, "action")>
				<section class="box"> 

					<div class="table-wrapper" id="resultTab">
						<table>
						<thead>
							<tr>
								<th>Start Date/Time</th>
								<th>End Date/Time</th>
								<th>Event Title</th>
								<th> &nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<cfif !eventRecs.recordCount>
								<tr>
									<td> &nbsp;</td>
									<td> &nbsp;</td>
									<td> No Event Listed</td>
								</tr>
							<cfelse>
								<cfloop query="eventRecs">
									<tr>
										<td>#dateFormat(eventRecs.startDateTime,"mm/dd/yyyy")#@#timeFormat(eventRecs.startDateTime)#</td>
										<td>#dateFormat(eventRecs.endDateTime,"mm/dd/yyyy")#@#timeFormat(eventRecs.endDateTime)#</td>
										<td>#eventRecs.title#</td>
										<td><a href="##" id="E-#eventRecs.eventId#" data-id="#eventRecs.eventId#">Edit</a>&nbsp;&nbsp;<a href="##" id="D-#eventRecs.eventId#" data-id="D-#eventRecs.eventId#">Delete</a></td>
									</tr>
								</cfloop>
							</cfif>
						</tbody>
						</table>
						
						<div class="8u 12u(mobilep)">
							<ul class="actions">
								<li><input type="button" id="newEventBtn" value="New Event"></li>
								<li><input type="button" id="returnBtn"   value="Return"></li>
							</ul> 
						</div>

					</div>

				</section>

				<section class="box" id="addEditArea" style="display:none;">
					<form method="post" action="eventMaint.cfm" name="editForm" id="editForm">
					<input type="hidden" name="action"    id="action"    value="addEdit" />
					<input type="hidden" name="eventId"   id="eventId"   value="0" />
					<input type="hidden" name="repeatFld" id="repeatFld" value="" />
					<div class="row uniform 50%">
						<div class="12u">
							<input type="text" name="eventTitle" id="eventTitle" value="" class="required" title="Event Title is Required" maxlength="100" placeholder="Event Title" />
						</div>
					</div>

					<div class="row uniform 50%">
						<div class="6u 12u(mobilep)">
							<input type="text" name="startDate" id="startDate" value="" class="required" title="Start Date is Required" placeholder="Start Date - MM/DD/YYYY" />
						</div>
						<div class="6u 12u(mobilep)">
							<input type="text" name="startTime" id="startTime" value="" title="Start Time is Required" class="required" placeholder="Start Time - HH:MM (24 Hour Clock)" />
						</div>

					</div>

					<div class="row uniform 50%">
						<div class="6u 12u(mobilep)">
							<input type="text" name="endDate" id="endDate" value="" class="required" title="End Date is Required" placeholder="End Date - MM/DD/YYYY" />
						</div>
						<div class="6u 12u(mobilep)">
							<input type="text" name="endTime" id="endTime" value="" title="End Time is Required" class="required" placeholder="End Time - HH:MM (24 Hour Clock)" />
						</div>

					</div>

					<div class="row uniform 50%">
						<div class="12u">
							<textarea name="shortDescription" id="shortDescription" class="required" title="Short Description is Required" maxlength="2000" placeholder="Short Description"></textarea>
						</div>
					</div>
					
					<div class="row uniform 50%">
						<div class="1u 12u(narrower)">
							<label for="locationId">Location</label>
						</div>
						<div class="3u 12u(narrower)">
							<select name="locationId" id="locationId">
							<option value="0" selected> None </option>
							<cfloop query="locRecs">
								<option value="#locRecs.locationId#">#locRecs.locationName#</option>
							</cfloop>
							</select>
						</div>
						<div class="2u 12u(narrower)">
							<label for="otherLocation">Other Location</label>
						</div>
						<div class="5u 12u(narrower)">
							<textarea name="otherLocation" id="otherLocation" maxlength="2000" placeholder="Other Location"></textarea>
						</div>
						<div class="2u 12u(narrower)">
							&nbsp;
						</div>
					</div>

					<div class="row uniform 50%">
						<div class="4u 12u(narrower)">
							<input type="checkbox" name="sendNotice" id="sendNotice" value="1">
							<label for="sendNotice">Send Reminder Notification</label>
						</div>
						<div class="2u 12u(narrower)">
							Committee Event:
						</div>
						<div class="3u 12u(narrower)">
							<select name="committeeId" id="committeeId">
							<option value="0" selected> None </option>
							<cfloop query="commRecs">
								<option value="#commRecs.committeeId#">#committeeName#</option>
							</cfloop>
							</select>
						</div>
						<div class="2u 12u(narrower)">
							&nbsp;
						</div>
					</div>
					<br /><br />
					<div class="8u 12u(mobilep)">
						<ul class="actions">
							<li><input type="button" name="eventSaveBtn" id="eventSaveBtn" value="Save Changes"></li>
							<li><input type="button" name="eventSaveRepeatBtn" id="eventSaveRepeatBtn" value="Save &amp; Repeat"></li>
							<li><input type="button" id="clearEABtn" value="Clear Area"></li>
						</ul> 
					</div>

				</form>
				</section>

			</cfif>

			<section class="box" id="repeatArea" style="display:none;">
			
				<form>
				<div class="row uniform 50%">
					<div class="12u">
						<p>
							Use this area to set the event on multiple dates.
						</p>
					</div>
				</div>

				<div class="row uniform 50%">
					<div class="12u 12u(mobilep)">
						<input type="text" name="repeatDate" id="repeatDate" value="" placeholder="Start Date - MM/DD/YYYY" />
					</div>
				</div>

				<br />
				<div class="8u 12u(mobilep)">
					<ul class="actions">
						<li><input type="button" name="addRepeatBtn" id="addRepeatBtn" value="Add New Data"></li>
					</ul> 
				</div>

				<div class="row uniform 50%">
					<div class="12u 12u(mobilep)">
						<select id="dateList" name="dateList" size="10" multiple>
						</select>
					</div>
					<div><input type="button" id="clearRepeatBtn" value="Clear List"></div>
				</div>
				<br />

				<div class="8u 12u(mobilep)">
					<ul class="actions">
						<li><input type="button" name="" id="saveRepeatBtn" value="Save Changes"></li>
					</ul> 
				</div>
				</form>

			</section>

		</section>

		<!--- DEFINE THE MODAL AREA --->
		<div id="missingFields" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="missingFields" style="margin-top:100px;">
			<div class="modal-dialog">
				<form class="form-horizontal">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" area-hidden="true">X</button>
						<h3>Missing or Invalid Fields</h3>
					</div>
					<div class="modal-body" style="height:200px;">
						<p> The following fields are missing information:
						<br />
						</p>
						<p>
							<div id="modalBodyInfo"> </div>
						</p>
					</div>
					<div class="modal-footer" style="height:100px;">
						<br />
						<button id="closeModalBtn" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</div>
				</form>
			</div>
		</div>
		<!--- END OF MODAL AREA --->

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!--- Set the script variables --->
	<script>
		var isMobile = "#session.isMobile#";
		var isPhone  = "#session.isPhone#";
	</script>

	<!--- Scripts --->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>