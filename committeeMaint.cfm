<!doctype html>
<!---                                                      
    CF Name:    committeeMaint.cfm

    Description:
      This module will allow the management to have access to committee members.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Check permissions
	if ( !structKeyExists(session, "userInfo") || 
		  ( !structKeyExists(session.userInfo, "permission") || 
	    		( session.userInfo.permission.blog  == 1 ||
	    		  session.userInfo.permission.committee == 1 ) 
	    	)
		)
	{
		// Send to home page
		location ( url="/index.cfm", addToken=false );
	}

	// Set the fields
	cacheValue    	= dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   	= "";
	emailAddress  	= "";
	selValue      	= "";
	titlePageName 	= "Committee Maintenance";

	// Create the object
	commsObj 		= createObject("component", "#application.cfcPath#committees");
	commMemObj  	= createObject("component", "#application.cfcPath#committeeMembers");

	// Check to see if action exists
	if ( structKeyExists(form, "action") )
	{

		// Chek the value of action
		switch ( form.action )
		{

			case 'selection':

				// Get the street information
				commMemRecs 	= commMemObj.getCommitteeMembersView(where="committeeId = '#form.committeeId#'",orderBy="name");
				selValue 		= form.committeeId;

				// Break the switch
				break;

		}

	}

	// Retreive the street information
	commRecs = queryExecute("Select * from committees where Active = 1 Order By committeeName");

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false,
		includeTiny     : true,
		jsInclude       : ["committeeMaint.#request.jsType#.js?#cacheValue#"]
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-users accent5"></span>
				<br />
				<h2>Committee Maintenance</h2>
				<p>
					The will allow for committee maintenance. This page will selection of a committee and 
					then display all the members who have signed up. There is also the ability to 
					make one or more members chairman. 
					<br />
			   </p>
			   <div id="message" class="msgFormat">#compMessage#</div>
			</header>
			<section class="box ">
				<form method="Post" action="committeeMaint.cfm?ws=1" name="form" id="form">
				<input type="hidden" name="action" value="selection">
				<div class="row uniform 50%">
					<h3>Search by Committee</h3>
					<div class="12u">
						<select name="committeeId" id="committeeId">
						<option value=""> -- Please Select --</option>
						<cfloop query="commRecs">
							<option value="#commRecs.committeeId#" <cfif selValue eq commRecs.committeeId> selected</cfif>>#commRecs.committeeName#</option>	
						</cfloop>
						</select>
					</div>
				</div>
				</form>
			</section>

			<cfif structKeyExists(form, "action")>
				<section class="box"> 

					<div class="table-wrapper" id="resultTab">
						<table>
						<thead>
							<tr>
								<th>Name</th>
								<th>Email</th>
								<th>Chair Person </th>
								<th> &nbsp; </th>
							</tr>
						</thead>
						<tbody>
							<cfif !commMemRecs.recordCount>
								<tr>
									<td>No Committee Members Exist</td>
									<td> &nbsp;</td>
									<td> &nbsp;</td>
									<td> &nbsp;</td>
								</tr>
							<cfelse>
								<cfloop query="commMemRecs">
									<tr>
										<td>#commMemRecs.name#</td>
										<td>#commMemRecs.emailAddress#</td>
										<td id="C-#commMemRecs.commitMemberId#"><cfif commMemRecs.chairman eq 0>No<cfelse>Yes</cfif></td>
										<td>
											<div class="8u 12u(mobilep)">
												<a href="##" id="#commMemRecs.commitMemberId#" data-id="#commMemRecs.commitMemberId#" class="fit">Make Chair Person</a>
												&nbsp;
												<a href="##" id="D-#commMemRecs.commitMemberId#" data-id="D-#commMemRecs.commitMemberId#" data-commid="#form.committeeId#" class="fit">Delete</a>
											</div>
										</td>
									</tr> 
								</cfloop>	
							</cfif>
						</tbody>
						</table>
						
					</div>

				</section> 

			</cfif>
		
		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>