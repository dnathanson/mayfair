<!doctype html>
<!---                                                      
    CF Name:    committeeReport.cfm

    Description:
      This module will allow for selection (admin & management) and display all the 
      members of a particular committee (by the leader).

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Check permissions
	if ( !structKeyExists(session, "userInfo") || 
		  ( !structKeyExists(session.userInfo, "permission") ||
		  	 ( session.userInfo.permission.blog  == 1 ) 
		  )
		)
	{
		// Send to home page
		location ( url="/index.cfm", addToken=false );
	}

	// Set the fields
	cacheValue    	= dateFormat(now(),"yyyymmdd")&timeFormat(now(),"HHmmss");
	compMessage   	= ( structKeyExists(session, "message") ) ? session.message : "";
	emailAddress  	= "";
	mgtPermission  = ( session.userInfo.permission.management ) ? 1 : 0;
	titlePageName 	= "Committee Report";

	if ( !mgtPermission )
	{
		if ( session.userInfo.permission.admin )
		{
			mgtPermission = 1;
		}
	}

	// Create the object
	commMemObj    	= createObject("component", "#application.cfcPath#committeeMembers");
	commObj        = createObject("component", "#application.cfcPath#committees");

	if ( !mgtPermission )
	{

		// Find any committee where the resident is the chairman
		commRecs 	  	= commMemObj.getCommitteeMembersView(where="residentId = #session.userInfo.residentId# and chairman = 1 and active = 1");
		commitCnt     	= commRecs.recordCount;
	
		// Retrieve the committees
		cmRecs         = commObj.getCommittees(where="committeeId = #commRecs.committeeId#", orderBy="committeeName");

	} else {

		// Find any committee where the resident is the chairman
		commRecs 	  	= commMemObj.getCommitteeMembersView(where="active = 1", orderBy="committeeId, chairman desc");
		commitCnt     	= commRecs.recordCount;

		// Retrieve all the committees
		cmRecs         = commObj.getCommittees(orderBy="CommitteeName");		
	}

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false,
		includeTiny     : true,
		jsInclude       : [ ]
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-users accent5"></span>
				<br />
				<h2>Committee Report</h2>
				<p>
					This will display all the members of the committee(s).
					<br /><br />
					Names marked with <span style="color:green;">*</span> are chairman of that committee.
			   </p>
			</header>
			
			<section class="box"> 

				<div class="table-wrapper">
					<table id="resultTab">
					<thead>
						<tr>
							<th>Committee Name</th>
							<th>Resident Name</th>
							<th>Email Address</th>
						</tr>
					</thead>
					<tbody>
					<cfloop query="cmRecs">
						
						<!--- Retreive the members --->
						<cfscript>
							cmmRecs = queryExecute("Select * from view_committeemembers where committeeId = :id",
								                    {
								                    		id : { cfsqltype:"CF_SQL_INTEGER", value:cmRecs.committeeId }
								                    	});
							
						</cfscript>

						<cfif !cmmRecs.recordCount>
							<tr>
								<td>#cmRecs.committeeName#</td>
								<td> &nbsp;</td>
								<td> &nbsp;</td>
							</tr>
							<tr>
								<td> &nbsp;</td>
								<td> No committee members exists</td>
								<td> &nbsp;</td>
							</tr>
						<cfelse>
							<cfloop query="cmmRecs">
								<cfif cmmRecs.currentRow eq 1>
									<tr>
										<td>#cmRecs.committeeName#</td>
										<td>&nbsp;
										<td>&nbsp;</td>
									</tr>
								</cfif>
								<tr>
									<td>&nbsp;</td>
									<td>#cmmRecs.name# <cfif cmmRecs.chairman eq 1><span style="color:green;">*</span></cfif></td>
									<td>#cmmRecs.emailAddress#</td>
								</tr>
							</cfloop>	
						</cfif>
					</cfloop>
					</tbody>
					</table>
					
				</div>

			</section>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!--- Check for session.message --->
	<cfif structKeyExists(session, "message")>
		<cfset structDelete(session, "message") />
	</cfif>

	<script>

		// Set current date and time
		var currentDate = '#dateFormat(now(), "mm/dd/yyyy")#';
		var currentTime = '#timeFormat(now(), "HH:mm")#';

	</script>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>