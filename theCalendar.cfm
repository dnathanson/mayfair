<!doctype html>
<!---                                                      
    CF Name:    theCalendar.cfm

    Description:
      This module will display the calendar information.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	titlePageName = "Calendar of Events";
	startDate     = Year(now()) & "-01-01";

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false,
		calendarJs      : true
	};

	// Retrieve the list of events from this month to the end of the year
	eventRecs = queryExecute("select * from events where startDateTime >= :sdt order by startDateTime",
										{
											sdt: { cfsqltype:"CF_SQL_DATE", value=startDate }
										});
	
	// Build an event list
	savecontent variable="calEvents"
	{
		for ( rec in eventRecs )
		{
			writeOutput("
{
	id   : #rec.eventId#,
	title: '#rec.title#',
	start: '#dateformat(rec.startDateTime,"yyyy-mm-dd")#T#timeFormat(rec.startDateTime,"HH:mm:00")#',
	end:   '#dateFormat(rec.endDateTime, "yyyy-mm-dd")#T#timeFormat(rec.endDateTime,"HH:mm:00")#',
	description: '#rereplace(rec.shortDescription,"'", "", "ALL")#'
},
			");

		}

	}
	
	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>

<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style>
	#calendar {
		max-width: 1024px;
		margin: 0 auto;
	}
</style>

<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 125%">
			<header>
				<span class="icon major fa-calendar accent3"></span>
				<br />
				<h2>The Calendar of Events</h2>
				<p>
					The following will display of the calendar of events going on at the Mayfair at Parkland Community. 
			   </p>
			   <p style="color:blue;font-weight:bold;">
			   	Please note, the day numbers are at the top of each box (above the divider line).
			   </p>
			</header>
			<section class="box">
				<div id='calendar' style="margin:0 auto;padding:0;max-width:900px;font-family:Lucida Grande,Helvetica,Arial,Verdana,sans-serif;"></div>
			</section>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!--- <script>

		var todayDate = '#dateFormat(now(),"yyyy-mm-dd")#';
		var calEvent   = #serializeJson(calEvents)#;

	</script> --->

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

	<!--- Set the field --->
	<cfoutput>
	<script>

		// Define variables
		var calendar     = $('##calendar');

		// Initialize for the calendar
		$(function()
		{
			
			calendar.fullCalendar({
					header: {
						left: 'prev,next today',
						center: 'title',
						right: 'month,agendaWeek,agendaDay'
					},
					defaultDate: '#dateFormat(now(),"yyyy-mm-dd")#',
					selectable: true,
					selectHelper: true,
					select: function(start, end)
					{
						// Set the date
						$('##calendar').fullCalendar('gotoDate', start);
					},
					editable: false,
					eventLimit: true, // allow "more" link when too many events
					events : [
						#calEvents#
					],
					eventRender: function(event, element) {
						element.find('.fc-title').append('<br />'+event.description);
					}

			});

		});	
	</script>
	</cfoutput>

</body>
</cfoutput>
</html>