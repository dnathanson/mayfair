<!doctype html>
<!---                                                      
    CF Name:    theWedge.cfm

    Description:
      This module will display the wedge information.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	titlePageName = "Wedge Detail";

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 75%">
			<header>
				<span class="icon major fa-pie-chart accent4"></span>
				<br />
				<h2>The Wedge Information</h2>
				<p>
					The following is the detail description of information relating to the Wedge. This information was 
					retrieve from the Master Plan. Please see the link provided at the bottom.<br />
			   </p>
			   <p>
			   	Want to read the Mayfair at Parkland Wedge Blog, visit it here: <a href="/theWedgeBlog.cfm">The Wedge Blog</a>
			   </p>
			</header>
			<section class="box">
				<h3>History</h3>
				<p style="text-align:left;">
					In 2011 House Bill 1315 approved transfer of a 1,949 acre wedge shaped property located between County Line Road and Loxahatchee Road from Palm Beach County to Broward County. It also approved the annexation of two parcels located within The Wedge in the City of Parkland at the sam time. The transfer of this property directly impacted the City of Parkland and the North Springs Improvement District (NSID), who provide services to all future residents within this Wedge. It was determined that all vacant lands on the north side of the City, which totals approximately 2,599 acres should be also taken into consideration. Some of these properties are not located within the City or within the annexation bill, but the ultimate build out will require services from both the city of Parkland and the NSID. 
				</p>
				<div class="table-wrapper">
					<table>
					<thead>
						<tr>
							<th>Property Owners</th>
							<th>Gross Acreage</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Gator Acres</td>
							<td>4.99</td>
						</tr>
						<tr>
							<td>NSID</td>
							<td>25.0</td>
						</tr>
						<tr>
							<td>Sabra</td>
							<td>62.58</td>
						</tr>
						<tr>
							<td>Salta</td>
							<td>51.44</td>
						</tr>
						<tr>
							<td>Bishops Pit</td>
							<td>401.09</td>
						</tr>
						<tr>
							<td>Dollyland</td>
							<td>63.43</td>
						</tr>
						<tr>
							<td>Palm Beach Farms</td>
							<td>10.04</td>
						</tr>
						<tr>
							<td>Misty Meadows</td>
							<td>53.2</td>
						</tr>
						<tr>
							<td>Drake</td>
							<td>3.31</td>
						</tr>
						<tr>
							<td>Cagle</td>
							<td>19.06</td>
						</tr>
						<tr>
							<td>Vannelli</td>
							<td>20.0</td>
						</tr>
						<tr>
							<td>Standand Pacific</td>
							<td>290.81</td>
						</tr>
						<tr>
							<td>Hendrix</td>
							<td>715.51</td>
						</tr>
						<tr>
							<td>Parkland</td>
							<td>1.7</td>
						</tr>
						<tr>
							<td>McJunkin West</td>
							<td>150.41</td>
						</tr>
						<tr>
							<td>Soowal</td>
							<td>10.0</td>
						</tr>
						<tr>
							<td>PBC</td>
							<td>0.87</td>
						</tr>
						<tr>
							<td>WSFM</td>
							<td>3.18</td>
						</tr>
						<tr>
							<td>EIGE</td>
							<td>0.53</td>
						</tr>
						<tr>
							<td>Towers</td>
							<td>27.48</td>
						</tr>
						<tr>
							<td>Right-of-Way</td>
							<td>1.91</td>
						</tr>
					</tbody>
					</table>		
				</div>
				<br />
				<p>Those properties under the jurisdiction of the City of Parkland are listed by Property Owner and Gross Acres below:</p>
				<div class="table-wrapper">
					<table>
					<thead>
						<tr>
							<th>Property Owner</th>
							<th>Gross Acreage</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td> TripleH </td>
							<td> 269.12</td>
						</tr>
						<tr>
							<td>Dubuys</td>
							<td>388.14</td>
						</tr>
						<tr>
							<td>McJunkin East</td>
							<td>150.41</td>
						</tr>
					</tbody>
					</table>
				</div>
				
				<div class="table-wrapper">
					<table>
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<td> Grand Total </td>
						<td> 2,599</td>
					</tbody>
					</table>
				</div>

				<h4>Land use and Zoning of the Wedge Properties</h4>
				<p>
					All but one property currently within the City of Parkland or within the current annexation bill will have a 
					Future Land Use designation of Residential allowing for 2 dwelling units per acre (R-2.0). The property that 
					differs is the McJunkin property , which is comprised of approximately 160 acres and has a Future Land Use 
					designation of Residential Estate allowing for 1 dwelling unit per acre (E-1.0).
					<br /><br />
					All remaining properties are currently within Unincorporated Broward County but still maintain a Palm Beach County Future 
					Land Use designation of Rural Residential-10 (RR-10) which allows one dwelling  unit per 10 acres. As these properties 
					annex over time into the City of Parkland, it is anticipated that these parcels will obtain Future Land Use designations 
					of R-2.0, consistent with the Standard Pacific, Triple H, Debuys and Hendrix parcels upon their annexation. 
					<br /><br />
					Within the City of Parkland the properties that are adjacent to the Master Plan area have similar Future Land Use designations 
					and subsequent densities. The majority of Heron Bay south of County Line Road and west of University Drive is designated as 
					R-3 and was constructed at a gross density of approximately two dwellings per acre. As you move east, the densities are also 
					very consistent within the communities of Cypress Head (2du/ac), Cypress Cay (2du/ac), Parkside Estates (3du/ac), 
					Mayfair (3du/ac) and Terramar (3du/ac).
					<br /><br />
					At the time of the Master Plan study, there were properties that had initiated and obtained annexation agreements with the City of 
					Parkland. These agreements defined specific dedications and densities for each property which have influence on the development of 
					the Master Plan and have been summarized below. 
				</p>
				<h4>House Bill 1317 – Subsequent to the HB approval in the City of Parkland annexed 657 acres of land into its boundary including Triple H and Debuys.</h4>
				<p>
					Resolution 2010-18-Bruschi Annexation Agreement states – Two units per acre (gross) residential development with approval by Parkland City 
					Commission. No multi-family units will be proposed or construct. Provide necessary funds and/or dedications to provide recreation facilities 
					at a rate of five acres per thousand residents of public parks and five acres per thousand of private parks and open space. Provide dedication 
					of appropriate easements and construction of trails (by owners) within and/or adjacent to development. Work with Broward County School Board 
					and Parkland to identify and donate an appropriate school site acceptable to the School Board up to a maximum of twelve acres in total size based on development density.
					<br /><br />
					Resolution 2007-41-Triple H & Bubuys & First Amendment to Agreement – Two Units per acre (gross) residential development with approval by 
					Parkland City Commission. Eleven and one half (11.5) acres of Commercial Land use with approval by Parkland City Commission at the northeast 
					corner of County line Road and University Drive. Provide necessary funds and/or land dedications to provide recreation facilities at a rate 
					of five acres per thousand residents of public parks and five acres per thousand of private open space. Provide dedication of appropriate 
					easements and construction of trails (by owners) within and/or adjacent to development. Work with Broward County School Board and Parkland 
					to identify and donate an appropriate school site acceptable to the School Board up to a maximum of twenty acres in total size based on development 
					density. 
					<br /><br />
					Agreement Between City of Parkland an Hendrix Farms Executed on April 5th, 2010 – Two units per acre (gross) residential development with approval 
					by Parkland City Commission. Forty acres of Commercial Land Use at the intersection of the University Drive and County Line Road. Dedication and 
					construction of County Line Road ROW for a four lane divided road with a width of 120, generally consistent as depicted on the Broward County Traffic 
					Ways Plan. Dedication and construction of Loxahatchee Road ROW for a four land divided road with a width of 120, generally consistent as depicted on 
					the Broward County Traffic Ways Plan. Thoroughfare ROW Identification Map. Dedicate the ROW for University Drive as depicted on the Broward County 
					Traffic Ways Plan. Dedicate and construct the ROW for Trails End. Provide necessary funds and/or land dedications to provide recreation facilities at 
					a rate of five acres per thousand residents of public parks and five acres per thousand of private open space. Work with Broward County School Board 
					an Parkland to identify and donate an appropriate school site acceptable to the School Board of approximately forty acres for a High School. 
					<br /><br />
					In conclusion, an analysis was completed to determine what the impacts would be on the existing and future levels of service for Administrative,
					Public Safety, Public Works and Library Facilities based on the ultimate development potential of the study area. Chapter 9 – Community Facilities 
					Element of the Parkland Comprehensive Plan (2008) was utilized to determine the existing facilities inventory and LOS Standards to be applied for each use. 
				</p>
				<h4>Population Projection</h4>
				<div class="table-wrapper">
					<table>
					<thead>
						<tr>
							<th>Population</th>
							<th>Description</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>23,795</td>
							<td>Existing Population (April 1, 2010 - BEBR)</td>
						</tr>
						<tr>
							<td>3,115</td>
							<td>Additional Units Built/Unbuilt</td>
						</tr>
						<tr>
							<td>15,220</td>
							<td>Potential Wedge Population (4,612 units x 3.3 persons/unit)</td>
						</tr>
						<tr>
							<td>42,130</td>
							<td>Total Residents</td>
						</tr>
					</tbody>
					</table>
				</div>
				<h4>Level of Service Impacts</h4>
				<div class="table-wrapper">
					<table>
					<thead>
						<tr>
							<th>Area</th>
							<th>Existing SD Built</th>
							<th>Existing LOS</th>
							<th>Deficit/Excess SD @ Total</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Administration</td>
							<td>49,226 SF</td>
							<td>+24,050 SF</td>
							<td>+10,299 SF</td>
						</tr>
						<tr>
							<td>Public Safety</td>
							<td>32,712 SF</td>
							<td>N/A</td>
							<td>N/A</td>
						</tr>
						<tr>
							<td>Public Works</td>
							<td>5,300 SF</td>
							<td>+541 SF</td>
							<td><span style="color:red;">-3,126 SF</span></td>
						</tr>
						<tr>
							<td>Library</td>
							<td>13,100 SF</td>
							<td><span style="color:red;">-1,177 SF</span></td>
							<td><span style="color:red;">-12,178 SF</span></td>
						</tr>
					</tbody>
					</table>
				</div>
				<h4>WHAT DOES ALL OF THIS MEAN TO MAYFAIR RESIDENTS?</h4>
				<p>
					Our concerns as a community is first and foremost the safety of our residents, particularly
					those properties boarding on Lox Rd and Hillsboro boulevard. Both of these roadways are scheduled to be widened from 2 to 4 lanes to accommodate 
					the increased traffic from the projected 42,130 additional residents, increased traffic density and all that entails.
					<br /><br />  
					As early as January 2011 residents in the community who attended meetings held by the City of Parkland to discuss these issues voiced concerns 
					regarding increased noise levels from the additional traffic, particularly truck traffic, safety concerns stemming from the same issues, speed 
					limits being exceeded from the posted 45 MPH speed limit as well as a suggestions that a wall be erected along Lox Rd to protect the homeowners 
					abutting this roadway.  
					<br /><br />
					For those of you not among the original homeowners, who may not be aware of it, Lox Road had very little if any traffic when this development was 
					initially built. Sundays it was practically non-existent. At that time there were two incidents that gave residents pause with regard to safety for 
					residents abutting Lox Rd. The first was when a tire from a passing commercial truck flew off, rolling through the berm, through the sliding glass 
					doors at the rear of a new resident’s property, through the length of the home and up the far wall leaving a tire trail of destruction where it 
					traversed the house. Thankfully no one was home as this occurred during the day, while the homeowners and their children were at work/school. The 
					then Parkland Police Department, contacted the homeowners insurance carrier as they had just moved in and that information was sitting on their
					kitchen counter. 
					<br /><br />
					The other incident occurred a few years later, when a truck left the roadway completely while traveling at a high rate of speed, through the berm, and 
					into the homeowners pool. Once again as luck would have it no one was in the pool at the time. That homeowner sold their home and moved shortly thereafter. 
					Mayfair residents are now justifiably concerned that with the current volume of traffic approaching that of State Rd 7/Rt. 441, that these incidents will 
					become more common place and of concern to these homeowners. We feel the BOD’s as well as the City of Parkland, the state, county, department of 
					transportation and any other involved entities have a fiduciary responsibility to these homeowners, to protect them, their families and their properties. 
					Also of concern to all residents is the diminished property values that will accompany these changes.
				</p>
				
				<h3>More Information About the Wedge</h3>
				<p>
					If you desire to read more from a historical perspective please click on this link to read the official Master Plan for “The Wedge”: <a href="http://www.cityofparkland.org/DocumentCenter/Home/View/3876" target="new">http://www.cityofparkland.org/DocumentCenter/Home/View/3876</a>.
				</p>
				
			</section>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>