<!doctype html>
<!---                                                      
    CF Name:    committeeDetail.cfm

    Description:
      This module will display the different committee's detail information.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:         
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	titlePageName = "Committee Detail";
	
	// Create the objects
	comObj  = createObject("component", "#application.cfcPath#Committees");
	miscObj = createObject("component", "#application.cfcPath#Misc");

	// Convert the queryString to the url
	miscObj.getURLVariables();

	// Check the results 
	if ( !structKeyExists(url, "com") || len(url.com) == 0 )
	{
		// Go back to the committee page
		location (url="committees.cfm", addToken=false);
	}

	// Retrieve the record
	comRecs = queryExecute("Select * from committees where committeeId = :id",
		                    {
		                    	id : { cfsqltype:"CF_SQL_INTEGER", value:url.com }
		                    });

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 75%">
			<header>
				<span class="icon major fa-users accent3"></span>
				<br />
				<h2>Committee Detail</h2>
				<p>
					The following is the detail description of the committee you might be interested in:<br />
			   </p>   
			</header>
			<section class="box">
				<h3>#comRecs.committeeName#</h3>
				<p style="text-align:left;">
					#comRecs.longDesc#
				</p>
				<br />
				<cfif !structKeyExists(session,"userInfo")>
					<b>You will need to Log In to the system before you can 
					sign up to be on the committee</b>
				</cfif>
				<br /><br />
				<ul class="actions" style="text-align:center;">
					<li><a href="/committees.cfm" class="button small">Return to Committee's Page</a></li>
				</ul>
			</section>

		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>