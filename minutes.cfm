<!doctype html>
<!---                                                      
    CF Name:    minutes.cfm

    Description:
      This module will home owners to read the minutes from previous meetings.

      Best viewed with Tabs = 3

    Program Information:
      Called from:  
      Calls:        
      Parameters:   
      Author:       Drew Nathanson

    Global Session Variables Used:

    Mayfair at Parkland - Technical Synergy, Inc.
    Copyright (c) 1999-2025. All Rights Reserved. 

--->

<cfscript>

	// Set the fields
	cacheValue    = dateFormat(now(),"mmddyyyy")&timeFormat(now(),"HHmmss");
	compMessage   = "";
	emailAddress  = "";
	titlePageName = "Meeting Minutes";

	// Build the date range
	if ( Month(Now()) == 1 )
	{
		startDate = Year(DateAdd("yyyy",-1,Now())) & "-01-01";
	} else {
		startDate = Year(Now()) & "-01-01";
	}
	endDate   = Year(Now()) & "-12-31";

	// Set the structure with information to the request scope
	str    = {
		bootstrap       : false
	};

	// Apppend the information to the request scope
	StructAppend(request,str);

	// Retreive the community news
	minRecs = queryExecute("Select * from minutes where minuteDate between '#startDate#' and '#endDate#' order by minuteDate desc");

</cfscript>


<!--- Include the header --->
<cfinclude template="common/header.cfm" />
<style type="text/css">
.msgFormat
{
	-webkt-border-radius: 8px;
   -moz-border-radius: 8px;
        border-radius: 8px;
   font-weight: bold;
   text-align: center;
   background-color: green;
   color: white;
}
</style>
<cfoutput>
<body>
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1><a href="index.cfm">MAYFAIR AT PARKLAND</a><cfif structKeyExists(session, "userInfo")> - <span style="color: rgba(255, 255, 255, 0.75);font-weight:400;">#ucase(session.userInfo.name)#</span></cfif></h1>
			<!--- Include the menus --->
			<cfinclude template="common/menu.cfm" />
		</header>

		<!-- Main -->
		<section id="main" class="container 100%">
			<header>
				<span class="icon major fa-bullhorn accent3"></span>
				<br />
				<h2>Mayfair Community Meeting Minutes</h2>
				<p>
					This page will display all the meeting minutes for the Mayfair at Parkland communuty.
					<br />
					Please be aware that any minutes on this web site are <b>NOT</b> official. <br />You will 
					need to contact the management company for an offical copy. 
			   </p>
			</header>
			<section class="box">
				<cfif !structKeyExists(session, "userInfo")>
					<b>In order to view the meeting minutes, you will need to log into the web site.</b> 
				<cfelse>
					<h3>Minutes</h3>
					<div class="table-wrapper">
						<table>
						<thead>
							<tr>
								<th>Meeting Date</th>
								<th>Minutes Status</th>
								<th> &nbsp; </th>
							</tr>
						</thead>
						<tbody>
							<cfloop query="minRecs">
								<tr>
									<td>#dateFormat(minRecs.minuteDate, "mm/dd/yyyy")#</td>
									<td><cfif minRecs.approved eq 0>Draft<cfelse>Final</cfif></td>
									<td><a href="/displayMins.cfm/min/#minRecs.minId#" target="new">View</a></td>
								</tr>
							</cfloop>
						</tbody>
						</table>
					</div>
				</cfif>
			</section>
			
		</section>

		<!-- Footer -->
		<footer id="footer">
			<cfinclude template="/common/icons.cfm">
			<ul class="copyright">
				<li>#Year(Now())# &copy; Technical Synergy, Inc. for Mayfair at Parkland HOA. All rights reserved.</li>
			</ul>
		</footer>

	</div>

	<script>
		var isMobile = "#session.isMobile#";
		var isPhone  = "#session.isPhone#";
	</script>

	<!-- Scripts -->
	<cfinclude template="common/scripts.cfm">

</body>
</cfoutput>
</html>